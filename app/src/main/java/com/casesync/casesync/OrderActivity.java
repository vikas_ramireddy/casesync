package com.casesync.casesync;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.OrderDetails;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrderActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    String caller = "";
    CasesyncUtill utill = new CasesyncUtill();
    private String TAG = "ORA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Orders");
        }
        caller = getIntent().getStringExtra("caller");
        recyclerView = findViewById(R.id.recycler_view_order);
        // Passing the column number 1 to show online one column in each row.
        recyclerViewLayoutManager = new GridLayoutManager(OrderActivity.this, 1);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);


        Intent i = getIntent();

        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();

            QueryBuilder<CaseObj, Long> queryBuilderint =
                    casedetDao.queryBuilder();
            //Sorting the latest orders first.
            queryBuilderint.orderBy("date_next_list", false);
            Where<CaseObj, Long> whereinterim = queryBuilderint.where();
            whereinterim.isNotNull("interimOrder");
            PreparedQuery<CaseObj> preparedQuery = queryBuilderint.prepare();
            List<CaseObj> caselistinterim = casedetDao.query(preparedQuery);
            //Re-assigning the helper class
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            QueryBuilder<CaseObj, Long> queryBuilderfin =
                    casedetDao.queryBuilder();
            queryBuilderfin.orderBy("date_of_decision", false);
            Where<CaseObj, Long> wherefinal = queryBuilderfin.where();
            wherefinal.isNotNull("finalOrder");
            PreparedQuery<CaseObj> preparedQueryfin = queryBuilderfin.prepare();

            List<CaseObj> caselistfinal = casedetDao.query(preparedQueryfin);

            List<OrderDetails> orderdataModels = new ArrayList<OrderDetails>();
            orderdataModels.addAll(utill.getOrderList(caselistinterim, true));
            orderdataModels.addAll(utill.getOrderList(caselistfinal, false));
            //orderdate
            Collections.sort(orderdataModels, new Comparator<OrderDetails>() {
                @Override
                public int compare(OrderDetails obj1, OrderDetails obj2) {
                    return obj2.getOrder_date().compareTo(obj1.getOrder_date());
                }
            });
            if (!CollectionUtils.isEmpty(orderdataModels)) {
                adapter = new OrderAdapter(OrderActivity.this, orderdataModels);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //recyclerView.setHasFixedSize(true);
        //recyclerView.setItemViewCacheSize(150);
        //recyclerView.setDrawingCacheEnabled(true);
        //recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        //Get the orders which are having interim and final order text available.Call Util to get the order object

        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onBackPressed() {
        if (!StringUtils.isEmpty(caller)) {
            try {

                Class callerClass = Class.forName(caller);
                Intent intent = new Intent(this, callerClass);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            int count = getFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                Intent intent = new Intent(OrderActivity.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } else {
                getFragmentManager().popBackStack();
            }
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (!StringUtils.isEmpty(caller)) {

                    if (caller.equals("AddCase")) {
                        Intent intent = new Intent(this, AddCase.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();
                    }
                    return true;
                } else {
                    Intent intent = new Intent(OrderActivity.this, NavActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(intent);
                    finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
