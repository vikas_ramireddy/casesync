package com.casesync.casesync;

/**
 * Created by Vikas on 3/15/2017.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//Our class extending fragment
public class CaseDet extends Fragment {

    public static final String TAG = CaseDet.class.getSimpleName();


    public CaseDet() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        CasedetailsTab activity = (CasedetailsTab) getActivity();
        String casenumber = activity.getcasenumber();
        //TODO Get Case Type also while getting above data from blank fragment.Based on that query the table and display the data.
        View v = inflater.inflate(R.layout.tab1, container, false);
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);

            Dao<CaseObj, Long> casedetDao = null;
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            List<CaseObj> todos = casedetDao.queryForEq("cino", casenumber);
            if (todos != null) {
                if (todos.size() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    TextView textcin = v.findViewById(R.id.cinno);
                    textcin.setText("CRN Number \n" + todos.get(0).getCino());

//                    TextView text = v.findViewById(R.id.casecode);
//                    text.setText("Case code \n" + todos.get(0).getCaseNo());
                    TextView casetype = v.findViewById(R.id.casetype);
                    casetype.setText("Case Type \n" + todos.get(0).getTypeName());
                    casetype.setBackgroundResource(R.drawable.plus);

                    TextView filingnumber = v.findViewById(R.id.filingnumber);
                    filingnumber.setText("Filing Number\n" + todos.get(0).getFilNo() + "/" + todos.get(0).getFilYear());
                    filingnumber.setBackgroundResource(R.drawable.plus);
                    TextView regnumber = v.findViewById(R.id.registration_number);
                    if (todos.get(0).getRegNo() != null) {
                        regnumber.setText("Registration Number\n" + todos.get(0).getRegNo() + "/" + todos.get(0).getRegYear());
                    } else {
                        regnumber.setText("Registration Number\n N/A");
                    }

                    regnumber.setBackgroundResource(R.drawable.plus);
                    TextView filingdate = v.findViewById(R.id.filingdate);
                    filingdate.setText("Filing Date\n" + sdf.format(todos.get(0).getDateOfFiling()));
                    filingdate.setBackgroundResource(R.drawable.plus);
                    TextView regdate = v.findViewById(R.id.regdate);
                    if (todos.get(0).getDtRegis() != null) {
                        regdate.setText("Registration Date\n" + sdf.format(todos.get(0).getDtRegis()));
                    } else {
                        regdate.setText("Registration Date\n N/A");
                    }

                    regdate.setBackgroundResource(R.drawable.plus);
                    TextView firsthearing = v.findViewById(R.id.firsthearing);
                    Date regdatetmp = null;
                    if (todos.get(0).getDateFirstList() != null) {
                        regdatetmp = todos.get(0).getDateFirstList();
                        firsthearing.setText("First hearing date\n" + sdf.format(regdatetmp));
                    } else {
                        firsthearing.setText("First hearing date\n N/A");
                    }
                    firsthearing.setBackgroundResource(R.drawable.plus);

                    try {
                        TextView stageofcase = v.findViewById(R.id.stageofcase);
                        String stagetext = "";
                        if (todos.get(0).getPurposeName() != null) {
                            stagetext = todos.get(0).getPurposeName().replaceAll("Â", "");
                            stageofcase.setText("Stage of Case \n " + stagetext.replaceAll(":", "\n"));
                            firsthearing.setBackgroundResource(R.drawable.plus);
                        } else {
                            stageofcase.setText("Stage of Case \n N/A");
                        }
                        TextView judge = v.findViewById(R.id.judge);
                        if (todos.get(0).getDesgname() == null) {
                            judge.setText("Court:" + todos.get(0).getCourtName());
                        } else {
                            judge.setText("Court:" + todos.get(0).getDesgname().replaceAll("Â", "") + "/" + todos.get(0).getCourtName());
                        }

                        //Adding Last Hearing Date & Last Business.
                        //last_hearing_details

                        TextView last_hearing_details = v.findViewById(R.id.last_hearing_details);
                        StringBuilder last_hearing_details_text = new StringBuilder();
                        if (todos.get(0).getDateLastList() != null) {
                            last_hearing_details_text.append("Last Hearing Date:" + sdf.format(todos.get(0).getDateLastList()));
                        }
                        if (StringUtils.isNotBlank(todos.get(0).getWritinfo()) && !todos.get(0).getWritinfo().equals("--")) {
                            if (StringUtils.isNotBlank(last_hearing_details_text)) {
                                last_hearing_details_text.append("\nLast Business:" + todos.get(0).getWritinfo());
                            } else {
                                last_hearing_details_text.append("Last Business:" + todos.get(0).getWritinfo());
                            }
                        }
                        last_hearing_details.setText(last_hearing_details_text.toString());
                        last_hearing_details.setBackgroundResource(R.drawable.plus);
                    } catch (Exception e) {
                        Log.e("DET", "EEOR" + e.getMessage());
                    }
                    try {
                        if (todos.get(0).getArchive().equals("Y")) {
                            TextView disposed = v.findViewById(R.id.disposeddate);
                            TextView nexthearing = v.findViewById(R.id.nexthearing);
                            if (todos.get(0).getArchive().equals("Y")) {
                                disposed.setText("Disposed \n Yes");
                            }
                            if (todos.get(0).getDateOfDecision() != null) {
                                nexthearing.setText("Decision Date \n " + sdf.format(todos.get(0).getDateOfDecision()));
                            } else {
                                nexthearing.setText("Decision Date \n N/A");
                            }
                        } else {
                            TextView disposed = v.findViewById(R.id.disposeddate);
                            disposed.setText("Disposed \n No");
                            TextView nexthearing = v.findViewById(R.id.nexthearing);
                            Date nexthearingtmp = null;
                            if (todos.get(0).getDateNextList() != null) {
                                nexthearingtmp = todos.get(0).getDateNextList();
                                nexthearing.setText("Next hearing date\n" + sdf.format(nexthearingtmp));
                            } else {
                                nexthearing.setText("Next hearing date \n  N/A");
                            }
                        }
                    } catch (Exception e) {
                        Log.e("DET", "EEOR" + e.getMessage());
                    }

                    TextView petadv = v.findViewById(R.id.petadv);
                    String pettext = "Petitioner Details\n" + todos.get(0).getPetNameAdd().replaceAll("&nbsp;", "").replaceAll("<br />", "\n")
                            + (todos.get(0).getStrError() != null && !todos.get(0).getStrError().isEmpty() ? todos.get(0).getStrError().replaceAll("<br>", "").replaceAll("&nbsp;", "").replaceAll("<br />", "\n").replaceAll("</span>", "") : "");
//                    Log.i("DET", "PET DET:" + todos.get(0).getPetName());
//                    Log.i("DET", "PET DETAILS:" + todos.get(0).getPetpartyName());
//                    Log.i("DET", "RES DETAILS:" + todos.get(0).getResName());
//                    Log.i("DET", "RES DETAILS:" + todos.get(0).getRespartyName());
                    petadv.setText(pettext);
                    petadv.setBackgroundResource(R.drawable.plus);
                    TextView repadv = v.findViewById(R.id.repadv);
                    String reptext = "Respondent Details\n" + todos.get(0).getResNameAdd().replaceAll("&nbsp;", "").replaceAll("<br />", "\n") + (todos.get(0).getStrError1() != null && !todos.get(0).getStrError1().isEmpty() ? todos.get(0).getStrError1().replaceAll("<br>", "").replaceAll("&nbsp;", "").replaceAll("<br />", "\n").replaceAll("</span>", "") : "");
                    repadv.setText(reptext);
                    repadv.setBackgroundResource(R.drawable.plus);
                    Log.i(TAG, "FIR:" + todos.get(0).getFirDetails());
                    TextView policestation = v.findViewById(R.id.policestation);
                    TextView firno = v.findViewById(R.id.firno);
                    TextView firyear = v.findViewById(R.id.firyear);
                    boolean firavailable = false;
                    if (todos.get(0).getFirDetails() != null) {
                        String[] result = todos.get(0).getFirDetails().split("\\^");
                        Log.i(TAG, "FIR DET:" + result.length);
                        if (result.length == 3) {
                            firavailable = true;
                            policestation.setText("Police Station:\n" + result[1]);
                            firno.setText("FIR NO:\n" + result[0]);
                            firyear.setText("FIR YEAR:\n" + result[2]);
                        } else {
                            policestation.setVisibility(View.GONE);
                            firno.setVisibility(View.GONE);
                            firyear.setVisibility(View.GONE);
                        }
                    } else {
                        policestation.setVisibility(View.GONE);
                        firno.setVisibility(View.GONE);
                        firyear.setVisibility(View.GONE);

                    }

                    if (todos.get(0).getArchive().equals("N")) {
                        String act = todos.get(0).getAct();
                        Document doc = Jsoup.parse(todos.get(0).getAct().replace("\\", ""));
                        Elements table = doc.select("table.tbl-result");
                        Elements rows = table.select("tr");
                        if (rows != null) {
                            String acttext = "";
                            String sectext = "";
                            for (int i = 0; i < rows.size(); i++) {
                                Element row = rows.get(i);
                                Elements cols = row.select("td");
                                for (int count = 0; count < cols.size(); count++) {
                                    if (count == 0) {
                                        acttext = acttext + " " + cols.get(count).text();
                                    } else if (count == 1) {
                                        sectext = sectext + " " + cols.get(count).text();
                                    }
                                    Log.d("TST", count + ":" + cols.get(count).text());
                                }
                            }
                            if (act != null) {
                                TextView actview = v.findViewById(R.id.caseact);
                                TextView section = v.findViewById(R.id.casesection);
                                actview.setText("Under Act(s)\n" + acttext);
                                section.setText("Under Section(s)\n" + sectext);
                                actview.setBackgroundResource(R.drawable.plus);
                                section.setBackgroundResource(R.drawable.plus);
//                            if (acttext.length() <= 10) {
//                                actview.setText("Under Act(s)\n" + acttext);
//                            } else {
//                                actview.setText("Under Act(s)\n" + acttext.replace(" ", "\n"));
//                            }
//
//                            if (sectext.length() <= 10) {
//                                section.setText("Under Section(s)\n" + sectext);
//                            } else {
//                                section.setText("Under Section(s)\n" + sectext.replace(" ", "\n"));
//                            }
                            } else {
                                TextView actview = v.findViewById(R.id.caseact);
                                TextView section = v.findViewById(R.id.casesection);
                                actview.setVisibility(View.GONE);
                                section.setVisibility(View.GONE);
                            }
                        }

                        //OLD Logic replacing with a single logic
                        if (todos.get(0).getTransfer() != null) {
                            Log.d(TAG, "Transfer HTML:" + todos.get(0).getTransfer());
                            TextView casedate = v.findViewById(R.id.casetrnsfdate);
                            TextView casesource = v.findViewById(R.id.casesource);
                            TextView casedes = v.findViewById(R.id.casedestination);
                            casedate.setBackgroundResource(R.drawable.plus);
                            casesource.setBackgroundResource(R.drawable.plus);
                            casedes.setBackgroundResource(R.drawable.plus);
                            Document document = Jsoup.parse(todos.get(0).getTransfer().replace("\\", ""));
                            Elements tabletransfer = document.select("table.tbl-result");
                            Elements rowstransfer = tabletransfer.select("tr");
                            if (rowstransfer != null) {
                                Log.d(TAG, "Transfer size:" + rowstransfer.size());
                                //TODO Handle case where more than one transfer is available
                                for (int i = 0; i < rowstransfer.size(); i++) {
                                    Element row = rowstransfer.get(i);
                                    Elements cols = rowstransfer.select("td");
                                    for (int count = 0; count < cols.size(); count++) {
                                        Log.d("TST", count + ":Transfer Data:" + cols.get(count).text());
                                        if (count == 0) {
                                            casedate.setText("Transfer Date \n" + cols.get(count).text());
                                        } else if (count == 1) {
                                            casesource.setText("From Court No.\n" + cols.get(count).text());
                                        } else if (count == 2) {
                                            casedes.setText("To" +
                                                    " Court No.\n" + cols.get(count).text());
                                        }
                                    }
                                }
                            }
                        } else {
                            TextView casedate = v.findViewById(R.id.casetrnsfdate);
                            TextView casesource = v.findViewById(R.id.casesource);
                            TextView casedes = v.findViewById(R.id.casedestination);
                            casedate.setVisibility(View.GONE);
                            casesource.setVisibility(View.GONE);
                            casedes.setVisibility(View.GONE);
//                            if (firavailable) {
//                                casedate.setBackgroundResource(R.drawable.plus);
//                                casesource.setBackgroundResource(R.drawable.plus);
//                                casedes.setBackgroundResource(R.drawable.plus);
//                                casedate.setText("Transfer Date:\n N/A");
//                                casesource.setText("From Court No.\n N/A");
//                                casedes.setText("To" +
//                                        " Court No.\n N/A");
//                            } else {
//
//                            }
                        }
                    } else {
                        TextView casedate = v.findViewById(R.id.casetrnsfdate);
                        TextView casesource = v.findViewById(R.id.casesource);
                        TextView casedes = v.findViewById(R.id.casedestination);

                        casedate.setVisibility(View.GONE);
                        casesource.setVisibility(View.GONE);
                        casedes.setVisibility(View.GONE);

                        TextView actview = v.findViewById(R.id.caseact);
                        TextView section = v.findViewById(R.id.casesection);
                        actview.setVisibility(View.GONE);
                        section.setVisibility(View.GONE);


                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("TAG", "" + e.getMessage());
        }
        return v;
    }
}