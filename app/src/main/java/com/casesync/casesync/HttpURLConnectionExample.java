package com.casesync.casesync;

/**
 * Created by Vikas on 2/22/2017.
 */

import android.content.Context;
import android.util.Log;

import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Casedetails;
import com.casesync.utill.Casehistory;
import com.casesync.utill.HttpRequestTask;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.casesync.utill.ResponseVO;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class HttpURLConnectionExample {

    /**
     * This class is a core class of fetching data from the source site*/

    public String USER_AGENT = "Mozilla/21.0";


    public static void main(String[] args) {

        String resp = "<table border=\\\"0\\\" class='table tbl-result'><thead><tr><th scope=\\\"col\\\" style=''>Judge<\\/th><th scope=\\\"col\\\" style=''>Business On Date<\\/th><th scope=\\\"col\\\" style=''>Hearing Date<\\/th><th scope=\\\"col\\\" style=''>Purpose of hearing<\\/th><\\/tr><\\/thead><tbody><tr><td align='left' style=''>Senior Civil Judge<\\/td><td align='left' style=''><a style='color:blue;text-decoration:underline;' href='#' onclick=viewBusiness('16','15','20150903','201700001152015','2','DisposedP','23-07-2015','1')>23-07-2015<\\/a><\\/td><td>03-09-2015<\\/td><td style=''> PET\\/PLAINTIFF EVIDENCE<\\/td><\\/tr><tr><td align='left' style=''>Senior Civil Judge<\\/td><td align='left' style=''><a style='color:blue;text-decoration:underline;' href='#' onclick=viewBusiness('16','15','20150918','201700001152015','2','DisposedP','03-09-2015','1')>03-09-2015<\\/a><\\/td><td>18-09-2015<\\/td><td style=''> PET\\/PLAINTIFF EVIDENCE<\\/td><\\/tr><tr><td align='left' style=''>Senior Civil Judge<\\/td><td align='left' style=''><a style='color:blue;text-decoration:underline;' href='#' onclick=viewBusiness('16','15','','201700001152015','2','Disposed','18-09-2015','1')>18-09-2015<\\/a><\\/td><td><\\/td><td style=''> Disposed<\\/td><\\/tr>";
        parseHistory(resp);

    }

    public static void parseHistory(String history) {
        Document doc = Jsoup.parse(history.replace("\\", ""));
        Elements table = doc.select("table.tbl-result");
        System.out.println("Data:" + table);
        Elements rows = table.select("tr");
        System.out.println("Row size:" + rows.size());
        for (int i = 1; i < rows.size(); i++) {
            Element row = rows.get(i);
            Elements cols = row.select("td");
            for (int count = 0; count < cols.size(); count++) {
                System.out.println("Data:" + cols.get(count).text());
            }
        }

    }

    // HTTP GET request
    public ResponseVO sendGet(String url,String cookie,String caseno,String casetype) throws Exception {
        //Change the logic such that it should fetch data automatically
        ResponseVO resp=new ResponseVO();

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Cookie", "PHPSESSID="+cookie);
        int responseCode = con.getResponseCode();
        //System.out.println("\nSending 'GET' request to URL : " + url);
        //System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //System.out.println(response);
        //Update the below part
        if(response.toString().contains(casetype))
        {
            if(response.toString().contains("guestlogin"))
            {
                resp.setStatus(false);
                resp.setResponse("Cookie is invalid");
            }
            else if(response.toString().contains("CASE IS NOT ALLOCATED TO COURT"))
            {
                resp.setResponse("CASE IS NOT ALLOCATED TO COURT");
                resp.setStatus(false);
            }
            else
            {
                resp.setResponse(response.toString());
                resp.setStatus(true);
            }
        }
        else
        {
            if(response.toString().contains("CASE IS NOT ALLOCATED TO COURT"))
            {
                resp.setResponse("CASE IS NOT ALLOCATED TO COURT");
                resp.setStatus(false);
            }
            else
            {
                //System.out.println(response.toString());
                resp.setStatus(false);
                resp.setResponse("Cookie is invalid");
            }

        }
        return resp;
    }
    public static List<Casehistory> parsehistory(Casedetails casedetails,String response,int historycount,String cookie,Context context)
    {
        List<Casehistory> history=new ArrayList<Casehistory>();
        try
        {
            Document doc = Jsoup.parse(response);

                String resphistory = "";
                Elements table = doc.select("table.history_table"); //select the first table.

                Elements rows = table.select("tr");
                //System.out.println(rows.size());
                Log.i("HISTORY:", "Row size:" + rows.size());
                Log.i("HISTORY:", "Row size:" + rows.size());
                for (int i = 1; i < rows.size(); i++) {

                    // Log.i("HISTORY:","Coming to i:"+i+" Size:"+rows.size());
                    //first row is the col names so skip it.
                    //Need last 1 case history
                    //Condition based on the size passed as a parameter
                    if (i >= rows.size() - historycount) {
                        // Log.i("HISTORY:","Executing the row::"+rows.size());
                        Element row = rows.get(i);
                        Elements cols = row.select("td");
                        Casehistory casehistory = new Casehistory();
                        // Log.i("HISTORY:","Hearing date:"+cols.get(3).text());
                        if (!cols.get(3).text().equals(""))
                            if (!cols.get(3).text().contains("Next Date Not Given")) {
                                casehistory.setHearingdate(HttpURLConnectionExample.getdatefromsdf(cols.get(3).text(), "dd-MM-yyyy"));

                            } else {
                                casehistory.setHearingdate(null);
                            }
                        casehistory.setPurposeofhearing(cols.get(4).text());
                        Elements links = row.select("a[href]");
                        for (Element link : links) {
                            //  Log.i("HISTORY:","link : " + link.attr("href"));
                            // Log.i("HISTORY:","text : " + link.text());
                            if (!link.text().equals(""))
                                casehistory.setBusinessondate(HttpURLConnectionExample.getdatefromsdf(link.text(), "dd-MM-yyyy"));

                            try {
                                String historyurl = PropertyUtil.getProperty("historyurl", context) +"" + link.attr("href");
                                LoginParams loginparams = new LoginParams();
                                loginparams.setUrl(historyurl);
                                loginparams.setUsername(cookie);
                                                        /*Do it using spring*/
                                resphistory = new HttpRequestTask(loginparams).execute().get();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String searchkey = "<td width='69%' valign='top' align='left' style='font-size:1em;'>";
                            int index1 = resphistory.indexOf(searchkey);
                            int index2 = resphistory.indexOf("<br />");

                            try {
                                casehistory.setBusiness(resphistory.substring(index1 + searchkey.length(), index2));
                                Log.i("HISTORY:", "reponse string " + response.substring(index1 + searchkey.length(), index2));
                            } catch (IndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                        }
                        casehistory.setCasenumber(casedetails.getCasenumber());
                        history.add(casehistory);
                                                                                /*Save the case history here */

                    }
                }


        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return history;
    }

    public static Casedetails parseData(String data, String caseno, String cookie, Context context) {
        Casedetails caseObj=null;
        try{
        //Check the case number already exists or not


        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                CaseSyncOpenDatabaseHelper.class);
        Log.e("insert data:", "After initializing");
//        Dao<Casedetails, Long> casedetDao = todoOpenDatabaseHelper.getDao();
//        List<Casedetails> todos = casedetDao.queryForEq("casenumber", caseno);
//        if (todos.size() > 0) {
//            Log.e("parseData", "Synchronizing the case");
//
//            caseObj=todos.get(0);
//        } else {
//            Log.e("parseData", "Case not available in the database");
//
//        }//The below condition should be for next hearing date
        if(caseObj==null)
        {
            if (!data.contains("CASE IS NOT ALLOCATED TO COURT")) {

                caseObj=new Casedetails();
                Document doc=Jsoup.parse(data);
                Elements courtdet = doc.getElementsByClass("h1class");
                for (Element link : courtdet){
                    Log.e("URL",""+link.text());
                    caseObj.setCourtname(link.text());
                }

                Elements casedet = doc.getElementsByClass("case_details_table");
                caseObj.setCasenumber(caseno);
                for (Element link : casedet){
                    if(link.text().contains("Case Type"))
                    {
                        Log.e("URL",""+link.text().replace("Case Type : ", ""));
                        //System.out.println("Casetype:"+link.text().replace("Case Type : ", ""));
                        caseObj.setCasetype(link.text().replace("Case Type : ", ""));
                    }
                    else if(link.text().contains("Filing Number"))
                    {
                        String filingdet=link.text().replace("Filing Number: ", "");
                        //Filing Date:
                        //System.out.println("filingdet:"+filingdet);
                        String[] filing = filingdet.split("Filing Date:");
                        //System.out.println("filing[0]:"+filing[0]);
                        caseObj.setFilingnumber(filing[0]);
                        //System.out.println("Filing date:"+filing[1]);
                        //System.out.println("Date"+HttpURLConnectionExample.getdatefromsdf(filing[1].replace(" ", ""),"dd-MM-yyyy"));
                        caseObj.setFilingdate(HttpURLConnectionExample.getdatefromsdf(filing[1].replace(" ", ""),"dd-MM-yyyy"));

                    }//Registration Number
                    else if(link.text().contains("Registration Number"))
                    {
                        String resdet=link.text().replace("Registration Number: ", "");
                        //Filing Date:
                        //System.out.println(resdet);
                        String[] filing = resdet.split("Registration Date: ");
                        //System.out.println(filing[0]);
                        caseObj.setRegistration_number(filing[0]);
                        //System.out.println("filing[1]:"+filing[1]);
                        caseObj.setRegistration_date(HttpURLConnectionExample.getdatefromsdf(filing[1],"dd-MM-yyyy"));
                    }

                }
                Elements casestatus = doc.getElementsByTag("span");

                for (Element link : casestatus){
                    //Log.e("CALL","-->"+link.text());
                    if(link.text().contains("First Hearing Date"))
                    {
                        //System.out.println("link.text():"+link.text());

                        caseObj.setFirst_hearing_date(CasesyncUtill.getdatefromsdfext(link.text().replace("First Hearing Date : ", "")));
                    }
                    else if(link.text().contains("Next Hearing Date"))
                    {
                        if(link.text().contains("Next Date is not given"))
                        {
                            caseObj.setNext_hearing_date(null);
                        }
                        else
                        {
                            //System.out.println("link.text():"+link.text());
                            //System.out.println("link.text():"+link.text().replace("Next Hearing Date: ", ""));
                            //System.out.println("NEXT HEARING:"+CasesyncUtill.getdatefromsdfext(link.text().replace("Next Hearing Date: ", "")));
                            caseObj.setNext_hearing_date(CasesyncUtill.getdatefromsdfext(link.text().replace("Next Hearing Date: ", "")));
                        }

                    }
                    //following two conditions for disposal add nature of disposal also
                    //Need to handle change in court
                    else if(link.text().contains("Decision Date: "))
                    {
                        //Log.e("CALL","COMING TO Decision Date-->"+link.text());
                        //System.out.println(link.text());
                        //System.out.println("Decision date:"+CasesyncUtill.getdatefromsdfext(link.text().replace("Decision Date: ", "")));
                        caseObj.setDisposed(true);
                        caseObj.setNext_hearing_date(CasesyncUtill.getdatefromsdfext(link.text().replace("Decision Date: ", "")));

                    }
                    else if(link.text().contains("Case Status"))
                    {
                        //Log.e("CALL","COMING TO Case Status-->"+link.text());
                        caseObj.setDisposed(true);
                        //for disposal
                        //System.out.println("Case disposed");
                        //set disposal flag
                    }//Nature of Disposal
                    else if(link.text().contains("Nature of Disposal"))
                    {
                       // Log.e("CALL","COMING TO Nature of Disposal-->"+link.text());
                        caseObj.setDisposed(true);
                        //for disposal
                        //System.out.println("Nature of Disposal"+link.text());
                        caseObj.setNatureofdisposal(link.text().replace("Nature of Disposal: ", ""));
                    }
                    else if(link.text().contains("Stage of Case:"))
                    {
                        //System.out.println(link.text());
                        caseObj.setStageofcase(link.text().replace("Stage of Case: ", ""));
                        //it wont come for disposal
                    }
                    else if(link.text().contains("Court Number and Judge :"))
                    {
                        //System.out.println(link.text().replace("Court Number and Judge : ", ""));
                        caseObj.setCourtnumberjudge(link.text().replace("Court Number and Judge : ", ""));
                    }
                }
                Elements subordinatecourt = doc.getElementsByClass("Lower_court_table");
                for (Element link : subordinatecourt){
                    caseObj.setSubcourtinfo(link.text());
                }
                //FIR_details_table
                Elements fir = doc.getElementsByClass("FIR_details_table");
                for (Element link : fir){
                    caseObj.setFirdetails(link.text());
                }
                //Acts_table
                Elements acts_table = doc.getElementsByClass("Acts_table");
                for (Element link : acts_table){
                    caseObj.setActs(link.text());
                }
                //transfer_table
                Elements transfer_table = doc.getElementsByClass("transfer_table");
                for (Element link : transfer_table){
                    caseObj.setCasetransfer(link.text());
                }

                Elements petadv = doc.getElementsByClass("Petitioner_Advocate_table");
                for (Element link : petadv){
                    caseObj.setPetitioner_advocate(link.text());
                    //System.out.println(link.text());
                    String petname="";
                    for(int count=1;count<15;count++)
                    {
                        if(count==1)
                        {
                            //save the whole value as well as individual details
                            ////System.out.println(link.text().substring(3,link.text().indexOf("Address -")));
                            petname=petname+""+link.text().substring(3,link.text().indexOf("Address -"));
                            if(link.text().contains("Advocate- "))
                            {
                                int advstart=link.text().indexOf("Advocate- ");
                                int advend=0;
                                advend=link.text().indexOf(" 2)");
                                if(advend>0)
                                {
                                    //System.out.println(link.text().substring(advstart, advend).replaceAll("Advocate- ", ""));
                                    caseObj.setPetitioner_advocate_name(link.text().substring(advstart, advend).replaceAll("Advocate- ", ""));


                                }
                                else
                                {
                                    //System.out.println(link.text().substring(advstart).replaceAll("Advocate- ", ""));
                                    caseObj.setPetitioner_advocate_name(link.text().substring(advstart).replaceAll("Advocate- ", ""));
                                    ////System.out.println("break");
                                    break;
                                }
                            }
                        }
                        else
                        {
                            try {

                                if (link.text().contains(count + ")")) {
                                    int namestart = link.text().indexOf(count + ")");
                                    int nameend = 0;
                                    try
                                    {
                                        if (link.text().contains((count +1)+")")) {
                                            nameend=link.text().indexOf((count +1)+")");

                                        }
                                    }
                                    catch (StringIndexOutOfBoundsException e) {
                                        e.getMessage();
                                    }
                                    if(nameend>0)
                                    {
                                        ////System.out.println(link.text().substring(namestart+3, nameend));
                                        petname=petname+"||"+link.text().substring(namestart+3, nameend);
                                    }
                                    else
                                    {
                                        petname=petname+"||"+link.text().substring(namestart+3);
                                        ////System.out.println(link.text().substring(namestart+3));
                                    }
                                }
                            } catch (StringIndexOutOfBoundsException e) {
                                e.getMessage();
                            }
                        }
                    }
                    //System.out.println(petname);
                    caseObj.setPetitioner_names(petname);
                }
                //Respondent code
                Elements resadv = doc.getElementsByClass("Respondent_Advocate_table");
                try
                {
                    for (Element link : resadv){
                        //System.out.println(link.text());
                        caseObj.setRespondentadvocate(link.text());
                        String resname="";
                        for(int count=1;count<15;count++)
                        {
                            if(count==1)
                            {
                                //save the whole value as well as individual details
                                ////System.out.println(link.text().substring(3,link.text().indexOf("Address -")));
                                resname=resname+""+link.text().substring(3,link.text().indexOf("Address -"));
                                if(link.text().contains("Advocate -"))
                                {
                                    int advstart=link.text().indexOf("Advocate -");
                                    int advend=0;
                                    advend=link.text().indexOf(" 2)");
                                    if(advend>0)
                                    {
                                        //System.out.println(link.text().substring(advstart, advend).replaceAll("Advocate - ", ""));
                                        caseObj.setRespondent_advocate_name(link.text().substring(advstart, advend).replaceAll("Advocate - ", ""));

                                    }
                                    else
                                    {
                                        //System.out.println(link.text().substring(advstart).replaceAll("Advocate - ", ""));
                                        caseObj.setRespondent_advocate_name(link.text().substring(advstart).replaceAll("Advocate - ", ""));
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                try {

                                    if (link.text().contains(count + ")")) {
                                        int namestart = link.text().indexOf(count + ")");
                                        int nameend = 0;
                                        try
                                        {
                                            if (link.text().contains((count +1)+")")) {
                                                nameend=link.text().indexOf((count +1)+")");

                                            }
                                        }
                                        catch (StringIndexOutOfBoundsException e) {
                                            e.getMessage();
                                        }
                                        if(nameend>0)
                                        {
                                            ////System.out.println(link.text().substring(namestart+3, nameend));
                                            resname=resname+"||"+link.text().substring(namestart+3, nameend);
                                        }
                                        else
                                        {
                                            ////System.out.println(link.text().substring(namestart+3));
                                            resname=resname+"||"+link.text().substring(namestart+3);
                                        }
                                    }
                                } catch (StringIndexOutOfBoundsException e) {
                                    e.getMessage();
                                }
                            }
                        }
                        System.out.println(resname);
                        caseObj.setRespondent_names(resname);
                        //Need to add the other required data last 3 judgements
                        //call a external method

                    }
                }catch (Exception e)
                {
                    Log.e("PAR",e.getCause().getMessage());
                }

            }
        }
        else
        {
            //System.out.println("Object aleready available");
        }
    }catch (Exception e)
    {
        Log.e("EXC",e.getMessage());
    }
        return caseObj;
    }
    public static Date getdatefromsdf(String startDateString,String format)
    {
        DateFormat df = new SimpleDateFormat(format);
        Date startDate=null;
        try {
            startDate = df.parse(startDateString.trim());
            String newDateString = df.format(startDate);
            //System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;

    }

    public static String getbusiness(String url,String cookie) throws IOException
    {
        //Here need to store the case history
        //get cookie here
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent","Mozilla/21.0");
        con.setRequestProperty("Cookie", "PHPSESSID="+cookie);
        int responseCode = con.getResponseCode();
        //System.out.println("\nSending 'GET' request to URL : " + url);
        //System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        ////System.out.println(response);
        return response.toString();
    }
}
