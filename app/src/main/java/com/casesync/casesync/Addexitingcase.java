package com.casesync.casesync;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Casehistory;
import com.casesync.utill.Cases;
import com.casesync.utill.Encryption;
import com.casesync.utill.ImportObj;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.scottyab.aescrypt.AESCrypt;

import org.json.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Addexitingcase extends AppCompatActivity {
    private static final String PREFS_NAME = "LoginPrefs";
    static DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    public LoginParams loginparams = new LoginParams();
    CasesyncUtill casesyncUtill = new CasesyncUtill();
    private String TAG = "ADDE";

    public static boolean insertdata(CaseObj casedetails, Context context, boolean firebase) throws java.sql.SQLException {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                CaseSyncOpenDatabaseHelper.class);
        Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
        List<CaseObj> oldcaseobj = casedetDao.queryForEq("cino", casedetails.getCino());
        try {
            CasesyncUtill syncutil = new CasesyncUtill();
            Log.d("insert data:", "inserting data into firebase");
            if (!CollectionUtils.isEmpty(oldcaseobj)) {
                //Object Already available. Auto or Manual Sync
                try {
                    casedetails.setPhone(oldcaseobj.get(0).getPhone() != null ? oldcaseobj.get(0).getPhone() : "");
                    casedetails.setEmail(oldcaseobj.get(0).getEmail() != null ? oldcaseobj.get(0).getEmail() : "");
                    casedetails.setContactname(oldcaseobj.get(0).getContactname() != null ? oldcaseobj.get(0).getContactname() : "");
                    casedetails.setAddnotes(oldcaseobj.get(0).getAddnotes() != null ? oldcaseobj.get(0).getAddnotes() : "");
                    //casedetails.setWritinfo(oldcaseobj.get(0).getWritinfo() != null ? oldcaseobj.get(0).getWritinfo() : "");

                    if (oldcaseobj.get(0).getDateNextList() != null) {
                        String olddate = df.format(oldcaseobj.get(0).getDateNextList());
                        if (casedetails.getDateNextList() != null) {
                            String newdate = df.format(casedetails.getDateNextList());
                            if (!olddate.equals(newdate)) {
                                //String clientname = casedetails.getCourtName() != null ? casedetails.getCourtName() : "";
                                syncutil.sendNotification("New Hearing Added for " + casedetails.getTypeName() + "/" + casedetails.getRegNo() + "/" + casedetails.getRegYear(),
                                        "Next hearing on " + newdate + "/" + casedetails.getPetName() + " VS " + casedetails.getResName(), casedetails.getCino(), context, CasesyncUtill.NOTITYPE.CASE);
                                //Below code will create calendar notifications
                                syncutil.createCalendarNotification(context, Arrays.asList(casedetails));
                                //If SMS is enabled send the notification to the client on when is the next hearing that will bring more value to the application.
                                syncutil.sendNextHearingDateSMS(context, casedetails);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Dao.CreateOrUpdateStatus status = casedetDao.createOrUpdate(casedetails);
                if (status.isCreated()) {

                    if (firebase) {
                        FirebaseUtil.insertuseraccount(casedetails.getCino(), true);
                    }
                } else {
                    Log.d("insert data:", "Updated" + status.isUpdated());
                }
            } else {
                //syncutil.createCalendarNotification(context, Arrays.asList(casedetails));
                Dao.CreateOrUpdateStatus status = casedetDao.createOrUpdate(casedetails);
                if (status.isCreated()) {
                    //Adding a new case or Initial Sync or Import from ecourts file.
                    if (firebase) {
                        FirebaseUtil.insertuseraccount(casedetails.getCino(), true);
                    }
                } else {
                    Log.d("insert data:", "Updated" + status.isUpdated());
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

/*        List<CaseObj> todos = casedetDao.queryForEq("case_no", casedetails.getCaseNo());
        if (todos.size() > 0) {
            CaseObj obj=todos.get(0);
            Log.i("Insertcase", "Case number already exists:");
            casedetDao.update(casedetails);

        } else {
            casedetDao.create(casedetails);
        }*/
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addexitingcase);
        Bundle bundle = getIntent().getExtras();
        Uri data = getIntent().getData();
        String casenumber = "";

        if (data != null) {
            casenumber = data.getQueryParameter("caseno");
            casenumber = casenumber.replace(" ", "+");
            Log.e(TAG, "casenumber:" + casenumber);
            try {

                if (!casenumber.isEmpty()) {
                    //*Check the shared preferences*//*
                    SharedPreferences pref = getSharedPreferences(PREFS_NAME, 0);
                    String status = pref.getString("logged", "");
                    if (!status.isEmpty()) {
                        String cinno = AESCrypt.decrypt(Encryption.password, casenumber);
                        Log.e(TAG, "cinno:" + cinno);
                        ImportObj obj = new ImportObj();
                        obj.setCino(cinno);

                        List<ImportObj> dataModel = new ArrayList<ImportObj>();
                        dataModel.add(obj);
                        if (dataModel != null) {
                            try {
                                String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                                LoginParams loginparams = new LoginParams();
                                loginparams.setUrl(url);
                                AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, null, dataModel);
                                String sleepTime = "1";
                                runner.execute(sleepTime);
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Please login to casesync ", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error " + "Not a valid request" + "", Toast.LENGTH_LONG).show();
                }

            } catch (IllegalArgumentException e) {
                Toast.makeText(getApplicationContext(), "Invalid encryption ", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //Adding a new case in to a database and open casedetailsTab
            Intent i = getIntent();
            PackageActivity.TYPE typ = (PackageActivity.TYPE) i.getSerializableExtra("key");

            if (typ == PackageActivity.TYPE.CASES) {
                List<Cases> dataModel = (List<Cases>) i.getSerializableExtra("LIST");
                if (dataModel != null) {
                    try {
                        String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                        LoginParams loginparams = new LoginParams();
                        loginparams.setUrl(url);
                        Log.i("CustomAd", "URL OF ADD CASES:" + url);
                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, dataModel, null);
                        String sleepTime = "1";
                        runner.execute(sleepTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (typ == PackageActivity.TYPE.IMPORTOBJ) {

                List<ImportObj> dataModel = (List<ImportObj>) i.getSerializableExtra("LIST");
                if (dataModel != null) {
                    try {
                        String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                        LoginParams loginparams = new LoginParams();
                        loginparams.setUrl(url);
                        Log.i("CustomAd", "URL OF ADD CASES:" + url);
                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, null, dataModel);
                        String sleepTime = "1";
                        runner.execute(sleepTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        }

    }

    /* Insert into firebase */

    public void insertcasehis(List<Casehistory> casehistory) throws java.sql.SQLException {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                CaseSyncOpenDatabaseHelper.class);
        Log.e("insert history:", "After initializing");
        Dao<Casehistory, Long> casehisDao = todoOpenDatabaseHelper.getCasehistorydao();

        Log.e("insert history:", "Casedetdao" + casehistory.get(0).getCasenumber());
        Log.e("insert history:", "Casedetdao" + casehistory.get(0).getBusiness());


        List<Casehistory> todos = casehisDao.queryForEq("casenumber", casehistory.get(0).getCasenumber());
        if (todos.size() > 0) {
            Log.e("history", "Case history already exists:");
        } else {
            for (Casehistory casehis : casehistory) {
                casehisDao.create(casehis);
            }
        }

    }

    class AsyncTaskRunner extends AsyncTask<String, Integer, Integer> {
        ProgressDialog progressDialog;
        private LoginParams parms;
        private List<Cases> cases;
        private List<ImportObj> importobj;

        public AsyncTaskRunner(LoginParams parms, List<Cases> casesobj, List<ImportObj> impobj) {
            this.parms = parms;
            this.cases = casesobj;
            this.importobj = impobj;
        }

        @Override
        protected Integer doInBackground(String... params) {

            Integer count = 0;
            try {
                Log.i("ADD", parms.getUrl());
                RestTemplate restTemplate = new RestTemplate();


                String url = "";
                publishProgress(0);
                if (!CollectionUtils.isEmpty(cases)) {
                    for (Cases dataModel : cases) {
                        if (dataModel.isSelected()) {

                            url = "";
                            String data_params = "?case_no=" + AESCryptCustom.encrypt(dataModel.getCase_no()) +
                                    "&state_code=" + AESCryptCustom.encrypt(dataModel.getState_code()) + "&dist_code=" + AESCryptCustom.encrypt(dataModel.getDist_code())
                                    + "&court_code=" + AESCryptCustom.encrypt(dataModel.getCourt_code());
                            url = parms.getUrl() + data_params;
                            String responsetmp = restTemplate.getForObject(url, String.class);
                            try {
                                if (!responsetmp.isEmpty() && responsetmp != null) {
                                    Log.i(TAG, responsetmp);
                                    Log.i(TAG, url);
                                    try {
                                        JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                                        Log.i(TAG, "obj:" + objtemp);
                                        ObjectMapper objectMapper = new ObjectMapper();
                                        CaseObj caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);

                                        if (caseobj != null) {
                                            boolean caseexists = CasesyncUtill.caseexist(caseobj.getCino(), getApplicationContext());
                                            if (caseexists == true) {
                                                count = count + 1;
                                                String businesstext = casesyncUtill.fetchBusiness(caseobj, getApplicationContext());
                                                caseobj.setWritinfo(businesstext);
                                                insertdata(caseobj, getApplicationContext(), true);
                                                casesyncUtill.createCalendarNotification(getApplicationContext(),Arrays.asList(caseobj));
                                                publishProgress(count);
                                            } else {
                                                count = count + 1;
                                                publishProgress(count);
                                                Log.i(TAG, "Case with registration number " + caseobj.getRegNo() + " already exists. Skipping the case import.");
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        //Exception to be handled properly.
                                        //Toast.makeText(getApplicationContext(), "Unable fetch the case details from ecourt's servers.Currently details not available .Please try after some time", Toast.LENGTH_LONG).show();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (!CollectionUtils.isEmpty(importobj)) {
                    Log.d("ASYNC", "Importing cases from import object " + importobj.size());
                    String baseurl = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";

                    for (ImportObj obj : importobj) {
                        if (obj != null) {
                            CaseObj caseobj = casesyncUtill.getCaseObjFromCino(getApplicationContext(), obj.getCino(), true);
                            if (caseobj.getCino() == null) {
                                Toast.makeText(getApplicationContext(), "Unable fetch the case details from ecourt's servers.Currently details are not available. Please try after some time", Toast.LENGTH_LONG).show();
                            } else {
                                count = count + 1;
                                if (obj.getCaseNo() == null) {
                                    importobj.get(0).setCaseNo(caseobj.getCaseNo());
                                }
                            }

                        }
                    }
                }

            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
            return count;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if (result > 1) {
                Intent i = new Intent(getApplicationContext(), NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                Toast.makeText(getApplicationContext(), "Added " + result + " records successfully.", Toast.LENGTH_LONG).show();
                getApplicationContext().startActivity(i);
            } else if (result == 0) {
                Intent i = new Intent(getApplicationContext(), NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                Toast.makeText(getApplicationContext(), "Unable fetch the case details from ecourt's servers.Currently details are not available.Please try after some time", Toast.LENGTH_LONG).show();
                getApplicationContext().startActivity(i);
            } else {
                Intent i = new Intent(getApplicationContext(), CasedetailsTab.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                if (!CollectionUtils.isEmpty(cases)) {
                    i.putExtra("casenumber", cases.get(0).getCino());
                    //Load from the database
                    if (CasesyncUtill.caseexist("" + cases.get(0).getCino(), getApplicationContext())) {
                        Toast.makeText(getApplicationContext(), "Unable fetch the case details from ecourt's servers.Currently details are not available.Please try after some time", Toast.LENGTH_LONG).show();
                    } else {
                        getApplicationContext().startActivity(i);
                    }
                }

                if (!CollectionUtils.isEmpty(importobj)) {
                    i.putExtra("casenumber", importobj.get(0).getCino());
                    getApplicationContext().startActivity(i);
                }
            }

        }

        @Override
        protected void onPreExecute() {
            if (!CollectionUtils.isEmpty(cases)) {
                progressDialog = ProgressDialog.show(Addexitingcase.this,
                        "Total cases " + cases.size(),
                        "Fetching Case Details,Please be patient ....");
            } else if (!CollectionUtils.isEmpty(importobj)) {
                progressDialog = ProgressDialog.show(Addexitingcase.this,
                        "Total cases " + importobj.size(),
                        "Fetching Case Details,Please be patient ....");
            } else {
                progressDialog = ProgressDialog.show(Addexitingcase.this,
                        "",
                        "Fetching Case Details ....");
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }
    }
}
