package com.casesync.casesync;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.Court;
import com.casesync.utill.District;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.LinkedHashMapAdapter;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class Causelist extends AppCompatActivity {


    private static final String PREFS_NAME = "LoginPrefs";
    private static final String AUTHORITY = "com.casesync.fileprovider";
    private static final String TAG = "CL";
    private static LinkedHashMap districtmapData = new LinkedHashMap<String, String>();
    private static LinkedHashMap courtmapData = new LinkedHashMap<String, String>();
    private static int statenumber = 0;
    private static String districtcode = "0";
    private static String courtcode = "0";
    private static String statecode = "0";
    final Calendar myCalendar = Calendar.getInstance();
    EditText editTxt;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    private Spinner state, district, court, courtname, type;
    private String courtcodefin = "0";
    private String court_est = "0";
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_causelist);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        Log.i(TAG, "Existing state code:" + statecode);
        statenumber = Integer.parseInt(prefs.getString("state_name", "0"));
        if (statenumber == 0 && !statecode.equals("0")) {
            statenumber = Integer.parseInt(statecode);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //Checking for internet before loading
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            initializeDropdowns();
            setOnclickListeners();
        } else {
            View parentLayout = findViewById(android.R.id.content);
            //This is for testing
            Snackbar.make(parentLayout, "Please enable internet.", Snackbar.LENGTH_LONG)
                    .setAction("CLOSE", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                    .show();
        }
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Spinner spinner = (Spinner) findViewById(R.id.cldistrict_spinner);
//
//        SharedPreferences prefs = getSharedPreferences("district", Context.MODE_PRIVATE);
//        int spinnerIndx = prefs.getInt("spinner_indx", 0);
//        spinner.setSelection(spinnerIndx);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//
//        Spinner spinner = (Spinner) findViewById(R.id.cldistrict_spinner);
//
//        SharedPreferences prefs = getSharedPreferences("district", Context.MODE_PRIVATE);
//        prefs.edit().putInt("spinner_indx", spinner.getSelectedItemPosition()).apply();
//    }

    @Override
    public void onBackPressed() {


        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            Intent intent = new Intent(Causelist.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case 16908332:
                Intent intent = new Intent(Causelist.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public LinkedHashMap getStatesData() {
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        mapData.put("0", "Select State");
        mapData.put("28", "Andaman and Nicobar");
        mapData.put("2", "Andhra Pradesh");
        mapData.put("6", "Assam");
        mapData.put("8", "Bihar");
        mapData.put("27", "Chandigarh");
        mapData.put("18", "Chhattisgarh");
        mapData.put("26", "Delhi");
        mapData.put("31", "Diu and Daman");
        mapData.put("32", "DNH at Silvasa");
        mapData.put("30", "Goa");
        mapData.put("17", "Gujarat");
        mapData.put("14", "Haryana");
        mapData.put("5", "Himachal Pradesh");
        mapData.put("12", "Jammu and Kashmir");
        mapData.put("7", "Jharkhand");
        mapData.put("3", "Karnataka");
        mapData.put("4", "Kerala");
        mapData.put("33", "Ladakh");
        mapData.put("23", "Madhya Pradesh");
        mapData.put("1", "Maharashtra");
        mapData.put("25", "Manipur");
        mapData.put("21", "Meghalaya");
        mapData.put("19", "Mizoram");
        mapData.put("34", "Nagaland");
        mapData.put("11", "Orissa");
        mapData.put("35", "Puducherry");
        mapData.put("22", "Punjab");
        mapData.put("9", "Rajasthan");
        mapData.put("24", "Sikkim");
        mapData.put("10", "Tamil Nadu");
        mapData.put("29", "Telangana");
        mapData.put("20", "Tripura");
        mapData.put("15", "Uttarakhand");
        mapData.put("13", "Uttar Pradesh");
        mapData.put("16", "West Bengal");

        return mapData;
    }

    public void initializeDropdowns() {
        state = findViewById(R.id.state_spinner);
        LinkedHashMap mapData = getStatesData();
        LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapData);
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        state = findViewById(R.id.clstate_spinner);
        state.setAdapter(dataAdapter);
        //It will Load from the default state
        Log.i(TAG, "State Number -- " + statenumber);

        //Change the logic.
        int pos = 0;
        int intialpos = 0;
        Set<String> keys = mapData.keySet();
        Log.i(TAG, "Size:" + keys.size());
        for (String key : keys) {
            Log.i(TAG, key + " -- "
                    + mapData.get(key));
            intialpos = intialpos + 1;
            if (key.equals("" + statenumber)) {
                pos = intialpos - 1;
                Log.i(TAG, pos + " Selected -- "
                        + mapData.get(key));
            }
        }
        Log.i(TAG, "Selected Position" + pos);
        state.setSelection(pos);

        /*Setting the initial data*/

        if (districtmapData.size() == 0) {
            districtmapData.put("0", "Select District");
        }

        LinkedHashMapAdapter<String, String> temp2 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, districtmapData);
        temp2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        district = findViewById(R.id.cldistrict_spinner);
        district.setAdapter(temp2);
        if (!districtcode.equals("0")) {
            int dispos = new ArrayList<String>(districtmapData.keySet()).indexOf(districtcode);
            district.setSelection(dispos);
        }
        //My Approach is simple. Load the districts data to

        if (courtmapData.size() == 0) {
            courtmapData.put("0", "Select Court Complex");
        }
        LinkedHashMapAdapter<String, String> temp1 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, courtmapData);
        temp1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        court = findViewById(R.id.clcourt_spinner);
        court.setAdapter(temp1);
        if (!courtcode.equals("0")) {
            int courtpos = new ArrayList<String>(courtmapData.keySet()).indexOf(courtcode);
            Log.i(TAG, "Selected Court position:" + courtpos);
            court.setSelection(courtpos);
        }

        LinkedHashMap mapcourtname = new LinkedHashMap<String, String>();
        mapcourtname.put("0", "Select Court Name");
        LinkedHashMapAdapter<String, String> courtnames = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapcourtname);
        courtnames.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        courtname = findViewById(R.id.clcourt_name);
        courtname.setAdapter(courtnames);

        editTxt = findViewById(R.id.causelist_date);
        btnSubmit = findViewById(R.id.gen_causelist);


    }

    public void loaddistricsbystateid(String response) {
        district = findViewById(R.id.cldistrict_spinner);
        districtmapData = new LinkedHashMap<String, String>();
        List<District> districtlist = null;
        try {

            JSONObject obj = new JSONObject(response);
            Log.i(TAG, "District JSON:" + obj.toString());
            String x = AESCryptCustom.decrypt(obj.getString("districts"));
            x = EcourtsUtility.formatJson(x);
            JSONArray jsonarray = new JSONArray(x);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                Log.i(TAG, "State JSON:" + jsonobject.toString());
                if (i == 0) {
                    districtmapData.put(0, "Select District");
                    districtmapData.put(jsonobject.getString("dist_code"), jsonobject.getString("dist_name"));
                } else {
                    districtmapData.put(jsonobject.getString("dist_code"), jsonobject.getString("dist_name"));
                }


            }
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, districtmapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            district.setAdapter(dataAdapter);
            if (!districtcode.equals("0") && districtmapData.size() > 0) {
                int pos = new ArrayList<String>(districtmapData.keySet()).indexOf(districtcode);
                Log.i(TAG, "Selected district position:" + pos);
                district.setSelection(pos);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }

    }

    public void loadcourtsbydisid(String response) {
        courtmapData = new LinkedHashMap<String, String>();
        List<Court> courtlist = null;
        try {
            //Log.i(TAG,response);
            JSONObject obj = new JSONObject(response);
            String x = AESCryptCustom.decrypt(obj.getString("courtComplex"));
            x = EcourtsUtility.formatJson(x);
            JSONArray jsonarray = new JSONArray(x);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                if (i == 0) {
                    courtmapData.put("0", "Select Court Complex");
                }
                //Log.i(TAG,"Court Json:"+jsonobject.toString());
                //jsonobject.getString("court_complex_name").contains("All") ? "Select Court" :
                courtmapData.put(jsonobject.getString("njdg_est_code"),  jsonobject.getString("court_complex_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            courtmapData.put("0", "Select Court Complex");
        } finally {
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, courtmapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            court.setAdapter(dataAdapter);
            if (!courtcode.equals("0") && courtmapData.size() > 0) {
                int pos = new ArrayList<String>(courtmapData.keySet()).indexOf(courtcode);
                Log.i(TAG, "Selected Court position:" + courtcode);
                court.setSelection(pos);
            }

        }

    }

    public void loadcourtnamesbycourt(String response) {
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        try {
            JSONObject obj = new JSONObject(response);
            String x = AESCryptCustom.decrypt(obj.getString("courtNames"));
            x = EcourtsUtility.formatJson(x);
            Log.i(TAG, "Courts data:" + x);
            String[] courts = x.replace("\"", "").split("#");
            Log.i(TAG, "courts size:" + courts.length);
            int disable_count = 0;
            for (String result : courts) {
                String[] resultarry = result.split("~");

                //TODO As of now ignore the disabling part. Do it in future complete the rest of the features.
                if (resultarry[0].equals("D")) {
                    mapData.put(resultarry[0] + "" + disable_count, "" + resultarry[1]);
                    Log.i(TAG, resultarry[0] + "" + disable_count + "," + resultarry[1]);
                    disable_count = disable_count + 1;
                } else {
                    mapData.put(resultarry[0] + "", "" + resultarry[1]);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            mapData.put("0", "Select Court Name");
        } finally {
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, mapData);
//            {
//                @Override
//                public boolean isEnabled(int position) {
//                //Customize this logic.
//                return true;
//            }
//            };
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            courtname.setAdapter(dataAdapter);
        }

    }

    private void updateLabel() {
        editTxt.setText(sdf.format(myCalendar.getTime()));
    }

    public void setOnclickListeners() {
        state.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        String[] stateid = parent.getSelectedItem().toString().split("=");
                        statecode = stateid[0];
                        if (!stateid[0].equals("0")) {
                            Log.i(TAG, stateid[0]);
                            try {
                                LoginParams loginparams = new LoginParams();
                                String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/districtWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                        "&test_param=" + AESCryptCustom.encrypt("pending");
                                //loginparams.setUrl("http://httpstat.us/500");
                                loginparams.setUrl(url);
                                AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.DISTRITCT);
                                String sleepTime = "1";
                                runner.execute(sleepTime);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                    }
                });
        district.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        if (!parent.getSelectedItem().toString().contains("Select District")) {
                            String[] disid = parent.getSelectedItem().toString().split("=");
                            Log.i(TAG, "Selected Item:" + parent.getSelectedItem().toString());
                            districtcode = disid[0];
                            LoginParams loginparams = new LoginParams();
                            try {
                                String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/courtEstWebService.php?action_code="
                                        + AESCryptCustom.encrypt("fillCourtComplex") + "&state_code=" + AESCryptCustom.encrypt(statecode) +
                                        "&dist_code=" + AESCryptCustom.encrypt(districtcode);
                                loginparams.setUrl(url);
                                AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.COURT);
                                String sleepTime = "1";
                                runner.execute(sleepTime);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                    }
                });

        court.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        if (!parent.getSelectedItem().toString().contains("Select Court Complex")) {
                            //TODO populate court names here.
                            Log.i(TAG, "Selected Court Data:" + parent.getSelectedItem().toString());
                            String[] courtid = parent.getSelectedItem().toString().split("=");
                            LoginParams loginparams = new LoginParams();
                            try {
                                court_est = courtid[0];
                                courtcode = courtid[0];
                                String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/courtNameWebService.php?court_code="
                                        + AESCryptCustom.encrypt(courtid[0]) + "&state_code=" + AESCryptCustom.encrypt(statecode) +
                                        "&dist_code=" + AESCryptCustom.encrypt(districtcode);
                                Log.i(TAG, "courtname url:" + url);
                                loginparams.setUrl(url);
                                AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.COURTNAME);
                                String sleepTime = "1";
                                runner.execute(sleepTime);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                    }
                });

        type = findViewById(R.id.cl_causelist_type);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        editTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(Causelist.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            Log.i(TAG, "On click on cancel button ");
                            editTxt.setText("");
                        }
                    }
                });
                dialog.show();
            }
        });


        //Submit button

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    //TODO First program every thing and then start testing.
                    //First get the HTML Data. Then work on converting in to PDF.
                    //Details what we need?
                    /*
                     * State,District,Court,Court Name,CLTYPE,YEAR.
                     * */
                    //TODO ADD VALIDATIONS.
                    String court_name = courtname.getSelectedItem().toString();
                    String cl_type = type.getSelectedItem().toString();
                    Date causelist_date = null;
                    if (editTxt.getText() != null) {
                        try {
                            causelist_date = sdf.parse(editTxt.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    String[] courtarr = court_name.split("=")[0].split("\\^");
                    String courtname_req = "D";
                    Log.i(TAG, "CourtArray Length:" + courtarr.length);
                    Log.i(TAG, "ARR1:" + courtarr[0]);
                    if (courtarr.length > 1) {
                        courtname_req = courtarr[1];
                    }
                    String court_code = courtarr[0];
                    Log.i(TAG, "court_name:" + court_name);
//                    if (court_code.contains(",")) {
//                        court_code = court_code.split(",")[0];
//                    }
                    String causelist_type = cl_type.equals("CIVIL") ? "civ_t" : "cri_t";
                    Log.i(TAG, "state_code:" + statecode);
                    Log.i(TAG, "district_code:" + districtcode);
                    Log.i(TAG, "court_code:" + court_code);
                    Log.i(TAG, "cl_type:" + causelist_type);
                    Log.i(TAG, "causelist_date:" + editTxt.getText().toString());
                    Log.i(TAG, "Court Code Selected:" + courtname_req);
                    ErrorMessage msg = validateRequest(statecode, districtcode, court_code, courtname_req, cl_type, causelist_date);
                    if (msg.isStatus()) {
                        //CAll PDF GENERATION.
                        try {
                            String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "cases_new.php?" + "court_code="
                                    + AESCryptCustom.encrypt(court_code) + "&state_code="
                                    + AESCryptCustom.encrypt(statecode) + "&dist_code="
                                    + AESCryptCustom.encrypt(districtcode) + "&flag="
                                    + AESCryptCustom.encrypt(causelist_type) + "&selprevdays="
                                    + AESCryptCustom.encrypt("0") + "&court_no="
                                    + AESCryptCustom.encrypt(courtname_req) + "&causelist_date="
                                    + AESCryptCustom.encrypt(editTxt.getText().toString());
                            Log.i(TAG, "PDF url:" + url);
                            LoginParams loginparams = new LoginParams();
                            loginparams.setUrl(url);
                            loginparams.setExtraparam1(editTxt.getText().toString());
                            AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.PDF);
                            String sleepTime = "1";
                            runner.execute(sleepTime);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        AlertDialog.Builder loginalert = new AlertDialog.Builder(Causelist.this);
                        loginalert.setMessage(msg.getMessage());
                        loginalert.setTitle("Error Message");
                        loginalert.setPositiveButton("OK", null);
                        loginalert.setCancelable(true);
                        loginalert.create().show();
                        loginalert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Causelist.this, NavActivity.class);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        }
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    }

                } else {
                    // display error
                    AlertDialog.Builder loginalert = new AlertDialog.Builder(Causelist.this);
                    loginalert.setMessage("Check your internet");
                    loginalert.setTitle("Error Message");
                    loginalert.setPositiveButton("OK", null);
                    loginalert.setCancelable(true);
                    loginalert.create().show();
                    loginalert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Causelist.this, NavActivity.class);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    }
                                    startActivity(intent);
                                    finish();
                                }
                            });
                }
            }
        });
    }

    public ErrorMessage validateRequest(String statecode, String districtcode, String courtcode, String courtname, String causelist_type, Date causelist_date) {
        ErrorMessage message = new ErrorMessage();
        String feildname = "";
        if (statecode.equals("0")) {
            feildname = feildname + "State is mandatory";
        }
        if (districtcode.equals("0")) {
            feildname = feildname + "\nDistrict is mandatory";
        }
        if (courtcode.equals("0")) {
            feildname = feildname + "\nCourt is mandatory";
        }

        if (courtname.equals("0")) {
            feildname = feildname + "\nCourt Name is mandatory";
        } else {
            if (courtname.contains("D")) {
                feildname = feildname + "\nInvalid Court Name Selection.";
            }
        }

        if (causelist_type.equals("Select Type")) {
            feildname = feildname + "\nCause list Type should be CIVIL/CRIMINAL";
        }
        if (causelist_date == null) {
            feildname = feildname + "\nCause list Date is mandatory";
        } else {
            long pastweek = 7l * 24 * 60 * 60 * 1000;
            long next_month = 30l * 24 * 60 * 60 * 1000;
            Date todays_date = new Date();
            boolean olderThanweek = causelist_date.after(new Date((todays_date.getTime() - pastweek)));
            boolean aftermonth = causelist_date.before(new Date((todays_date.getTime() + next_month)));
            if (olderThanweek == false) {
                feildname = feildname + "\nCauselist date should not be older than a week";
            }

            if (aftermonth == false) {
                feildname = feildname + "\nCauselist date should not be after one month";
            }
        }


        if (StringUtils.isEmpty(feildname)) {
            message.setStatus(true);
            message.setMessage("All validations are successful");
        } else {
            message.setStatus(false);
            message.setMessage(feildname);
        }


        return message;
    }

    public String constructHTML(String html) {
        if (html.contains("table")) {
            html = html + "</table>";
        }
        String style = "<link href=\"https://services.ecourts.gov.in/ecourtindia_v6/css/bootstrap.min.css\" rel=\"stylesheet\">";
        String output = "<html><head>" + style + "</head><body>" + html.replace("\"", "") + "</body></html>";
        return output;
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        ProgressDialog progressDialog;
        private LoginParams parms;
        private LoginParams.TYPE type;
        private String[] resulttmp = new String[10];


        public AsyncTaskRunner(LoginParams parms, LoginParams.TYPE type) {
            this.parms = parms;
            this.type = type;
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            String response = null;
            try {
                String USER_AGENT = "Mozilla/21.0";
                //Log.i(TAG," In background "+parms.getUrl());
                Log.i(TAG, " LoginParams.TYPE " + type.toString());
                if (parms.getUrl().contains("app.ecourts.gov.in")) {
                    if (type == LoginParams.TYPE.ADDCASE) {
                        Log.i(TAG, " Add case " + courtcodefin);
                        if (courtcodefin.contains(",")) {
                            int i = 0;
                            for (String courtcode : courtcodefin.split(",")) {
                                Log.i("ADDCAS", "courtcode:" + courtcode);
                                String uri = parms.getUrl() + AESCryptCustom.encrypt(courtcode.trim());
                                Log.i("ADDCAS", "uri:" + uri);
                                RestTemplate restTemplate = new RestTemplate();
                                resulttmp[i] = restTemplate.getForObject(uri, String.class);
                                response = resulttmp[i];
                                i = i + 1;
                            }

                        } else {
                            String uri = parms.getUrl() + AESCryptCustom.encrypt(courtcode);
                            Log.i("ASYNC", "uri:" + uri);
                            RestTemplate restTemplate = new RestTemplate();
                            resulttmp[0] = restTemplate.getForObject(uri, String.class);
                            response = resulttmp[0];
                        }


                    } else {
                        String uri = parms.getUrl();
                        RestTemplate restTemplate = new RestTemplate();
                        response = restTemplate.getForObject(uri, String.class);

                    }

                } else {
                    RestTemplate restTemplate = new RestTemplate();
                    response = restTemplate.getForObject(parms.getUrl(), String.class);
                }

                //Need to handle add case seperately

            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            /*If result is null then show a message like site is not available*/
            if (result != null) {
                /*Based on flag this should execute*/
                if (type == LoginParams.TYPE.DISTRITCT) {
                    loaddistricsbystateid(result);
                } else if (type == LoginParams.TYPE.COURT) {
                    //call courts
                    loadcourtsbydisid(result);
                } else if (type == LoginParams.TYPE.COURTNAME) {
                    //LOAD COURT NAMES.
                    //loadcourtsbydisid(result);
                    loadcourtnamesbycourt(result);
                } else if (type == LoginParams.TYPE.PDF) {
                    //HANDLING PDF GENERATION HERE.
                    try {
                        if (!result.contains("error")) {
                            JSONObject cases = new JSONObject(result);
                            String caselist = cases.getString("cases");
                            String html = AESCryptCustom.decrypt(caselist).replaceAll("\\\\r\\\\n\\\\t\\\\t", "")
                                    .replaceAll("\\\\t", "").replaceAll("\\\\", "").replaceAll("\\\\", "");
                            String output = constructHTML(html);
                            Log.i(TAG, "Output:" + output);
                            Intent intent = new Intent(Causelist.this, Causelistout.class);
                            intent.putExtra("html", (Serializable) output);
                            intent.putExtra("cldate", (Serializable) parms.getExtraparam1());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            }
                            startActivity(intent);
                            finish();

//                            String DEST = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/causelist_" + parms.getExtraparam1() + "bbb.pdf";
//                            File file = new File(DEST);
//                            FileOutputStream fop = new FileOutputStream(file);
//                            //Convert the html to pdf using open pdf library to ensure there won't be any licence issues.
//                            //HtmlConverter.convertToPdf(html, fop);
//                            Document doc = new Document();
////                            doc.addAuthor("casesync");
////                            doc.addCreationDate();
////                            doc.addProducer();
////                            doc.addCreator("vikasramireddy");
////                            doc.addTitle("");
////                            doc.setPageSize(PageSize.A4);
//                            PdfWriter pdf = PdfWriter.getInstance(doc, fop);
//                            //pdf.setLinearPageMode();
//                            //pdf.setFullCompression();
//                            doc.open();
//                            HTMLWorker htmlWorker = new HTMLWorker(doc);
//                            htmlWorker.parse(new StringReader(output));
//                            doc.close();
//                            pdf.close();

                        } else {
                            Log.i(TAG, "Error occured:" + result);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            } else {
                progressDialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Site is temporarily unavailable", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(Causelist.this,
                    "",
                    "Loading ....");
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

}