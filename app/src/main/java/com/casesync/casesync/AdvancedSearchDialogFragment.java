package com.casesync.casesync;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.casesync.utill.AutoCompleteAdapter;
import com.casesync.utill.CaseObj;

import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;

/**
 * Created by Vikas on 3/27/2017.
 */

public class AdvancedSearchDialogFragment extends DialogFragment {
    private AutoCompleteTextView mEditText;
    private ImageButton searchbtn;
    private Spinner sp;

    public AdvancedSearchDialogFragment() {
    }

    public static AdvancedSearchDialogFragment newInstance(String title) {
        AdvancedSearchDialogFragment frag = new AdvancedSearchDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final LinkedHashMap mapData = new LinkedHashMap<String, String>();

        View v = inflater.inflate(R.layout.fragment_advanced_search, container, false);
        sp = v.findViewById(R.id.spinner_type);
        mEditText = v.findViewById(R.id.autoCompleteTextView);
        searchbtn = v.findViewById(R.id.search_button);
        mEditText.setEnabled(false);
        searchbtn.setEnabled(false);

        mEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                CaseObj casedetails = (CaseObj) adapterView.getItemAtPosition(position);
                mEditText.setText(casedetails.getCino());
                mEditText.setEnabled(false);
            }
        });
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                mEditText.setEnabled(true);
                searchbtn.setEnabled(true);
                mEditText.setEnabled(false);
                mEditText.setText("");
                //set the adapter based on selection
                if (sp.getSelectedItem().toString().equals("Case Number")) {
                    try {
                        mEditText.setEnabled(true);
                        mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "reg_no"));
                        mEditText.setThreshold(1);
                    } catch (Exception e) {
                        Log.e("SEARCH", e.getMessage());
                    }
                } else if (sp.getSelectedItem().toString().equals("Petitioner")) {
                    try {
                        mEditText.setEnabled(true);
                        mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "pet_name"));
                        mEditText.setThreshold(1);
                    } catch (Exception e) {
                        Log.e("SEARCH", e.getMessage());
                    }
                } else if (sp.getSelectedItem().toString().equals("Repondent")) {
                    try {
                        mEditText.setEnabled(true);
                        mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "res_name"));
                        mEditText.setThreshold(1);
                    } catch (Exception e) {
                        Log.e("SEARCH", e.getMessage());
                    }

                } else if (sp.getSelectedItem().toString().equals("Case type")) {
                    try {
                        mEditText.setEnabled(true);
                        mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "type_name"));
                        mEditText.setThreshold(1);
                    } catch (Exception e) {
                        Log.e("SEARCH", e.getMessage());
                    }

                } else if (sp.getSelectedItem().toString().equals("Court")) {
                    try {
                        mEditText.setEnabled(true);
                        mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "desgname"));
                        mEditText.setThreshold(1);
                    } catch (Exception e) {
                        Log.e("SEARCH", e.getMessage());
                    }

                } else if (sp.getSelectedItem().toString().equals("FIR Details")) {
                    mEditText.setEnabled(true);
                    mEditText.setAdapter(new AutoCompleteAdapter(getContext(), "fir_details"));
                    mEditText.setThreshold(1);
                }

                Toast.makeText(getContext(), "Search by " + sp.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!StringUtils.isEmpty(mEditText.getText().toString())) {
                    if (mEditText.getText().toString().length() == 16) {   //TO avoid the empty screen
                        Intent intent = new Intent(getContext(), CasedetailsTab.class);
                        intent.putExtra("casenumber", mEditText.getText().toString());
                        intent.putExtra("caller", "AdvancedSearchDialogFragment");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        getActivity().startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), "Please select a case displayed in dropdown ",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        mEditText = view.findViewById(R.id.autoCompleteTextView);
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        mEditText.setEnabled(false);
        searchbtn.setEnabled(false);
        //mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


    }
}
