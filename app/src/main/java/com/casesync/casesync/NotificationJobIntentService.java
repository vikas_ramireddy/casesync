package com.casesync.casesync;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.JobIntentService;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NotificationJobIntentService extends JobIntentService {

    public static final String TAG = "Scheduling Demo";
    /**
     * Unique job ID for this service.
     */
    static final int JOB_ID = 464;
    final Handler mHandler = new Handler();
    CasesyncUtill utility = new CasesyncUtill();

    /**
     * Convenience method for enqueuing work in to this service.
     */
    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, NotificationJobIntentService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(Intent intent) {
        // We have received work to do.  The system or framework is already
        // holding a wake lock for us at this point, so we can just go.
        Log.i("SimpleJobIntentService", "Executing work: " + intent);
        String label = intent.getStringExtra("label");
        if (label == null) {
            label = intent.toString();
        }
        toast("Executing: " + label);
        backendjob(getApplicationContext());
        Log.i("SimpleJobIntentService", "Completed service @ " + SystemClock.elapsedRealtime());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        toast("All work complete");
    }

    // Helper for showing tests
    void toast(final CharSequence text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(NotificationJobIntentService.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Helper for creating notifications
    void backendjob(final Context context) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                createNotifications(context);
            }
        });
    }

    public void createNotifications(Context context) {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                CaseSyncOpenDatabaseHelper.class);
        Dao<CaseObj, Long> casedetDao = null;
        try {
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            boolean today = true;
            if (currentHour >= 18 && currentHour <= 24) {
                today = false;
                c.add(Calendar.DATE, 1);
            }
            dt = c.getTime();
            List<CaseObj> casedet = casedetDao.queryForEq("date_next_list", dt);
            if (casedet != null) {
                int size = 0;

                if (casedet.size() > 0) {
                    for (com.casesync.utill.CaseObj casedetails : casedet) {
                        String text = "";
                        if (today == false) {
                            text = "Tomarrow";
                        } else {
                            text = "Today";
                        }
                        utility.sendNotification(text + " there is a Case Hearing ",
                                " Case number:" + casedetails.getTypeName() + "/" + casedetails.getRegNo() + "/" + casedetails.getRegYear() + " Court:" + casedetails.getCourtName(), casedetails.getCaseNo(), context, CasesyncUtill.NOTITYPE.CASE);

                    }
                } else {
                    //Comment it after testing.
                    //utility.sendNotification("No cases available for hearing ", "", "", context, CasesyncUtill.NOTITYPE.GENERAL);
                }
            } else {
                //utility.sendNotification("No cases available for hearing", "", "", context, CasesyncUtill.NOTITYPE.GENERAL);

            }
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
    }
}
