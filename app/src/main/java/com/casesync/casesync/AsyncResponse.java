package com.casesync.casesync;

/**
 * Created by Vikas on 6/11/2017.
 */

public interface AsyncResponse {
    void processFinish(String output);
}
