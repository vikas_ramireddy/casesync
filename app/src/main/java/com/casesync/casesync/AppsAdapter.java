package com.casesync.casesync;

/**
 * Created by Juned on 4/14/2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.casesync.utill.Cases;
import com.casesync.utill.ImportObj;

import java.util.List;

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {
    private static String TAG = "APSADA";
    Context context;
    List<Cases> stringList;
    List<ImportObj> importobjList;
    boolean status = false;

    public AppsAdapter(Context context, List<Cases> list) {
        this.context = context;
        stringList = list;
        this.status = false;
    }

    public AppsAdapter(Context context, boolean status, List<ImportObj> list) {
        this.context = context;
        importobjList = list;
        this.status = true;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view2 = LayoutInflater.from(context).inflate(R.layout.cardview_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view2);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setIsRecyclable(false);

        if (status == false) {
            Cases dataModel = stringList.get(position);
            viewHolder.court.setText(dataModel.getEstablishment_name() != null ? dataModel.getEstablishment_name().trim() : "");
            viewHolder.case_number.setText(dataModel.getType_name() + "/" + dataModel.getCase_no2() + "/" + dataModel.getReg_year());
            viewHolder.petitioner.setText(dataModel.getPet_name() + " VS");
            viewHolder.respondent.setText(dataModel.getRes_name());
            if (dataModel.getFir_no() != null) {
                viewHolder.case_number.setText(dataModel.getType_name() + "/" + dataModel.getCase_no2() + "/" + dataModel.getReg_year() + "/FIR NO:" + dataModel.getFir_no());

            } else {
//                if (dataModel.getAdv_name1() != null) {
//                    if (dataModel.getAdv_name2() != null) {
//                        viewHolder.case_number.setText(dataModel.getType_name() + "/" + dataModel.getCase_no2() + "/" + dataModel.getReg_year() +"/"+ dataModel.getAdv_name1() + "/" + dataModel.getAdv_name2());
//
//                    } else {
//                        viewHolder.case_number.setText(dataModel.getType_name() + "/" + dataModel.getCase_no2() + "/" + dataModel.getReg_year() +"/"+ dataModel.getAdv_name1());
//                    }
//                }


            }

            viewHolder.checkBox.setChecked(dataModel.isSelected());
            viewHolder.checkBox.setTag(Integer.valueOf(position));
        } else {
            ImportObj dataModel = importobjList.get(position);
            viewHolder.court.setText(dataModel.getEstablishmentName() != null ? dataModel.getEstablishmentName() : "");
            viewHolder.case_number.setText(dataModel.getTypeName() + "/" + dataModel.getRegNo() + "/" + dataModel.getRegYear());
            viewHolder.petitioner.setText(dataModel.getPetpartyName() + " VS ");
            viewHolder.respondent.setText(dataModel.getRespartyName());
            viewHolder.checkBox.setChecked(true);
            viewHolder.checkBox.setTag(Integer.valueOf(position));
            viewHolder.checkBox.setEnabled(false);
            PackageActivity.currentimport.add(dataModel);
        }


        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CheckBox cb = (CheckBox) buttonView;
                int position = (Integer) buttonView.getTag();
                if (status) {
                    ImportObj object = importobjList.get(position);
                    if (isChecked) {
                        Log.d(TAG, "added " + object.getCino());

                    } else {
                        Log.d(TAG, "removed " + object.getCino());
                    }
                } else {
                    Cases object = stringList.get(position);
                    if (isChecked) {
                        Log.d(TAG, "added " + object.getCino());
                        stringList.get(position).setSelected(true);
                        //PackageActivity.currentcases.add(object);
                    } else {
                        Log.d(TAG, "removed " + object.getCino());
                        stringList.get(position).setSelected(false);
                        //PackageActivity.currentcases.remove(object);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (status) {
            return importobjList.size();
        } else {
            return stringList.size();
        }


    }

    public void setSelectedPosition(int position) {
        if (status) {

            for (int i = 0; i < importobjList.size(); i++) {
                importobjList.get(i).setSelected(i == position);
            }

        } else {
            stringList.get(position).setSelected(true);
        }
        notifyDataSetChanged();
    }

    public void clearSelection() {
        if (status) {
            for (int i = 0; i < importobjList.size(); i++) {
                importobjList.get(i).setSelected(false);
            }
        } else {
            for (int i = 0; i < stringList.size(); i++) {
                stringList.get(i).setSelected(false);
            }
        }

        notifyDataSetChanged();
    }

/*    interface OnItemCheckListener {
        void onItemCheck(Object item);

        void onItemUncheck(Object item);
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView court;
        public TextView case_number;
        public TextView petitioner;
        public TextView respondent;
        public CheckBox checkBox;
        View itemView;

        public ViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            court = view.findViewById(R.id.court);
            case_number = view.findViewById(R.id.case_number);
            petitioner = view.findViewById(R.id.petitioner);
            respondent = view.findViewById(R.id.respondent);
            checkBox = view.findViewById(R.id.checkBox);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}