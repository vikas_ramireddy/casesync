package com.casesync.casesync;

/**
 * Created by Vikas on 4/12/2017.
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;

/**
 * This {@code IntentService} does the app's actual work.
 * {@code SampleAlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class CaseSyncSchedulingService extends IntentService {
    public CaseSyncSchedulingService() {
        super("SchedulingService");
    }

    public static final String TAG = "BackendSync";
    private static final String PREFS_NAME = "LoginPrefs";
    private NotificationManager mNotificationManager;
    public static boolean status = false;

    @Override
    protected void onHandleIntent(Intent intent) {


    }


}
