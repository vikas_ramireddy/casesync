package com.casesync.casesync;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.casesync.casesync.Fragment.HistoryViewFragment;
import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class History extends Fragment {

    public static final String TAG = "HIST";

    public History() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CasedetailsTab activity = (CasedetailsTab) getActivity();
        String casenumber = activity.getcasenumber();
        final View v = inflater.inflate(R.layout.fragment_history, container, false);
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            //TODO Get Case Type also while getting above data from blank fragment.Based on that query the table and display the data.
            Dao<CaseObj, Long> casedetDao = null;
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            final List<CaseObj> todos = casedetDao.queryForEq("cino", casenumber);
            TableLayout ll = v.findViewById(R.id.casehistory);
            //TODO History logic will vary from casetype.
            //Added as a fix for null pointer exception For cases where there is no history
            if (!CollectionUtils.isEmpty(todos)) {
                CaseObj caseobj = todos.get(0);
                if (caseobj.getHistoryOfCaseHearing() != null) {
                    //Log.i("TST", "Case Hearing :" + caseobj.getHistoryOfCaseHearing());
                    Document doc = Jsoup.parse(caseobj.getHistoryOfCaseHearing().replace("\\", ""));
                    Elements table = doc.select("table.tbl-result");
                    Elements rows = table.select("tr");
                    Log.i("TST", "Row Size :" + rows.size());
                    for (int i = 0; i < rows.size(); i++) {
                        TableRow tblrow = new TableRow(getContext());

                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                        TableRow.LayoutParams rowspan = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                        TextView text = v.findViewById(R.id.casehistemp);
                        TableRow.LayoutParams params = (TableRow.LayoutParams) text.getLayoutParams();
                        params.span = 1;
                        rowspan.span = 2;
                        tblrow.setPadding(30, 0, 0, 0);
                        tblrow.setBackgroundResource(R.drawable.plus);
                        tblrow.setLayoutParams(lp);
                        TextView qty = new TextView(getContext());
                        TextView tv = new TextView(getContext());

                        tv.setLayoutParams(rowspan);
                        qty.setLayoutParams(params);
                        String businessdate = "";
                        String dates = "";
                        String business = "";
                        Element row = rows.get(i);
                        if (caseobj.getCourtNo().equalsIgnoreCase("" + caseobj.getCourtno())) {
                            String case_data = rows.get(i).getElementsByTag("a").attr("onclick");
                            if (!StringUtils.isEmpty(case_data)) {
                                Log.i("TST", "Function Data:" + case_data);
                                case_data = case_data.replace("viewBusiness", "");
                                case_data = case_data.replace("(", "");
                                case_data = case_data.replaceAll("\\)", "");
                                String[] split = case_data.split(",");
                                if (!StringUtils.isEmpty(split[0])) {
                                    //Need to test the following feature.Check if this line is commented history will work or not
                                    caseobj.setCourtno(Long.parseLong(split[0].replace("'", "")));
                                    Addexitingcase.insertdata(caseobj, getContext(), false);
                                }
                            }
                        }

                        Elements cols = row.select("td");
                        for (int count = 0; count < cols.size(); count++) {
                            //Log.i("TST", count + ":" + cols.get(count).text());
                            if (count == 1) {
                                businessdate = cols.get(count).text();

                            } else if (count == 2) {
                                dates = dates + "" + cols.get(count).text();
                            } else if (count == 3) {
                                business = cols.get(count).text();
                            }
                        }
                        if (!businessdate.isEmpty() && !business.isEmpty()) {
                            final String businesstemp = businessdate;
                            final String next_date1 = dates;
                            final CaseObj caseObjtemp = caseobj;

                            qty.setText(businessdate);
                            qty.setTextSize(15);
                            qty.setTextColor(Color.BLACK
                            );
                            qty.setPadding(15, 0, 10, 0);
                            tv.setText(business + "\n");
                            tv.setTextSize(15);
                            tv.setTextColor(Color.BLACK
                            );
                            tv.setPadding(15, 0, 0, 0);
                            tblrow.addView(qty);
                            tblrow.addView(tv);

                            tblrow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    ConnectivityManager connMgr = (ConnectivityManager)
                                            getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                                    if (networkInfo != null && networkInfo.isConnected()) {
                                        String url = null;
                                        try {
                                            url = PropertyUtil.getProperty("apiurl", getView().getContext()) + "s_show_business.php";
                                            LoginParams loginparams = new LoginParams();
                                            loginparams.setUrl(url);
                                            AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, caseObjtemp, businesstemp, next_date1);
                                            String sleepTime = "1";
                                            runner.execute(sleepTime);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        AlertDialog.Builder loginalert = new AlertDialog.Builder(v.getContext());
                                        loginalert.setMessage("Internet is required");
                                        loginalert.setTitle("Please turn on the internet");
                                        loginalert.setPositiveButton("OK", null);
                                        loginalert.setCancelable(true);
                                        loginalert.create().show();
                                        loginalert.setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });
                                    }
                                }
                            });


                        }
                        ll.addView(tblrow, i);
                    }
                }
            }




/*            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<Casehistory, Long> casehisDao = null;
            casehisDao = todoOpenDatabaseHelper.getCasehistorydao();
            List<Casehistory> history = casehisDao.queryForEq("casenumber", casenumber);*/

/*            if (history != null) {
                if (history.size() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    for (int count = 0; count < history.size(); count++) {
                        TableRow row = new TableRow(getContext());
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                        TableRow.LayoutParams rowspan = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT);
                        TextView text = (TextView) v.findViewById(R.id.casehistemp);
                        TableRow.LayoutParams params = (TableRow.LayoutParams) text.getLayoutParams();
                        params.span = 1;
                        rowspan.span = 2;

                        row.setLayoutParams(lp);
                        TextView qty = new TextView(getContext());
                        TextView tv = new TextView(getContext());
                        tv.setLayoutParams(rowspan);
                        qty.setLayoutParams(params);

                        qty.setText(sdf.format(history.get(count).getBusinessondate()) + "/\n" + history.get(count).getPurposeofhearing());
                        qty.setTextSize(15);
                        qty.setTextColor(Color.BLACK
                        );
                        qty.setPadding(15, 0, 10, 0);
                        tv.setText(history.get(count).getBusiness() + "\n");
                        tv.setTextSize(15);
                        tv.setTextColor(Color.BLACK
                        );
                        tv.setPadding(15, 0, 0, 0);
                        row.addView(qty);
                        row.addView(tv);
                        ll.addView(row, count);
                    }
                    ;
                }
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException ae) {
            ae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return v;
    }

    class AsyncTaskRunner extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog;
        private LoginParams parms;
        private CaseObj caseobj;
        private String businessdate;
        private String nxt_date;


        public AsyncTaskRunner(LoginParams parms, CaseObj casesobj, String business, String nextdate) {
            this.parms = parms;
            this.caseobj = casesobj;
            this.businessdate = business;
            this.nxt_date = nextdate;
        }


        @Override
        protected String doInBackground(String... params) {
            String responsetmp = "";
            try {
                RestTemplate restTemplate = new RestTemplate();
                String url = "";
                publishProgress(0);
                DateFormat sc = new SimpleDateFormat("yyyyMMdd");
                DateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
                Date nxtdate = null;
                if (nxt_date != null) {
                    if (!nxt_date.equals("")) {
//                        Log.i(TAG, sc.format(nxtdate) + ":" + businessdate);
                        nxtdate = dt.parse(nxt_date);
                    }
                }
                String data_params = "court_code=" + AESCryptCustom.encrypt("" + caseobj.getCourtno());
                data_params += "&dist_code=" + AESCryptCustom.encrypt(caseobj.getDistrictCode());
                //Bugfix for disposed cases there wont be any next date
                if (nxtdate != null) {
                    data_params += "&nextdate1=" + AESCryptCustom.encrypt(sc.format(nxtdate));
                } else {
                    data_params += "&nextdate1=";
                }

                data_params += "&case_number1=" + AESCryptCustom.encrypt(caseobj.getCaseNo());
                data_params += "&state_code=" + AESCryptCustom.encrypt(caseobj.getStateCode());
                data_params += "&disposal_flag=" + AESCryptCustom.encrypt((caseobj.getArchive().equals("N") ? "Pending" : "Disposed"));
                data_params += "&businessDate=" + AESCryptCustom.encrypt(businessdate);
                data_params += "&court_no=" + AESCryptCustom.encrypt("" + caseobj.getCourtNo());
                data_params += "&language_flag=" + AESCryptCustom.encrypt("english");
                data_params += "&bilingual_flag=" + AESCryptCustom.encrypt("0");

                url = parms.getUrl() + "?" + data_params;
                Log.i("TST", url);

                try {
                    responsetmp = restTemplate.getForObject(url, String.class);
                    if (!responsetmp.isEmpty() && responsetmp != null) {
                        if (responsetmp.contains("status")) {
                            return "";
                        }
                        Log.d(TAG, "" + AESCryptCustom.decrypt(responsetmp));
                    }
                } catch (HttpServerErrorException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
            return responsetmp;
        }


        @Override
        protected void onPostExecute(String result) {

            progressDialog.dismiss();

            final String mimeType = "text/html";
            final String encoding = "UTF-8";

            JSONObject history = null;
            try {
                Log.i(TAG, "Result:" + result);
                if (StringUtils.isEmpty(result)) {
                    Toast.makeText(getContext(),
                            "Case history is not available.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    history = new JSONObject(AESCryptCustom.decrypt(result));
                    String business = null;
                    if (history != null) {
                        business = history.getString("viewBusiness");
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        HistoryViewFragment historyFragment = HistoryViewFragment.newInstance(business, "text/html");
                        Log.i(TAG, business);
                        historyFragment.show(fm, "fragment_history_view");
                    } else {
                        Toast.makeText(getContext(),
                                "Unable to load the history",
                                Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            }


        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getView().getContext(),
                    "",
                    "Fetching business details");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }
}
