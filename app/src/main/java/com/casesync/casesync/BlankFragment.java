package com.casesync.casesync;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final String TAG = "BLF";
    ArrayList<String> dataModels = new ArrayList<String>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ListView listView;
    ArrayList<CaseObj> caseDetails = new ArrayList<CaseObj>();

    private OnFragmentInteractionListener mListener;

    public BlankFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_caseview, container, false);
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            //TODO Load data from other tables also Like High Courts & Consumer Courts.
            Dao<com.casesync.utill.CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Date nxtdate = HttpURLConnectionExample.getdatefromsdf(mParam1, "dd-MM-yyyy");
            List<CaseObj> casedet = casedetDao.queryForEq("date_next_list", nxtdate);
            if (casedet != null && casedet.size() > 0) {
                Log.e("VIEWFRG", "" + casedet.size());
                for (CaseObj cases : casedet) {
                    dataModels.add("" + cases.getTypeName() + "/" + cases.getRegNo() + "/" + cases.getRegYear() + "/" + cases.getCourtName() + ((cases.getContactname()) != null ? "/" + cases.getContactname() : ""));
                    //+"/"+cases.getPetName()+" VS "+cases.getResName())
                    //+"/"+cases.getContactname()
                    caseDetails.add(cases);
                }
            }
            ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, dataModels);
            listView = view.findViewById(R.id.fileslist);
            listView.setAdapter(adapter);
            registerForContextMenu(listView);
            //listView.setBackgroundResource(R.drawable.plus);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CaseObj dataModel = caseDetails.get(position);
                    Intent intent = new Intent(getContext(), CasedetailsTab.class);
                    intent.putExtra("caller", "BlankFragment");
                    intent.putExtra("casenumber", dataModel.getCino());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    getActivity().startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage());
        }
        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select The Option");
        menu.add(0, v.getId(), 0, "View Case");//groupId, itemId, order, title
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
