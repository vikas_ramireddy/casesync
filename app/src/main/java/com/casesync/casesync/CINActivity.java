package com.casesync.casesync;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.casesync.utill.CaseObj;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.google.android.material.snackbar.Snackbar;

import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class CINActivity extends AppCompatActivity {
    private static final String TAG = "CINACT";
    public LoginParams loginparams = new LoginParams();
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cin);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Add Case Using CRN Number");
        /**
         * Action which will be used to add case using cin number.In future include the QR Code scan.
         * */

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnSubmit = findViewById(R.id.add_case);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(final View v) {
                                             EditText crn = findViewById(R.id.cinnumber);
                                             if (crn.getText() != null) {
                                                 Log.i(TAG, "" + crn.getText() + ":" + crn.getText().length());
                                                 if (crn.getText().length() == 16) {
                                                     if (CasesyncUtill.caseexist("" + crn.getText(), getApplicationContext())) {
                                                         //Call async task
                                                         AsyncTaskRunner runner = new AsyncTaskRunner("" + crn.getText());
                                                         String sleepTime = "1";
                                                         runner.execute(sleepTime);
                                                     } else {
                                                         //Case already exists
                                                         AlertDialog.Builder loginalert = new AlertDialog.Builder(CINActivity.this);
                                                         loginalert.setMessage("");
                                                         loginalert.setTitle("Case Already exists");
                                                         loginalert.setPositiveButton("View",
                                                                 new DialogInterface.OnClickListener() {
                                                                     public void onClick(DialogInterface dialog, int which) {
                                                                         Intent i = new Intent(getApplicationContext(), CasedetailsTab.class);
                                                                         Log.i(TAG, crn.getText().toString());
                                                                         i.putExtra("casenumber", crn.getText().toString());
                                                                         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                                                             i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                         }
                                                                         getApplicationContext().startActivity(i);

                                                                     }
                                                                 });

                                                         loginalert.setNegativeButton("Cancel",
                                                                 new DialogInterface.OnClickListener() {
                                                                     public void onClick(DialogInterface dialog, int which) {
                                                                         Log.i(TAG, "Opening Case detiail tab");
                                                                     }
                                                                 });
                                                         loginalert.setCancelable(true);
                                                         loginalert.create().show();
                                                     }
                                                 } else {
                                                     //Invalid length
                                                     AlertDialog.Builder loginalert = new AlertDialog.Builder(CINActivity.this);
                                                     loginalert.setMessage("");
                                                     loginalert.setTitle("CRN Number length should be equal to 16");
                                                     loginalert.setPositiveButton("OK", null);
                                                     loginalert.setCancelable(true);
                                                     loginalert.create().show();
                                                     loginalert.setPositiveButton("Ok",
                                                             new DialogInterface.OnClickListener() {
                                                                 public void onClick(DialogInterface dialog, int which) {
                                                                     Log.i(TAG, "ON CLICK ON ALERT DIALOG");
                                                                 }
                                                             });
                                                 }
                                             }
                                         }
                                     }
        );
    }


    private void launchCasedetail(String casenumber) {
        Intent i = new Intent(CINActivity.this, CasedetailsTab.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        i.putExtra("casenumber", casenumber);

        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            Intent intent = new Intent(CINActivity.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case 16908332:
                Intent intent = new Intent(CINActivity.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class AsyncTaskRunner extends AsyncTask<String, Float, CaseObj> {
        ProgressDialog progressDialog;
        private String cinno;

        public AsyncTaskRunner(String cinno) {
            this.cinno = cinno;
        }

        @Override
        protected CaseObj doInBackground(String... params) {
            CaseObj caseobj = new CaseObj();
            try {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                RestTemplate restTemplate = new RestTemplate();
                String url = "";
                if (!StringUtils.isEmpty(cinno)) {
                    Log.d("ASYNC", "Importing cases cino " + cinno);
                    String baseurl = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                    CasesyncUtill util = new CasesyncUtill();
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        if (CasesyncUtill.caseexist(cinno, getApplicationContext())) {
                            caseobj = util.getCaseObjFromCino(getApplicationContext(), cinno, true);
                            if (caseobj != null) {
                                util.createCalendarNotification(getApplicationContext(), Arrays.asList(caseobj));
                                Log.i(TAG, caseobj.getCino() + "Imported successfully");
                            }
                        } else {
//                            Toast.makeText(getApplicationContext(), "Case already exist",
//                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //Add a Toast
//                        Toast.makeText(getApplicationContext(), "Internet is not available",
//                                Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
            return caseobj;
        }

        @Override
        protected void onPostExecute(CaseObj result) {

            if (result != null) {
                if (result.getCino() != null) {
                    Intent intent = new Intent(CINActivity.this, CasedetailsTab.class);
                    Intent i = new Intent(getApplicationContext(), CasedetailsTab.class);
                    i.putExtra("casenumber", result.getCino());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    getApplicationContext().startActivity(i);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Unable to find the record with current CIN Number",
                            Toast.LENGTH_SHORT).show();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Unable to find the record with current CIN Number", Snackbar.LENGTH_SHORT)
                            .setAction("No action", null).show();
                }
            } else {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Unable to find the record",
                        Toast.LENGTH_SHORT).show();
                Snackbar.make(getWindow().getDecorView().getRootView(), "Unable to find the record", Snackbar.LENGTH_SHORT)
                        .setAction("No action", null).show();
            }

        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(CINActivity.this,
                    "",
                    "Fetching Details ....");
        }
    }

}
