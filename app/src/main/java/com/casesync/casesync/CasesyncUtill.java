package com.casesync.casesync;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;

import com.casesync.casesync.Fragment.ImageGenerator;
import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Casedetails;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.FileUtility;
import com.casesync.utill.GenericUtil;
import com.casesync.utill.OrderDetails;
import com.casesync.utill.PropertyUtil;
import com.casesync.utill.Useraccount;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class CasesyncUtill {
    public static final String NOTIFICATION_CHANNEL_ID = "com.casesync.alerts";
    //Added for calendar sync
    public static final String[] EVENT_PROJECTION = new String[]{
            CalendarContract.Calendars._ID,                           // 0
            CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
            CalendarContract.Calendars.OWNER_ACCOUNT                  // 3
    };
    private static final String AUTHORITY = "com.casesync.fileprovider";
    private static final String TAG = CasesyncUtill.class.getSimpleName();
    //Added for calendar sync
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;
    public static ArrayList<com.casesync.utill.CaseObj> sortedModels = new ArrayList<com.casesync.utill.CaseObj>();
    public static ArrayList<String> temp = new ArrayList<String>();
    private static CaseSyncOpenDatabaseHelper dbHelper = null;
    private static Dao<CaseObj, Long> casedetailDao = null;
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    RestTemplate rt = null;

    public static void main(String[] args) {
        CasesyncUtill.getdatefromsdf("23-July-2005", "dd-MMMM-yyyy");
        String currentDate = "31st October 2016";
        Date parsedDate = null;
        String[] formats = {"d'st' MMMM yyyy", "d'nd' MMMM yyyy", "d'rd' MMMM yyyy", "d'th' MMMM yyyy"};
        ParsePosition position = new ParsePosition(0);
        for (String format : formats) {
            position.setIndex(0);
            position.setErrorIndex(-1);
            // no ParseException but a null return instead
            parsedDate = new SimpleDateFormat(format).parse(currentDate, position);
            if (parsedDate != null) {
                SimpleDateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                System.out.println("parsedDate: " + date_formatter.format(parsedDate));
            }
        }
    }

    public static Bitmap createimagegenerator(ImageGenerator mImageGenerator, Calendar calendar, boolean status) {
        try {
            // Set the icon size to the generated in dip.
            mImageGenerator.setIconSize(512, 512);
            // Set the size of the date and month font in dip.
            mImageGenerator.setDateSize(200);
            mImageGenerator.setMonthSize(85);
            // Set the position of the date and month in dip.
            mImageGenerator.setDatePosition(380);
            mImageGenerator.setMonthPosition(140);
            // Set the color of the font to be generated
            //mImageGenerator.setDateColor(Color.parseColor("#3c6eaf"));
            mImageGenerator.setDateColor(Color.BLACK);
            mImageGenerator.setMonthColor(Color.WHITE);

            if (status == true) {
                Bitmap bitmap = mImageGenerator.generateDateImage(calendar, R.drawable.empty_calendar);
                //Log.i("UTIL", "Is mutable:" + bitmap.isMutable());
                return bitmap;
            } else {
                Bitmap bitmap = mImageGenerator.generateDateImage(calendar, R.drawable.empty_calendar_dis);
                return bitmap;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static Date getdatefromsdf(String startDateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date startDate = null;
        try {
            startDate = df.parse(startDateString);
            String newDateString = df.format(startDate);
            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static Date getdatefromsdfext(String currentDate) {

        Date parsedDate = null;
        Date startDate = null;
        String[] formats = {"d'st' MMMM yyyy", "d'nd' MMMM yyyy", "d'rd' MMMM yyyy", "d'th' MMMM yyyy"};
        ParsePosition position = new ParsePosition(0);
        for (String format : formats) {
            position.setIndex(0);
            position.setErrorIndex(-1);
            // no ParseException but a null return instead
            parsedDate = new SimpleDateFormat(format).parse(currentDate, position);
            if (parsedDate != null) {
                SimpleDateFormat date_formatter = new SimpleDateFormat("dd-MM-yyyy");
                System.out.println("parsedDate: " + date_formatter.format(parsedDate));
                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    startDate = df.parse(date_formatter.format(parsedDate));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return startDate;
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public static CaseSyncOpenDatabaseHelper getDBHelper(Context context) {
        if (dbHelper == null) {
            dbHelper = OpenHelperManager.getHelper(context,
                    CaseSyncOpenDatabaseHelper.class);
        }
        return dbHelper;
    }

    public static Dao<CaseObj, Long> getCaseObjdao(Context context) throws SQLException {

        if (casedetailDao != null) {
            return casedetailDao;
        } else {
            if (dbHelper != null) {
                casedetailDao = dbHelper.getCaseObjdao();
            } else {
                CaseSyncOpenDatabaseHelper helper = getDBHelper(context);
                casedetailDao = dbHelper.getCaseObjdao();
            }
        }
        return casedetailDao;
    }

    public static List<CaseObj> getcasecountofaday(Context context, Date dt) {
        List<CaseObj> caselist = new ArrayList<CaseObj>();
        try {
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormat.format(dt);
            if (!NavActivity.extraData.containsKey(strDate)) {
                Calendar c = new GregorianCalendar();
                c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                Date cdt = c.getTime();

                if ((c.get(Calendar.MONTH) + 1) >= dt.getMonth()) {
                    Dao<CaseObj, Long> casedetDao = getCaseObjdao(context);
                    QueryBuilder<CaseObj, Long> queryBuilder =
                            casedetDao.queryBuilder();
                    Where<CaseObj, Long> where = queryBuilder.where();
                    where.eq("archive", "N");
                    where.and();
                    where.eq("date_next_list", dt);
                    PreparedQuery<CaseObj> preparedQuery = queryBuilder.prepare();
                    caselist = casedetDao.query(preparedQuery);
                    if (!CollectionUtils.isEmpty(caselist)) {
                        //Log.i(TAG,strDate+":"+caselist.size());
                        NavActivity.extraData.put(strDate, caselist.size());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return caselist;
    }

    public static boolean caseexist(String cinno, Context context) {
        Dao<CaseObj, Long> casedetDao = null;
        try {
            casedetDao = getCaseObjdao(context);
            List<CaseObj> casedet = casedetDao.queryForEq("cino", cinno);
            if (CollectionUtils.isEmpty(casedet)) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean initDefaultSharedPreferences(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(CasesyncUtill.getDefaultSharedPreferencesName(context), MODE_PRIVATE);
        boolean status = prefs.getBoolean("pref_sync_gc", true);
        if (status == true) {
            //Work arround to init shared_preferences on load.
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("pref_sync_gc", true);
            editor.commit();
        }
        return true;
    }

    public static String getDefaultSharedPreferencesName(Context context) {
        return context.getPackageName() + "_preferences";
    }

    public static CaseObj fetchcasebycaseno(String casenumber, Context context) {
        CaseObj casedetails = null;
        Dao<CaseObj, Long> casedetDao = null;
        try {
            casedetDao = getCaseObjdao(context);
            List<CaseObj> casedet = casedetDao.queryForEq("case_no", casenumber);
            if (!CollectionUtils.isEmpty(casedet)) {
                casedetails = casedet.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return casedetails;
    }

    public List<CaseObj> getSearchableResults(List<CaseObj> dataSet, String constraint) {
        if (CollectionUtils.isEmpty(dataSet)) {
            return null;
        }
        if (StringUtils.isEmpty(constraint)) {
            return dataSet;
        }
        List<CaseObj> nPlanetList = new ArrayList<CaseObj>(dataSet.size());
        for (CaseObj p : dataSet) {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

            if (p.getArchive().equalsIgnoreCase("Y")) {
                if (df.format(p.getDateOfDecision()).startsWith(constraint)) {
                    Log.i("Filter", "Coming inside the getDateOfDecision:" + p.getDateOfDecision());
                    nPlanetList.add(p);
                }
            } else {
                if (p.getDateNextList() != null) {
                    if (df.format(p.getDateNextList()).startsWith(constraint)) {
                        Log.i("Filter", "Coming inside the getNext_hearing_date:" + p.getDateNextList());
                        nPlanetList.add(p);
                    }
                }

            }

            if (p.getTypeName() != null) {
                if (p.getTypeName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getRegNo() != null) {
                if (p.getRegNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getFilNo() != null) {
                if (p.getFilNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getFirNo() != null) {
                if (p.getFirNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }

            if (p.getCourtName() != null) {
                if (p.getCourtName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getPetName() != null) {
                if (p.getPetName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getResName() != null) {
                if (p.getResName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getPetAdv() != null) {
                if (p.getPetAdv().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getResAdv() != null) {
                if (p.getResAdv().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }
            if (p.getContactname() != null) {
                if (p.getContactname().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    nPlanetList.add(p);
                }
            }


        }
        return nPlanetList;


    }

    /*Generates a csv file
     * 1)Store it in the local then try to attach as a attachment*/
    public File generatecsv(List<CaseObj> dataSet, Context context) throws IOException {
        //LOGIC Needs to be changed.
        String data = "";
        if (CollectionUtils.isEmpty(dataSet)) {
            return null;
        }

        CsvMapper mapper = new CsvMapper();
        mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false); //Optional

        // initialize the schema
        CsvSchema schema = null;
//.addColumn("writinfo")
        if (dataSet.get(0).getArchive().equals("N")) {
            schema = CsvSchema.builder().addColumn("cino").addColumn("district_name").addColumn("court_name").addColumn("desgname").addColumn("type_name").addColumn("reg_year").addColumn("reg_no")
                    .addColumn("petparty_name").addColumn("resparty_name")
                    .addColumn("date_next_list").addColumn("purpose_name").addColumn("writinfo").build().withHeader();
        } else {
            schema = CsvSchema.builder().addColumn("cino").addColumn("district_name").addColumn("court_name").addColumn("desgname").addColumn("type_name").addColumn("reg_year").addColumn("reg_no")
                    .addColumn("petparty_name").addColumn("resparty_name")
                    .addColumn("date_of_decision").addColumn("purpose_name").addColumn("writinfo").build().withHeader();
        }
        // map the bean with our schema for the writer
        try {
            ObjectWriter writer = mapper.writerFor(CaseObj.class).with(schema);
            String filepath = FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.EXPORT, null);
            File file = new File(filepath + "export.csv");
            Log.i(TAG, file.getAbsolutePath());
            writer.writeValues(file).writeAll(dataSet);
            return file;
        } catch (FileNotFoundException e) {
            Toast.makeText(context, "Please allow storage permissions ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertdata(Casedetails casedetails, Context context) {

        try {
            Log.i("insert data:", "Inserting data");
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                    CaseSyncOpenDatabaseHelper.class);
            Dao<Casedetails, Long> casedetDao = todoOpenDatabaseHelper.getDao();

            Log.i("insert data:", "Casedetdao" + casedetails.getCasenumber());
            Log.i("insert data:", "Casedetdao" + casedetails.getNext_hearing_date());


            List<Casedetails> todos = casedetDao.queryForEq("casenumber", casedetails.getCasenumber());
            if (todos.size() > 0) {
                Log.i("Insertcase", "Case number already exists:");
                //update the existing case
                casedetDao.update(casedetails);
            } else {
                casedetDao.create(casedetails);
            }
            /*Insert the case history also complete it and test it by the end of the day*/

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("SYNC", "INSERT" + e.getMessage());
        }
    }
    /*Method used to find the number of cases pending for update*/
/*
    public int getpendingcasescount(Context context) {
        int pendingcount = 0;
        try {
            List<CaseObj> casedet = new ArrayList<CaseObj>();
            List<CaseObj> temp = new ArrayList<CaseObj>();
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                    CaseSyncOpenDatabaseHelper.class);
            Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Dao<CaseObj, Long> casedetDao = null;
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            if (currentHour >= 0 && currentHour <= 10) {
                c.add(Calendar.DATE, -1);
            }
            dt = c.getTime();
            temp = casedetDao.queryForAll();
            for (int count = 0; count < temp.size(); count++) {
                if (temp.get(count).getArchive().equals("N")) {
                    Date date1 = temp.get(count).getDateNextList();
                    Date date2 = dt;
                    if (date1 != null) {
                        if (date1.compareTo(date2) < 0) {
                            casedet.add(temp.get(count));
                        } else if (date1.compareTo(date2) == 0) {
                            casedet.add(temp.get(count));
                        } else {
                        }
                    } else {
                        casedet.add(temp.get(count));
                        Log.e("SYNC", "Case hearing is null " + temp.get(count).getCaseNo());
                    }
                }
            }
            pendingcount = casedet.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pendingcount;
    }
*/


/*    public List<CaseObj> getpendingcaseslist(Context context) {
        List<CaseObj> casedet = new ArrayList<CaseObj>();
        List<CaseObj> temp = new ArrayList<CaseObj>();
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                    CaseSyncOpenDatabaseHelper.class);
            Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Dao<CaseObj, Long> casedetDao = null;
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            if (currentHour >= 0 && currentHour <= 10) {
                c.add(Calendar.DATE, -1);
            }
            dt = c.getTime();
            temp = casedetDao.queryForAll();
            for (int count = 0; count < temp.size(); count++) {
                if (temp.get(count).getArchive().equals("N")) {
                    Date date1 = temp.get(count).getDateNextList();
                    Date date2 = dt;
                    if (date1 != null) {
                        if (date1.compareTo(date2) < 0) {
                            casedet.add(temp.get(count));
                        } else if (date1.compareTo(date2) == 0) {
                            casedet.add(temp.get(count));
                        } else {
                            // Log.e("SYNC","How to get here?");
                        }
                    } else {
                        casedet.add(temp.get(count));
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return casedet;
    }*/

    public List<CaseObj> getdistinctlist(Context context, String isdisposed) {
        List<CaseObj> caselist = new ArrayList<CaseObj>();
        try {
            Dao<CaseObj, Long> casedetDao = getCaseObjdao(context);
            Calendar c = new GregorianCalendar();
            c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            Date dt = c.getTime(); //the midnight, that's the first second of the day.


            QueryBuilder<CaseObj, Long> queryBuilder =
                    casedetDao.queryBuilder();
            if (isdisposed.equals("N")) {
                queryBuilder.orderBy("date_next_list", true);
                Where<CaseObj, Long> where = queryBuilder.where();
                where.eq("archive", isdisposed);
                caselist = queryBuilder
                        .distinct().selectColumns("date_next_list").query();
            } else {
                queryBuilder.orderBy("date_of_decision", false);
                Where<CaseObj, Long> where = queryBuilder.where();
                where.eq("archive", isdisposed);
                caselist = queryBuilder
                        .distinct().selectColumns("date_of_decision").query();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return caselist;
    }

/*    public void cleanCalendar(Context context) {
        try {
            ContentResolver cr = context.getContentResolver();
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Cursor cursor = cr
                    .query(CalendarContract.Events.CONTENT_URI,
                            new String[]{CalendarContract.Events._ID, CalendarContract.Events.CUSTOM_APP_PACKAGE},
                            null, null, null);
            if (cursor == null) return;
            cursor.moveToFirst();

            String idsToDelete = "";
            //Log.e("CAL ","Cursor count:"+ cursor.getCount());
            for (int i = 0; i < cursor.getCount(); i++) {
                // it might be also smart to check CALENDAR_ID here
                if (context.getPackageName().equals(cursor.getString(1))) {
                    //Log.e("CAL ","Cursor CAL:"+ String.format("_ID = %s OR ", cursor.getString(0)));
                    idsToDelete += String.format("_ID = %s OR ", cursor.getString(0));
                }
                cursor.moveToNext();
            }
            if (!idsToDelete.isEmpty()) {
                if (idsToDelete.endsWith(" OR ")) {
                    idsToDelete = idsToDelete.substring(0, idsToDelete.length() - 4);
                }
                cr.delete(CalendarContract.Events.CONTENT_URI, idsToDelete, null);
            }
        } catch (Exception e) {
            Log.e("CAL ", "exception delete:" + e.getMessage());
        }
    }*/

    public List<CaseObj> getpendingcaseslist(Context context, QUERY_TYPE type) {
        List<CaseObj> caselist = new ArrayList<CaseObj>();
        try {
            Dao<CaseObj, Long> casedetDao = getCaseObjdao(context);

            Calendar c = new GregorianCalendar();
            c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            Date dt = c.getTime();

            /* Calendar c = new GregorianCalendar();
            c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            Date dt = c.getTime(); //the midnight, that's the first second of the day.

            Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);

           if (currentHour >= 0 && currentHour <= 10) {
                c.add(Calendar.DATE, -1);
            }
            dt = c.getTime();
            Log.i(TAG,dt.toString());*/
            QueryBuilder<CaseObj, Long> queryBuilder =
                    casedetDao.queryBuilder();
            Where<CaseObj, Long> where = queryBuilder.where();
            if (type == QUERY_TYPE.PENDING) {
                where.eq("archive", "N");
                where.and();
                where.le("date_next_list", dt);

            } else if (type == QUERY_TYPE.TOTAL) {
                where.eq("archive", "N");
                where.and();
                where.eq("archive", "Y");
                //No where clause //Becarefull check this case
            } else if (type == QUERY_TYPE.TOTAL_NEDIS) {
                where.eq("archive", "N");
            } else if (type == QUERY_TYPE.CURRENT_DATE) {
                where.eq("archive", "N");
                where.and();
                where.eq("date_next_list", dt);
            } else if (type == QUERY_TYPE.SYNC_PENDING) {
                //Even disposed cases should come here
                where.le("date_next_list", dt);

            } else if (type == QUERY_TYPE.FILING_HIS) {
                //Only it will pick cases where the registration number is not assigned ie filing history
                where.isNull("reg_no");
                where.and();
                where.isNull("date_next_list");
            }

            PreparedQuery<CaseObj> preparedQuery = queryBuilder.prepare();

            caselist = casedetDao.query(preparedQuery);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return caselist;
    }

    public List<CaseObj> getpendingcaseslist(Context context) {
        List<CaseObj> caselist = new ArrayList<CaseObj>();
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean consider_disposed = prefs.getBoolean("pref_sync_disposed", true);
            Log.i(TAG, "Casesync:Consider_disposed:" + consider_disposed);
            if (consider_disposed) {
                caselist = getpendingcaseslist(context, QUERY_TYPE.SYNC_PENDING);
                List<CaseObj> filing_list = getpendingcaseslist(context, QUERY_TYPE.FILING_HIS);
                if (!CollectionUtils.isEmpty(filing_list)) {
                    Log.i(TAG, "Casesync:Considering all the cases:" + caselist.size());
                    Log.i(TAG, "Casesync:Considering filing history:" + filing_list.size());
                    caselist.addAll(filing_list);
                    Log.i(TAG, "After adding both:" + caselist.size());
                }

            } else {
                caselist = getpendingcaseslist(context, QUERY_TYPE.PENDING);
                List<CaseObj> filing_list = getpendingcaseslist(context, QUERY_TYPE.FILING_HIS);
                if (!CollectionUtils.isEmpty(filing_list)) {
                    Log.i(TAG, "Casesync:Considering only active cases:" + caselist.size());
                    Log.i(TAG, "Casesync:Considering filing history:" + filing_list.size());
                    caselist.addAll(filing_list);
                    Log.i(TAG, "After adding both:" + caselist.size());
                }


                Log.i(TAG, "Casesync:Considering only active cases:" + caselist.size());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return caselist;
    }

    public Long getCasesCount(Context context, QUERY_TYPE type) {
        Long pending_count = 0L;
        try {
            Dao<CaseObj, Long> casedetDao = getCaseObjdao(context);

            Calendar c = new GregorianCalendar();
            c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            Date dt = c.getTime(); //the midnight, that's the first second of the day.

           /* Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            //Condition is not required
            if (currentHour >= 0 && currentHour <= 10) {
                c.add(Calendar.DATE, -1);
            }*/
            dt = c.getTime();
            QueryBuilder<CaseObj, Long> queryBuilder =
                    casedetDao.queryBuilder();
            queryBuilder.setCountOf(true);
            Where<CaseObj, Long> where = queryBuilder.where();
            if (type == QUERY_TYPE.TOTAL) {
                where.eq("archive", "N");
                where.or();
                where.eq("archive", "Y");
            } else if (type == QUERY_TYPE.PENDING) {
                where.eq("archive", "N");
                where.and();
                where.le("date_next_list", dt);
            } else if (type == QUERY_TYPE.TOTAL_NEDIS) {
                where.eq("archive", "N");
            } else if (type == QUERY_TYPE.CURRENT_DATE) {
                where.eq("archive", "N");
                where.and();
                where.eq("date_next_list", dt);
            } else if (type == QUERY_TYPE.TOTAL_DIS) {
                where.eq("archive", "Y");
            } else if (type == QUERY_TYPE.FILING_HIS) {
                where.isNull("reg_no");
                where.and();
                where.isNull("date_next_list");
            }
            pending_count = casedetDao.countOf(queryBuilder.prepare());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pending_count;
    }

    public List<CaseObj> syncallcases(Context context, TYPE type) {
        List<CaseObj> dataModel = new ArrayList<>();
        if (type == null) {
            dataModel = getpendingcaseslist(context);
        } else if (type == TYPE.GET) {
            dataModel = getpendingcaseslist(context);
        } else {
            //Need to sync all the cases
            try {
                dataModel = getpendingcaseslist(context);
                for (CaseObj model : dataModel) {
                    getCaseObjFromCino(context, model.getCino(), false);
                }
                //TODO Take cases which are having hearing dates as null and loop those cases also.
                //Also need to check in the above case dates which are null are considered or not.
                // if considered then no need to do anything else logic to be modified.
            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
        }
        return dataModel;
    }

    public Cell createCell(String content, float borderWidth, int colspan, TextAlignment alignment) {
        Cell cell = new Cell(1, colspan).add(new Paragraph(content));
        cell.setTextAlignment(alignment);
        cell.setBorder(new SolidBorder(borderWidth));
        return cell;
    }

    public void createStorageIfNA(Context context) {
        Storage storage = null;
        if (SimpleStorage.isExternalStorageWritable()) {
            storage = SimpleStorage.getExternalStorage();
        } else {
            storage = SimpleStorage.getInternalStorage(context);
        }
        boolean dirExists = storage.isDirectoryExists("casesync");
        Log.d("More", "Storage: dirExists" + dirExists);
        if (dirExists == false) {
            storage.createDirectory("casesync");
            Log.d("More", "dirExists:");
        }
        boolean subdirExists = storage.isDirectoryExists("casesync/export");
        if (subdirExists == false) {
            Log.d("More", "subdirExists:");
            storage.createDirectory("casesync/export");
        }
    }

    public File createCauseListPdf(List<CaseObj> caselist, List<CaseObj> caselistnyu, Context context, boolean today) throws IOException {

        Log.i(TAG, "caselist:" + caselist.size());
        Log.i(TAG, "caselist nyc:" + caselistnyu.size());
        //Based on the date entered the avaialable cases should be exported
        try {
            String filepath = FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.EXPORT, null);

            String filename = "causelist" + sdf.format(new Date()).replaceAll("-", "") + ".pdf";
            FileUtility.deleteExistingFile(context, filepath + filename);
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(filepath + filename));
            com.itextpdf.layout.Document doc = new com.itextpdf.layout.Document(pdfDoc);
            Table table = new Table(new float[]{1, 1, 1, 2});
            table.setWidthPercent(100);

            Calendar todaydate = Calendar.getInstance();
            todaydate.clear(Calendar.HOUR);
            todaydate.clear(Calendar.MINUTE);
            todaydate.clear(Calendar.SECOND);
            Date todayDate = todaydate.getTime();

            boolean status = today;
            if (status) {
                //Morning status
                table.addCell(createCell("Last Hearing Date", 1, 1, TextAlignment.LEFT));
                //table.addCell(createCell("Next Hearing Date", 1, 1, TextAlignment.LEFT));
            } else {
                //Evening status
                //table.addCell(createCell("Last Hearing Date", 1, 1, TextAlignment.LEFT));
                table.addCell(createCell("Next Hearing Date", 1, 1, TextAlignment.LEFT));
            }
            table.addCell(createCell("Status", 1, 1, TextAlignment.LEFT));
            table.addCell(createCell("Case Details", 1, 1, TextAlignment.LEFT));
            table.addCell(createCell("Business", 1, 2, TextAlignment.LEFT));

            for (CaseObj obj : caselist) {
                if (status) {
                    //Morning status
                    if (obj.getDateLastList() != null) {
                        table.addCell(createCell(sdf.format(obj.getDateLastList()), 1, 1, TextAlignment.LEFT));
                    } else {
                        table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                    }

                    //table.addCell(createCell(sdf.format(obj.getDateNextList()), 1, 1, TextAlignment.LEFT));
                } else {
                    //Evening status
                    //table.addCell(createCell(sdf.format(obj.getDateLastList()), 1, 1, TextAlignment.LEFT));
                    if (obj.getDateNextList() != null) {
                        table.addCell(createCell(sdf.format(obj.getDateNextList()), 1, 1, TextAlignment.LEFT));
                    } else {
                        table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                    }

                }
                //FIXED Logic is wrong.If the date next listed is ge current date it is updated
                if (obj.getDateNextList() != null) {
                    if (obj.getDateNextList().after(todayDate)) {
                        //FIXME Even the date is not updated it is showing as updated Some thing is going for a toss here.
                        table.addCell(createCell("Updated", 1, 1, TextAlignment.LEFT));
                    } else {
                        //This case won't exist
                        table.addCell(createCell("Yet to update", 1, 1, TextAlignment.LEFT));
                    }
                } else {
                    table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                }
                table.addCell(createCell("Stage: " + obj.getPurposeName() + "," + obj.getTypeName() + "/" + obj.getRegNo() + "/" + obj.getRegYear() + "-" + obj.getPetName() + " VS " + obj.getResName() + "-" + obj.getCourtName() + "/" + obj.getDesgname(), 1, 1, TextAlignment.LEFT));
                table.addCell(createCell(obj.getWritinfo() != null ? obj.getWritinfo() : "", 1, 2, TextAlignment.LEFT));
            }
            for (CaseObj obj : caselistnyu) {
                if (obj.getDateNextList() != null) {
                    table.addCell(createCell(sdf.format(obj.getDateNextList()), 1, 1, TextAlignment.LEFT));
                } else {
                    table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                }

                table.addCell(createCell("Yet to update", 1, 1, TextAlignment.LEFT));
                table.addCell(createCell("Stage: " + obj.getPurposeName() + "," + obj.getTypeName() + "/" + obj.getRegNo() + "/" + obj.getRegYear() + "-" + obj.getPetName() + " VS " + obj.getResName() + "-" + obj.getCourtName() + "/" + obj.getDesgname(), 1, 1, TextAlignment.LEFT));
                table.addCell(createCell(obj.getWritinfo() != null ? obj.getWritinfo() : "", 1, 2, TextAlignment.LEFT));

            }
            table.addCell(createCell("Total Cases", 1, 3, TextAlignment.LEFT));
            table.addCell(createCell("" + (caselist.size() + caselistnyu.size()), 1, 1, TextAlignment.LEFT));
            doc.add(table);
            doc.close();

            Toast.makeText(context, "Generated PDF Successfully.", Toast.LENGTH_LONG).show();
            File file = new File(filepath + filename);
            Log.i(TAG, "File Generated at " + file.getAbsolutePath());
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    public void syncscheduler(Context context) {
        try {
            Calendar now = Calendar.getInstance();
            Log.i(TAG, now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE));
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                Long pendingcount = getCasesCount(context, QUERY_TYPE.PENDING);
                pendingcount = pendingcount + getCasesCount(context, QUERY_TYPE.FILING_HIS);
                if (pendingcount > 0) {
                    syncallcases(context, CasesyncUtill.TYPE.FULL);
                    Long count = getCasesCount(context, QUERY_TYPE.PENDING);
                    String message = "";
                    if (count > 0) {
                        if (count == 1) {
                            message = count + " case is not yet updated";
                        } else {
                            message = count + " cases are not yet updated";
                        }
                    } else {
                        message = "All cases updated successfully";
                    }
                    //Comment after testing
                    sendNotification("Automatic synchronization successful ", message, null, context, CasesyncUtill.NOTITYPE.GENERAL);

                } else {
                    //No cases available for auto sync
                    //Comment after testing
                    //sendNotification("No cases available", "Automatic synchronization successful ", null, context, CasesyncUtill.NOTITYPE.GENERAL);
                }

            } else {
                Toast.makeText(context, "Please enable the internet to use auto sync feature in " + getApplicationName(context), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void sendNotification(String title, String msg, String caseno, Context context, NOTITYPE type) {
        //TODO Make it compatable with URL Notiifcation also.
        NotificationManager
                mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = null;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        if (type == null) {
            notificationIntent = new Intent(context, NavActivity.class);
            stackBuilder.addParentStack(NavActivity.class);
            stackBuilder.addNextIntent(notificationIntent);
        } else if (type == NOTITYPE.CASE) {
            notificationIntent = new Intent(context, CasedetailsTab.class);
            notificationIntent.putExtra("casenumber", caseno);
            stackBuilder.addParentStack(NavActivity.class);
            stackBuilder.addNextIntent(notificationIntent);
        } else {
            notificationIntent = new Intent(context, NavActivity.class);
            stackBuilder.addParentStack(NavActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

        }
        int min = 1562;
        int max = 100000;
        int randomNum = min + (int) (Math.random() * ((max - min) + 1));
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(randomNum, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Creating a notification channel and assigning
            createNotificationChannel(context, mNotificationManager);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mBuilder.setContentIntent(pendingIntent);
            mBuilder.setCategory(NotificationCompat.CATEGORY_REMINDER);
            mBuilder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            mNotificationManager.notify(randomNum, mBuilder.build());
        } else {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(title))
                            .setContentText(msg);
            mBuilder.setContentIntent(pendingIntent);
            mNotificationManager.notify(randomNum, mBuilder.build());
        }


    }

    public NotificationChannel createNotificationChannel(Context context, NotificationManager notificationManager) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                CharSequence name = context.getString(R.string.channel_name);
                String description = context.getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
/*				NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
				channel.setDescription(description);
				// Register the channel with the system; you can't change the importance
				// or other notification behaviors after this
				notificationManager.createNotificationChannel(channel);*/

                NotificationChannel mChannel = notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID);
                if (mChannel == null) {
                    mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notificationManager.createNotificationChannel(mChannel);
                }
                return mChannel;

            } else {
                Log.e(TAG, Build.VERSION.SDK_INT + " : " + Build.VERSION_CODES.O + " notificationManager is null");
            }
        }
        return null;
    }

    public String fetchBusiness(CaseObj caseobj, Context context) {
        String business_text = "--";
        String data_params = getDataparamsv2(caseobj);
        if (data_params == null) {
            return business_text;
        }
        try {
            String baseurl = PropertyUtil.getProperty("apiurl", context) + "s_show_business.php?" + data_params;
            Log.i(TAG, "url:" + baseurl.toString());
            RestTemplate restTemplate = getRestTemplate();
            {
                ResponseEntity<String> out = restTemplate.exchange(baseurl, HttpMethod.GET, null, String.class);
                Log.d(TAG, out.getStatusCode().toString());
                if (out.getStatusCode() == HttpStatus.OK) {
                    //Error Case....
                    if (out.getBody().contains("status")) {
                        return business_text;
                    }
                    business_text = AESCryptCustom.decrypt(out.getBody());
                    Log.d(TAG, business_text.toString());
                    if (GenericUtil.isJSONValid(business_text)) {
                        JSONObject history = new JSONObject(business_text);
                        String business = history.getString("viewBusiness");
                        Document doc = Jsoup.parse(business);
                        for (Element table : doc.select("table")) { //this will work if your doc contains only one table element
                            String businesstext = "";
                            if (table.select("tr").get(1).select("td").size() > 1 || table.select("tr").get(0).select("td").size() > 1) {
                                if (table.select("tr").get(0).select("td").get(0).text().contains("Business")) {
                                    businesstext = table.select("tr").get(0).select("td").get(2).text();
                                    return businesstext;
                                } else if (table.select("tr").get(1).select("td").get(0).text().contains("Business")) {
                                    businesstext = table.select("tr").get(1).select("td").get(2).text();
                                }
                                //String businesstext = table.select("tr").get(1).select("td").get(2).text();
                                //Log.i(TAG, "businesstext:" + businesstext);
                                return businesstext;
                            } else {
                                return business_text;
                            }
                        }
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    public String fetchBusinessv1(CaseObj caseobj, Context context) {
        String responsetmp = "";
        try {
//            //TODO Handle in future once the active cases business fetch is completed
//            if (caseobj.getArchive().equals("Y")) {
//                return "";
//            }

            String data_params = getDataparams(caseobj);
            if (data_params == null) {
                return "--";
            }
            String baseurl = PropertyUtil.getProperty("apiurl", context) + "s_show_business.php?" + data_params;
            Log.i(TAG, "url:" + baseurl.toString());
            RestTemplate restTemplate = new RestTemplate();
            try {
                ResponseEntity<String> out = restTemplate.exchange(baseurl, HttpMethod.GET, null, String.class);
                Log.i(TAG, out.getStatusCode().toString() + "");
                if (out.getStatusCode() == HttpStatus.OK) {
                    responsetmp = out.getBody();
                    if (!responsetmp.isEmpty() && responsetmp != null) {
                        //Log.i(TAG, responsetmp);
                        if (!responsetmp.contains("error_ERROR_")) {
                            responsetmp = AESCryptCustom.decrypt(responsetmp);
                            JSONObject history = new JSONObject(responsetmp);
                            String business = history.getString("viewBusiness");
                            //Log.i(TAG, "business:" + business);
                            Document doc = Jsoup.parse(business);
                            for (Element table : doc.select("table")) { //this will work if your doc contains only one table element
                                String businesstext = "";
                                if (table.select("tr").get(1).select("td").size() > 1 || table.select("tr").get(0).select("td").size() > 1) {
                                    if (table.select("tr").get(0).select("td").get(0).text().contains("Business")) {
                                        businesstext = table.select("tr").get(0).select("td").get(2).text();
                                        return businesstext;
                                    } else if (table.select("tr").get(1).select("td").get(0).text().contains("Business")) {
                                        businesstext = table.select("tr").get(1).select("td").get(2).text();
                                    }
                                    //String businesstext = table.select("tr").get(1).select("td").get(2).text();
                                    //Log.i(TAG, "businesstext:" + businesstext);
                                    return businesstext;
                                } else {
                                    return "--";
                                }
                            }
                        } else {
                            Log.i(TAG, "Error Occured");
                        }
                    }
                }

            } catch (HttpServerErrorException e) {
                Log.i(TAG, "Status code is not OK" + ":" + caseobj.getCourtNo() + ":" + caseobj.getCourtno());
//                if (caseobj.getCourtNo().equalsIgnoreCase("" + caseobj.getCourtno())) {
//                    Long courtcode = getCourtNumber(caseobj);
//                    Log.i(TAG, "courtcode:" + courtcode);
//                    if (!courtcode.equals(0L)) {
//                        caseobj.setCourtno(courtcode);
//                        String nwqdata_params = getDataparams(caseobj);
//                        String newbaseurl = PropertyUtil.getProperty("apiurl", context) + "s_show_business.php?" + nwqdata_params;
//                        ResponseEntity<String> output = restTemplate.exchange(newbaseurl, HttpMethod.GET, null, String.class);
//                        if (output.getStatusCode() == HttpStatus.OK) {
//                            responsetmp = output.getBody();
//                            if (!responsetmp.isEmpty() && responsetmp != null) {
//                                Log.i(TAG, responsetmp);
//                                if (!responsetmp.equals("error_ERROR_")) {
//                                    responsetmp = AESCryptCustom.decrypt(responsetmp);
//                                    JSONObject history = new JSONObject(responsetmp);
//                                    String business = history.getString("viewBusiness");
//                                    Log.i(TAG, "business:" + business);
//                                    Document doc = Jsoup.parse(business);
//                                    for (Element table : doc.select("table")) { //this will work if your doc contains only one table element
//                                        String businesstext = "";
//                                        if (table.select("tr").get(1).select("td").size() > 1) {
//                                            if (table.select("tr").get(0).select("td").get(0).text().contains("Business")) {
//                                                businesstext = table.select("tr").get(0).select("td").get(2).text();
//                                            } else if (table.select("tr").get(1).select("td").get(0).text().contains("Business")) {
//                                                businesstext = table.select("tr").get(1).select("td").get(2).text();
//                                            }
//                                            //String businesstext = table.select("tr").get(1).select("td").get(2).text();
//                                            //Log.i(TAG, "businesstext:" + businesstext);
//                                            return businesstext;
//                                        } else {
//                                            return "--";
//                                        }
//                                    }
//                                } else {
//                                    Log.i(TAG, "Error Occured");
//                                }
//                            }
//                        } else {
//                            Log.i(TAG, "Response is not OK");
//                        }
//                    } else {
//                        Log.i(TAG, "courtcode:" + courtcode + " is invalid");
//                    }
//                }
                return "N/A";
            }
            //OLD Code
            // responsetmp = restTemplate.getForObject(baseurl, String.class);
        } catch (Exception e) {
            Log.e("ASYNC", e.getMessage(), e);
        }

        return null;
    }

    //Filing History is not getting updated in this.
    public CaseObj getCaseObjFromCino(Context context, String cinno, boolean firebase) throws IOException, GeneralSecurityException {
        String baseurl = PropertyUtil.getProperty("apiurl", context) + "caseHistoryWebService.php";
        CaseObj caseobj = new CaseObj();
        //Need to add URLEncoder.Some cases are having issue.
        String data_params = "?cino=" + AESCryptCustom.encrypt(cinno);
        String url = baseurl + data_params;
        Log.d(TAG, cinno + ":url:" + url);
        RestTemplate restTemplate = getRestTemplate();

        try {
            String responsetmp = restTemplate.getForObject(url, String.class);
            Log.i(TAG, responsetmp);
            if (!responsetmp.isEmpty() && responsetmp != null) {
                Log.i(TAG, "responsetmp:" + AESCryptCustom.decrypt(responsetmp));
                if (!AESCryptCustom.decrypt(responsetmp).equals("null")) {
                    try {
                        JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                        if (objtemp != null) {
                            Log.i(TAG, objtemp.toString());
                            ObjectMapper objectMapper = new ObjectMapper();
                            caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);
                            if (caseobj != null) {
                                Log.i(TAG, caseobj.getCaseNo());
                                String businesstext = new CasesyncUtill().fetchBusiness(caseobj, context);
                                caseobj.setWritinfo(businesstext);
                                saveObj(caseobj, context, firebase);
                            } else {
                                //Normally it won't come in to this block this is to handle the wrost case scenario.
                                caseobj = getFilingCaseHistory(context, cinno);
                                if (caseobj == null) {
                                    Log.e(TAG, "Case not found ");
                                } else {
                                    saveObj(caseobj, context, firebase);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (GeneralSecurityException e) {
                        Log.e(TAG, e.getCause().getMessage());
                    } catch (Exception e) {
                        Log.e(TAG, e.getCause().getMessage());
                    }
                } else {
                    caseobj = getFilingCaseHistory(context, cinno);
                    if (caseobj == null) {
                        Log.e(TAG, "Case not found ");
                    } else {
                        saveObj(caseobj, context, firebase);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return caseobj;
    }

    /**
     * Caseobj is null so need to try to fetch the case which is having only filing_case_history.
     **/
    public CaseObj getFilingCaseHistory(Context context, String cinno) {
        CaseObj caseobj = null;
        String baseurl = null;
        RestTemplate restTemplate = getRestTemplate();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            baseurl = PropertyUtil.getProperty("apiurl", context) + "filingCaseHistory.php";
            String data_params = "?cino=" + AESCryptCustom.encrypt(cinno);
            String url = baseurl + data_params;
            String responsetmp = restTemplate.getForObject(url, String.class);
            if (!responsetmp.isEmpty() && responsetmp != null) {
                if (!AESCryptCustom.decrypt(responsetmp).equals("null")) {
                    JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                    Log.i(TAG, "Casedetails are not available only filing history is available.");
                    caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);
                    Log.i(TAG, caseobj.getResName());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return caseobj;
    }

    public RestTemplate getRestTemplate() {

        if (rt == null) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
            SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate
                    .getRequestFactory();
            rf.setReadTimeout(60000);
            rf.setConnectTimeout(60000);
            rt = restTemplate;
            return restTemplate;
        } else {
            return rt;
        }
    }

    public CaseObj fetchobjusingcinsave(Context context, Useraccount account, boolean firebase) throws IOException, GeneralSecurityException {
        String baseurl = PropertyUtil.getProperty("apiurl", context) + "caseHistoryWebService.php";
        CaseObj caseobj = new CaseObj();
        String data_params = "?cino=" + AESCryptCustom.encrypt(account.getCinno());
        String url = baseurl + data_params;
        Log.d(TAG, "url:" + url);
        RestTemplate restTemplate = new RestTemplate();

        try {
            String responsetmp = restTemplate.getForObject(url, String.class);
            Log.d(TAG, "response:" + responsetmp);
            if (!StringUtils.isEmpty(responsetmp)) {
                if (!responsetmp.contains("error_")) {
                    if (!AESCryptCustom.decrypt(responsetmp).equals("null")) {
                        JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                        Log.i(TAG, objtemp.toString());
                        ObjectMapper objectMapper = new ObjectMapper();
                        caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);
                        if (caseobj != null) {
                            String businesstext = fetchBusiness(caseobj, context);
                            caseobj.setWritinfo(businesstext);
                            caseobj.setPhone(account.getClientmobnumber());
                            caseobj.setContactname(account.getClientname());
                            caseobj.setEmail(account.getClientemail());
                            caseobj.setAddnotes(account.getClientnotes());
                            saveObj(caseobj, context, firebase);
                        } else {
                            caseobj = getFilingCaseHistory(context, account.getCinno());
                            if (caseobj == null) {
                                Log.e(TAG, "Case not found ");
                            } else {
                                caseobj.setPhone(account.getClientmobnumber());
                                caseobj.setContactname(account.getClientname());
                                caseobj.setEmail(account.getClientemail());
                                caseobj.setAddnotes(account.getClientnotes());
                                saveObj(caseobj, context, firebase);

                            }
                        }
                    } else {
                        caseobj = getFilingCaseHistory(context, account.getCinno());
                        if (caseobj == null) {
                            Log.e(TAG, "Case not found ");
                        } else {
                            caseobj.setPhone(account.getClientmobnumber());
                            caseobj.setContactname(account.getClientname());
                            caseobj.setEmail(account.getClientemail());
                            caseobj.setAddnotes(account.getClientnotes());
                            saveObj(caseobj, context, firebase);
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //Toast.makeText(context, "Error occured while importing  "+cinno, Toast.LENGTH_SHORT).show();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            //caseobj=null;
            Log.e(TAG, e.getMessage());
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return caseobj;
    }

    public void saveObj(CaseObj obj, Context context, boolean firebase) {
        try {
            Addexitingcase.insertdata(obj, context, firebase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getDataparamsv2(CaseObj caseobj) {

        //        var viewBusinessData = {court_code:encryptData(court_code),
        //        dist_code:encryptData(dist_code),
        //        nextdate1:encryptData(n_dt),
        //        case_number1:encryptData(case_number),
        //        state_code:encryptData(state_code),
        //        disposal_flag:encryptData(businessStatus),
        //        businessDate:encryptData(todays_date1),
        //        court_no:encryptData(court_no),
        //        language_flag:encrypted_data4.toString(),
        //        bilingual_flag:encrypted_data5.toString()};
        DateFormat sc = new SimpleDateFormat("yyyyMMdd");
        DateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        String data_params = "";
        try {
            if (StringUtils.isEmpty(caseobj.getHistoryOfCaseHearing())) {
                //No history available.
                return null;
            }
            Long courtcode = getCourtNumber(caseobj);
            String courtno = "court_code=" + AESCryptCustom.encrypt("" + courtcode);
            //String courtno = "court_code=" + AESCryptCustom.encrypt("" + caseobj.getCourtno());
            data_params = courtno;
            data_params += "&dist_code=" + AESCryptCustom.encrypt(caseobj.getDistrictCode());
            if (caseobj.getArchive().equals("N")) {
                data_params += "&nextdate1=" + AESCryptCustom.encrypt(sc.format(caseobj.getDateNextList()));
            } else {
                data_params += "&nextdate1=";
            }
            data_params += "&case_number1=" + AESCryptCustom.encrypt(caseobj.getCaseNo());
            data_params += "&state_code=" + AESCryptCustom.encrypt(caseobj.getStateCode());
            data_params += "&disposal_flag=" + AESCryptCustom.encrypt((caseobj.getArchive().equals("N") ? "Pending" : "Disposed"));
            if (caseobj.getArchive().equals("Y")) {
                if (caseobj.getDateOfDecision() != null) {
                    data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateOfDecision()));
                } else {
                    data_params += "&businessDate=";
                }

            } else {
                Log.i(TAG, caseobj.getCino() + ":" + dt.format(caseobj.getDateLastList()) + ":" + sc.format(caseobj.getDateNextList()) + ":" + caseobj.getCourtno() + ":" + caseobj.getCourtNo());
                data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateLastList()));
            }
            data_params += "&court_no=" + AESCryptCustom.encrypt("" + caseobj.getCourtNo());
            //FUTURE IT MIGHT CHANGE
            data_params += "&language_flag=" + AESCryptCustom.encrypt("english");
            data_params += "&bilingual_flag=" + AESCryptCustom.encrypt("0");
            //language_flag:encrypted_data4.toString(), bilingual_flag:encrypted_data5.toString()


        } catch (Exception e) {
            e.printStackTrace();
        }


        return data_params;
    }

    public String getDataparams(CaseObj caseobj) {
        DateFormat sc = new SimpleDateFormat("yyyyMMdd");
        DateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        String data_params = "";
        try {
            if (StringUtils.isEmpty(caseobj.getHistoryOfCaseHearing())) {
                //No history available.
                return null;
            }
            Long courtcode = getCourtNumber(caseobj);
            String courtno = "court_code=" + AESCryptCustom.encrypt("" + courtcode);
            //String courtno = "court_code=" + AESCryptCustom.encrypt("" + caseobj.getCourtno());
            data_params = courtno;
            data_params += "&dist_code=" + AESCryptCustom.encrypt(caseobj.getDistrictCode());
            if (caseobj.getArchive().equals("N")) {
                data_params += "&nextdate1=" + AESCryptCustom.encrypt(sc.format(caseobj.getDateNextList()));
            } else {
                data_params += "&nextdate1=";
            }
            data_params += "&case_number1=" + AESCryptCustom.encrypt(caseobj.getCaseNo());
            data_params += "&state_code=" + AESCryptCustom.encrypt(caseobj.getStateCode());
            data_params += "&disposal_flag=" + AESCryptCustom.encrypt((caseobj.getArchive().equals("N") ? "Pending" : "Disposed"));
            if (caseobj.getArchive().equals("Y")) {
                if (caseobj.getDateOfDecision() != null) {
                    data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateOfDecision()));
                } else {
                    data_params += "&businessDate=";
                }

            } else {
                Log.i(TAG, caseobj.getCino() + ":" + dt.format(caseobj.getDateLastList()) + ":" + sc.format(caseobj.getDateNextList()) + ":" + caseobj.getCourtno() + ":" + caseobj.getCourtNo());
                data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateLastList()));
            }
            data_params += "&court_no=" + AESCryptCustom.encrypt("" + caseobj.getCourtNo());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return data_params;
    }

    public Long getCourtNumber(CaseObj caseobj) {
        if (!StringUtils.isEmpty(caseobj.getHistoryOfCaseHearing())) {
            Document doc = Jsoup.parse(caseobj.getHistoryOfCaseHearing().replace("\\", ""));
            Elements table = doc.select("table.tbl-result");
            Elements rows = table.select("tr");
            try {
                if (rows.size() > 0) {
                    String case_data = rows.get(rows.size() - 1).getElementsByTag("a").attr("onclick");
                    if (!StringUtils.isEmpty(case_data)) {
                        Log.i("TST", "Function Data:" + case_data);
                        case_data = case_data.replace("viewBusiness", "");
                        case_data = case_data.replace("(", "");
                        case_data = case_data.replaceAll("\\)", "");
                        String[] split = case_data.split(",");
                        if (!StringUtils.isEmpty(split[0])) {
                            Log.i(TAG, "Court Code" + split[0]);
                            return Long.parseLong(split[0].replace("'", ""));
                        }
                    } else {
                        Log.i("TST", "No case data");
                    }
                } else {
                    Log.i("TST", "No records");
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TST", "No History");
        }
        return 0L;
    }

    public List<OrderDetails> getOrderList(List<CaseObj> casedetails, boolean interim) {
        List<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
        for (CaseObj casedetail : casedetails) {
            if (interim == true) {
                if (casedetail.getInterimOrder() != null) {
                    // Fetch Interim Details
                    Document doc = Jsoup.parse(casedetail.getInterimOrder());
                    Elements table = doc.select("table.tbl-result");
                    Elements rows = table.select("tr");
                    Log.i("TST", "Row Size :" + rows.size());
                    for (int i = 0; i < rows.size(); i++) {
                        OrderDetails order = new OrderDetails();
                        Element row = rows.get(i);
                        Elements cols = row.select("td");
                        for (int count = 0; count < cols.size(); count++) {
                            Log.i(TAG, "Text:" + cols.get(count).text());
                            if (count == 0) {
                                order.setOrderno(cols.get(count).text());
                            }
                            if (count == 1) {
                                String orderdate = cols.get(count).text();
                                order.setOrderdate("Order Date:" + orderdate);
                                try {
                                    order.setOrder_date(sdf.parse(orderdate.trim().replace("  ", "")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            if (count == 2) {
                                Log.i(TAG, "URL :" + cols.get(count).getElementsByTag("a").attr("abs:href"));
                                order.setUrl(cols.get(count).getElementsByTag("a").attr("abs:href"));
                            }
                        }
                        if (order.getOrderno() != null) {
                            order.setOrderdetails(casedetail.getCino());
                            order.setCourtname(casedetail.getCourtName());
                            order.setCasenumber(casedetail.getTypeName() + "/" + casedetail.getRegNo() + "/" + casedetail.getRegYear());
                            order.setPetres(casedetail.getPetName() + " VS " + casedetail.getResName());
                            order.setOrdertype("Type:Interim Order");
                            if (casedetail.getArchive() != null && casedetail.getArchive().equals("N")) {
                                order.setCase_status("Status:Active");
                            } else {
                                order.setCase_status("Status:Disposed");
                            }

                            orderDetails.add(order);
                        }

                    }

                }
            } else {
                if (casedetail.getFinalOrder() != null) {
                    //Fetching Final Order Details
                    Log.i(TAG, "Order Text :" + casedetail.getFinalOrder());
                    Document doc = Jsoup.parse(casedetail.getFinalOrder());
                    Elements table = doc.select("table.tbl-result");
                    Elements rows = table.select("tr");
                    Log.i(TAG, "Row Size :" + rows.size());

                    for (int i = 0; i < rows.size(); i++) {
                        OrderDetails order = new OrderDetails();
                        Element row = rows.get(i);
                        Elements cols = row.select("td");
                        for (int count = 0; count < cols.size(); count++) {
                            Log.i(TAG, "Text:" + cols.get(count).text());
                            if (count == 0) {
                                order.setOrderno(cols.get(count).text());
                            }
                            if (count == 1) {
                                String orderdate = cols.get(count).text();
                                order.setOrderdate("Order Date:" + orderdate);
                                try {
                                    order.setOrder_date(sdf.parse(orderdate.trim().replace("  ", "")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            if (count == 2) {
                                Log.i(TAG, "URL :" + cols.get(count).getElementsByTag("a").attr("abs:href"));
                                order.setUrl(cols.get(count).getElementsByTag("a").attr("abs:href"));
                            }
                        }
                        if (order.getOrderno() != null) {
                            order.setOrderdetails(casedetail.getCino());
                            order.setOrderdetails(casedetail.getCino());
                            order.setCourtname(casedetail.getCourtName());
                            order.setCasenumber(casedetail.getTypeName() + "/" + casedetail.getRegNo() + "/" + casedetail.getRegYear());
                            order.setPetres(casedetail.getPetName() + " VS " + casedetail.getResName());
                            order.setOrdertype("Type:Final Order");
                            if (casedetail.getArchive() != null && casedetail.getArchive().equals("N")) {
                                order.setCase_status("Status:Active");
                            } else {
                                order.setCase_status("Status:Disposed");
                            }
                            orderDetails.add(order);
                        }


                    }
                }
            }
        }
        //If orderdetails are not null. Sort it by descending order.
        //Sorting order details orderDetails.
//        Collections.sort(orderDetails, new Comparator<OrderDetails>() {
//            @Override
//            public int compare(OrderDetails order1, OrderDetails order2) {
//                return order2.getOrder_date().compareTo(order1.getOrder_date());
//            }
//        });

        return orderDetails;
    }

    public List<OrderDetails> getOrderList(CaseObj casedetails) {
        DateFormat format = new SimpleDateFormat("");
        List<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
        if (casedetails.getInterimOrder() != null) {
            Document doc = Jsoup.parse(casedetails.getInterimOrder());
            Elements table = doc.select("table.tbl-result");
            Elements rows = table.select("tr");
            Log.i("TST", "Row Size :" + rows.size());
            for (int i = 0; i < rows.size(); i++) {
                OrderDetails order = new OrderDetails();
                Element row = rows.get(i);
                Elements cols = row.select("td");
                for (int count = 0; count < cols.size(); count++) {
                    Log.i(TAG, "Text:" + cols.get(count).text());
                    if (count == 0) {
                        order.setOrderno(cols.get(count).text());
                    }
                    if (count == 1) {
                        order.setOrderdate(cols.get(count).text());
                    }
                    if (count == 2) {
                        Log.i(TAG, "URL :" + cols.get(count).getElementsByTag("a").attr("abs:href"));
                        order.setUrl(cols.get(count).getElementsByTag("a").attr("abs:href"));
                    }
                }
                order.setOrderdetails(casedetails.getCino());
                orderDetails.add(order);
            }

        }
        //Fetching Final Order Details
        if (casedetails.getFinalOrder() != null) {
            Log.i(TAG, "Order Text :" + casedetails.getFinalOrder());
            Document doc = Jsoup.parse(casedetails.getFinalOrder());
            Elements table = doc.select("table.tbl-result");
            Elements rows = table.select("tr");
            Log.i(TAG, "Row Size :" + rows.size());

            for (int i = 0; i < rows.size(); i++) {
                OrderDetails order = new OrderDetails();
                Element row = rows.get(i);
                Elements cols = row.select("td");
                for (int count = 0; count < cols.size(); count++) {
                    Log.i(TAG, "Text:" + cols.get(count).text());
                    if (count == 0) {
                        order.setOrderno(cols.get(count).text());
                    }
                    if (count == 1) {
                        order.setOrderdate(cols.get(count).text());
                    }
                    if (count == 2) {
                        Log.i(TAG, "URL :" + cols.get(count).getElementsByTag("a").attr("abs:href"));
                        order.setUrl(cols.get(count).getElementsByTag("a").attr("abs:href"));
                    }
                }
                order.setOrderdetails(casedetails.getCino());
                orderDetails.add(order);
            }

        }
        return orderDetails;
    }

    public void createCalendarEvent(Context context) {
        if ((ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED)
                & (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)) {
            //Find for the existing event with event id as CRN.If available update else create.
            // Run query
            Cursor cur = null;
            ContentResolver cr = context.getContentResolver();
            Uri uri = CalendarContract.Calendars.CONTENT_URI;
            String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                    + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?))";
            String[] selectionArgs = new String[]{"hera@example.com", "com.google"};
// Submit the query and get a Cursor object back.
            cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);
            // Use the cursor to step through the returned records
            while (cur.moveToNext()) {
                long calID = 0;
                String displayName = null;
                String accountName = null;
                String ownerName = null;

                // Get the field values
                calID = cur.getLong(PROJECTION_ID_INDEX);
                displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
                accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
                ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);
                //Store the calendar id in shared_preferences
                Log.i(TAG, "calID:" + calID);
                Log.i(TAG, "displayName:" + displayName);
                Log.i(TAG, "accountName:" + accountName);
                Log.i(TAG, "ownerName:" + ownerName);

                // Do something with the values...

            }

        } else {
            Log.i(TAG, "No Permissions.");
        }
    }

    Long convertDateForCalendar(Date date, Context context) {
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, -5);
        cal.add(Calendar.MINUTE, -30);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String notificationtiming = prefs.getString("notification_preference", "0:00");
        String[] timing = notificationtiming.split(":");
        int hour = Integer.parseInt(timing[0]);
        int minute = Integer.parseInt(timing[1]);
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, minute);


        return date.getTime();
    }

    public ErrorMessage createCalendarNotificationBulk(Context context, List<CaseObj> caselist, boolean calendar_sync, boolean calendar_reminder, boolean calendar_email) {
        ErrorMessage message = new ErrorMessage();
        try {
            if ((ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED)
                    & (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)) {
                int count = 0;
                if (calendar_sync) {
                    for (CaseObj caseObj : caselist) {
                        if (caseObj.getDateNextList() != null) {
                            count = count + 1;
                            addAppointmentsToCalender(context, buildTitle(caseObj), buildDesc(caseObj), caseObj.getCourtName(), 1, caseObj.getDateNextList(), calendar_reminder, calendar_email);
                        }

                    }
                    message.setStatus(true);
                    message.setMessage(count + " event/events added.");
                } else {
                    message.setStatus(false);
                    message.setMessage("Calender Sync is not enabled");
                }
            } else {
                message.setStatus(false);
                message.setMessage("Enable Read & Write permissions for calendar.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            message.setStatus(false);
            message.setMessage("Error.");
        }


        return message;
    }

    public ErrorMessage createCalendarNotification(Context context, List<CaseObj> caselist) {
        ErrorMessage message = new ErrorMessage();
        try {
            if ((ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED)
                    & (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                boolean calendar_sync = prefs.getBoolean("pref_sync_gc", false);
                boolean calendar_reminder = prefs.getBoolean("pref_sync_reminder", false);
                boolean calendar_email = prefs.getBoolean("pref_sync_email", false);
                int count = 0;
                if (calendar_sync) {
                    for (CaseObj caseObj : caselist) {
                        if (caseObj.getDateNextList() != null) {
                            count = count + 1;
                            addAppointmentsToCalender(context, buildTitle(caseObj), buildDesc(caseObj), caseObj.getCourtName(), 1, caseObj.getDateNextList(), calendar_reminder, calendar_email);
                        }

                    }
                    message.setStatus(true);
                    message.setMessage(count + " event/events added.");
                } else {
                    message.setStatus(false);
                    message.setMessage("Calender Sync is not enabled");
                }
            } else {
                message.setStatus(false);
                message.setMessage("Enable Read & Write permissions for calendar.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            message.setStatus(false);
            message.setMessage("Error.");
        }


        return message;
    }

    public void clearCalendar(Context context) {
        try {
            long startTime = System.nanoTime();
            //Max 100 events will be there that should not take more than 100sec ie 1-2 min....
            Log.d(TAG, "Started Calendar clean.....");
            if ((ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED)
                    & (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)) {

                String selection = "((" + "calendar_id" + " = " + GenericConstants.CALENDAR_ID + ")  AND ( deleted != 1 ) )";

                Uri eventUri = Uri.parse("content://com.android.calendar/events");
                Cursor cursor = context.getContentResolver().query(eventUri, new String[]{"_id", "calendar_id", "title", "description"}, selection, null, null); // calendar_id can change in new versions
                while (cursor.moveToNext()) {
                    Integer eventId = cursor.getInt(0);
                    int count = cursor.getColumnCount();
                    StringBuilder sb = new StringBuilder("column: ");
                    for (int i = 0; i < count; i++) {
                        sb.append(cursor.getString(i) + ", ");
                    }
                    Log.d(TAG, sb.toString());
                    //String title = cursor.getString(16);
                    //Log.d(TAG, "Clearing " + eventId);
                    Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, Long.parseLong(String.valueOf(eventId)));
                    int rows = context.getContentResolver().delete(deleteUri, null, null);
                    Log.d(TAG, "Event Deleted:" + rows);
                }
                long endTime = System.nanoTime();
                long durationInNano = (endTime - startTime);  //Total execution time in nano seconds
                long durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);  //Total execution time in nano seconds
                Log.d(TAG, "Time took to clean the calendar:" + durationInMillis + " ms");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String buildTitle(CaseObj caseObj) {
        String title = caseObj.getTypeName() + "/" + caseObj.getRegNo() + "/" + caseObj.getRegYear() + "/" + caseObj.getPurposeName();
        return title;
    }

    private String buildDesc(CaseObj caseObj) {
        String name = caseObj.getContactname() != null ? caseObj.getContactname() : "";
        String desc = name + " " + caseObj.getPetName() + " VS " + caseObj.getResName() + "/" + caseObj.getTypeName() + "/" + caseObj.getDesgname().replaceAll("Â", "") + " ," + caseObj.getCourtName() + " \n \n Last Business:" + caseObj.getWritinfo();
        return desc;
    }

    public void sendNextHearingDateSMS(Context context, CaseObj casedetails) {
        boolean smsstatus = false;
        String smstext = "";
        try {
            if (!StringUtils.isEmpty(casedetails.getPhone())) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sendalert = prefs.getBoolean("sendsms_alerts", false);
                    boolean send_hearing_alert = prefs.getBoolean("sendhearing_alerts", false);
                    smstext = prefs.getString("smstemplate", "");
                    if (sendalert && send_hearing_alert) {
                        smsstatus = true;
                    }
                }
                if (smsstatus) {
                    sendSMS(casedetails.getPhone(), getSMSTemplate(smstext, casedetails, SMSTYPE.NEXT));
                    //sendSMSUsingBR(context,casedetails.getPhone(), smstext_temp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getSMSTemplate(String smstext, CaseObj casedetails, SMSTYPE smstype) {
        String date = "";

        if (smstype == SMSTYPE.NEXT) {
            date = sdf.format(casedetails.getDateNextList());
        } else if (smstype == SMSTYPE.NOTI) {
            date = sdf.format(casedetails.getDateNextList()) + "(" + deriveTodayOrTomorrow() + ")";
        } else {
            date = sdf.format(casedetails.getDateNextList());
        }
        String smstext_temp = smstext.replaceAll("@reg_no@", casedetails.getRegNo()).replaceAll("@case_type@", casedetails.getTypeName())
                .replaceAll("@reg_year@", casedetails.getRegYear())
                .replaceAll("@pet_name@", casedetails.getPetName())
                .replaceAll("@res_name@", casedetails.getResName()).replaceAll("@court@", casedetails.getDesgname())
                .replaceAll("@business@", casedetails.getWritinfo() != null ? casedetails.getWritinfo() : "N/A")
                .replaceAll("@purpose@", casedetails.getPurposeName())
                .replaceAll("@next_hearing@", date).replaceAll("  ", " ");
//        if (smstype == SMSTYPE.NEXT) {
//            smstext_temp = "UPDATE! " + smstext_temp;
//        } else if (smstype == SMSTYPE.NOTI) {
//            smstext_temp = "REMINDER! " + smstext_temp;
//        }

        return smstext_temp;
    }

    public String deriveTodayOrTomorrow() {
        Calendar now = Calendar.getInstance();
        int currentHour = now.get(Calendar.HOUR_OF_DAY);
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        boolean today = true;
        if (currentHour >= 18 && currentHour <= 24) {
            today = false;
        }
        String text = "";
        if (today == false) {
            text = "Tomorrow";
        } else {
            text = "Today";
        }
        return text;
    }

    public void sendSMS(String phoneNo, String msg) {
        //Split logic to be implemented because of the length of the message.
        try {
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> parts = smsManager.divideMessage(msg);
            smsManager.sendMultipartTextMessage(phoneNo, null, parts, null, null);
            //smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendSMSUsingBR(Context context, String phoneNo, String msg) {
        //
        Intent intent = new Intent();
        intent.setAction("com.casesync.SMSBROADCAST");
        intent.putExtra(Intent.EXTRA_TITLE, phoneNo);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(intent);

    }

    public long addAppointmentsToCalender(Context context, String title,
                                          String desc, String place, int status, Date startDate,
                                          boolean needReminder, boolean needMailService) {
/***************** Event: add event *******************/
        //TODO Modify the email alert logic do only if there is email configured and test it.
        long eventID = -1;
        try {
            String eventUriString = "content://com.android.calendar/events";
            ContentValues eventValues = new ContentValues();
            //Keep the calendar_id in preferences. So it can be fetched and used in multiple places.
            eventValues.put("calendar_id", 1); // id, We need to choose from
            // our mobile for primary its 1
            eventValues.put("title", title);
            eventValues.put("description", desc);
            eventValues.put("eventLocation", place);
            eventValues.put("allDay", true);
            long start_date = convertDateForCalendar(startDate, context);
            long endDate = start_date + 1000 * 10 * 180; // For next 180 min
            eventValues.put("dtstart", start_date);
            eventValues.put("dtend", endDate);

            // values.put("allDay", 1); //If it is bithday alarm or such
            // kind (which should remind me for whole day) 0 for false, 1
            // for true
            eventValues.put("eventStatus", status); // This information is
            // sufficient for most
            // entries tentative (0),
            // confirmed (1) or canceled
            // (2):
            eventValues.put("eventTimezone", "UTC/GMT +5:30");
            /*
             * Comment below visibility and transparency column to avoid
             * java.lang.IllegalArgumentException column visibility is invalid
             * error
             */
            // eventValues.put("allDay", 1);
            // eventValues.put("visibility", 0); // visibility to default (0),
            // confidential (1), private
            // (2), or public (3):
            // eventValues.put("transparency", 0); // You can control whether
            // an event consumes time
            // opaque (0) or transparent (1).

            eventValues.put("hasAlarm", 1); // 0 for false, 1 for true

            Uri eventUri = context
                    .getContentResolver()
                    .insert(Uri.parse(eventUriString), eventValues);
            eventID = Long.parseLong(eventUri.getLastPathSegment());

            if (needReminder) {
                /***************** Event: Reminder(with alert) Adding reminder to event ***********        ********/

                String reminderUriString = "content://com.android.calendar/reminders";
                ContentValues reminderValues = new ContentValues();
                reminderValues.put("event_id", eventID);
                //TODO Change it
                reminderValues.put("minutes", 5); // Default value of the
                // system. Minutes is a integer
                reminderValues.put("method", 1); // Alert Methods: Default(0),
                // Alert(1), Email(2),SMS(3)

                Uri reminderUri = context
                        .getContentResolver()
                        .insert(Uri.parse(reminderUriString), reminderValues);
            }

/***************** Event: Meeting(without alert) Adding Attendies to the meeting *******************/

            if (needMailService) {
                String attendeuesesUriString = "content://com.android.calendar/attendees";
                /********
                 * To add multiple attendees need to insert ContentValues
                 * multiple times
                 ***********/
                ContentValues attendeesValues = new ContentValues();
                attendeesValues.put("event_id", eventID);
                //TODO Change it.
                attendeesValues.put("attendeeName", "xxxxx"); // Attendees name
                attendeesValues.put("attendeeEmail", "yyyy@gmail.com");// Attendee Email
                attendeesValues.put("attendeeRelationship", 0); // Relationship_Attendee(1),
                // Relationship_None(0),
                // Organizer(2),
                // Performer(3),
                // Speaker(4)
                attendeesValues.put("attendeeType", 0); // None(0), Optional(1),
                // Required(2),
                // Resource(3)
                attendeesValues.put("attendeeStatus", 0); // NOne(0),
                // Accepted(1),
                // Decline(2),
                // Invited(3),
                // Tentative(4)

                Uri eventsUri = Uri.parse("content://calendar/events");
                Uri url = context
                        .getContentResolver()
                        .insert(eventsUri, attendeesValues);

                // Uri attendeuesesUri = curActivity.getApplicationContext()
                // .getContentResolver()
                // .insert(Uri.parse(attendeuesesUriString), attendeesValues);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Error in adding event on calendar" + ex.getMessage());
        }

        return eventID;

    }

    public enum TYPE {
        FULL, GET
    }

    public enum NOTITYPE {
        CASE, GENERAL
    }

    public enum JOBTYPE {
        NOTIFICATION
    }

    public enum QUERY_TYPE {
        TOTAL, PENDING, TOTAL_NEDIS, CURRENT_DATE, DISTINCT_DT, TOTAL_DIS, SYNC_PENDING, FILING_HIS
    }

    public enum SMSTYPE {
        NEXT, NOTI
    }

    public static class HearingDateComparator implements Comparator<CaseObj> {

        public int compare(CaseObj c1, CaseObj c2) {
            if (c1.getDateNextList() == null) {
                return (c2.getCourtName() == null) ? 0 : -1;
            }
            if (c2.getDateNextList() == null) {
                return 1;
            }
            return c1.getDateNextList().compareTo(c2.getDateNextList());
        }
    }

    public static class CourtNameComparator implements Comparator<CaseObj> {

        public int compare(CaseObj c1, CaseObj c2) {
            if (c1.getCourtName() == null) {
                return (c2.getCourtName() == null) ? 0 : -1;
            }
            if (c2.getCourtName() == null) {
                return 1;
            }
            return c1.getCourtName().compareTo(c2.getCourtName());
        }
    }

    public static class StateNameComparator implements Comparator<CaseObj> {

        public int compare(CaseObj c1, CaseObj c2) {
            if (c1.getStateName() == null) {
                return (c2.getStateName() == null) ? 0 : -1;
            }
            if (c2.getStateName() == null) {
                return 1;
            }
            return c1.getStateName().compareTo(c2.getStateName());
        }
    }

    public static class DistrictNameComparator implements Comparator<CaseObj> {

        public int compare(CaseObj c1, CaseObj c2) {
            if (c1.getDistrictName() == null) {
                return (c2.getDistrictName() == null) ? 0 : -1;
            }
            if (c2.getDistrictName() == null) {
                return 1;
            }
            return c1.getDistrictName().compareTo(c2.getDistrictName());
        }
    }

}
