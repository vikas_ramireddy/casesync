package com.casesync.casesync;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.casesync.casesync.Fragment.OrderFragment;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Encryption;
import com.casesync.utill.LoginParams;
import com.casesync.utill.OrderDetails;
import com.casesync.utill.PropertyUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.scottyab.aescrypt.AESCrypt;

import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.List;


public class CasedetailsTab extends AppCompatActivity {

    private static final String TAG = CasedetailsTab.class.getSimpleName();

    String casenum = "";
    String disposed = "";
    String caller = "";
    String filter = "";
    CasesyncUtill util = new CasesyncUtill();
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Tracker mTracker;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casedetails_tab);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        setTitle("Case details");
        Bundle bundle = getIntent().getExtras();
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        if (bundle != null) {
            //Changed to CINNO
            String casenumber = bundle.getString("casenumber");
            casenum = casenumber;
            disposed = bundle.getString("disposed");
            filter = bundle.getString("filter", null);
            caller = getIntent().getStringExtra("caller");
            if (caller != null) {
                if (caller.equals("AddCase"))
                    sendScreenName("Added Case successfully " + casenumber);
            }


        }


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_attachment_white_18dp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_casedetails_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                AlertDialog.Builder loginalert = new AlertDialog.Builder(CasedetailsTab.this);
                loginalert.setMessage("Deleting the case");
                loginalert.setTitle("Are you sure ?");
                loginalert.setPositiveButton("OK", null);
                loginalert.setNegativeButton("CANCEL", null);
                loginalert.setCancelable(true);
                loginalert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                                            CaseSyncOpenDatabaseHelper.class);
                                    Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
                                    LoginParams loginparams = new LoginParams();
                                    List<CaseObj> todos = casedetDao.queryForEq("cino", casenum);
                                    if (!CollectionUtils.isEmpty(todos)) {
                                        boolean result = FirebaseUtil.deleteuseraccount(todos.get(0));
                                        if (result) {
                                            /*Delete from the local db*/
                                            DeleteBuilder<CaseObj, Long> deleteBuilder = casedetDao.deleteBuilder();
                                            deleteBuilder.where().eq("cino", todos.get(0).getCino());
                                            deleteBuilder.delete();

                                            Intent intent = new Intent(CasedetailsTab.this, CustomListView.class);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            }
                                            startActivity(intent);
                                            finish();

                                            Snackbar.make(getWindow().getDecorView().getRootView(), "Case deleted sucessfully  ", Snackbar.LENGTH_LONG)
                                                    .setAction("No action", null).show();
                                        } else {
                                            Snackbar.make(getWindow().getDecorView().getRootView(), "Case deletion is a failure ", Snackbar.LENGTH_LONG)
                                                    .setAction("No action", null).show();
                                        }
                                    } else {
                                        Snackbar.make(getWindow().getDecorView().getRootView(), "Case deletion is a failure ", Snackbar.LENGTH_LONG)
                                                .setAction("No action", null).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("ERROR", "" + e.getMessage());
                                }
                            }
                        });
                loginalert.setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.e("CASELIST", "DeleteCancel :");
                                dialog.cancel();
                            }
                        });
                loginalert.create().show();
            } else {
                AlertDialog.Builder loginalert = new AlertDialog.Builder(CasedetailsTab.this);
                loginalert.setMessage("Internet is required while deleting a case ");
                loginalert.setTitle("Please turn on the internet");
                loginalert.setPositiveButton("OK", null);
                loginalert.setCancelable(true);
                loginalert.create().show();
                loginalert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
            return true;
        } else if (id == R.id.search) {
            FragmentManager fm = getSupportFragmentManager();
            AdvancedSearchDialogFragment searchDialogFragment = AdvancedSearchDialogFragment.newInstance("Advanced Search");
            searchDialogFragment.show(fm, "fragment_advanced_search");
        } else if (id == android.R.id.home) {
            try {
                if (caller != null) {
                    if (caller.equals("CustomListView")) {

                        Intent intent = new Intent(CasedetailsTab.this, CustomListView.class);
                        intent.putExtra("disposed", disposed);
                        if (filter != null) {
                            intent.putExtra("filter", filter);
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();
                    } else if (caller.equals("AddCase")) {

                        Intent intent = new Intent(CasedetailsTab.this, AddCase.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(CasedetailsTab.this, NavActivity.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();

                    }
                } else {
                    Intent intent = new Intent(CasedetailsTab.this, NavActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(intent);
                    finish();

                }

            } catch (Exception e) {
                Log.e("CASEDET", e.getMessage());
            }
        } else if (id == R.id.action_share) {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<CaseObj, Long> caseDao = null;
            try {
                caseDao = todoOpenDatabaseHelper.getCaseObjdao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            List<CaseObj> todos = null;
            try {
                todos = caseDao.queryForEq("cino", casenum);
                if (!CollectionUtils.isEmpty(todos)) {
                    CaseObj casedetails = todos.get(0);
                    String casnum = AESCrypt.encrypt(Encryption.password, casedetails.getCino());
                    String buildurl = PropertyUtil.getProperty("sitename", getApplicationContext()) + "/addcase?caseno=" + casnum;
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Add Case");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "" + buildurl);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (GeneralSecurityException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else if (id == R.id.action_download) {
            try {
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
                Log.i(TAG, "CINO:" + casenum);
                List<CaseObj> casedet = casedetDao.queryForEq("cino", casenum);
                if (!CollectionUtils.isEmpty(casedet)) {
                    CaseObj casedetails = casedet.get(0);
                    if (casedetails.getInterimOrder() == null && casedetails.getFinalOrder() == null) {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "No Orders available ", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    } else {
                        FragmentManager fm = getSupportFragmentManager();
                        OrderFragment orderDialogFragment = OrderFragment.newInstance(casenum, "");
                        orderDialogFragment.show(fm, "fragment_orderview");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


//        else if (id == R.id.action_download) {
//            try {
//                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
//                        CaseSyncOpenDatabaseHelper.class);
//                Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
//                Log.i(TAG, "CINO:" + casenum);
//                List<CaseObj> casedet = casedetDao.queryForEq("cino", casenum);
//                if (!CollectionUtils.isEmpty(casedet)) {
//                    CaseObj casedetails = casedet.get(0);
//                    if (casedetails.getInterimOrder() == null && casedetails.getFinalOrder() == null) {
//                        Snackbar.make(getWindow().getDecorView().getRootView(), "No Orders available ", Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();
//                    } else {
//                        //TODO Download from here
////                        List<OrderDetails> orderlist = new OrderFragment().getOrderList(casedetails);
////                        if (!CollectionUtils.isEmpty(orderlist)) {
////                            Log.i(TAG, "OrderURL:" + orderlist.get(0).getUrl());
////                            Snackbar.make(getWindow().getDecorView().getRootView(), orderlist.get(0).getUrl(), Snackbar.LENGTH_LONG)
////                                    .setAction("No action", null).show();
////
////                        } else {
////                            Snackbar.make(getWindow().getDecorView().getRootView(), "No Orders available ", Snackbar.LENGTH_LONG)
////                                    .setAction("No action", null).show();
////                        }
//
////                        FragmentManager fm = getSupportFragmentManager();
////                        OrderFragment orderDialogFragment = OrderFragment.newInstance(casenum, "");
////                        orderDialogFragment.show(fm, "fragment_orderview");
//
//
//                        ConnectivityManager connMgr = (ConnectivityManager)
//                                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//                        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//                        if (networkInfo != null && networkInfo.isConnected()) {
//                            boolean status = false;
//                            boolean status1 = false;
//
//                            status = new NavActivity().askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0, getApplicationContext(), CasedetailsTab.this);
//                            status1 = new NavActivity().askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0, getApplicationContext(), CasedetailsTab.this);
//                            if (status && status1) {
//                                //Download the judge
//                                Log.i(TAG, "casenum:" + casenum);
//
//                                DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//                                Uri uri = null;
//                                try {
//                                    List<OrderDetails> orderDetails = getOrderList(casedetails);
//                                    if (!CollectionUtils.isEmpty(orderDetails)) {
//                                        for (OrderDetails order : orderDetails) {
//                                            if (order.getOrderno() != null) {
//                                                uri = Uri.parse(order.getUrl());
//                                                Log.i(TAG, "URL:" + order.getUrl());
//                                                //http://app.ecourts.gov.in/ecourt_mobile_encrypted_DC/display_pdf.php?filename=/orders/2018/203200000252018_1.pdf
//                                                // &caseno=MC/0000025/2018&cCode=2&appFlag=&state_cd=2&dist_cd=15&court_code=2
//                                                DownloadManager.Request request = new DownloadManager.Request(uri);
//                                                request.setTitle(casenum + "_order.pdf");
//                                                request.setDescription("Downloading");
//                                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                                                request.setVisibleInDownloadsUi(false);
//
//                                                Storage storage = null;
//                                                if (SimpleStorage.isExternalStorageWritable()) {
//                                                    storage = SimpleStorage.getExternalStorage();
//                                                } else {
//                                                    storage = SimpleStorage.getInternalStorage(getApplicationContext());
//                                                }
//                                                boolean dirExists = storage.isDirectoryExists("casesync");
//                                                Log.d("More", "Storage: dirExists" + dirExists);
//                                                if (dirExists == false) {
//                                                    storage.createDirectory("casesync");
//                                                    Log.d("More", "dirExists:");
//                                                }
//                                                boolean subdirExists = storage.isDirectoryExists("casesync/export");
//                                                if (subdirExists == false) {
//                                                    Log.d("More", "subdirExists:");
//                                                    storage.createDirectory("casesync/export");
//                                                }
//                                                request.setDestinationInExternalPublicDir("/casesync", casenum + "_order.pdf");
//                                                downloadmanager.enqueue(request);
//                                            }
//                                        }
//                                    }
//
//                                } catch (SecurityException e) {
//                                    Snackbar.make(getWindow().getDecorView().getRootView(), "Required permissions are not available ", Snackbar.LENGTH_SHORT)
//                                            .setAction("No action", null).show();
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                Snackbar.make(getWindow().getDecorView().getRootView(), "Required permissions are not available ", Snackbar.LENGTH_SHORT)
//                                        .setAction("No action", null).show();
//                            }
//                        } else {
//                            AlertDialog.Builder loginalert = new AlertDialog.Builder(getApplicationContext());
//                            loginalert.setMessage("Internet is required");
//                            loginalert.setTitle("Please turn on the internet");
//                            loginalert.setPositiveButton("OK", null);
//                            loginalert.setCancelable(true);
//                            loginalert.create().show();
//                            loginalert.setPositiveButton("Ok",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//
//                                        }
//                                    });
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        else if (id == R.id.action_download) {
//
//            ConnectivityManager connMgr = (ConnectivityManager)
//                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//            if (networkInfo != null && networkInfo.isConnected()) {
//                boolean status = false;
//                boolean status1 = false;
//                status = new NavActivity().askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0, getApplicationContext(), CasedetailsTab.this);
//                status1 = new NavActivity().askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0, getApplicationContext(), CasedetailsTab.this);
//                if (status && status1) {
//                    //Download the judge
//                    Log.i(TAG, "casenum:" + casenum);
//                    DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//                    Uri uri = null;
//                    try {
//                        List<CaseObj> todos = CasesyncUtill.getCaseObjdao(getApplicationContext()).queryForEq("cino", casenum);
//                        if (!CollectionUtils.isEmpty(todos)) {
//                            CaseObj obj = todos.get(0);
//                            //TODO Display both interim and final order in the case view it self
//                            if (obj.getArchive().equalsIgnoreCase("Y")) {
////                                final String URL = PropertyUtil.getProperty("apiurl", getApplicationContext()) +
////                                        "ecourt_mobile_encrypted_DC/display_pdf.php?filename=/orders/" + obj.getRegYear()
////                                        + "/" + obj.getCaseNo() + "_1.pdf&caseno=" + obj.getTypeName()
////                                        + "/" + obj.getCaseNo().substring(4, 11) + "/" + obj.getRegYear() +
////                                        "&cCode=" + obj.getCourtno() + "&appFlag=&state_cd=" + obj.getStateCode() +
////                                        "&dist_cd=" + obj.getDistrictCode() + "&court_code=" + obj.getCourtno();
//                                String URL = getOrderURL(obj);
//                                uri = Uri.parse(URL);
//                                Log.i(TAG, "URL:" + URL);
//                                //http://app.ecourts.gov.in/ecourt_mobile_encrypted_DC/display_pdf.php?filename=/orders/2018/203200000252018_1.pdf
//                                // &caseno=MC/0000025/2018&cCode=2&appFlag=&state_cd=2&dist_cd=15&court_code=2
//                                DownloadManager.Request request = new DownloadManager.Request(uri);
//                                request.setTitle(casenum + "_order.pdf");
//                                request.setDescription("Downloading");
//                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                                request.setVisibleInDownloadsUi(false);
//
//                                Storage storage = null;
//                                if (SimpleStorage.isExternalStorageWritable()) {
//                                    storage = SimpleStorage.getExternalStorage();
//                                } else {
//                                    storage = SimpleStorage.getInternalStorage(getApplicationContext());
//                                }
//                                boolean dirExists = storage.isDirectoryExists("casesync");
//                                Log.d("More", "Storage: dirExists" + dirExists);
//                                if (dirExists == false) {
//                                    storage.createDirectory("casesync");
//                                    Log.d("More", "dirExists:");
//                                }
//                                boolean subdirExists = storage.isDirectoryExists("casesync/export");
//                                if (subdirExists == false) {
//                                    Log.d("More", "subdirExists:");
//                                    storage.createDirectory("casesync/export");
//                                }
//                                //request.setDestinationUri(Uri.parse("file://casesync/export"));
//                                request.setDestinationInExternalPublicDir("/casesync", casenum + "_order.pdf");
//                                downloadmanager.enqueue(request);
//
//                            } else {
//                                Snackbar.make(getWindow().getDecorView().getRootView(), "Order/Judgement won't be available for active cases", Snackbar.LENGTH_SHORT)
//                                        .setAction("No action", null).show();
//                            }
//                        } else {
//                            Log.i(TAG, "No cases are available");
//                        }
//                    } catch (SecurityException e) {
//                        Snackbar.make(getWindow().getDecorView().getRootView(), "Required permissions are not available ", Snackbar.LENGTH_SHORT)
//                                .setAction("No action", null).show();
//                    } catch (SQLException e) {
//                        e.printStackTrace();
//                    }
//
//
//                } else {
//                    Snackbar.make(getWindow().getDecorView().getRootView(), "Required permissions are not available ", Snackbar.LENGTH_SHORT)
//                            .setAction("No action", null).show();
//                }
//            } else {
//                AlertDialog.Builder loginalert = new AlertDialog.Builder(getApplicationContext());
//                loginalert.setMessage("Internet is required");
//                loginalert.setTitle("Please turn on the internet");
//                loginalert.setPositiveButton("OK", null);
//                loginalert.setCancelable(true);
//                loginalert.create().show();
//                loginalert.setPositiveButton("Ok",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//            }
//        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//        /**
//         * The fragment argument representing the section number for this
//         * fragment.
//         */
//        private static final String ARG_SECTION_NUMBER = "section_number";
//
//        public PlaceholderFragment() {
//        }
//
//        /**
//         * Returns a new instance of this fragment for the given section
//         * number.
//         */
//        public static PlaceholderFragment newInstance(int sectionNumber) {
//            PlaceholderFragment fragment = new PlaceholderFragment();
//            Bundle args = new Bundle();
//            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//            fragment.setArguments(args);
//            return fragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_casedetails_tab, container, false);
//            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
//            return rootView;
//        }
//    }
    @Override
    public void onBackPressed() {
        try {

            if (caller.equals("CustomListView")) {

                Intent intent = new Intent(CasedetailsTab.this, CustomListView.class);
                intent.putExtra("disposed", disposed);
                if (filter != null) {
                    intent.putExtra("filter", filter);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } else if (caller.equals("AddCase")) {

                Intent intent = new Intent(CasedetailsTab.this, AddCase.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } else if (caller.equals("Order")) {
                Intent intent = new Intent(CasedetailsTab.this, OrderActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(CasedetailsTab.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();

            }
        } catch (Exception e) {
            Log.e("CASEDET", e.getMessage());
        }

    }

    public String getcasenumber() {
        return casenum;
    }

    private void sendScreenName(String activity) {

        // [START screen_view_hit]

        mTracker.setScreenName("ADD~" + activity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        // [END screen_view_hit]
    }

    public String getOrderURL(CaseObj casedetails) {
        String uri = "";
        List<OrderDetails> orderlist = util.getOrderList(casedetails);
        if (orderlist != null) {
            for (OrderDetails order : orderlist) {
                if (order.getOrderno() != null) {
                    uri = order.getUrl();
                }
            }
        }
        return uri;
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position) {
                case 0:
                    CaseDet tab1 = new CaseDet();
                    return tab1;
                case 1:
                    History tab2 = new History();
                    return tab2;
                case 2:
                    MoreDetails tab3 = new MoreDetails();
                    return tab3;
                case 3:
                    AttachmentFragment tab4 = new AttachmentFragment();
                    return tab4;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Details";
                case 1:
                    return "History";
                case 2:
                    return "More..";
                case 3:
                    return "";
            }
            return null;
        }
    }
}
