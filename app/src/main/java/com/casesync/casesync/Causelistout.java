package com.casesync.casesync;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.CauseListUtil;
import com.casesync.utill.FileUtility;
import com.casesync.utill.PdfViewCustom;
import com.casesync.utill.PermissionUtility;
import com.webviewtopdf.PdfView;

import java.io.File;

public class Causelistout extends AppCompatActivity {

    private static final String TAG = "CLO";
    private static final String AUTHORITY = "com.casesync.fileprovider";
    WebView webViewtemp;
    String cl_date = "";
    CasesyncUtill utill = new CasesyncUtill();
    PermissionUtility permissionUtility = new PermissionUtility();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_causelistout);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Intent i = getIntent();
        String html = (String) i.getSerializableExtra("html");
        cl_date = (String) i.getSerializableExtra("cldate");
        WebView webView = findViewById(R.id.causelist_view);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(false);
        webSettings.setBuiltInZoomControls(true);
        //webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        //webSettings.setLoadsImagesAutomatically(true);
        webView.setWebViewClient(new WebViewClient());
        //webView.setWebChromeClient(new WebChromeClient());
        webView.loadData(Uri.encode(html), "text/html", "UTF-8");
        //webView.loadDataWithBaseURL(null, Uri.encode(html), null, "UTF-8", null);
        webViewtemp = webView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.causelist_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.download_cl) {
//            if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
//                    & (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
//                utill.createStorageIfNA(getApplicationContext());
//
//                String filepath = "storage/emulated/0/casesync/export/";
//                String filename = "causelist_" + cl_date + ".pdf";
//                File directory = new File(filepath);
//                File existingfile = new File(filepath + filename);
//                if (existingfile.exists()) {
//                    existingfile.delete();
//                    Log.i(TAG, "File deleted");
//                }
//
//                final ProgressDialog progressDialog = new ProgressDialog(Causelistout.this);
//                progressDialog.setMessage("Please wait");
//                progressDialog.show();
//                PdfViewCustom.createWebPrintJob(Causelistout.this, webViewtemp, directory, filename, new PdfView.Callback() {
//
//                    @Override
//                    public void success(String path) {
//                        progressDialog.dismiss();
//                        //PdfViewCustom.openPdfFile(Causelistout.this, getString(R.string.app_name), "Do you want to open the pdf file?" + filename, path);
//                        File file = new File(filepath);
//                        Uri uri = FileProvider.getUriForFile(getApplicationContext(), "com.casesync.fileprovider", file);
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setDataAndType(uri, "resource/folder");
//                        if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
//                            startActivity(intent);
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Unable to open the folder", Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//
//                    @Override
//                    public void failure() {
//                        progressDialog.dismiss();
//
//                    }
//                });
//            } else {
//                Toast.makeText(getApplicationContext(), "No permissions available to read/write a file", Toast.LENGTH_LONG).show();
//            }
//
//
//            return true;
//        }

        if (id == R.id.share_cl) {
            permissionUtility.checkForFilePermissions(getApplicationContext(), Causelistout.this);
            boolean status = permissionUtility.validateFilePermissions(getApplicationContext());
            if(status)
            {
                String filepath = FileUtility.getSubFolderPath(getApplicationContext(), FileUtility.ACTIONTYPE.EXPORT, null);
                String filename = "causelist_" + cl_date + ".pdf";
                FileUtility.deleteExistingFile(getApplicationContext(), filepath + filename);
                File directory = new File(filepath);
                final ProgressDialog progressDialog = new ProgressDialog(Causelistout.this);
                progressDialog.setMessage("Please wait");
                progressDialog.show();
                PdfViewCustom.createWebPrintJob(Causelistout.this, webViewtemp, directory, filename, new PdfView.Callback() {

                    @Override
                    public void success(String path) {
                        progressDialog.dismiss();
                        try {
                            //TODO Do the permission testing in different android versions.
                            new CauseListUtil().shareIntent(getApplicationContext(), new File(path), Causelistout.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure() {
                        progressDialog.dismiss();

                    }
                });
            }else
            {
                Toast.makeText(getApplicationContext(), "No permissions are available to read/write a file.", Toast.LENGTH_LONG).show();
            }
            return true;
        } else {
            Intent intent = new Intent(Causelistout.this, Causelist.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
            return true;
        }

    }


    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            Intent intent = new Intent(Causelistout.this, Causelist.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }
    }


}