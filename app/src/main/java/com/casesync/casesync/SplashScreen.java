package com.casesync.casesync;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.casesync.casesync.jobs.AutoSync;
import com.casesync.casesync.jobs.EvernoteJobCreator;
import com.casesync.casesync.jobs.NotificationJob;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Casehistory;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.FileUtility;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PermissionUtility;
import com.casesync.utill.PropertyUtil;
import com.evernote.android.job.JobManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;


public class SplashScreen extends AppCompatActivity {
    private static final String PREFS_NAME = "LoginPrefs";
    private static final int MY_CAL_WRITE_REQ = 0;
    private static int SPLASH_TIME_OUT = 3000;
    public String TAG = "Splash";
    String USER_AGENT = "Mozilla/21.0";
    ErrorMessage errorMessage = new ErrorMessage();
    private Tracker mTracker;
    private View mProgressView;


    public static boolean setProperty(Context context) throws IOException {
        AssetManager am = context.getAssets();
        Properties pp = new Properties();
        InputStream isConfig = am.open("casesync.properties", Context.MODE_PRIVATE);
        pp.load(isConfig);
        pp.setProperty("SHOP_URL", "NEW_SHOP_URL");//This key exists

        try {
            pp.store(new FileOutputStream("casesync.properties"), null);
            Log.e("PROP", "PROP:" + PropertyUtil.getProperty("SHOP_URL", context));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("PROP", "ERROR:" + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("PROP", "ERROR:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PROP", "ERROR:" + e.getMessage());
        }
        return false;
    }

    public static int compareVersionNames(String oldVersionName, String newVersionName) {
        int res = 0;
        Log.e("SPL", oldVersionName + ":" + newVersionName);
        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");

        // To avoid IndexOutOfBounds
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);

        for (int i = 0; i < maxIndex; i++) {
            int oldVersionPart = Integer.valueOf(oldNumbers[i]);
            int newVersionPart = Integer.valueOf(newNumbers[i]);

            if (oldVersionPart < newVersionPart) {
                res = -1;
                break;
            } else if (oldVersionPart > newVersionPart) {
                res = 1;
                break;
            }
        }

        // If versions are the same so far, but they have different length...
        if (res == 0 && oldNumbers.length != newNumbers.length) {
            res = (oldNumbers.length > newNumbers.length) ? 1 : -1;
        }

        return res;
    }

    private static PackageInfo getPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String getAppVersionCode(Context context) {
        PackageInfo packageInfo = getPackageInfo(context);
        return packageInfo.versionName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            mTracker = application.getDefaultTracker();
            CasesyncUtill.initDefaultSharedPreferences(getApplicationContext());
            SharedPreferences pref = getSharedPreferences(PREFS_NAME, 0);
            if (pref.getString("email", "") != null && !pref.getString("email", "").equals("")) {
                SPLASH_TIME_OUT = 100;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error" + e.getMessage());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences pref = getSharedPreferences(PREFS_NAME, 0);
                String email = pref.getString("logged", "");
                String username = pref.getString("email", "");
                String password = pref.getString("password", "");
                if (email.equals("")) {
                    sendScreenName("New Sign in ");
                    Intent i = new Intent(SplashScreen.this, Welcome.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(i);
                    finish();

                } else {
                    mProgressView = findViewById(R.id.progressBar3);
                    sendScreenName("username:" + email + " Logged in successfully");
                    LoginParams loginparams = new LoginParams();
                    try {
                        boolean permissions = new PermissionUtility().validateFilePermissions(getApplicationContext());
                        if (permissions) {
                            new FileUtility().performMigrate(getApplicationContext());
                        }
                        JobManager.create(getApplicationContext()).addJobCreator(new EvernoteJobCreator());
                        AutoSync.scheduleJob(getApplicationContext());
                        //Job is working
                        NotificationJob.schedule(getApplicationContext());
                        //If a notifications are already generated for a particular day don't generate it
                        //For creating todays notifications
                        //new NotificationJobIntentService().createNotifications(getApplicationContext());
                        //NotificationJob.schedule(getApplicationContext());

                        Intent i = new Intent(SplashScreen.this, NavActivity.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(i);
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void sendScreenName(String activity) {
        Log.d(TAG, "Setting screen name: " + activity);
        mTracker.setScreenName("Splash~" + activity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /*New asyn task class to care of Onload Update*/
    /*private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        private LoginParams parms;
        public AsyncResponse delegate = null;

        // a constructor so that you can pass the object and use
        public AsyncTaskRunner(LoginParams parms) {
            this.parms = parms;
        }

        private String resp;
        private String token;


        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()

            try {
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.add("User-Agent", USER_AGENT);
                HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
                RestTemplate restTemplate = new RestTemplate();

                ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(20000);
                ResponseEntity rssResponse = restTemplate.exchange(parms.getUrl(),
                        HttpMethod.GET,
                        requestEntity,
                        String.class);

                //Log.e(TAG,"Response TEST:"+rssResponse.getBody());
                if (rssResponse.getStatusCode().value() == 200) {
                    resp = rssResponse.getBody().toString();
                    // Log.e(TAG,"Response:"+rssResponse.getStatusCode().value());
                } else {
                    Log.e(TAG, "Response:" + rssResponse.getStatusCode().value());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            String response = result;

            // Log.e("LOGIN  ", "Length:" + array.length);
            int versionresult = 0;
            String appurl = "";
            try {
                if (response != null) {
                    String array[] = response.split(Pattern.quote("|"));

                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = settings.edit();
                    for (int count = 0; count < array.length; count++) {
                        // Log.e("LOGIN  ", "response:" + array[count]);
                        if (array[count].contains("appversion")) {

                            //String version[]=array[count].split(Pattern.quote("="));
                            String version[] = array[count].split(Pattern.quote("="));
                            //Old logic
//                            Version a = new Version(version[1]);
//                            Version b = new Version(getVersion(getBaseContext()));
//                            Log.e(TAG,"a version"+a +" b version:"+b);
//                            if(a.compareTo(b)==1)
//                            {
//                                Log.e(TAG,"Less version");
//                                versionresult=1;
//                            }else if(a.compareTo(b)==0)
//                            {
//                                Log.e(TAG,"Same version");
//                                versionresult=0;
//                            }else
//                            {
//                                versionresult=-1;
//                                Log.e(TAG,"Greater version");
//                            }
                            String oldversion = getAppVersionCode(getApplicationContext());
                            int versiontemp = SplashScreen.compareVersionNames(oldversion, version[1]);
                            if (versiontemp == 1) {
                                Log.e(TAG, "Future version");
                            } else if (versiontemp == -1) {
                                Log.e(TAG, "Need to upgrade");
                                versionresult = 1;
                            } else {
                                Log.e(TAG, "Same version");
                            }


                        } else if (array[count].contains("appurl")) {//Change the .quote if it is URL
                            String url[] = array[count].split(Pattern.quote("="));
                            appurl = url[1];
                        } else {
                            //setProperty(getApplicationContext());
                        }
                    }
                    mProgressView.setVisibility(View.GONE);
                    if (versionresult == 0) {
                        Toast.makeText(getApplicationContext(), "Logging in",
                                Toast.LENGTH_SHORT).show();

                        String caseurl = PropertyUtil.getProperty("domainname", getApplicationContext()) + "actions/gettoken?email=" + parms.getUsername();
                        Log.e(TAG, "URL:" + caseurl);
                        Log.e(TAG, parms.getUsername() + ":" + parms.getPassword());
                        LoginParams loginparams = new LoginParams();
                        loginparams.setUrl(caseurl);
                        loginparams.setUsername(parms.getUsername());
                        loginparams.setPassword(parms.getPassword());
                        CasesyncUtill syncutil = new CasesyncUtill();
                        int pendingcount = syncutil.getpendingcasescount(getApplicationContext());
                        if (syncutil.getpendingcasescount(getApplicationContext()) > 0 && pendingcount > 0 && pendingcount < 5) {
                            List<CaseObj> casedet = syncutil.getpendingcaseslist(getApplicationContext());
                            //Need to code a single util class which should take care of entire sync which should be equivalent to Add existing case
*//*                            AsyncTaskRunnerSync runner = new AsyncTaskRunnerSync(loginparams, casedet, 1, getApplicationContext());
                            String sleepTime = "1";
                            runner.execute(sleepTime);*//*

                        } else {
                            Log.i(TAG, "No pending cases  ");
                            Intent i = new Intent(SplashScreen.this, NavActivity.class);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "app version is outdated please download the latest version",
                                Toast.LENGTH_LONG).show();
                        if (appurl.contains("casesync")) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        } else {
                            Intent viewIntent =
                                    new Intent("android.intent.action.VIEW",
                                            Uri.parse("" + appurl));
                            startActivity(viewIntent);
                        }
                    }
                } else {
                    mProgressView.setVisibility(View.GONE);
                    *//*Need to use a async task to check the appversions and latest URL'S Any message to user please send it using it*//*
                    Intent i = new Intent(SplashScreen.this, Log.class);
                    startActivity(i);
                    finish();

                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
*/
    /*private class AsyncTaskRunnerSync extends AsyncTask<String, String, String> {

        // a constructor so that you can pass the object and use
        private LoginParams parms;
        private List<Casedetails> caselist;
        int historycount = 0;
        Context context = null;

        public AsyncTaskRunnerSync(LoginParams parms, List<Casedetails> caselist, int historycount, Context context) {
            this.parms = parms;
            this.caselist = caselist;
            this.historycount = historycount;
            this.context = context;

        }

        private String resp;
        ProgressDialog progressDialog;


        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping...");
            *//*It is not that easy to implement every thing async take some time and do it*//*
            try {
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                        CaseSyncOpenDatabaseHelper.class);
                Dao<Casedetails, Long> casedetDao = todoOpenDatabaseHelper.getDao();
                String responsetemp = "";
                String uri = parms.getUrl();
                RestTemplate restTemplate = new RestTemplate();
                String plainCreds = parms.getUsername() + ":" + parms.getPassword();
                HttpHeaders headers = new HttpHeaders();
                byte[] plainCredsBytes = plainCreds.getBytes();
                byte[] base64CredsBytes = Base64.encode(plainCredsBytes, Base64.DEFAULT);
                String base64Creds = new String(base64CredsBytes);
                headers.add("Authorization", "Basic " + base64Creds);
                HttpEntity<String> request = new HttpEntity<String>(headers);
                ((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(5000);
                ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, request, String.class);
                if (response != null) {
                    if (response.getStatusCode().value() == 200)
                        responsetemp = response.getBody();
                } else {
                    Log.i("MainActivity", "Error occured while fetching  the response");
                }

                if (responsetemp != null && !responsetemp.equals("")) {
                    Log.i("SYNC", "Looping all the cases with token " + responsetemp);
                    //Token is available start sync if some thing goes wrong i will revert with synccases
                    int casecount = 0;
                    for (Casedetails casedettemp : caselist) {
                        String url = PropertyUtil.getProperty("caseurl", context) + "?captchaValid=valid&state_code=" + casedettemp.getStatecode() + "&dist_code=" + casedettemp.getDistrictcode() + "&court_code=" + casedettemp.getCourtcode() + "&case_number=" + casedettemp.getCasenumber();
                        //Log.e(TAG,url);
                        String casenumber = casedettemp.getCasenumber();
                        HttpHeaders requestHeaders = new HttpHeaders();
                        requestHeaders.add("Cookie", "PHPSESSID=" + responsetemp);
                        requestHeaders.add("User-Agent", USER_AGENT);
                        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
                        RestTemplate resttemplate = new RestTemplate();
                        // ((SimpleClientHttpRequestFactory)resttemplate.getRequestFactory()).setReadTimeout(2000);
                        ResponseEntity rssResponse = resttemplate.exchange(url,
                                HttpMethod.GET,
                                requestEntity,
                                String.class);
                        String casereponse = rssResponse.getBody().toString();
                        if (casereponse.toString().contains("guestlogin")) {

                        } else if (casereponse.toString().contains("CASE IS NOT ALLOCATED TO COURT")) {
                            // messagebuilder.append("Case number " + casedettemp.getRegistration_number() + " is no longer available \n");
                        } else {

                            try {
                                Casedetails casedetails = HttpURLConnectionExample.parseData(casereponse, casenumber, responsetemp, context);
                                if (casedetails.getNext_hearing_date() != null && casedettemp.getNext_hearing_date() != null) {
                                    Log.e(TAG, "Disposed:" + casedetails.isDisposed());
                                    if (casedetails.getNext_hearing_date().after(casedettemp.getNext_hearing_date())) {
                                        Log.e(TAG, "CASEDETAILS UPDATED" + casedetails.getNext_hearing_date() + " " + casedetails.getRegistration_number());
                                        UpdateBuilder<Casedetails, Long> updateBuilder = casedetDao.updateBuilder();
                                        updateBuilder.updateColumnValue("next_hearing_date", casedetails.getNext_hearing_date());
                                        //for disposed cases
                                        updateBuilder.updateColumnValue("disposed", casedetails.isDisposed());
                                        updateBuilder.where().eq("casenumber", casedetails.getCasenumber());
                                        updateBuilder.update();
                                        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                                        String reportDate = df.format(casedetails.getNext_hearing_date());
                                        addToDeviceCalendar(casedetails);
                                        *//*Updating the history*//*
                                        // Log.e(TAG,casereponse);
                                        Document doc = Jsoup.parse(casereponse);
                                        try {
                                            String resphistory = "";
                                            Elements table = doc.select("table.history_table");
                                            Elements rows = table.select("tr");
                                            for (int i = 1; i < rows.size(); i++) {
                                                if (i >= rows.size() - historycount) {

                                                    Element row = rows.get(i);
                                                    Elements cols = row.select("td");
                                                    Casehistory casehistory = new Casehistory();
                                                    if (!cols.get(3).text().equals(""))
                                                        if (!cols.get(3).text().contains("Next Date Not Given")) {
                                                            casehistory.setHearingdate(HttpURLConnectionExample.getdatefromsdf(cols.get(3).text(), "dd-MM-yyyy"));

                                                        } else {
                                                            casehistory.setHearingdate(null);
                                                        }
                                                    casehistory.setPurposeofhearing(cols.get(4).text());
                                                    Elements links = row.select("a[href]");
                                                    for (Element link : links) {
                                                        if (!link.text().equals(""))
                                                            casehistory.setBusinessondate(HttpURLConnectionExample.getdatefromsdf(link.text(), "dd-MM-yyyy"));
                                                        try {
                                                            String historyurl = PropertyUtil.getProperty("historyurl", context) + "" + link.attr("href");
                                                            HttpHeaders requestHeadershis = new HttpHeaders();
                                                            requestHeadershis.add("Cookie", "PHPSESSID=" + responsetemp);
                                                            requestHeadershis.add("User-Agent", USER_AGENT);
                                                            HttpEntity requestEntityhis = new HttpEntity(null, requestHeadershis);
                                                            RestTemplate resttemplatehis = new RestTemplate();
                                                            //((SimpleClientHttpRequestFactory)resttemplatehis.getRequestFactory()).setReadTimeout(2000);
                                                            ResponseEntity rssResponsehis = resttemplatehis.exchange(historyurl,
                                                                    HttpMethod.GET,
                                                                    requestEntityhis,
                                                                    String.class);
                                                            resphistory = rssResponsehis.getBody().toString();


                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }

                                                        String searchkey = "<td width='69%' valign='top' align='left' style='font-size:1em;'>";
                                                        int index1 = resphistory.indexOf(searchkey);
                                                        int index2 = resphistory.indexOf("<br />");

                                                        try {
                                                            casehistory.setBusiness(resphistory.substring(index1 + searchkey.length(), index2));
                                                        } catch (IndexOutOfBoundsException e) {
                                                            e.printStackTrace();
                                                            Log.e("HISTORY:", e.getMessage());
                                                        }
                                                    }
                                                    casehistory.setCasenumber(casedetails.getCasenumber());
                                                    *//*Save the case history here *//*
                                                    insertcasehis(casehistory);
                                                }
                                            }

                                        } catch (Exception e) {
                                            Log.i(TAG, "Error While creating casehistory:" + e.getMessage());
                                            e.printStackTrace();
                                        }

                                    } else {
                                        //Need to handle the disposed case here
                                        if (casedetails != null) {
                                            if (casedetails.isDisposed() == true) {
                                                Log.i(TAG, "Coming to disposed case");
                                                UpdateBuilder<Casedetails, Long> updateBuilder = casedetDao.updateBuilder();
                                                updateBuilder.updateColumnValue("disposed", casedetails.isDisposed());
                                                updateBuilder.where().eq("casenumber", casedetails.getCasenumber());
                                                updateBuilder.update();
                                            }
                                        }
                                        if (casedetails.getNext_hearing_date() != null) {
                                            *//*Compare with the current date and then mark it as not updated*//*
                                            Log.i(TAG, "Case numbers coming to else part :" + casedetails.getRegistration_number() + " " + casedetails.getNext_hearing_date());
                                            if (casedetails.getNext_hearing_date().before(new Date()) && casedetails.isDisposed() == false) {
                                                //no need to do any thing
                                            } else {
                                                Log.i(TAG, "Case is already updated");
                                                addToDeviceCalendar(casedetails);
                                            }

                                        }
                                    }
                                }


                            } catch (Exception e) {
                                Log.e(TAG, "Error while paarsing casedetails:" + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }

                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {

            try {

                Intent i = new Intent(SplashScreen.this, NavActivity.class);
                startActivity(i);
                finish();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(SplashScreen.this,
                    "",
                    "Updating the next hearing dates ....");
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
*/
    private String getVersion(Context context) {
        String version = "";
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(context.getPackageName(), 0);
            version = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public void insertcasehis(Casehistory history) throws SQLException {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                CaseSyncOpenDatabaseHelper.class);
        Dao<Casehistory, Long> casehisDao = todoOpenDatabaseHelper.getCasehistorydao();


        List<Casehistory> todos = casehisDao.queryForMatching(history);
        if (todos.size() > 0) {
            Log.i("history", "Case history already exists no need to insert");
        } else {
            casehisDao.create(history);
        }


    }


}
