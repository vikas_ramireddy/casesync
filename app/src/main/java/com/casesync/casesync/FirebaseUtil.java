package com.casesync.casesync;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.casesync.casesync.pojo.Profile;
import com.casesync.utill.CaseObj;
import com.casesync.utill.User;
import com.casesync.utill.Useraccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.springframework.util.StringUtils;

import static android.content.Context.MODE_PRIVATE;

public class FirebaseUtil {
    private static final String TAG = "FUtil";
    public static String userid = "";

    public static DatabaseReference getDatabase(String reference) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = null;
        if (reference == null) {
            myRef = database.getReference();
        } else {
            myRef = database.getReference(reference);
        }


        return myRef;
    }

    public static void writeNewUser(String userId, String name, String email, final Context context) {
        final User user = new User(name, email);
        //Issue fix need to check if a user is already available if available don't create
        new FirebaseUtil().mReadDataOnceUser(new FirebaseUtil.OnGetDataListener() {
            @Override
            public void onStart() {
                //DO SOME THING WHEN START GET DATA HERE
            }

            @Override
            public void onSuccess(DataSnapshot data) {
                //DO SOME THING WHEN GET DATA SUCCESS HERE
                if (data.getKey() == null) {
                    FirebaseUtil.getDatabase(null).child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(user);
                } else {
                    //Already data is available
                    for (DataSnapshot postSnapshot : data.getChildren()) {
                        //Loop for n number of times
                        //Log.d(TAG,postSnapshot.getKey());
                        if (postSnapshot.getKey().equals("useraccount")) {
                            for (DataSnapshot shot : postSnapshot.getChildren()) {
                                Log.d(TAG, shot.getKey());
                                String cinno = (String) shot.child("cinno").getValue();
                                boolean active = (Boolean) shot.child("active").getValue();
                                String clientmobnumber = (String) shot.child("clientmobnumber").getValue();
                                String clientname = (String) shot.child("clientname").getValue();
                                String clientemail = (String) shot.child("clientemail").getValue();
                                String clientnotes = (String) shot.child("clientnotes").getValue();
                                Useraccount useraccount = new Useraccount.UserBuilder(cinno, active).clientMobileNumber(clientmobnumber).clientName(clientname).clientEmail(clientemail).clientNotes(clientnotes).build();
                                NavActivity.account.add(useraccount);
                            }
                        }
                    }
                    if (NavActivity.account.size() == 0) {
                        Useraccount useraccount = new Useraccount.UserBuilder("", false).build();
                        NavActivity.account.add(useraccount);
                    }
                }

            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                //DO SOME THING WHEN GET DATA FAILED HERE
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    public static void insertuseraccount(String cino, boolean status) {
        //Useraccount useracct = new Useraccount(cino, status);
        Useraccount useracct = new Useraccount.UserBuilder(cino, status).build();
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUtil.getDatabase(null).child("users").child(currentFirebaseUser.getUid()).child("useraccount").child(cino).setValue(useracct);
        //FirebaseUtil.getDatabase(null).child("useraccount").child(cino).setValue(useracct);
    }

    public static void insertuseraccountObj(Useraccount useracct) {
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUtil.getDatabase(null).child("users").child(currentFirebaseUser.getUid()).child("useraccount").child(useracct.getCinno()).setValue(useracct);
    }

    public static void insertuseraccountObj(CaseObj obj) {
        //Useraccount useracct = new Useraccount(obj.getCino(), true, obj.getPhone(), obj.getContactname(), obj.getEmail(), obj.getAddnotes());
        Useraccount useracct = new Useraccount.UserBuilder(obj.getCino(), true).clientMobileNumber(obj.getPhone()).clientName(obj.getContactname()).clientEmail(obj.getEmail()).clientNotes(obj.getAddnotes()).build();
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseUtil.getDatabase(null).child("users").child(currentFirebaseUser.getUid()).child("useraccount").child(obj.getCino()).setValue(useracct);
        //FirebaseUtil.getDatabase(null).child("useraccount").child(cino).setValue(useracct);
    }

    public static void insertProfile(Profile profile) {
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentFirebaseUser.getUid() != null) {
            FirebaseUtil.getDatabase(null).child("users").child(currentFirebaseUser.getUid()).child("profile").setValue(profile);
        }
    }

    public static boolean deleteuseraccount(CaseObj obj) {
        boolean status = true;
        //Useraccount useracct = new Useraccount(obj.getCino(), false, obj.getPhone(), obj.getContactname(), obj.getEmail(), obj.getAddnotes());
        Useraccount useracct = new Useraccount.UserBuilder(obj.getCino(), false).clientMobileNumber(obj.getPhone()).clientName(obj.getContactname()).clientEmail(obj.getEmail()).clientNotes(obj.getAddnotes()).build();
        FirebaseUtil.getDatabase(null).child("users").child(FirebaseAuth.getInstance().getCurrentUser()
                .getUid()).child("useraccount").child(obj.getCino()).setValue(useracct);
        return status;
    }


    public void mReadDataOnce(final OnGetDataListener listener) {
        listener.onStart();
        FirebaseUtil.getDatabase(null).child("users").child(FirebaseAuth.getInstance().getCurrentUser()
                .getUid()).child("useraccount").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
            }
        });
    }

    public void mReadDataOnceUser(final OnGetDataListener listener) {
        listener.onStart();
        FirebaseUtil.getDatabase(null).child("users").child(FirebaseAuth.getInstance().getCurrentUser()
                .getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
            }
        });
    }

    public void insertFirebaseTokenOnBoot(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(CasesyncUtill.getDefaultSharedPreferencesName(context), MODE_PRIVATE);
        String token = prefs.getString("firebase_token", "");
        if (!StringUtils.isEmpty(token)) {
            Profile profile = new Profile();
            profile.setFirebase_token(token);
            FirebaseUtil.insertProfile(profile);
        }

    }

    public interface OnGetDataListener {
        void onStart();

        void onSuccess(DataSnapshot data);

        void onFailed(DatabaseError databaseError);
    }

}
