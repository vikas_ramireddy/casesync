package com.casesync.casesync;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.CauseListUtil;
import com.casesync.utill.CustomAdapter;
import com.casesync.utill.LoginParams;
import com.google.android.material.snackbar.Snackbar;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CustomListView extends AppCompatActivity {
    private static final String AUTHORITY = "com.casesync.fileprovider";
    private static CustomAdapter adapter;
    private static String TAG = "LV";
    final Calendar myCalendar = Calendar.getInstance();
    public int caseposition;
    ArrayList<com.casesync.utill.CaseObj> dataModels;
    ArrayList<com.casesync.utill.CaseObj> dataModelTemp;
    ListView listView;
    EditText editTxt = null;
    List<CaseObj> casedet = null;
    String isdisposed = "N";
    String charsequence = "";
    String filter = "";
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    CauseListUtil causeListUtil=new CauseListUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);
        if (getSupportActionBar() != null) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isdisposed = bundle.getString("disposed");
            filter = bundle.getString("filter", null);

        }
//        final FloatingActionButton fab = findViewById(R.id.floatingActionButton);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FilterFragment dialogFrag = FilterFragment.newInstance();
//                dialogFrag.setParentFab(fab);
//                dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
//            }
//        });

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        listView = findViewById(R.id.list);
        dataModels = new ArrayList<>();
        dataModelTemp = new ArrayList<>();
        editTxt = findViewById(R.id.search);


        try {
            final ArrayList<com.casesync.utill.CaseObj> sortedModels = new ArrayList<com.casesync.utill.CaseObj>();
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                    CaseSyncOpenDatabaseHelper.class);
            /*Added for optimization if the same window is opened multiple times no need to load the data from database
             * Show the same list which is available but need to think more on how to implement*/


            Dao<com.casesync.utill.CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            casedet = casedetDao.queryForEq("archive", isdisposed);
            if (casedet != null && casedet.size() > 0) {
                Log.i(TAG, "" + CollectionUtils.isEmpty(CasesyncUtill.sortedModels));
                Log.i(TAG, isdisposed);
                Log.i(TAG, casedet.size() + ":" + CasesyncUtill.sortedModels.size());

                if (!CollectionUtils.isEmpty(CasesyncUtill.sortedModels) && (isdisposed.equals("N")) && (casedet.size() == CasesyncUtill.sortedModels.size())) {
                    Log.i(TAG, "Loading data from cache");
                    //Only if the count is same as
                    sortedModels.addAll(CasesyncUtill.sortedModels);
                    adapter = new CustomAdapter(sortedModels, CasesyncUtill.temp, getApplicationContext());
                } else {
                    Log.i(TAG, isdisposed + " Cases loading again");
                    for (CaseObj cases : casedet) {
                        if (isdisposed.equalsIgnoreCase("Y")) {
                            if (cases.getDateOfDecision() != null) {
                                dataModels.add(cases);
                            } else {
                                dataModelTemp.add(cases);
                            }
                        } else {
                            if (cases.getDateNextList() != null) {
                                dataModels.add(cases);
                            } else {
                                dataModelTemp.add(cases);
                            }
                        }

                    }

                    if (isdisposed.equalsIgnoreCase("Y")) {
                        Log.i(TAG, "Sorting based on date of decision");
                        Collections.sort(dataModels, new Comparator<CaseObj>() {
                            @Override
                            public int compare(CaseObj fruit2, CaseObj fruit1) {
                                return fruit2.getDateOfDecision().compareTo(fruit1.getDateOfDecision());
                            }
                        });

                    } else {
                        Collections.sort(dataModels, new Comparator<CaseObj>() {
                            @Override
                            public int compare(CaseObj fruit2, CaseObj fruit1) {
                                return fruit2.getDateNextList().compareTo(fruit1.getDateNextList());
                            }
                        });
                    }
                    if (dataModelTemp.size() > 0) {
                        Log.i(TAG, "" + dataModelTemp.size());
                        for (CaseObj castemp : dataModelTemp) {
                            dataModels.add(castemp);
                        }
                    }
                    List<CaseObj> casedet = new CasesyncUtill().getdistinctlist(getApplicationContext(), isdisposed);
                    ArrayList<String> temp = new ArrayList<String>();

                    String est_code = "";
                    String est_code_temp = "";
                    String date = "";
                    for (com.casesync.utill.CaseObj cases : casedet) {
                        if (isdisposed.equals("N")) {
                            if (cases.getDateNextList() != null) {
                                //Above list returns all the distinct hearing dates available
                                QueryBuilder<CaseObj, Long> queryBuilder =
                                        casedetDao.queryBuilder();
                                queryBuilder.orderBy("est_code", true);
                                queryBuilder.orderBy("type_name", true);

                                Where<CaseObj, Long> where = queryBuilder.where();
                                where.eq("archive", isdisposed);
                                where.and();
                                where.eq("date_next_list", cases.getDateNextList());
                                PreparedQuery<CaseObj> preparedQuery = queryBuilder.prepare();
                                List<CaseObj> case_details = casedetDao.query(preparedQuery);
                                for (CaseObj obj : case_details) {
                                    if (est_code.isEmpty()) {
                                        est_code = obj.getEstCode();
                                        temp.add(obj.getCino());
                                        date = sdf.format(obj.getDateNextList());
                                    } else if (!date.equals(sdf.format(obj.getDateNextList()))) {

                                        temp.add(obj.getCino());
                                        est_code = obj.getEstCode();
                                        date = sdf.format(obj.getDateNextList());
                                    } else if (!est_code.equals(obj.getEstCode())) {
                                        temp.add(obj.getCino());
                                        est_code = obj.getEstCode();
                                        date = sdf.format(obj.getDateNextList());
                                    } else {
                                        date = sdf.format(obj.getDateNextList());
                                    }

                                }
                                sortedModels.addAll(case_details);

                            } else {
                                //Take all the cases where date is null directly and add those details.
                                //TODO Court based sorting is pending.
                                QueryBuilder<CaseObj, Long> queryBuilder =
                                        casedetDao.queryBuilder();
                                queryBuilder.orderBy("est_code", true);
                                queryBuilder.orderBy("type_name", true);

                                Where<CaseObj, Long> where = queryBuilder.where();
                                where.eq("archive", isdisposed);
                                where.and();
                                where.isNull("date_next_list");
                                PreparedQuery<CaseObj> preparedQuery = queryBuilder.prepare();
                                List<CaseObj> case_details = casedetDao.query(preparedQuery);

                                for (CaseObj obj : case_details) {
                                    if (StringUtils.isEmpty(est_code_temp)) {
                                        est_code_temp = obj.getEstCode();
                                        temp.add(obj.getCino());
                                    } else if (!est_code_temp.equals(obj.getEstCode())) {
                                        est_code_temp = obj.getEstCode();
                                        temp.add(obj.getCino());
                                    }
                                }
                                sortedModels.addAll(case_details);
                            }
                        } else {
                            if (cases.getDateOfDecision() != null) {
                                //Above list returns all the distinct hearing dates available
                                QueryBuilder<CaseObj, Long> queryBuilder =
                                        casedetDao.queryBuilder();
                                queryBuilder.orderBy("est_code", true);
                                queryBuilder.orderBy("type_name", true);

                                Where<CaseObj, Long> where = queryBuilder.where();
                                where.eq("archive", isdisposed);
                                where.and();
                                where.eq("date_of_decision", cases.getDateOfDecision());
                                PreparedQuery<CaseObj> preparedQuery = queryBuilder.prepare();
                                List<CaseObj> case_details = casedetDao.query(preparedQuery);
                                for (CaseObj obj : case_details) {
                                    if (est_code.isEmpty()) {
                                        est_code = obj.getEstCode();
                                        temp.add(obj.getCino());
                                        date = sdf.format(obj.getDateOfDecision());
                                    } else if (!date.equals(sdf.format(obj.getDateOfDecision()))) {

                                        temp.add(obj.getCino());
                                        est_code = obj.getEstCode();
                                        date = sdf.format(obj.getDateOfDecision());
                                    } else if (!est_code.equals(obj.getEstCode())) {
                                        temp.add(obj.getCino());
                                        est_code = obj.getEstCode();
                                        date = sdf.format(obj.getDateOfDecision());
                                    } else {
                                        date = sdf.format(obj.getDateOfDecision());
                                    }

                                }
                                sortedModels.addAll(case_details);

                            }
                        }

                    }
                    CasesyncUtill.sortedModels.clear();
                    CasesyncUtill.temp.clear();
                    CasesyncUtill.sortedModels.addAll(sortedModels);
                    CasesyncUtill.temp.addAll(temp);
                    adapter = new CustomAdapter(sortedModels, temp, getApplicationContext());

                }
                //adapter = new CustomAdapter(dataModels, getApplicationContext());
                adapter.notifyDataSetChanged();
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        //CaseObj dataModel = dataModels.get(position);

                        CaseObj dataModel = adapter.getItem(position);
                        Log.i(TAG, dataModel.getCino() + ":/" + dataModel.getRegNo() + "/" + dataModel.getRegYear() + "/" + position);
                        Intent intent = new Intent(CustomListView.this, CasedetailsTab.class);
                        intent.putExtra("caller", "CustomListView");
                        intent.putExtra("casenumber", dataModel.getCino());
                        intent.putExtra("disposed", isdisposed);
                        if (editTxt.getText() != null) {
                            Log.i(TAG, editTxt.getText().toString());
                            intent.putExtra("filter", "" + editTxt.getText().toString());
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();
                    }
                });

                registerForContextMenu(listView);

                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };

                final DatePickerDialog.OnDateSetListener date_event = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };

                editTxt.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        DatePickerDialog dialog = new DatePickerDialog(CustomListView.this, date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH));
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_NEGATIVE) {
                                    Log.i(TAG, "On click on cancel button ");

                                    editTxt.setText("");
                                    charsequence = "";
                                    adapter.getFilter().filter("");
                                }
                            }
                        });

                        dialog.show();
                    }


                });

                //TODO Need to design a new filter which should be able to filter cases based on


                //TODO Make it as a date
//                editTxt.addTextChangedListener(new TextWatcher() {
//
//                    @Override
//                    public void onTextChanged(CharSequence s, int start, int before, int count) {
//                        charsequence = s.toString();
//                        Log.i("TAG", "Text [" + s + "]");
//                        adapter.getFilter().filter(s.toString());
//                        Log.i("TAG", "Count:" + adapter.getCount());
//                    }
//
//                    @Override
//                    public void beforeTextChanged(CharSequence s, int start, int count,
//                                                  int after) {
//                        //Need to implement in future
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable s) {
//                        //Need to implement in future
//                    }
//                });


            }
            if (filter != null) {
                editTxt.setText(filter);
                charsequence = filter;
                adapter.getFilter().filter(filter);
            }

        } catch (Exception e) {
            Log.e("ListView", e.getMessage());
            e.printStackTrace();
        }

    }

    private void updateLabel() {
        editTxt.setText(sdf.format(myCalendar.getTime()));
        charsequence = sdf.format(myCalendar.getTime());
        adapter.getFilter().filter(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("PRINT", "SEARCH" + item.getItemId());
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(CustomListView.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.export) {
            causeListUtil.excelUpload(getApplicationContext(), CustomListView.this, CauseListUtil.CAUSELISTACTION.SHARE, casedet, charsequence);
        } else if (id == R.id.pdfexport) {
            causeListUtil.pdfExport(getApplicationContext(), CustomListView.this, CauseListUtil.CAUSELISTACTION.SHARE, casedet, charsequence);
        } else if (id == R.id.advancedsearch) {
            SearchView mSearchView = (SearchView) item.getActionView();
            mSearchView.setQueryHint("Search");
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i(TAG, "Text typed:" + newText);
                    charsequence = newText;
                    editTxt.setText("");
                    if (adapter != null) {
                        if (adapter.getFilter() != null) {
                            //FIXED null pointer
                            adapter.getFilter().filter(newText);
                        }
                    }
                    return true;
                }
            });
        }


        return super.onOptionsItemSelected(item);

    }
//
//    private File createPdf(String sometext) {
//        // create a new document
//        PdfDocument document = new PdfDocument();
//        // crate a page description
//        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
//        // start a page
//        PdfDocument.Page page = document.startPage(pageInfo);
//        Canvas canvas = page.getCanvas();
//        Paint paint = new Paint();
//        paint.setColor(Color.RED);
//        canvas.drawCircle(50, 50, 30, paint);
//        paint.setColor(Color.BLACK);
//        canvas.drawText(sometext, 80, 50, paint);
//        //canvas.drawt
//        // finish the page
//        document.finishPage(page);
//// draw text on the graphics object of the page
//        // Create Page 2
//        pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 2).create();
//        page = document.startPage(pageInfo);
//        canvas = page.getCanvas();
//        paint = new Paint();
//        paint.setColor(Color.BLUE);
//        canvas.drawCircle(100, 100, 100, paint);
//        document.finishPage(page);
//        // write the document content
//        File file = new File("storage/emulated/0/casesync/export/export.pdf");
//        try {
//            document.writeTo(new FileOutputStream(file));
//            Toast.makeText(this, "Done", Toast.LENGTH_LONG).show();
//            return file;
//        } catch (IOException e) {
//            Log.e("main", "error " + e.toString());
//            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
//        }
//        // close the document
//        document.close();
//        return null;
//    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;

        // We know that each row in the adapter is a Map
        CaseObj planet = adapter.getItem(aInfo.position);

        menu.setHeaderTitle("Options:" + planet.getTypeName() + "/" + planet.getRegNo() + "/" + planet.getRegYear());
        menu.add(1, 1, 1, "Details");
        menu.add(1, 2, 2, "Delete");

    }

    // This method is called when user selects an Item in the Context menu
    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        final MenuItem temp = item;
        int itemId = item.getItemId();
        if (item.getTitle() == "Details") {
            Log.i(TAG, "Details:" + item.getItemId());
            AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) temp.getMenuInfo();
            int position = aInfo.position;
            Log.i(TAG, "Position:" + aInfo.position);
            CaseObj dataModel = adapter.getItem(position);
            Log.i(TAG, dataModel.getCino() + ":/" + dataModel.getRegNo() + "/" + dataModel.getRegYear());

            Intent intent = new Intent(CustomListView.this, CasedetailsTab.class);
            intent.putExtra("caller", "CustomListView");
            intent.putExtra("casenumber", dataModel.getCino());
            intent.putExtra("disposed", isdisposed);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();

        }
        if (item.getTitle() == "Delete") {
            AlertDialog.Builder loginalert = new AlertDialog.Builder(CustomListView.this);
            loginalert.setMessage("Deleting the case");
            loginalert.setTitle("Are you sure ?");
            loginalert.setPositiveButton("OK", null);
            loginalert.setNegativeButton("CANCEL", null);
            loginalert.setCancelable(true);
            loginalert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CaseObj dataModel = null;
                            AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) temp.getMenuInfo();
                            int position = aInfo.position;
                            Object object = dataModels.get(position);
                            dataModel = (CaseObj) object;
                            try {
                                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                                        CaseSyncOpenDatabaseHelper.class);
                                Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
                                LoginParams loginparams = new LoginParams();
                                boolean result = FirebaseUtil.deleteuseraccount(dataModel);
                                if (result) {
                                    /*Delete from the local db*/
                                    DeleteBuilder<CaseObj, Long> deleteBuilder = casedetDao.deleteBuilder();
                                    deleteBuilder.where().eq("cino", dataModel.getCino());
                                    deleteBuilder.delete();
                                    dataModels.remove(aInfo.position);
                                    adapter.notifyDataSetChanged();
                                    Snackbar.make(getWindow().getDecorView().getRootView(), "Case deleted sucessfully  ", Snackbar.LENGTH_LONG)
                                            .setAction("No action", null).show();

                                } else {
                                    Snackbar.make(getWindow().getDecorView().getRootView(), "Case deletion is a failure ", Snackbar.LENGTH_LONG)
                                            .setAction("No action", null).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("ERROR", "" + e.getMessage());
                            }
                        }
                    });
            loginalert.setNegativeButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Log.e("CASELIST", "DeleteCancel :");
                            dialog.cancel();
                        }
                    });
            loginalert.create().show();
        }
        Log.e("CASELIST", "" + item.getItemId());
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CustomListView.this, NavActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }


    public boolean askForPermission(String permission, Integer requestCode, Activity activity) {
        boolean status = false;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            status = true;
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
        } else {
            status = true;
        }
        return status;
    }

}

