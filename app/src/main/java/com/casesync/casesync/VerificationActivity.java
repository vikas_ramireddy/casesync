package com.casesync.casesync;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.ErrorMessage;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.snackbar.Snackbar;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class VerificationActivity extends AppCompatActivity {
    private static final String PREFS_NAME = "LoginPrefs";
    private String email;
    private String password;
    private ProgressDialog progressBar;
    public LoginParams loginparams = new LoginParams();
    public ErrorMessage errorMessage = new ErrorMessage();
    private int progressBarStatus = 0;
    private UserRegTask mAuthTask = null;
     EditText mPasswordViewtemp;
    private View mProgressView;
    private View mLoginFormView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        Button verityButton = findViewById(R.id.verify);
        verityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                /*Redirect to register activity*/
                Bundle bundle = getIntent().getExtras();
                final EditText mPasswordView = findViewById(R.id.pin);
                Log.e("VERIFY", "Clicked on verify button");
                mPasswordViewtemp=mPasswordView;
                if (mPasswordView.getText().length() == 4) {
                    if (bundle != null) {

                        email = bundle.getString("email");
                        password = bundle.getString("password");
                        Log.e("VERIFY", "Calling alert");
                        try
                        {
//                            AlertDialog.Builder loginalert = new AlertDialog.Builder(getApplicationContext());
//                            loginalert.setMessage(R.string.intro_message);
//                            loginalert.setTitle("Terms & Conditions");
//                            loginalert.setPositiveButton("AGREE", null);
//                            loginalert.setPositiveButton("AGREE",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                /*On accepting terms and conditions this will work*/
//
//                                /*After the validation is passed need to call my method*/
//                                        }
//                                    });
//                            loginalert.create().show();
                            attemptLogin(email,mPasswordView.getText().toString());


                        }catch (Exception e)
                        {
                            Log.e("EXCEPTION",""+e.getMessage());
                        }
                    }

                } else {
                    mPasswordView.setError(errorMessage.getMessage());
                    Snackbar.make(view, "OTP Should be 4 digit", Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                    Toast.makeText(VerificationActivity.this, "OTP Should be 4 digit", Toast.LENGTH_LONG).show();
                }
            }

        });
        mLoginFormView = findViewById(R.id.reg_form);
        mProgressView = findViewById(R.id.reg_progress);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Verification Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(String email,String password) {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPasswordViewtemp.setError(null);
        // Store values at the time of the login attempt.

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.



            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserRegTask(email, password);
            mAuthTask.execute((Void) null);


    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    public class UserRegTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private boolean status=false;


        UserRegTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                //Call the facebook register
                String url = PropertyUtil.getProperty("domainname", getApplicationContext()) + "lists/verifyuser?id=" + mEmail + "&code=" + mPassword;
               // String url= PropertyUtil.getProperty("domainname", getApplicationContext()) + "lists/register?id=" +mEmail+"&password="+mPassword+"&username="+mEmail;
                loginparams.setUrl(url);
                Log.e("LOGIN","url"+url);
                String response=null;
                try {
                    String USER_AGENT = "Mozilla/21.0";
                    Log.i("MainActivity",""+loginparams.getUrl());
                    //parms
                    String uri =url;

                    RestTemplate restTemplate = new RestTemplate();
                    response = restTemplate.getForObject(uri, String.class);


                } catch (Exception e) {
                    Log.e("MainActivity", e.getMessage(), e);
                }
                //response
                Log.e("LOGIN","response"+response);

                ObjectMapper mapper = new ObjectMapper();
                try {
                    errorMessage = mapper.readValue(response, new TypeReference<ErrorMessage>() {
                    });
                }catch (Exception e)
                {

                    Log.e("REGISTER","ERROR:"+e.getMessage());
                }
                Log.e("LOGIN", "errorMessage:" + errorMessage.isStatus());
                Log.e("LOGIN","errorMessage:"+errorMessage.getMessage());
                if (errorMessage.isStatus())
                {
                    status=true;
                }


            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }



            // TODO: register the new account here.
            return status;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            Log.e("REGISTER",""+success);
            Log.e("REGISTER","ERRROR MSG"+errorMessage.getMessage());

            if (success) {
                /*Start the alert activity with terms and conditions */

                AlertDialog.Builder loginalert = new AlertDialog.Builder(VerificationActivity.this);
                loginalert.setMessage(R.string.intro_message);
                loginalert.setTitle("Terms & Conditions");
                loginalert.setPositiveButton("AGREE",null);
                loginalert.setPositiveButton("AGREE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("logged", "logged");
                                editor.putString("email", email);
                                editor.putString("password", password);
                                editor.putString("username", email);
                                editor.putString("timings",  errorMessage.getMessage());
                                editor.commit();
/*                try {
                    SampleAlarmReceiver alarm = new SampleAlarmReceiver();
                    CaseSyncAlarmReceiver syncalarm = new CaseSyncAlarmReceiver();
                    alarm.setAlarm(getApplicationContext());
                    syncalarm.setAlarmForSync(getApplicationContext());
                } catch (Exception e) {
                    Log.e("LOGIN", e.getMessage());
                }*/
                Intent intent = new Intent(VerificationActivity.this, NavActivity.class);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                }
                startActivity(intent);
                finish();
                            }
                        });
                AlertDialog alertLogin = loginalert.create();
                alertLogin.show();




            } else {
                /**/
                 /*If pending directly forward to next indent*/
                mPasswordViewtemp.setError(errorMessage.getMessage());
                mPasswordViewtemp.requestFocus();
                Log.e("ERROR", "" + errorMessage.getMessage());
                Toast.makeText(VerificationActivity.this, errorMessage.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

}
