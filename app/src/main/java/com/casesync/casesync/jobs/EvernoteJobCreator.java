package com.casesync.casesync.jobs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

public class EvernoteJobCreator implements JobCreator {
    @Nullable
    @Override
    public Job create(@NonNull String tag) {

        if (tag.equals(AutoSync.TAG)) {
            return new AutoSync();
        } else if (tag.equals(NotificationJob.TAGJOB)) {
            return new NotificationJob();
        }
        return null;
    }
}
