package com.casesync.casesync.jobs;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.casesync.casesync.CasesyncUtill;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.evernote.android.job.DailyJob;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Service to handle callbacks from the JobScheduler. Requests scheduled with the JobScheduler
 * ultimately land on this service's "onStartJob" method. It runs jobs for a specific amount of time
 * and finishes them. It keeps the activity updated with changes via a Messenger.
 */
public class NotificationJob extends DailyJob {
    public static final String TAGJOB = "NotificationJob";
    public static final String TAG = "" + NotificationJob.class.getSimpleName();
    private static DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    //enable after reboot
    public static void schedule(Context context) {

        if (!JobManager.instance().getAllJobRequestsForTag(TAGJOB).isEmpty()) {
            Log.i(TAG, "Job already scheduled");
            Set<JobRequest> jobs = JobManager.instance().getAllJobRequestsForTag(TAGJOB);
            for (JobRequest job : jobs) {
                Date date = new Date(job.getScheduledAt());
                Log.i(TAG, "Date:" + date);
            }
            //Un comment after testing
            return;
        }

        // schedule between 1 and 6 AM
        //DailyJob.schedule(new JobRequest.Builder(TAG), TimeUnit.HOURS.toMillis(22), TimeUnit.HOURS.toMillis(23));

        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String notificationtiming = prefs.getString("notification_preference", "0:00");
            String[] timing = notificationtiming.split(":");
            int hour = Integer.parseInt(timing[0]);
            int minute = Integer.parseInt(timing[1]);
            if (hour == 0 && minute == 0) {
                Log.i(TAG, "" + TimeUnit.HOURS.toMillis(1));
                int jobid = DailyJob.schedule(new JobRequest.Builder(TAGJOB), TimeUnit.HOURS.toMillis(7), TimeUnit.HOURS.toMillis(8));
                Log.i(TAG, "new jobid:" + jobid);
            } else {
                //Log.i(TAG, "" + TimeUnit.HOURS.toMillis(1)+" Hour:"+hour);
                int jobid = DailyJob.schedule(new JobRequest.Builder(TAGJOB), TimeUnit.HOURS.toMillis(hour), TimeUnit.HOURS.toMillis(hour >= 23 ? 1 : hour + 1));
                //Try to test for the next hour then it will work fine
                Log.i(TAG, "new jobid:" + jobid);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(context, e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    public static void cancelallJobs(String TAG) {
        if (!JobManager.instance().getAllJobRequestsForTag(TAGJOB).isEmpty()) {

            Log.i(TAG, "Job are available with the current tag");
            Set<JobRequest> jobs = JobManager.instance().getAllJobRequestsForTag(TAGJOB);
            for (JobRequest job : jobs) {
                Date date = new Date(job.getScheduledAt());
                Log.i(TAG, "Date:" + date);
                JobManager.instance().cancel(job.getJobId());
            }

        }
    }


    @NonNull
    @Override
    protected DailyJobResult onRunDailyJob(Params params) {
        Log.i(TAG, "Paramns TAG:" + params.getTag());
        createNotifications(getContext());
        return DailyJobResult.SUCCESS;
    }

    public void createNotifications(Context context) {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                CaseSyncOpenDatabaseHelper.class);
        CasesyncUtill util=new CasesyncUtill();
        Dao<CaseObj, Long> casedetDao = null;
        boolean smsstatus = false;
        String smstext = "";
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean send_noti_alert = prefs.getBoolean("sendnoti_alerts", false);
            boolean sendalert = prefs.getBoolean("sendsms_alerts", false);
            smstext = prefs.getString("smstemplate", "");
            if (sendalert && send_noti_alert) {
                smsstatus = true;
            }
        }
        try {
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Calendar now = Calendar.getInstance();
            int currentHour = now.get(Calendar.HOUR_OF_DAY);
            Date dt = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            boolean today = true;
            if (currentHour >= 18 && currentHour <= 24) {
                today = false;
                c.add(Calendar.DATE, 1);
            }
            //FIXME Remove after testing
            //c.add(Calendar.DATE,-1);
            dt = c.getTime();
            Log.i(TAG, dt.toString());
            List<CaseObj> casedet = casedetDao.queryForEq("date_next_list", dt);
            if (casedet != null) {
                int size = 0;

                if (casedet.size() > 0) {
                    for (com.casesync.utill.CaseObj casedetails : casedet) {
                        String text = "";
                        if (today == false) {
                            text = "Tomorrow";
                        } else {
                            text = "Today";
                        }
                        util.sendNotification(text + " there is a Case Hearing ",
                                " Case number:" + casedetails.getTypeName() + "/" + casedetails.getRegNo() + "/" + casedetails.getRegYear() + " Court:" + casedetails.getCourtName(), casedetails.getCino(), context, CasesyncUtill.NOTITYPE.CASE);
                        Log.i(TAG, "smsstatus:" + smsstatus);
                        if (smsstatus) {
                            if(casedetails.getPhone()!=null) {
                                util.sendSMS(casedetails.getPhone(), util.getSMSTemplate(smstext, casedetails, CasesyncUtill.SMSTYPE.NOTI));
                            }
                        }

                    }
                } else {
                    //Comment it after testing.
                    //utility.sendNotification("No cases available for hearing ", "", "", context, CasesyncUtill.NOTITYPE.GENERAL);
                }
            } else {
                //utility.sendNotification("No cases available for hearing", "", "", context, CasesyncUtill.NOTITYPE.GENERAL);

            }
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            //sendSMS("9066658022","SQL Exception:"+e.getMessage());
        } catch (Exception e) {
            //Some exception is happening unable to find the exception.
            //If this is working
            //sendSMS("9066658022","Exception:"+e.getMessage());
            e.printStackTrace();


        }
    }

    public void sendSMS(String phoneNo, String msg) {
        //TODO Internal logic will change. Instead of sending alerts from current APP.It will send from another Application if it exists
        //Sending SMS is going to be difficult so Need to think of creating a infrastructure that will send the Alerts using whatsapp/w4b.
        //This feature to be a paid one. All the alerts which are to be send to be inserted in to one table.
        //Job Should be initiated once the insertion is completed. That job will create new jobs and runs until there are no left over messages.
        //And insertions should happen once the case hearing got updated.

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}