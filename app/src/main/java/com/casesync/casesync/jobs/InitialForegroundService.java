package com.casesync.casesync.jobs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.casesync.casesync.CasesyncUtill;
import com.casesync.casesync.FirebaseUtil;
import com.casesync.casesync.NavActivity;
import com.casesync.casesync.R;
import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.CaseObj;
import com.casesync.utill.Cases;
import com.casesync.utill.FileUtility;
import com.casesync.utill.ImportObj;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PermissionUtility;
import com.casesync.utill.PropertyUtil;
import com.casesync.utill.Useraccount;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.casesync.casesync.Addexitingcase.insertdata;
import static com.casesync.casesync.CasesyncUtill.NOTIFICATION_CHANNEL_ID;

public class InitialForegroundService extends Service {

    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    public static final String ACTION_PAUSE = "ACTION_PAUSE";
    public static final String ACTION_PLAY = "ACTION_PLAY";

    public static final String ACTION_IMPORT = "ACTION_IMPORT";
    public static final int NOTIFICATION_ID = 464;
    private static final String TAG_FOREGROUND_SERVICE = "FOREGROUND_SERVICE";
    // Create notification builder.
    NotificationCompat.Builder builder = null;

    CasesyncUtill util = new CasesyncUtill();


    // Build the notification.
    Notification notification = null;


    public InitialForegroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG_FOREGROUND_SERVICE, "On Bind intent");
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        startForeground(NOTIFICATION_ID, notification);
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG_FOREGROUND_SERVICE, "My foreground service onCreate().");
        // Create notification default intent.
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        NotificationManager
                mNotificationManager = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Creating a notification channel and assigning
            util.createNotificationChannel(getApplicationContext(), mNotificationManager);
            builder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        } else {
            builder =
                    new NotificationCompat.Builder(getApplicationContext());
        }


        // Make notification show big text.
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle("Foreground process ....");
        bigTextStyle.bigText("");
        // Set big text style.
        builder.setStyle(bigTextStyle);

        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        Bitmap largeIconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        builder.setLargeIcon(largeIconBitmap);
        // Make the notification max priority.
        builder.setPriority(Notification.PRIORITY_MAX);

        builder.setOngoing(true);
        builder.setOnlyAlertOnce(true);
        builder.setProgress(100, 0, false);

        notification = builder.build();
        startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();

            switch (action) {
                case ACTION_START_FOREGROUND_SERVICE:
                    startForegroundService(ACTION_START_FOREGROUND_SERVICE, intent);
                    Toast.makeText(getApplicationContext(), "Started initial sync..", Toast.LENGTH_LONG).show();
                    break;
                case ACTION_STOP_FOREGROUND_SERVICE:
                    stopForegroundService();
                    Toast.makeText(getApplicationContext(), "Foreground service is stopped.", Toast.LENGTH_LONG).show();
                    break;
                case ACTION_IMPORT:
                    Toast.makeText(getApplicationContext(), "Started Importing data from file ....", Toast.LENGTH_LONG).show();
                    startForegroundService(ACTION_IMPORT, intent);
                    break;
                case ACTION_PAUSE:
                    Toast.makeText(getApplicationContext(), "You click Pause button.", Toast.LENGTH_LONG).show();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /* Used to build and start foreground service. */
    private void startForegroundService(String action, Intent intent) {
        Log.i(TAG_FOREGROUND_SERVICE, "Start foreground service.");
        //Based on the action the service call should change
        if (action.equals(ACTION_START_FOREGROUND_SERVICE)) {
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle("Initial Sync ...");
            bigTextStyle.bigText("Downloading cases from  " + CasesyncUtill.getApplicationName(getApplicationContext()) + " server...");
            // Set big text style.
            builder.setStyle(bigTextStyle);
            notification = builder.build();
            NotificationManager
                    mNotificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIFICATION_ID, notification);

            new FirebaseUtil().insertFirebaseTokenOnBoot(getApplicationContext());
            InitialSync runner = new InitialSync();
            String sleepTime = "1";
            runner.execute(sleepTime);
        } else if (action.equals(ACTION_IMPORT)) {

            List<ImportObj> dataModel = new ArrayList<>();
            String sdcard = Environment.getExternalStorageDirectory() + File.separator + "";
            //TODO This to be changed to ensure file is taken from a correct path.
            File file =null;
            int version = new PermissionUtility().getEcourtsVersion(getApplicationContext());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                file=FileUtility.loadEcourtsFile(getApplicationContext());
            }else
            {
                //In case of file is getting choosed.
                file=new File(FileUtility.getSubFolderPath(getApplicationContext(), FileUtility.ACTIONTYPE.IMPORT,null) + "myCases.txt");
            }
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                bufferedReader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (!text.toString().isEmpty()) {
                    Log.i(TAG_FOREGROUND_SERVICE, text.toString());
                    ObjectMapper objectMapper = new ObjectMapper();

                    JSONArray jsonarray = new JSONArray(text.toString());
                    for (int i = 0; i < jsonarray.length(); i++) {
                        String jsonostring = jsonarray.getString(i);
                        ImportObj caseobj = objectMapper.readValue(jsonostring, ImportObj.class);
                        if (caseobj != null) {
                            dataModel.add(caseobj);
                        } else {
                            Log.i(TAG_FOREGROUND_SERVICE, "Object not available");
                        }

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (dataModel != null) {
                try {
                    NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
                    bigTextStyle.setBigContentTitle("Importing cases from file...");
                    bigTextStyle.bigText("Importing " + dataModel.size() + " cases from  " + CasesyncUtill.getApplicationName(getApplicationContext()) + " server...");
                    // Set big text style.
                    builder.setStyle(bigTextStyle);
                    notification = builder.build();
                    NotificationManager
                            mNotificationManager = (NotificationManager)
                            getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(NOTIFICATION_ID, notification);

                    Log.i(TAG_FOREGROUND_SERVICE, "Total Number of cases :" + dataModel.size());
                    String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                    LoginParams loginparams = new LoginParams();
                    loginparams.setUrl(url);
                    Log.i("CustomAd", "URL OF ADD CASES:" + url);
                    AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, null, dataModel);
                    String sleepTime = "1";
                    runner.execute(sleepTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), "Action is not implemented ", Toast.LENGTH_LONG).show();
        }
    }

    private void stopForegroundService() {
        Log.i(TAG_FOREGROUND_SERVICE, "Stop foreground service.");
        stopForeground(true);
        stopSelf();
    }

    /*Once the firebase is configured then use the below code to do a initial sync  */
    private class InitialSync extends AsyncTask<String, Float, String> {
        String response = null;
        ProgressDialog progressDialog;
        private FirebaseUser auth;

        @Override
        protected String doInBackground(String... params) {
            publishProgress(0.0f); // Calls onProgressUpdate()
            try {
                while (NavActivity.account.size() <= 0) {
                    //two  seconds is enough because user needs to wait for more time
                    Thread.sleep(2000);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (NavActivity.account.size() > 0) {
                float progress = 0.0f;

                Log.i(TAG_FOREGROUND_SERVICE, "Total cases available :" + NavActivity.account.size());
                int totalcases = NavActivity.account.size();
                int count = 0;
                ConnectivityManager connMgr = (ConnectivityManager)
                        getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                util.clearCalendar(getApplicationContext());
                SharedPreferences prefs = getSharedPreferences(CasesyncUtill.getDefaultSharedPreferencesName(getApplicationContext()), MODE_PRIVATE);
                boolean calendar_sync = prefs.getBoolean("pref_sync_gc", false);
                boolean calendar_reminder = prefs.getBoolean("pref_sync_reminder", false);
                boolean calendar_email = prefs.getBoolean("pref_sync_email", false);

                //Enable below code if the foreground service is not working
                for (Useraccount acct : NavActivity.account) {
                    if (acct.isActive()) {
                        //publishProgress(0);
                        try {
                            //Not required to push to firebase
                            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                            if (networkInfo != null && networkInfo.isConnected()) {
                                CaseObj obj = util.fetchobjusingcinsave(getApplicationContext(), acct, false);
                                if (obj == null) {
                                    //Unable to fetch the case details.
                                    util.sendNotification("Unable to import CIN Number", "" + acct.getCinno(), null, getApplicationContext(), CasesyncUtill.NOTITYPE.GENERAL);
                                }else
                                {   //Added newly to load the calendar notifications. To improve the performence.
                                    util.createCalendarNotificationBulk(getApplicationContext(), Arrays.asList(obj),calendar_sync,calendar_reminder,calendar_email);
                                }
                            } else {
                                publishProgress(100f);
                                util.sendNotification("Please switch on the internet ", "Internet is mandatory while downloading cases from server. Logout and try to login for downloading all the existing cases", null, getApplicationContext(), CasesyncUtill.NOTITYPE.GENERAL);
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (GeneralSecurityException e) {
                            e.printStackTrace();
                        }
                        count++;
                        progress = ((float) count / (float) totalcases) * 100;
                        publishProgress(progress);
                        Log.i(TAG_FOREGROUND_SERVICE, count + ":Percentile:" + progress);

                    }

                }
                boolean permissions = new PermissionUtility().validateFilePermissions(getApplicationContext());
                if (permissions) {
                    new FileUtility().performMigrate(getApplicationContext());
                }

            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            stopForegroundService();
            Intent i = new Intent(getApplicationContext(), NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            getApplicationContext().startActivity(i);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Float... progress) {

            builder.setProgress(100, progress[0].intValue(), false);
            //Send the notification:
            notification = builder.build();
            NotificationManager
                    mNotificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIFICATION_ID, notification);

        }
    }


    class AsyncTaskRunner extends AsyncTask<String, Float, Integer> {
        ProgressDialog progressDialog;
        private LoginParams parms;
        private List<Cases> cases;
        private List<ImportObj> importobj;

        public AsyncTaskRunner(LoginParams parms, List<Cases> casesobj, List<ImportObj> impobj) {
            this.parms = parms;
            this.cases = casesobj;
            this.importobj = impobj;
        }

        @Override
        protected Integer doInBackground(String... params) {

            Integer count = 0;
            float progress = 0.0f;
            try {
                ConnectivityManager connMgr = (ConnectivityManager)
                        getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                Log.i("ADD", parms.getUrl());
                RestTemplate restTemplate = new RestTemplate();


                String url = "";
                publishProgress(0f);
                if (!CollectionUtils.isEmpty(cases)) {
                    //Un-used logic
                    for (Cases dataModel : cases) {
                        url = "";
                        String data_params = "?case_no=" + AESCryptCustom.encrypt(dataModel.getCase_no()) +
                                "&state_code=" + AESCryptCustom.encrypt(dataModel.getState_code()) + "&dist_code=" + AESCryptCustom.encrypt(dataModel.getDist_code())
                                + "&court_code=" + AESCryptCustom.encrypt(dataModel.getCourt_code());
                        url = parms.getUrl() + data_params;

                        try {
                            String responsetmp = restTemplate.getForObject(url, String.class);
                            if (!responsetmp.isEmpty() && responsetmp != null) {

                                JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                                Log.i(TAG_FOREGROUND_SERVICE, "obj:" + objtemp);
                                ObjectMapper objectMapper = new ObjectMapper();
                                CaseObj caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);

                                if (caseobj != null) {
                                    String businesstext = util.fetchBusiness(caseobj, getApplicationContext());
                                    caseobj.setWritinfo(businesstext);
                                    insertdata(caseobj, getApplicationContext(), true);
                                    count = count + 1;
                                    progress = ((float) count / (float) cases.size()) * 100;
                                    publishProgress(progress);

                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (!CollectionUtils.isEmpty(importobj)) {

                    Log.d("ASYNC", "Importing cases from import object " + importobj.size());
                    String baseurl = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";

                    for (ImportObj obj : importobj) {
                        if (obj != null) {
                            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                            if (networkInfo != null && networkInfo.isConnected()) {
                                if (CasesyncUtill.caseexist(obj.getCino(), getApplicationContext())) {
                                    //If case already exists case creation will escape
                                    CaseObj caseobj = util.getCaseObjFromCino(getApplicationContext(), obj.getCino(), true);
                                    if (obj.getCaseNo() == null) {
                                        importobj.get(0).setCaseNo(caseobj.getCaseNo());
                                    }else
                                    {
                                        util.createCalendarNotification(getApplicationContext(),Arrays.asList(caseobj));
                                    }
                                } else {
                                    Log.i(TAG_FOREGROUND_SERVICE, "escaping the case :" + obj.getCino());
                                }
                            } else {
                                publishProgress(100f);
                                util.sendNotification("Please switch on the internet and re-import", "", null, getApplicationContext(), CasesyncUtill.NOTITYPE.GENERAL);
                                break;
                            }
                            count = count + 1;
                            progress = ((float) count / (float) importobj.size()) * 100;
                            publishProgress(progress);
                        }
                    }
                }

            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
            return count;
        }

        @Override
        protected void onPostExecute(Integer result) {
            stopForegroundService();
            Intent i = new Intent(getApplicationContext(), NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            getApplicationContext().startActivity(i);
        }

        @Override
        protected void onPreExecute() {
            Intent i = new Intent(getApplicationContext(), NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            getApplicationContext().startActivity(i);
        }

        @Override
        protected void onProgressUpdate(Float... progress) {

            builder.setProgress(100, progress[0].intValue(), false);
            //Send the notification:
            notification = builder.build();
            NotificationManager
                    mNotificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIFICATION_ID, notification);

        }
    }


}
