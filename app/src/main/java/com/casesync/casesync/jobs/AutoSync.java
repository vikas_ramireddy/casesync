package com.casesync.casesync.jobs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.casesync.casesync.CasesyncUtill;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

import java.util.concurrent.TimeUnit;

public class AutoSync extends Job {
    public static final String TAG = "autosync";

    public static void scheduleJob(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int storedPreference = Integer.parseInt(prefs.getString("sync_frequency", "0"));
        if (storedPreference != -1) {
            if (!JobManager.instance().getAllJobRequestsForTag(TAG).isEmpty()) {
                Log.i(TAG, "Already the job is scheduled");
                return;
            } else {
                if (storedPreference == 0) {
                    Log.i(TAG, "storedPreference:" + storedPreference);
                    int jobId = new JobRequest.Builder(TAG)
                            .setPeriodic(TimeUnit.MINUTES.toMillis(12 * 60), TimeUnit.MINUTES.toMillis(6))
                            .build()
                            .schedule();
                    Log.i(TAG, "jobId:" + jobId);
                } else {
                    Log.i(TAG, "storedPreference:" + storedPreference);
                    int jobId = new JobRequest.Builder(TAG)
                            .setPeriodic(TimeUnit.MINUTES.toMillis(storedPreference), TimeUnit.MINUTES.toMillis(6))
                            .build()
                            .schedule();
                    Log.i(TAG, "jobId:" + jobId);
                }
            }
        } else {
            Log.i(TAG, "auto sync is disabled");
        }
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        new CasesyncUtill().syncscheduler(getContext());
        return Result.SUCCESS;
    }
}
