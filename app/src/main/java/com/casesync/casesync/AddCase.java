package com.casesync.casesync;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.AESCryptCustom;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Casedetails;
import com.casesync.utill.Casehistory;
import com.casesync.utill.Cases;
import com.casesync.utill.Court;
import com.casesync.utill.District;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.LinkedHashMapAdapter;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.snackbar.Snackbar;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class AddCase extends AppCompatActivity {
    private static final String PREFS_NAME = "LoginPrefs";
    public static String response = "";
    private static LinkedHashMap courtmapData = new LinkedHashMap<String, String>();
    private static LinkedHashMap disctrictmapData = new LinkedHashMap<String, String>();
    private static String districtcode = "0";
    private static int statenumber = 0;
    private static String statecode = "0";
    private static String courtcode_reload = "0";
    public boolean status = false;
    public LoginParams loginparams = new LoginParams();
    public Casedetails casedetails = null;
    protected boolean enabled = true;
    String addcaseres = null;
    String TAG = "ADDCASE";
    EcourtsUtility utility = new EcourtsUtility();
    private String courtcode = "0";
    private Spinner state, district, court, casetype, case_addtype, case_year, policestation_spinner;
    //, pending_type;
    private EditText case_number, advocate_name, fir_number, filingnumber, petresp;
    private Button btnSubmit;
    private String stateref = "0";
    private String districtref = "0";
    private String courtcodefin = "0";
    private int casetypecode = 0;
    private String casetypetemp = "0";
    private String pscode = "0";

    private String casenumber = "0";
    private String caseyear = "0";
    private String case_add_type;
    private String email = "";
    private String password = "";
    private ProgressBar spinner;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    //    private View mProgressView;
    private View mLoginFormView;
    private Tracker mTracker;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_case);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        setTitle("Add Case");
        mLoginFormView = findViewById(R.id.addcase_view);
        //mProgressView = findViewById(R.id.addcase_process);
        spinner = findViewById(R.id.addcase_process);

        spinner.setMax(10);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        statenumber = Integer.parseInt(prefs.getString("state_name", "0"));
        if (statenumber == 0 && !statecode.equals("0")) {
            statenumber = Integer.parseInt(statecode);
        }
        email = settings.getString("email", "");
        password = settings.getString("password", "");

        /*Check for the internet*/
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            addItemsOnload();
        } else {
            AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
            loginalert.setMessage("Check your internet");
            loginalert.setTitle("Error Message");
            loginalert.setPositiveButton("OK", null);
            loginalert.setCancelable(true);
            loginalert.create().show();
            loginalert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
        }

//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            String message = "";
//
//            message = bundle.getString("cookie");
//
//            if (message != null && !message.equals("")) {
//                getIntent().removeExtra("message");
//
//            }
//        }
    }

    public void addItemsOnload() {
        state = findViewById(R.id.state_spinner);
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        mapData.put("0", "Select State");
        mapData.put("28", "Andaman and Nicobar");
        mapData.put("2", "Andhra Pradesh");
        mapData.put("6", "Assam");
        mapData.put("8", "Bihar");
        mapData.put("27", "Chandigarh");
        mapData.put("18", "Chhattisgarh");
        mapData.put("26", "Delhi");
        mapData.put("31", "Diu and Daman");
        mapData.put("32", "DNH at Silvasa");
        mapData.put("30", "Goa");
        mapData.put("17", "Gujarat");
        mapData.put("14", "Haryana");
        mapData.put("5", "Himachal Pradesh");
        mapData.put("12", "Jammu and Kashmir");
        mapData.put("7", "Jharkhand");
        mapData.put("3", "Karnataka");
        mapData.put("4", "Kerala");
        mapData.put("33", "Ladakh");
        mapData.put("23", "Madhya Pradesh");
        mapData.put("1", "Maharashtra");
        mapData.put("25", "Manipur");
        mapData.put("21", "Meghalaya");
        mapData.put("19", "Mizoram");
        mapData.put("34", "Nagaland");
        mapData.put("11", "Orissa");
        mapData.put("35", "Puducherry");
        mapData.put("22", "Punjab");
        mapData.put("9", "Rajasthan");
        mapData.put("24", "Sikkim");
        mapData.put("10", "Tamil Nadu");
        mapData.put("29", "Telangana");
        mapData.put("20", "Tripura");
        mapData.put("15", "Uttarakhand");
        mapData.put("13", "Uttar Pradesh");
        mapData.put("16", "West Bengal");


        LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapData);
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        state.setAdapter(dataAdapter);
        //It will Load from the default state
        Log.i(TAG, "State Number -- " + statenumber);

        //Change the logic.
        int pos = 0;
        int intialpos = 0;
        Set<String> keys = mapData.keySet();
        Log.d(TAG, "Size:" + keys.size());
        for (String key : keys) {
            Log.d(TAG, key + " -- "
                    + mapData.get(key));
            intialpos = intialpos + 1;
            if (key.equals("" + statenumber)) {
                pos = intialpos - 1;
                Log.d(TAG, pos + " Selected -- "
                        + mapData.get(key));
            }
        }
        state.setSelection(pos);

        /*Setting the initial data*/
        court = findViewById(R.id.court_spinner);
        case_addtype = findViewById(R.id.case_addtype);
        advocate_name = findViewById(R.id.advocate_name);
        fir_number = findViewById(R.id.fir_number);
        //Added for hiding the data
        case_number = findViewById(R.id.case_number);
        case_year = findViewById(R.id.case_year);
        filingnumber = findViewById(R.id.filingnumber);
        petresp = findViewById(R.id.petresp);

        casetype = findViewById(R.id.casetype_spinner);
        policestation_spinner = findViewById(R.id.policestation_spinner);
        //pending_type = findViewById(R.id.pending_type);
        case_year.setVisibility(View.GONE);
        case_number.setVisibility(View.GONE);
        casetype.setVisibility(View.GONE);
        advocate_name.setVisibility(View.GONE);
        fir_number.setVisibility(View.GONE);
        policestation_spinner.setVisibility(View.GONE);
        petresp.setVisibility(View.GONE);
        filingnumber.setVisibility(View.GONE);


        //Customized such that the old selection will be available and loaded automatically.
        if (CollectionUtils.isEmpty(disctrictmapData)) {
            disctrictmapData.put("0", "Select District");
        }
        LinkedHashMapAdapter<String, String> temp2 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, disctrictmapData);
        temp2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        district = findViewById(R.id.district_spinner);
        district.setAdapter(temp2);
        if (!districtcode.equals("0")) {
            int dispos = new ArrayList<String>(disctrictmapData.keySet()).indexOf(districtcode);
            district.setSelection(dispos);
        }

        if (CollectionUtils.isEmpty(courtmapData)) {
            courtmapData.put("0", "Select Court Complex");
        }


        LinkedHashMapAdapter<String, String> temp1 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, courtmapData);
        temp1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        court.setAdapter(temp1);
        if (!courtcode_reload.equals("0")) {
            Log.i(TAG, "Existing courtcode:" + courtcode_reload + ":" + courtmapData.size());
            int court_pos = new ArrayList<String>(courtmapData.keySet()).indexOf(courtcode_reload);
            Log.i(TAG, "Selected Court position:" + court_pos);
            court.setSelection(court_pos);
        }


        LinkedHashMap mapDatatemp2 = new LinkedHashMap<String, String>();
        mapDatatemp2.put("0", "Select Case Type");
        LinkedHashMapAdapter<String, String> temp3 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapDatatemp2);
        temp2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        casetype.setAdapter(temp3);

        LinkedHashMap mapDatatemp3 = new LinkedHashMap<String, String>();
        mapDatatemp3.put("0", "Select Police Station");
        LinkedHashMapAdapter<String, String> temp4 = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapDatatemp3);
        temp2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        policestation_spinner.setAdapter(temp4);

    }

    @Override
    protected void onResume() {

        super.onResume();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        /*Check for the internet*/
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            state = findViewById(R.id.state_spinner);
            state.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                            Spinner state = (Spinner) parent;

                            String[] stateid = parent.getSelectedItem().toString().split("=");
                            statecode = stateid[0];
                            stateref = stateid[1];
                            if (!stateid[0].equals("0")) {
                                Log.i(TAG, stateid[0]);
                                try {
                                    LoginParams loginparams = new LoginParams();
                                    String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/districtWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                            "&test_param=" + AESCryptCustom.encrypt("pending");
                                    //loginparams.setUrl("http://httpstat.us/500");
                                    loginparams.setUrl(url);
                                    AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.DISTRITCT);
                                    String sleepTime = "1";
                                    runner.execute(sleepTime);


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                        }
                    });
            district = findViewById(R.id.district_spinner);
            district.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {

                            Spinner district = (Spinner) parent;
                            if (!parent.getSelectedItem().toString().contains("Select District")) {
                                String[] disid = parent.getSelectedItem().toString().split("=");
                                Log.i(TAG, "Selected Item:" + parent.getSelectedItem().toString());
                                districtcode = disid[0];
                                LoginParams loginparams = new LoginParams();
                                try {
                                    String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/courtEstWebService.php?action_code="
                                            + AESCryptCustom.encrypt("fillCourtComplex") + "&state_code=" + AESCryptCustom.encrypt(statecode) +
                                            "&dist_code=" + AESCryptCustom.encrypt(districtcode);
                                    loginparams.setUrl(url);
                                    AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.COURT);
                                    String sleepTime = "1";
                                    runner.execute(sleepTime);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                        }
                    });
            case_addtype.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                            //If there is there is a change between old and new selected item auto-population should happen
                            Spinner district = (Spinner) parent;
                            if (!parent.getSelectedItem().toString().contains("Select District")) {
                                String[] disid = parent.getSelectedItem().toString().split("=");
                                Log.i(TAG, "Selected Item:" + parent.getSelectedItem().toString());
                                if (case_addtype.getSelectedItem().toString().equals("Add Case Using Case Number")) {
                                    //Enabling case number and year to enter the data
                                    if (!case_addtype.getSelectedItem().toString().equals(case_add_type)) {
                                        LoginParams loginparams = new LoginParams();
                                        try {
                                            String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/caseNumberWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                                    "&dist_code=" + AESCryptCustom.encrypt(districtcode) + "&court_code=" + AESCryptCustom.encrypt(courtcode);
                                            Log.d(TAG, "URL:" + url);
                                            loginparams.setUrl(url);
                                            AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.CASETYPE);
                                            String sleepTime = "1";
                                            runner.execute(sleepTime);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    case_year.setVisibility(View.VISIBLE);
                                    case_number.setVisibility(View.VISIBLE);
                                    casetype.setVisibility(View.VISIBLE);
                                    //pending_type.setVisibility(View.GONE);
                                    advocate_name.setVisibility(View.GONE);
                                    fir_number.setVisibility(View.GONE);
                                    policestation_spinner.setVisibility(View.GONE);
                                    petresp.setVisibility(View.GONE);
                                    filingnumber.setVisibility(View.GONE);
                                } else if (case_addtype.getSelectedItem().toString().equals("Add Case Using Advocate Name")) {
                                    advocate_name.setVisibility(View.VISIBLE);
                                    case_year.setVisibility(View.GONE);
                                    case_number.setVisibility(View.GONE);
                                    casetype.setVisibility(View.GONE);
                                    fir_number.setVisibility(View.GONE);
                                    policestation_spinner.setVisibility(View.GONE);
                                    petresp.setVisibility(View.GONE);
                                    filingnumber.setVisibility(View.GONE);

                                } else if (case_addtype.getSelectedItem().toString().equals("Add Case Using FIR")) {
                                    if (!case_addtype.getSelectedItem().toString().equals(case_add_type)) {
                                        //Need to populate police stations
                                        LoginParams loginparams = new LoginParams();
                                        try {
                                            String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/policeStationWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                                    "&dist_code=" + AESCryptCustom.encrypt(districtcode) + "&court_code=" + AESCryptCustom.encrypt(courtcode);
                                            Log.d(TAG, courtcode + ":URL:" + url);
                                            loginparams.setUrl(url);
                                            AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.POLICE);
                                            String sleepTime = "1";
                                            runner.execute(sleepTime);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    case_year.setVisibility(View.VISIBLE);
                                    fir_number.setVisibility(View.VISIBLE);
                                    policestation_spinner.setVisibility(View.VISIBLE);
                                    case_number.setVisibility(View.GONE);
                                    casetype.setVisibility(View.GONE);
                                    petresp.setVisibility(View.GONE);
                                    filingnumber.setVisibility(View.GONE);
                                    advocate_name.setVisibility(View.GONE);

                                } else if (case_addtype.getSelectedItem().toString().equals("Add Case Using Petitioner/Respondent")) {
                                    petresp.setVisibility(View.VISIBLE);
                                    case_year.setVisibility(View.VISIBLE);

                                    fir_number.setVisibility(View.GONE);
                                    policestation_spinner.setVisibility(View.GONE);
                                    case_number.setVisibility(View.GONE);
                                    casetype.setVisibility(View.GONE);
                                    filingnumber.setVisibility(View.GONE);
                                    advocate_name.setVisibility(View.GONE);

                                } else if (case_addtype.getSelectedItem().toString().equals("Add Case Using Filing Number")) {
                                    filingnumber.setVisibility(View.VISIBLE);
                                    case_year.setVisibility(View.VISIBLE);
                                    fir_number.setVisibility(View.GONE);
                                    policestation_spinner.setVisibility(View.GONE);
                                    case_number.setVisibility(View.GONE);
                                    casetype.setVisibility(View.GONE);
                                    petresp.setVisibility(View.GONE);
                                    petresp.setVisibility(View.GONE);
                                    advocate_name.setVisibility(View.GONE);
                                }

                                if (!case_addtype.getSelectedItem().toString().equals("Add Case Using")) {
                                    case_add_type = case_addtype.getSelectedItem().toString();
                                }
                            }
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                        }
                    });
            court = findViewById(R.id.court_spinner);
            court.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                            if (!statecode.equals("0") && !districtcode.equals("") && !parent.getSelectedItem().toString().split("=")[0].equals("") && !parent.getSelectedItem().toString().split("=")[0].equals("0") && !parent.getSelectedItem().toString().equals("Select Court Complex")) {
                                Log.i(TAG, "est_codes:" + parent.getSelectedItem().toString());
                                Log.i(TAG, "districtcode:" + districtcode);

                                courtcode = parent.getSelectedItem().toString().split("=")[0];
                                courtcode_reload = courtcode;
                                Log.d(TAG, "courtcode_reload:" + courtcode_reload);
                                if (courtcode.contains(",")) {
                                    courtcodefin = parent.getSelectedItem().toString().split("=")[0];
                                    courtcode = parent.getSelectedItem().toString().split("=")[0].split(",")[0];
                                    Log.d(TAG, "courtcode:" + courtcode);
                                } else {
                                    //courtcodefin should be marked as empty other wise old court will be considered while adding the case
                                    courtcodefin = "";
                                }
                                if (case_addtype.getSelectedItem().toString().equals("Add Case Using Case Number")) {
                                    LoginParams loginparams = new LoginParams();
                                    try {
                                        String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/caseNumberWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                                "&dist_code=" + AESCryptCustom.encrypt(districtcode) + "&court_code=" + AESCryptCustom.encrypt(courtcode);
                                        Log.d(TAG, "URL:" + url);
                                        loginparams.setUrl(url);
                                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.CASETYPE);
                                        String sleepTime = "1";
                                        runner.execute(sleepTime);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                if (case_addtype.getSelectedItem().toString().equals("Add Case Using FIR")) {
                                    //Need to populate police stations
                                    LoginParams loginparams = new LoginParams();
                                    try {
                                        String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "/policeStationWebService.php?state_code=" + AESCryptCustom.encrypt(statecode) +
                                                "&dist_code=" + AESCryptCustom.encrypt(districtcode) + "&court_code=" + AESCryptCustom.encrypt(courtcode);
                                        Log.d(TAG, courtcode + ":URL:" + url);
                                        loginparams.setUrl(url);
                                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.POLICE);
                                        String sleepTime = "1";
                                        runner.execute(sleepTime);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                        }
                    });
            casetype.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            Log.i("MainActivity", parent.getSelectedItem() + ":Nothing got selected");
                        }
                    });
            btnSubmit = findViewById(R.id.add_case);
            btnSubmit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    ConnectivityManager connMgr = (ConnectivityManager)
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        try {
                            /*Put the thread here*/
                            try {
                                String[] casetyp = casetype.getSelectedItem().toString().split("=");
                                Log.i(TAG, casetyp[0]);
                                casetypecode = Integer.parseInt(String.valueOf(casetyp[0]));
                                casetypetemp = String.valueOf(casetyp[1]);
                                //Log.i(TAG, "Police Station Selected:" + policestation_spinner.getSelectedItem().toString());
                                String[] poiice_station = policestation_spinner.getSelectedItem().toString().split("=");
                                pscode = String.valueOf(poiice_station[0]);
                            } catch (Exception e) {
                                Log.e("ADDCASE", "" + e.getMessage());
                            }

                            casenumber = case_number.getText().toString();
                            caseyear = case_year.getSelectedItem().toString();

                            try {
                                ErrorMessage message = validateRequest(statecode, districtcode, courtcode, case_addtype.getSelectedItem().toString(), casetypecode, casenumber, caseyear, advocate_name.getText().toString(), pscode, fir_number.getText().toString(), filingnumber.getText().toString(), petresp.getText().toString());
                                if (message.isStatus() == true) {
                                    ErrorMessage messageurl = buildURL(statecode, districtcode, courtcode, case_addtype.getSelectedItem().toString(), casetypecode, casenumber, caseyear, advocate_name.getText().toString(), pscode, fir_number.getText().toString(), filingnumber.getText().toString(), petresp.getText().toString());
                                    if (messageurl.isStatus()) {
                                        Log.i("Addcase", messageurl.getMessage());
                                        loginparams.setUrl(messageurl.getMessage());
                                        loginparams.setUsername("add");
                                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, LoginParams.TYPE.ADDCASE);
                                        String sleepTime = "1";
                                        runner.execute(sleepTime);

                                    } else {
                                        AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
                                        loginalert.setMessage(messageurl.getMessage());
                                        loginalert.setTitle("Error Message");
                                        loginalert.setPositiveButton("OK", null);
                                        loginalert.setCancelable(true);
                                        loginalert.create().show();
                                        loginalert.setPositiveButton("Ok",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Log.i("DEBUGAD", "ON CLICK ON ALERT DIALOG");
                                                    }
                                                });
                                    }
                                } else {
                                    AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
                                    loginalert.setMessage(message.getMessage());
                                    loginalert.setTitle("Error Message");
                                    loginalert.setPositiveButton("OK", null);
                                    loginalert.setCancelable(true);
                                    loginalert.create().show();
                                    loginalert.setPositiveButton("Ok",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Log.i("DEBUGAD", "ON CLICK ON ALERT DIALOG");
                                                }
                                            });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        // display error
                        AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
                        loginalert.setMessage("Check your internet");
                        loginalert.setTitle("Error Message");
                        loginalert.setPositiveButton("OK", null);
                        loginalert.setCancelable(true);
                        loginalert.create().show();
                        loginalert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(AddCase.this, NavActivity.class);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        }
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    }
                }

            });
        } else {
            AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
            loginalert.setMessage("Check your internet");
            loginalert.setTitle("Error Message");
            loginalert.setPositiveButton("OK", null);
            loginalert.setCancelable(true);
            loginalert.create().show();
            loginalert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();


    }

    public ErrorMessage buildURL(String statecode, String districtcode, String courtcode, String casetype, int casetypecode, String casenumber, String caseyear, String advocateName, String pscode, String fir, String filingnumber, String petadv) {
        ErrorMessage message = new ErrorMessage();
        String url = "";
        try {
            if (casetype.equals("Add Case Using Case Number")) {
                Log.i(TAG, "Courtcodes:" + courtcodefin);
                url = PropertyUtil.getProperty("apiurl", getApplicationContext()) +
                        "caseNumberSearch.php?case_number=" + AESCryptCustom.encrypt(casenumber) + "&case_type="
                        + AESCryptCustom.encrypt("" + casetypecode) + "&year=" + AESCryptCustom.encrypt(caseyear) +
                        "&state_code=" + AESCryptCustom.encrypt(statecode) + "&dist_code=" + AESCryptCustom.encrypt(districtcode) + "&court_code=";
            } else if (casetype.equals("Add Case Using Advocate Name")) {
                //|| casetype.equals("Add Case Using Bar code")
                //Because the number of cases will be more pendingDisposed:Both is ignored
                url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "searchByAdvocateName.php" + "?state_code=" + AESCryptCustom.encrypt(statecode) + "&dist_code="
                        + AESCryptCustom.encrypt(districtcode)
                        + "&checkedSearchByRadioValue=" + AESCryptCustom.encrypt(casetype.equals("Add Case Using Advocate Name") ? "1" : "2")
                        + "&advocateName=" + AESCryptCustom.encrypt(advocateName) + "&pendingDisposed="
                        + AESCryptCustom.encrypt("Pending") + "&year=" + AESCryptCustom.encrypt("") + "&barstatecode="
                        + AESCryptCustom.encrypt("") + "&barcode=" + AESCryptCustom.encrypt("") + "&date="
                        + AESCryptCustom.encrypt("") + "&court_code=";
                ;
            } else if (casetype.equals("Add Case Using FIR")) {
                url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "firNumberSearch.php" + "?state_code=" + AESCryptCustom.encrypt(statecode) + "&dist_code="
                        + AESCryptCustom.encrypt(districtcode)
                        + "&police_stationcode=" + AESCryptCustom.encrypt(pscode)
                        + "&firNumber=" + AESCryptCustom.encrypt(fir) + "&pendingDisposed=" + AESCryptCustom.encrypt("Both") + "&year=" + AESCryptCustom.encrypt(caseyear) + "&court_code=";
                ;
            } else if (casetype.equals("Add Case Using Petitioner/Respondent")) {
                url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "showDataWebService.php" + "?state_code=" + AESCryptCustom.encrypt(statecode) + "&dist_code="
                        + AESCryptCustom.encrypt(districtcode)
                        + "&pet_name=" + AESCryptCustom.encrypt(petadv) + "&pendingDisposed=" + AESCryptCustom.encrypt("Both") + "&year=" + AESCryptCustom.encrypt(caseyear) + "&court_code=";
                ;
            } else if (casetype.equals("Add Case Using Filing Number")) {
                url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "searchByFilingNumberWebService.php" + "?state_code=" + AESCryptCustom.encrypt(statecode) + "&dist_code="
                        + AESCryptCustom.encrypt(districtcode)
                        + "&filingNumber=" + AESCryptCustom.encrypt(filingnumber)
                        + "&year=" + AESCryptCustom.encrypt(caseyear) + "&court_code=";
                ;
            }

            //var request_data = {filingNumber:encryptData(filingNumber), year:encryptData(year)};
            //"searchByFilingNumberWebService.php"
            //  var request_data = {pet_name:encrypted_data1, pendingDisposed:encrypted_data2.toString(), year:encrypted_data3.toString()};
            //showDataWebService.php
            message.setStatus(true);
            message.setMessage(url);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return message;
    }

    public ErrorMessage validateRequest(String statecode, String districtcode, String courtcode, String casetype, int casetypecode, String casenumber, String caseyear, String advocateName, String pscode, String fir, String filingnumber, String petadv) {
        ErrorMessage message = new ErrorMessage();
        if (caseyear.equals("Select Year"))
            caseyear = "0";
        //TODO all the validations should be handled here

        String feildname = "";
        if (statecode.equals("0")) {
            feildname = feildname + "State is mandatory";
        }
        if (districtcode.equals("0")) {
            feildname = feildname + "\n District is mandatory";
        }
        if (courtcode.equals("0")) {
            feildname = feildname + "\n Court is mandatory";
        }
        if (casetype.equals("Add Case Using")) {
            feildname = feildname + "\n Add Case Using is mandatory";
        }
        if (casetype.equals("Add Case Using Case Number")) {
            if (casetypecode == 0) {
                feildname = feildname + "\n Case type is mandatory";
            }
            if (casenumber.equals("") || casenumber.equals("0")) {
                feildname = feildname + "\n Registration Number is mandatory";
            }
            if (caseyear.equals("") || caseyear.equals("0")) {
                feildname = feildname + "\n Year is mandatory";
            }
        }
        if (casetype.equals("Add Case Using Advocate Name")) {
            if (advocateName.length() < 4) {
                feildname = feildname + "\n at least type 4 characters as advocate name";
            }
        }
        if (casetype.equals("Add Case Using FIR")) {
            if (fir.length() == 0) {
                feildname = feildname + "\n Fir Number is mandatory";
            }
            if (caseyear.equals("") || caseyear.equals("0")) {
                feildname = feildname + "\n Year is mandatory";
            }
            if (pscode.equals("0")) {
                feildname = feildname + "\n Police Station is mandatory";
            }
        }
        if (casetype.equals("Add Case Using Petitioner/Respondent")) {
            //PET/RES and year should be mandatory
            if (petadv.length() < 4) {
                feildname = feildname + "\n Minimum 4 characters should be available in Petitioner/Respondent";
            }
            if (petadv.equals("") || petadv.equals("0")) {
                feildname = feildname + "\n Petitioner/Respondent is mandatory";
            }
            if (caseyear.equals("") || caseyear.equals("0")) {
                feildname = feildname + "\n Year is mandatory";
            }
        }
        if (casetype.equals("Add Case Using Filing Number")) {
            //Filing number and year should be mandatory
            if (filingnumber.equals("") || filingnumber.equals("0")) {
                feildname = feildname + "\n Filing Number is mandatory";
            }
            if (caseyear.equals("") || caseyear.equals("0")) {
                feildname = feildname + "\n Year is mandatory";
            }
        }

        if (StringUtils.isEmpty(feildname)) {
            message.setStatus(true);
            message.setMessage("All validations are successful");
        } else {
            message.setStatus(false);
            message.setMessage(feildname);
        }


        return message;
    }

    public void loadcasetype(String response) {
        casetype = findViewById(R.id.casetype_spinner);
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        try {
            if (response != null && !response.isEmpty()) {
                String result = AESCryptCustom.decrypt(response);
                result = EcourtsUtility.formatJson(result);
                JSONObject objtemp = new JSONObject(result);
                JSONArray arry = objtemp.getJSONArray("case_types");
                JSONObject obj = arry.getJSONObject(0);
                String[] casetypes = obj.getString("case_type").split("#");
                mapData.put("0", "Select Case Type");
                for (String type : casetypes) {
                    String[] types = type.split("~");
                    mapData.put(types[0], types[1]);
                }
            } else {
                Snackbar.make(getWindow().getDecorView().getRootView(), "Site is unavailable please try after some time", Snackbar.LENGTH_SHORT)
                        .setAction("No action", null).show();
            }
        } catch (Exception e) {
            mapData.put("0", "Select Case type ");
            Log.e("ADDCASE", e.getMessage());
            e.printStackTrace();
        } finally {
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, mapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            if (mapData != null)
                casetype.setAdapter(dataAdapter);
        }


    }

    public void loadpolicestation(String response) {
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        try {
            if (response != null && !response.isEmpty()) {
                String result = AESCryptCustom.decrypt(response);
                result = EcourtsUtility.formatJson(result);
                JSONObject objtemp = new JSONObject(result);
                JSONArray arry = objtemp.getJSONArray("police_stationlist");
                JSONObject obj = arry.getJSONObject(0);
                String[] casetypes = obj.getString("police_station").split("#");
                mapData.put("0", "Select Police Station");
                for (String type : casetypes) {
                    String[] types = type.split("~");
                    mapData.put(types[0], types[1]);
                }
            } else {
                Snackbar.make(getWindow().getDecorView().getRootView(), "Site is unavailable please try after some time", Snackbar.LENGTH_SHORT)
                        .setAction("No action", null).show();
            }
        } catch (Exception e) {
            mapData.put("0", "Select Police Station");
            Log.e("ADDCASE", e.getMessage());
            e.printStackTrace();
        } finally {
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, mapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            if (mapData != null)
                policestation_spinner.setAdapter(dataAdapter);
        }


    }

    public void loadcourtsbydisid(String response) {
        court = findViewById(R.id.court_spinner);
        List<Court> courtlist = null;
        try {
            //Log.i(TAG,response);
            JSONObject obj = new JSONObject(response);
            String x = AESCryptCustom.decrypt(obj.getString("courtComplex"));
            x = EcourtsUtility.formatJson(x);
            JSONArray jsonarray = new JSONArray(x);
            if (courtmapData.size() > 0 && jsonarray.length() > 0) {
                //Clear the old data and insert the new data.
                courtmapData.clear();
                courtmapData.put("0", "Select Court Complex");
            }
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                //jsonobject.getString("court_complex_name").contains("All") ? "Select Court Complex" :
                courtmapData.put(jsonobject.getString("njdg_est_code"),  jsonobject.getString("court_complex_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            courtmapData.put("0", "Select Court Complex");
        } finally {
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, courtmapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            court.setAdapter(dataAdapter);
            if (!courtcode_reload.equals("0")) {
                int court_pos = new ArrayList<String>(courtmapData.keySet()).indexOf(courtcode_reload);
                Log.i(TAG, "Selected Court position:" + court_pos);
                court.setSelection(court_pos);
            }
        }

    }

/*    public void loaddistricsbystateid(String response) {
        district = findViewById(R.id.district_spinner);
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        List<District> districtlist = null;
        try {
            String resp = response.replaceAll("District:-", "").trim();
            Document doc = Jsoup.parse(resp);
            Elements options = doc.getElementsByAttributeValue("name", "district_code").get(0).children();
            for (Element option : options) {
                String[] parts = option.val().split(Pattern.quote("~"));
                mapData.put(parts[0], option.text().contains("All") ? "Select District" : option.text());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                R.layout.support_simple_spinner_dropdown_item, mapData);
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        district.setAdapter(dataAdapter);

    }*/

    public void loaddistricsbystateid(String response) {
        district = findViewById(R.id.district_spinner);
        disctrictmapData = new LinkedHashMap<String, String>();
        List<District> districtlist = null;
        try {

            JSONObject obj = new JSONObject(response);
            Log.i(TAG, "District JSON:" + obj.toString());
            String x = AESCryptCustom.decrypt(obj.getString("districts"));
            x = EcourtsUtility.formatJson(x);
            JSONArray jsonarray = new JSONArray(x);
            for (int i = 0; i < jsonarray.length(); i++) {
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                Log.i(TAG, "State JSON:" + jsonobject.toString());
                if (i == 0) {
                    disctrictmapData.put(0, "Select District");
                    disctrictmapData.put(jsonobject.getString("dist_code"), jsonobject.getString("dist_name"));
                } else {
                    disctrictmapData.put(jsonobject.getString("dist_code"), jsonobject.getString("dist_name"));
                }


            }
            LinkedHashMapAdapter<String, String> dataAdapter = new LinkedHashMapAdapter<String, String>(this,
                    R.layout.support_simple_spinner_dropdown_item, disctrictmapData);
            dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            district.setAdapter(dataAdapter);
            if (!districtcode.equals("0")) {
                int pos = new ArrayList<String>(disctrictmapData.keySet()).indexOf(districtcode);
                Log.i(TAG, "Selected district position:" + pos);
                district.setSelection(pos);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void insertdata(Casedetails casedetails) throws java.sql.SQLException {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                CaseSyncOpenDatabaseHelper.class);
        Dao<Casedetails, Long> casedetDao = todoOpenDatabaseHelper.getDao();

        List<Casedetails> todos = casedetDao.queryForEq("casenumber", casedetails.getCasenumber());
        if (todos.size() > 0) {
            Log.i("Insertcase", "Case number already exists:");
        } else {
            casedetDao.create(casedetails);
        }

    }

    public void insertcasehis(List<Casehistory> casehistory) throws java.sql.SQLException {
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(this,
                CaseSyncOpenDatabaseHelper.class);
        Dao<Casehistory, Long> casehisDao = todoOpenDatabaseHelper.getCasehistorydao();

        List<Casehistory> todos = casehisDao.queryForEq("casenumber", casehistory.get(0).getCasenumber());
        if (todos.size() > 0) {
            Log.i("history", "Case history already exists:");
        } else {
            for (Casehistory casehis : casehistory) {
                casehisDao.create(casehis);
            }
        }

    }

    public void onItemSelected(AdapterView<?> spinner, View view,
                               int position, long arg3) {
        int id = spinner.getId();  //You can also use int id= view.getId();
        switch (id) {
            case R.id.state_spinner:
                // Do what you want
                break;
            case R.id.district_spinner:
                // Your another task
                break;

        }
    }

    @Override
    public void onBackPressed() {


        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            //moveTaskToBack(true);
            //AddCase.this.finish();
            //super.onBackPressed();
            Intent intent = new Intent(AddCase.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case 16908332:
                Intent intent = new Intent(AddCase.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void enable(boolean b) {
        enabled = b;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !enabled || super.dispatchTouchEvent(ev);
    }

    private void sendScreenName(String activity) {
        // [START screen_view_hit]
        Log.i(TAG, "Setting screen name: " + activity);
        mTracker.setScreenName("ADDCASE~" + activity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        // [END screen_view_hit]
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orientation = newConfig.orientation;

        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                // do what you want when user is in LANDSCAPE
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                // do what you want when user is in PORTRAIT
                break;
        }

    }

    public class AsyncTaskRunner extends AsyncTask<String, String, String> {
        LinkedHashMap mapData = new LinkedHashMap<String, String>();
        ProgressDialog progressDialog;
        private LoginParams parms;
        private LoginParams.TYPE type;
        private String[] resulttmp = new String[10];


        public AsyncTaskRunner(LoginParams parms, LoginParams.TYPE type) {
            this.parms = parms;
            this.type = type;
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            String response = null;
            try {
                String USER_AGENT = "Mozilla/21.0";
                //Log.i(TAG," In background "+parms.getUrl());
                Log.i(TAG, " LoginParams.TYPE " + type.toString());
                if (parms.getUrl().contains("app.ecourts.gov.in")) {
                    if (type == LoginParams.TYPE.ADDCASE) {
                        Log.i(TAG, " Add case " + courtcodefin);
                        if (courtcodefin.contains(",")) {
                            int i = 0;
                            for (String courtcode : courtcodefin.split(",")) {
                                Log.i("ADDCAS", "courtcode:" + courtcode);
                                String uri = parms.getUrl() + AESCryptCustom.encrypt(courtcode.trim());
                                Log.i("ADDCAS", "uri:" + uri);
                                RestTemplate restTemplate = new RestTemplate();
                                resulttmp[i] = restTemplate.getForObject(uri, String.class);
                                response = resulttmp[i];
                                i = i + 1;
                            }

                        } else {
                            String uri = parms.getUrl() + AESCryptCustom.encrypt(courtcode);
                            Log.i("ASYNC", "uri:" + uri);
                            RestTemplate restTemplate = new RestTemplate();
                            resulttmp[0] = restTemplate.getForObject(uri, String.class);
                            response = resulttmp[0];
                        }


                    } else {
                        String uri = parms.getUrl();
                        RestTemplate restTemplate = new RestTemplate();
                        response = restTemplate.getForObject(uri, String.class);

                    }

                } else {
                    RestTemplate restTemplate = new RestTemplate();
                    response = restTemplate.getForObject(parms.getUrl(), String.class);
                }

                //Need to handle add case seperately

            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            /*If result is null then show a message like site is not available*/
            if (result != null) {
                progressDialog.dismiss();
                /*Based on flag this should execute*/
                if (type == LoginParams.TYPE.DISTRITCT)
                    loaddistricsbystateid(result);
                else if (type == LoginParams.TYPE.COURT) {
                    //call courts
                    loadcourtsbydisid(result);
                } else if (type == LoginParams.TYPE.CASETYPE) {
                    Log.i(TAG, result);
                    loadcasetype(result);
                } else if (type == LoginParams.TYPE.POLICE) {
                    Log.i(TAG, result);
                    loadpolicestation(result);
                } else if (type == LoginParams.TYPE.ADDCASE) {
                    try {
                        List<Cases> cases = new ArrayList<Cases>();
                        for (String resulttemp : resulttmp) {
                            if (resulttemp != null) {
                                if (!resulttemp.isEmpty()) {
                                    if (!resulttemp.contains("error_")) {
                                        Log.i(TAG, "Result:" + resulttemp);
                                        JSONObject obj = new JSONObject(resulttemp);
                                        Log.i(TAG, "obj:" + obj);
                                        if (AESCryptCustom.decrypt(obj.getString("caseNos")).length() == 2) {
                                            Log.e(TAG, "No cases available ");
                                            //Commented every time it is showing like no cases available even there are cases
                                            //Toast.makeText(getApplicationContext(), "No cases available", Toast.LENGTH_LONG).show();
                                        } else {
                                            ObjectMapper objectMapper = new ObjectMapper();
                                            JSONArray arry = new JSONArray(AESCryptCustom.decrypt(obj.getString("caseNos")));
                                            if (arry.length() > 150) {
                                                Snackbar.make(getWindow().getDecorView().getRootView(), "Too many results available with the search criteria.Please try with more near key words", Snackbar.LENGTH_SHORT)
                                                        .setAction("No action", null).show();
                                            } else {
                                                for (int count = 0; count < arry.length(); count++) {
                                                    //TODO Keep a limit on number of cases to 100 so it won't crash the application if limit exceeds just show a message like results are more than count please search with appropriate names
                                                    JSONObject objtmp = arry.getJSONObject(count);
                                                    Log.i(TAG, objtmp.toString());
                                                    if (objtmp != null) {
                                                        Cases caseobj = objectMapper.readValue(objtmp.toString(), Cases.class);
                                                        caseobj.setEstablishment_name(AESCryptCustom.decrypt(obj.getString("establishment_name")).replaceAll("\"", " "));
                                                        caseobj.setCourt_code(AESCryptCustom.decrypt(obj.getString("court_code")).replaceAll("\"", " "));
                                                        caseobj.setState_code(statecode);
                                                        caseobj.setDist_code(districtcode);
                                                        caseobj.setSelected(false);
                                                        cases.add(caseobj);
                                                    } else {
                                                        Log.e(TAG, "No cases available objtmp");
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        Snackbar.make(getWindow().getDecorView().getRootView(), "Invalid Data", Snackbar.LENGTH_SHORT)
                                                .setAction("No action", null).show();
                                    }
                                }
                            } else {
                                Log.e(TAG, "Empty array break in future  ");
                                //break;
                            }
                        }
                        //Convert all the jsonobjects in to pojo classes
                        Log.i(TAG, "Size:" + cases.size());
                        if (!CollectionUtils.isEmpty(cases)) {
                            Intent intent = new Intent(AddCase.this, PackageActivity.class);
                            intent.putExtra("LIST", (Serializable) cases);
                            intent.putExtra("caller", "AddCase");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            }
                            startActivity(intent);
                            finish();
                        } else {
//                            Snackbar.make(getWindow().getDecorView().getRootView(), "No cases available", Snackbar.LENGTH_LONG)
//                                    .setAction("No action", null).show();
                            AlertDialog.Builder loginalert = new AlertDialog.Builder(AddCase.this);
                            loginalert.setMessage("No cases available");
                            loginalert.setTitle("Error Message");
                            loginalert.setPositiveButton("OK", null);
                            loginalert.setCancelable(true);
                            loginalert.create().show();
                            loginalert.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Log.i("DEBUGAD", "ON CLICK ON ALERT DIALOG");
                                        }
                                    });
                        }
                    } catch (Exception e) {
                        Log.i("ASYNC", "" + e.getMessage());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                }
            } else {
                progressDialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Site is temporarily unavailable", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(AddCase.this,
                    "",
                    "Loading ....");
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
}