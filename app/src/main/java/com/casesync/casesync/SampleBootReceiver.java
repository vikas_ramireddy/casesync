package com.casesync.casesync;

/**
 * Created by Vikas on 4/12/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.casesync.casesync.jobs.AutoSync;
import com.casesync.casesync.jobs.EvernoteJobCreator;
import com.casesync.casesync.jobs.NotificationJob;
import com.evernote.android.job.JobManager;

/**
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 */
// BEGIN_INCLUDE(autostart)
public class SampleBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") || intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON") || intent.getAction().equals("android.intent.action.REBOOT")) {
            //Running background process for notifications

            //Create a new instance for JobManager
            JobManager.create(context).addJobCreator(new EvernoteJobCreator());
            //Kill all the jobs and recreate with the new preference
            NotificationJob.cancelallJobs(AutoSync.TAG);
            NotificationJob.cancelallJobs(NotificationJob.TAGJOB);
            AutoSync.scheduleJob(context);
            NotificationJob.schedule(context);



        }
    }
}
