package com.casesync.casesync.WhatsAppFeature;

public class Const {
    public static final String PKG = "com.whatsapp";//Whatsapp Package
    public static final String BOT = "bot";//Bot
    public static final String STATUS = "status";//Prefrences toggle keys
    public static final String MESSAGE = "whatsappcmd";//Prefrences toggle keys
    public static final String LOG = "WAR-Log";
}