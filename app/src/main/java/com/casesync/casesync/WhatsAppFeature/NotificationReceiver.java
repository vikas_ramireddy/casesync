package com.casesync.casesync.WhatsAppFeature;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.springframework.util.CollectionUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class NotificationReceiver extends NotificationListenerService {
    public static final String KEY_QUICK_REPLY_TEXT = "quick_reply";
    private static final String TAG = "NL";
    static boolean isRunning = false;//is Running Bool
    SharedPreferences sharedPreferences;
    SharedPreferences prefs;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    //List<Integer> notificationid = new ArrayList<Integer>();
    // static Bundle bundle;
    private ArrayList<RemoteInput> remoteInputs = new ArrayList<RemoteInput>();

    @Override
    public void onNotificationPosted(final StatusBarNotification sbn) {
        if (Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners").contains(getPackageName())// Check for Notification Permission
                && isOn() && sbn != null && !sbn.isOngoing() && sbn.getPackageName().equals(Const.PKG) && sbn.getTag() != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Notification triggered:" + sbn.getKey() + ":" + sbn.getTag());
                    try {
                        Notification notification = sbn.getNotification();//Latest Notification of WhatsApp
                        if (notification != null) {
                            //if (!notificationid.contains(notification.number)) {
                            Bundle bundle = notification.extras;
                            //value = notification.number;
                            //notificationid.add(notification.number);
                            //Log.i(TAG, "Notification Number:" + notification.number + ": Status Noti Id:" + sbn.getId());
                            //logBundle(bundle);
                            remoteInputs = getRemoteInputs(notification);
                            if (!CollectionUtils.isEmpty(remoteInputs)) {
                                Object isGroupConversation = bundle.get("android.isGroupConversation");
                                String conversationTitle = bundle.getString("android.conversationTitle");
                                if (isGroupConversation != null) {
                                    boolean isGroup = (((boolean) isGroupConversation) && (conversationTitle != null));//Group Params
                                    Log.i(TAG, "Group Conversation:" + isGroup);
                                    if (isGroup) {
                                        Log.i(TAG, "Group Conversation");
                                    } else {
                                        Object title = bundle.get("android.title");//Chat Title
                                        Object text = bundle.get("android.text");//Chat Text
                                        if (title != null && text != null) {
                                            Log.i(TAG, "Text:" + text.toString());
                                            HashSet<String> mobileNumbers = getMobileNumbers(getApplicationContext(), title.toString());
                                            //String mobilenumber = get_Number(title.toString(), getApplicationContext());
                                            if (text.toString().equalsIgnoreCase("help")) {
                                                if (CollectionUtils.isEmpty(mobileNumbers)) {
                                                    sendMsg("Your mobile number is not saved in the advocates mobile.Please contact him to save your contact", notification, bundle);
                                                } else {
                                                    sendMsg("*Casesync Bot Commands* :-\n\n" + getAutoreplyText() + " - To get case wise hearing dates and business details.\n\nYou can download the Casesync-ecourts app from \nhttps://play.google.com/store/apps/details?id=com.casesync&hl=en\n\n", notification, bundle);
                                                }
                                            } else if (text.toString().equalsIgnoreCase(getAutoreplyText())) {
                                                if (CollectionUtils.isEmpty(mobileNumbers)) {
                                                    sendMsg("Your mobile number is not saved in the advocates mobile.Please contact him to save your contact", notification, bundle);
                                                } else {
                                                    String message = getMessage(text.toString(), mobileNumbers, getApplicationContext());
                                                    Log.d(TAG, ":" + message);
                                                    sendMsg(message, notification, bundle);

                                                }
                                            } else {
                                                Log.d(TAG, "Command Not found1");
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {

                    }
                }
            }).start();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "On create");
        isRunning = true;
        sharedPreferences = getSharedPreferences(Const.BOT, Context.MODE_PRIVATE);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "On destroy");
        isRunning = false;
        super.onDestroy();
    }

    /*
     * Get Status of Toggle
     */
    private boolean isOn() {
        return sharedPreferences.getBoolean(Const.STATUS, false);
    }

    /*
     * Get Message Text
     */
    private String getAutoreplyText() {
        return prefs.getString(Const.MESSAGE, "1");
    }


    /*
     * get Remote Input of Notification
     */
    private ArrayList<RemoteInput> getRemoteInputs(Notification notification) {
        ArrayList<RemoteInput> remoteInputs = new ArrayList<>();
        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender(notification);
        for (NotificationCompat.Action act : wearableExtender.getActions()) {
            if (act != null && act.getRemoteInputs() != null) {
                remoteInputs.addAll(Arrays.asList(act.getRemoteInputs()));
            }
        }
        return remoteInputs;
    }

    public String getMessage(String command, HashSet<String> mobilenumber, Context context) {
        HashSet<String> mobno = new HashSet<String>();
        String messagetext = "";
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                CaseSyncOpenDatabaseHelper.class);
        try {
            if (!CollectionUtils.isEmpty(mobilenumber)) {
                for (String mobile : mobilenumber) {
                    if (mobile.substring(0, 1).equalsIgnoreCase("0")) {
                        mobile = mobile.substring(1, mobile.length());
                    } else {
                        mobile = mobile.replaceAll(" ", "").replace("+91", "");
                    }
                    //Because it is a hashset duplicates won't be available
                    mobno.add(mobile);
                }
            }
            Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            List<CaseObj> casedet = new ArrayList<CaseObj>();
            for (String mobile : mobno) {
                Log.i(TAG, "Mobile Number:" + mobile);
                List<CaseObj> casedetails = casedetDao.queryForEq("phone", mobile.replaceAll(" ", "").replace("+91", ""));
                if (!CollectionUtils.isEmpty(casedetails)) {
                    //Added to ensure only active cases will be available.
                    List<CaseObj> filtered_list=new ArrayList<CaseObj>();
                    for(CaseObj obj:casedetails)
                    {
                        if(obj.getArchive().equals("N"))
                        {
                            filtered_list.add(obj);
                        }
                    }
                    casedet.addAll(filtered_list);
                }
            }
            if (!CollectionUtils.isEmpty(casedet)) {
                messagetext = messagetext + "*Case details* \n\n";
                Log.d(TAG, "Total number of cases " + casedet.size());
                int count = 0;
                for (CaseObj caseObj : casedet) {
                    if (command.equalsIgnoreCase(getAutoreplyText())) {
                        if (caseObj.getDateNextList() != null) {
                            if (count == 0) {
                                messagetext = messagetext + caseObj.getTypeName() + "/" + caseObj.getRegNo() + "/" + caseObj.getRegYear() + "\n" + caseObj.getPetName() + " VS " + caseObj.getResName() + "\n*Court*:" + caseObj.getDesgname() + " \n*Business*:" + caseObj.getWritinfo() + "\n*Next Hearing Date*:" + sdf.format(caseObj.getDateNextList());
                            } else {
                                messagetext = messagetext + "\n\n" + caseObj.getTypeName() + "/" + caseObj.getRegNo() + "/" + caseObj.getRegYear() + "\n" + caseObj.getPetName() + " VS " + caseObj.getResName() + "\n*Court*:" + caseObj.getDesgname() + " \n*Business*:" + caseObj.getWritinfo() + "\n*Next Hearing Date*:" + sdf.format(caseObj.getDateNextList());
                            }
                        } else {
                            if (count == 0) {
                                messagetext = messagetext + caseObj.getTypeName() + "/" + caseObj.getRegNo() + "/" + caseObj.getRegYear() + "\n" + caseObj.getPetName() + " VS " + caseObj.getResName() + "\n*Court*:" + caseObj.getDesgname() + "\n*Business*:" + caseObj.getWritinfo() + "\n *Next Hearing Date*:N/A";
                                ;
                            } else {
                                messagetext = messagetext + "\n\n" + caseObj.getTypeName() + "/" + caseObj.getRegNo() + "/" + caseObj.getRegYear() + "\n" + caseObj.getPetName() + " VS " + caseObj.getResName() + "\n*Court*:" + caseObj.getDesgname() + "\n*Business*:" + caseObj.getWritinfo() + "\n *Next Hearing Date*:N/A";
                                ;
                            }
                        }
                    }
                    count = count + 1;
                }
            } else {
                messagetext = messagetext + "No cases registered with your mobile number.Please contact your advocate to add your number against your case/cases";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messagetext;
    }

    //TODO Need to check for the permissions in the future
    public String get_Number(String name, Context context) {
        String number = "";


        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor people = context.getContentResolver().query(uri, projection, null, null, null);

        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        do {
            String Name = people.getString(indexName);
            String Number = people.getString(indexNumber);
            if (Name.equalsIgnoreCase(name)) {
                return Number.replace("-", "");
            }
            // Do work...
        } while (people.moveToNext());
        if (!number.equalsIgnoreCase("")) {
            return number.replace("-", "");
        } else return number;
    }

    public HashSet<String> getMobileNumbers(Context context, String name) {
        HashSet<String> mobilelist = new HashSet<String>();
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                "DISPLAY_NAME = '" + name + "'", null, null);
        if (cursor.moveToFirst()) {
            String contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
            while (phones.moveToNext()) {
                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                Log.i(TAG, type + ":" + number);
                mobilelist.add(number);
            }
            phones.close();
        }
        cursor.close();
        return mobilelist;
    }

    public HashSet<String> getContactdetails(Context context, String name) {
        HashSet<String> mobilelist = new HashSet<String>();
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, projection, "DISPLAY_NAME = '" + name + "'", null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred        = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                Log.i(contactName + ": TYPE_MOBILE", " " + phoneNo);
                                mobilelist.add(phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                Log.i(contactName + ": TYPE_HOME", " " + phoneNo);
                                mobilelist.add(phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                mobilelist.add(phoneNo);
                                Log.i(contactName + ": TYPE_WORK", " " + phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                mobilelist.add(phoneNo);
                                Log.i(contactName + ": TYPE_WORK_MOBILE", " " + phoneNo);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                mobilelist.add(phoneNo);
                                Log.i(contactName + ": TYPE_OTHER", " " + phoneNo);
                                break;
                            default:
                                break;
                        }
                    }
                    pCursor.close();
                }
            }
            cursor.close();
        }
        return mobilelist;
    }

    /*
     * Send/Reply through Notification
     */
    private void sendMsg(String msg, Notification notification, Bundle bundle) {
        RemoteInput[] allremoteInputs = new RemoteInput[remoteInputs.size()];
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Iterator it = remoteInputs.iterator();
        int i = 0;
        while (it.hasNext()) {
            allremoteInputs[i] = (RemoteInput) it.next();
            bundle.putCharSequence(allremoteInputs[i].getResultKey(), msg);//This work, apart from Hangouts as probably they need additional parameter (notification_tag?)
            i++;
        }
        RemoteInput.addResultsToIntent(allremoteInputs, localIntent, bundle);
        try {
            Objects.requireNonNull(replyAction(notification)).actionIntent.send(this, 0, localIntent);
        } catch (PendingIntent.CanceledException e) {
            Log.e(Const.LOG, "replyToLastNotification error: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private NotificationCompat.Action replyAction(Notification notification) {
        NotificationCompat.Action action;
        for (NotificationCompat.Action action2 : new NotificationCompat.WearableExtender(notification).getActions()) {
            if (isAllowFreeFormInput(action2)) {
                return action2;
            }
        }
        if (!(notification == null || notification.actions == null)) {
            for (int i = 0; i < NotificationCompat.getActionCount(notification); i++) {
                action = NotificationCompat.getAction(notification, i);
                if (isAllowFreeFormInput(action)) {
                    return action;
                }
            }
        }
        return null;
    }

    /*
     * Checks for Text Input
     */
    private boolean isAllowFreeFormInput(NotificationCompat.Action action) {
        if (action.getRemoteInputs() == null) {
            return false;
        }
        for (RemoteInput allowFreeFormInput : action.getRemoteInputs()) {
            if (allowFreeFormInput.getAllowFreeFormInput()) {
                return true;
            }
        }
        return false;
    }

    /*
     * Log Notification Bundle
     */
    private void logBundle(Bundle bundle) {
        try {
            FileOutputStream out = new FileOutputStream(getCacheDir() + "/BundleDump-" + System.currentTimeMillis() + ".txt");
            for (String key : bundle.keySet()) {
                String data = key + " | " + bundle.get(key) + "\n";
                out.write(data.getBytes());
            }
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*
        Android 9.0 Pie Group Chat Notification:
        android.title | WAChatBot
        android.android.reduced.images | true
        android.conversationTitle | WAChatBot
        android.subText | null
        android.car.EXTENSIONS | Bundle[mParcelledData.dataSize=1028]
        android.template | android.aandroid.progress | 0
        android.progressMax | 0
        android.appInfo | ApplicationInfo{f965cc9 com.whatsapp}
        android.showWhen | true
        android.largeIcon | null
        android.infoText | null
        android.progressIndeterminate | false
        android.remoteInputHistory android.messages | [Landroid.os.Parcelable;@58b0eef
        android.showWhen | true
        android.largeIcon | android.graphics.Bitmap@69c5dfc
        android.messagingUser | android.app.Person@21efc85
        android.infoText | null
        android.wearable.EXTENSIONS | Bundle[mParcelledData.dataSize=996]
        android.progressIndeterminate | false
        android.remoteInputHistory | null
        android.isGroupConversation | true

        Android 9.0 Pie Personal Chat Notification:
        android.title | My Jio
        android.reduced.images | true
        android.subText | null
        android.car.EXTENSIONS | Bundle[mParcelledData.dataSize=1020]
        android.template | android.app.Notification$MessagingStyle
        android.showChronometer | false
        android.icon | 2131231578
        android.text | Hello
        android.progress | 0
        android.progressMax | 0
        android.selfDisplayName | You
        android.appInfo | ApplicationInfo{6c058a7 com.whatsapp}
        android.messages | [Landroid.os.Parcelable;@312d154
        android.showWhen | true
        android.largeIcon | android.graphics.Bitmap@5c2e8fd
        android.messagingUser | android.app.Person@7a879f2
        android.infoText | null
        android.wearable.EXTENSIONS | Bundle[mParcelledData.dataSize=980]
        android.progressIndeterminate | false
        android.remoteInputHistory | null
        android.isGroupConversation | false
        */
    }

    /*
     * Parse Group title with Combined Messages
     */
    private String[] parseTitle(String title) {
        String[] title_split = String.valueOf(title).split("[^\\s\\w\\d]");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < title_split.length; i++) {
            if (i > 0) {
                builder.append(",");
            }
            builder.append(title_split[i]);
        }
        return builder.toString().split(",");
    }
}