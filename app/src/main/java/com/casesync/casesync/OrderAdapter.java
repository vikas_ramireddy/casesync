package com.casesync.casesync;

/**
 * Created by Juned on 4/14/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.casesync.utill.OrderDetails;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private static String TAG = "APSADA";
    Context context;
    List<OrderDetails> stringList;


    public OrderAdapter(Context context, List<OrderDetails> list) {
        this.context = context;
        stringList = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view2 = LayoutInflater.from(context).inflate(R.layout.orderview_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view2);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setIsRecyclable(false);
        final OrderDetails dataModel = stringList.get(position);
        viewHolder.court.setText(dataModel.getCourtname());
        viewHolder.case_number.setText(dataModel.getCasenumber());
        viewHolder.petres.setText(dataModel.getPetres());
        viewHolder.order_type.setText(dataModel.getOrdertype());
        viewHolder.orderdate.setText(dataModel.getOrderdate());
        viewHolder.case_status.setText(dataModel.getCase_status());

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i(TAG, "" + dataModel.getUrl());
                //Need to handle order is not uploaded case
                try {
                    ConnectivityManager connMgr = (ConnectivityManager)
                            arg0.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
//                        Intent intent = new Intent();
//                        intent.setDataAndType(Uri.parse(dataModel.getUrl()), "application/pdf");
//                        arg0.getContext().startActivity(intent);

//                        AppCompatActivity activity = (AppCompatActivity) arg0.getContext();
//                        FragmentManager fm = activity.getSupportFragmentManager();
//                        OrderViewFragment historyFragment = OrderViewFragment.newInstance(dataModel.getUrl(), "text/html");
//                        historyFragment.show(fm, "web_view");
                        //TODO Redirect to activity with URL Share and download option
                        AppCompatActivity activity = (AppCompatActivity) arg0.getContext();
                        Intent intent = new Intent(activity, OrderPDF.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        intent.putExtra("order", (Serializable) dataModel);
                        arg0.getContext().startActivity(intent);


                    } else {
                        Snackbar.make(arg0, "Please switch on the internet ", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Snackbar.make(arg0, "Order is not uploaded for  " + dataModel.getCasenumber(), Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;

        public TextView court;
        public TextView case_number;
        public TextView petres;
        public TextView orderdate;
        public TextView order_type;
        public TextView case_status;
        View itemView;

        public ViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.order_view);
            court = view.findViewById(R.id.court);
            case_number = view.findViewById(R.id.case_number);
            petres = view.findViewById(R.id.petres);
            orderdate = view.findViewById(R.id.orderdate);
            order_type = view.findViewById(R.id.order_type);
            case_status = view.findViewById(R.id.case_status);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}