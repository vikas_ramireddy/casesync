package com.casesync.casesync;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.casesync.utill.Cases;
import com.casesync.utill.ImportObj;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PackageActivity extends AppCompatActivity {
    static List<ImportObj> currentimport = new ArrayList<>();
    List<Cases> currentcases = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    ArrayList<Cases> casedataModels = new ArrayList<Cases>();
    String caller = "";
    private String TAG = "RCV";
    private Button btnSubmit;
    private CheckBox checkall;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            //getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Results");
        }
        caller = getIntent().getStringExtra("caller");
        //TODO New menu should be added which should show total number of cases

        recyclerView = findViewById(R.id.recycler_view);
        // Passing the column number 1 to show online one column in each row.
        recyclerViewLayoutManager = new GridLayoutManager(PackageActivity.this, 1);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        currentcases.clear();
        currentimport.clear();
        Intent i = getIntent();
        ArrayList<Object> dataModels = (ArrayList<Object>) i.getSerializableExtra("LIST");

        //recyclerView.setHasFixedSize(true);
        //recyclerView.setItemViewCacheSize(150);
        //recyclerView.setDrawingCacheEnabled(true);
        //recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        if (dataModels != null) {
            if (!CollectionUtils.isEmpty(dataModels)) {
                if (dataModels.get(0) instanceof Cases) {
                    casedataModels = (ArrayList<Cases>) i.getSerializableExtra("LIST");
                    adapter = new AppsAdapter(PackageActivity.this, casedataModels);

                } else if (dataModels.get(0) instanceof ImportObj) {
                    ArrayList<ImportObj> importModels = (ArrayList<ImportObj>) i.getSerializableExtra("LIST");
                    adapter = new AppsAdapter(PackageActivity.this, true, importModels);
                }
            }

        }

       /* adapter = new AppsAdapter(PackageActivity.this,dataModels, new AppsAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(Object item) {

                currentSelectedItems.add(item);
                Log.d(TAG,"Added "+currentSelectedItems.size());
            }

            @Override
            public void onItemUncheck(Object item) {
                        currentSelectedItems.remove(item);
                        Log.d(TAG,"Removed "+currentSelectedItems.size());
            }
        });*/
        spinner = findViewById(R.id.addcase_process);
        recyclerView.setAdapter(adapter);
        btnSubmit = findViewById(R.id.addselected);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Log.i(TAG, "Button click");
                for (Cases cases : casedataModels) {
                    if (cases.isSelected()) {
                        currentcases.add(cases);
                    }
                }
                if (!CollectionUtils.isEmpty(currentcases)) {
                    Intent i = new Intent(getApplicationContext(), Addexitingcase.class);
                    i.putExtra("LIST", (Serializable) currentcases);
                    i.putExtra("key", TYPE.CASES);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    getApplicationContext().startActivity(i);

                }
                if (!CollectionUtils.isEmpty(currentimport)) {
                    Intent i = new Intent(getApplicationContext(), NavActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    i.putExtra("key", TYPE.IMPORTOBJ);
                    getApplicationContext().startActivity(i);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (!StringUtils.isEmpty(caller)) {
            try {

                Class callerClass = Class.forName(caller);
                Intent intent = new Intent(this, callerClass);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            int count = getFragmentManager().getBackStackEntryCount();
            if (count == 0) {
                Intent intent = new Intent(PackageActivity.this, NavActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            } else {
                getFragmentManager().popBackStack();
            }
        }

    }

/*    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select, menu);
        CheckBox checkBox = (CheckBox) menu.findItem(R.id.resultmenu).getActionView();
        checkBox.setText("");
        checkBox.setChecked(false);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // perform logic
                    //Need to mark all the
                }
            }
        });

        return true;
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (!StringUtils.isEmpty(caller)) {

                    if (caller.equals("AddCase")) {
                        Intent intent = new Intent(this, AddCase.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        startActivity(intent);
                        finish();
                    }
                    return true;
                } else {
                    Intent intent = new Intent(PackageActivity.this, NavActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(intent);
                    finish();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public enum TYPE {
        CASES,
        IMPORTOBJ
    }


}
