package com.casesync.casesync.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.casesync.casesync.CasesyncUtill;
import com.casesync.casesync.OrderPDF;
import com.casesync.casesync.R;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.OrderDetails;
import com.google.android.material.snackbar.Snackbar;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OrderFragment extends DialogFragment {

    private static String ARG_PARAM1 = "param1";
    private static String ARG_PARAM2 = "param2";
    private final String TAG = "ORD";
    ListView listView;
    ArrayList<String> dataModels = new ArrayList<String>();
    ArrayList<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
    private String mParam1;
    private String mParam2;
    CasesyncUtill utill = new CasesyncUtill();

    public OrderFragment() {
        // Required empty public constructor
    }

    public static OrderFragment newInstance(String cino, String param2) {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, cino);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orderview, container, false);
        //TODO Get the CIN Number as a parameter
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            Log.e(TAG, "CINO:" + mParam1);
            List<CaseObj> casedet = casedetDao.queryForEq("cino", mParam1);
            if (!CollectionUtils.isEmpty(casedet)) {
                CaseObj casedetails = casedet.get(0);
                if (casedetails.getInterimOrder() == null && casedetails.getFinalOrder() == null) {
                    AlertDialog.Builder loginalert = new AlertDialog.Builder(view.getContext());
                    loginalert.setMessage("No Orders Available");
                    loginalert.setTitle("Error Message");
                    loginalert.setPositiveButton("OK", null);
                    loginalert.setCancelable(true);
                    loginalert.create().show();
                    loginalert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.i("DEBUGAD", "ON CLICK ON ALERT DIALOG");
                                }
                            });
                } else {
                    List<OrderDetails> orderlist = utill.getOrderList(casedetails);
                    for (OrderDetails order : orderlist) {
                        if (order.getOrderno() != null) {
                            orderDetails.add(order);
                            dataModels.add("Order Number:" + order.getOrderno() + "\nOrder Date:" + order.getOrderdate());
                        }
                    }
                }
            } else {
                //Invalid case number
                Log.i(TAG, "No Data available");

            }

            ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_list_item_1, dataModels);
            listView = view.findViewById(R.id.fileslist);
            listView.setAdapter(adapter);
            registerForContextMenu(listView);
            listView.setBackgroundResource(R.drawable.plus);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.i(TAG, "position:" + position + ":orderDetails:" + orderDetails.size());
                    OrderDetails dataModel = orderDetails.get(position);
                    Log.i(TAG, dataModel.getUrl());
                    //TODO Download the order here
                    ConnectivityManager connMgr = (ConnectivityManager)
                            parent.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {

                        //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataModel.getUrl()));
                        //startActivity(browserIntent);
//                        Uri uri = Uri.parse("http://docs.google.com/viewer?url=" + dataModel.getUrl());
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setDataAndType(uri, "text/html");
//                        startActivity(intent);


//                        String format = "https://drive.google.com/viewerng/viewer?embedded=true&url=%s";
//                        String fullPath = String.format(Locale.ENGLISH, format, dataModel.getUrl());
//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fullPath));
//                        startActivity(browserIntent);

//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataModel.getUrl()));
//                        startActivity(browserIntent);

//                        Intent intent = new Intent();
//                        intent.setDataAndType(Uri.parse(dataModel.getUrl()), "application/pdf");
//                        startActivity(intent);
                        AppCompatActivity activity = (AppCompatActivity) view.getContext();
                        Intent intent = new Intent(activity, OrderPDF.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        }
                        intent.putExtra("order", (Serializable) dataModel);
                        intent.putExtra("caller", "CaseDetail");
                        view.getContext().startActivity(intent);

                    } else {
                        Snackbar.make(parent, "Please switch on the internet ", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage());
        }

        return view;
    }




}
