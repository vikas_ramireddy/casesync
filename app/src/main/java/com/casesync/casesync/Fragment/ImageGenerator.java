package com.casesync.casesync.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ImageGenerator {
    private final String LIBRARY_TAG = "Dynamic Calendar Icon";
    private final String IMAGE_GENERATED = "com.dynamic.image.generator";
    private final String IMAGE_GENERATED_KEY = "com.dynamic.image.generator.count";
    SharedPreferences mPrefs;
    private Context mContext;
    private Bitmap mDestination;
    private float mScaleFactor;
    private int mIconWidth;
    private int mIconHeight;
    private float mMonthSize;
    private float mDateSize;
    private int mDatePosition;
    private int mMonthPosition;
    private int mMonthColor;
    private int mDateColor;
    private boolean mMonthColorSet = false;
    private boolean mDateColorSet = false;
    private Typeface mDateTypeFace;
    private Typeface mMonthTypeFace;
    private boolean mDateTypeFaceSet = false;
    private boolean mMonthTypeFaceSet = false;
    private boolean mNeedToStoreInStorage = false;
    private String mDate;
    private String mMonth;

    public ImageGenerator(Context context) {
        this.mContext = context;
        this.mScaleFactor = this.mContext.getResources().getDisplayMetrics().density;
        this.mPrefs = this.mContext.getSharedPreferences("com.dynamic.image.generator", 0);
    }

    public void setIconSize(int width, int height) {
        this.mIconWidth = (int) this.mScaleFactor * width;
        this.mIconHeight = (int) this.mScaleFactor * height;
    }

    public void setMonthSize(float monthSize) {
        this.mMonthSize = (float) ((int) this.mScaleFactor) * monthSize;
    }

    public void setDateSize(float dateSize) {
        this.mDateSize = (float) ((int) this.mScaleFactor) * dateSize;
    }

    public void setMonthPosition(int y) {
        this.mMonthPosition = (int) this.mScaleFactor * y;
    }

    public void setDatePosition(int y) {
        this.mDatePosition = (int) this.mScaleFactor * y;
    }

    public void setMonthColor(int color) {
        this.mMonthColor = color;
        this.mMonthColorSet = true;
    }

    public void setDateColor(int color) {
        this.mDateColor = color;
        this.mDateColorSet = true;
    }

    public void setDateTypeFace(String fontName) {
        this.mDateTypeFace = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/" + fontName);
        this.mDateTypeFaceSet = true;
    }

    public void setMonthTypeFace(String fontName) {
        this.mMonthTypeFace = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/" + fontName);
        this.mMonthTypeFaceSet = true;
    }

    public void setStorageToSDCard(boolean toSDCard) {
        this.mNeedToStoreInStorage = toSDCard;
    }

    public Bitmap generateDateImage(Calendar dateString, int backgroundImage) {
        Log.d("Dynamic Calendar Icon", "The destination size set is: " + this.mIconWidth + "x" + this.mIconHeight);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
        this.mDate = dateFormat.format(dateString.getTime());
        this.mMonth = monthFormat.format(dateString.getTime());
        Log.d("Dynamic Calendar Icon", this.mDate + ":" + this.mMonth);
        Options options = new Options();
        options.inScaled = false;
        this.mDestination = BitmapFactory.decodeResource(this.mContext.getResources(), backgroundImage, options).copy(Bitmap.Config.ARGB_8888, true);
        Log.d("Dynamic Calendar Icon", "Size of the image selected: " + this.mDestination.getWidth() + " x " + this.mDestination.getHeight());
        this.mDestination = Bitmap.createScaledBitmap(this.mDestination, this.mIconWidth, this.mIconHeight, false);
        Rect bounds = new Rect();
        Canvas canvas = new Canvas(this.mDestination);
        Paint paint = new Paint(1);
        canvas.drawBitmap(this.mDestination, 0.0F, 0.0F, (Paint) null);
        paint.setTextSize(this.mMonthSize);
        if (this.mMonthTypeFaceSet) {
            paint.setTypeface(this.mMonthTypeFace);
        }

        if (this.mMonthColorSet) {
            paint.setColor(this.mMonthColor);
        } else {
            paint.setColor(-16776961);
        }

        paint.getTextBounds(this.mMonth, 0, this.mMonth.length(), bounds);
        canvas.drawText(this.mMonth, (float) ((canvas.getWidth() - bounds.width()) / 2), (float) this.mMonthPosition, paint);
        paint.setTextSize(this.mDateSize);
        if (this.mDateTypeFaceSet) {
            paint.setTypeface(this.mDateTypeFace);
        }

        if (this.mDateColorSet) {
            paint.setColor(this.mDateColor);
        } else {
            paint.setColor(-16711936);
        }

        paint.getTextBounds(this.mDate, 0, this.mDate.length(), bounds);
        canvas.drawText(this.mDate, (float) ((canvas.getWidth() - bounds.width()) / 2), (float) this.mDatePosition, paint);
        Log.d("Dynamic Calendar Icon", "Image has been generated!");
        this.mPrefs.edit().putInt("com.dynamic.image.generator.count", this.mPrefs.getInt("com.dynamic.image.generator.count", 0) + 1).apply();
        if (this.mNeedToStoreInStorage) {
            try {
                File dirMake = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/CalendarImageGenerated/");
                dirMake.mkdirs();
                this.mDestination.compress(CompressFormat.PNG, 100, new FileOutputStream(new File(dirMake, this.mPrefs.getInt("com.dynamic.image.generator.count", 0) + ".png")));
                Log.d("Dynamic Calendar Icon", "Image Stored in " + dirMake.getAbsolutePath() + "GeneratedCalendar.png");
                return this.mDestination;
            } catch (FileNotFoundException var10) {
                var10.printStackTrace();
            }
        }

        return this.mDestination;
    }
}
