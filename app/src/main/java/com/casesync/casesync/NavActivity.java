package com.casesync.casesync;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.casesync.casesync.jobs.InitialForegroundService;
import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.CauseListUtil;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.FileImportUtility;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PermissionUtility;
import com.casesync.utill.PropertyUtil;
import com.casesync.utill.Useraccount;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import static android.provider.DocumentsContract.EXTRA_INITIAL_URI;


@SuppressLint("SimpleDateFormat")
public class NavActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AsyncResponse, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    public static final int maxcases = 2100;
    private static final int FILE_PICKER_RESULT = 2;
    private static final int MY_CAL_WRITE_REQ = 0;
    private static final String PREFS_NAME = "LoginPrefs";
    private static final String AUTHORITY = "com.casesync.fileprovider";
    public static String email = "";
    public static String password = "";
    public static String username = "";
    public static List<Useraccount> account = new CopyOnWriteArrayList<Useraccount>();
    public static Map<String, Object> extraData = new HashMap<>();
    private final String TAG = "NAVACT";
    protected boolean enabled = true;
    CasesyncUtill syncutil = new CasesyncUtill();
    CalendarView calendar;
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences preference;
    LoginParams loginparams = new LoginParams();
    private Tracker mTracker;
    private CaldroidFragment caldroidFragment;
    private ProgressBar mProgressView;
    private CaldroidFragment dialogCaldroidFragment;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;

    private void sendScreenName(String activity) {

        // [START screen_view_hit]
        Log.i(TAG, "Setting screen name: " + activity);
        mTracker.setScreenName("Sync~" + activity);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        // [END screen_view_hit]
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setTitle("Case sync");
        extraData.clear();
        Log.i(TAG, "Clearing the list..............." + FirebaseInstanceId.getInstance().getId());

        Bundle bundle = getIntent().getExtras();
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mProgressView = findViewById(R.id.sync_progress);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferences pref = getSharedPreferences(PREFS_NAME, 0);
        preference = pref;
        NavActivity.email = pref.getString("email", "");
        NavActivity.password = pref.getString("password", "");
        NavActivity.username = pref.getString("username", "");
        try {
            if (bundle != null) {

                if (bundle.getString("intialsync") != null && !bundle.getString("intialsync").equals("")) {
                    getIntent().removeExtra("intialsync");
                    Intent intent = new Intent(NavActivity.this, InitialForegroundService.class);
                    intent.setAction(InitialForegroundService.ACTION_START_FOREGROUND_SERVICE);
                    startService(intent);

                } else {
                    PackageActivity.TYPE typ = (PackageActivity.TYPE) getIntent().getSerializableExtra("key");
                    if (typ != null) {
                        if (typ == PackageActivity.TYPE.IMPORTOBJ) {
                            Intent intent = new Intent(NavActivity.this, InitialForegroundService.class);
                            intent.setAction(InitialForegroundService.ACTION_IMPORT);
                            intent.putExtra("key", PackageActivity.TYPE.IMPORTOBJ);
                            startService(intent);
                        }

                    }
                }

            }
            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);

            TextView name = header.findViewById(R.id.username);
            TextView email = header.findViewById(R.id.useremail);
            TextView application_version = header.findViewById(R.id.application_version);
            application_version.setText("Version:" + BuildConfig.BUILD_TYPE + "_" + BuildConfig.VERSION_NAME);
            if (NavActivity.username != null && !NavActivity.username.equals(""))
                name.setText(NavActivity.username);
            if (NavActivity.email != null && !NavActivity.email.equals(""))
                email.setText(NavActivity.email);

        } catch (Exception e) {
            Log.i("NAV", "Error :" + e.getMessage());
            e.printStackTrace();
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NavActivity.this, AddCase.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            }
        });

        //Caldroid  code starts from here
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        //Map<String, Object> extraData = new HashMap<>();
        caldroidFragment = new CaldroidSampleCustomFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        } else {
            Bundle args = new Bundle();
            java.util.Calendar cal = java.util.Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(java.util.Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(java.util.Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
            args.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
            // Uncomment this to customize startDayOfWeek
            args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
                    CaldroidFragment.MONDAY);
            // Uncomment this line to use Caldroid in compact mode
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, true);
            // Uncomment this line to use dark theme
            //            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);
            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();
        try {
            caldroidFragment.setExtraData(extraData);
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage());
        }

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                try {
                    CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getApplicationContext(),
                            CaseSyncOpenDatabaseHelper.class);
                    try {
                        StringBuilder casedetails = new StringBuilder();
                        Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
                        List<CaseObj> casedet = casedetDao.queryForEq("date_next_list", date);
                        if (casedet != null && casedet.size() > 0) {
                            int casesize = 0;
                            for (CaseObj cases : casedet) {
                                casedetails.append("" + cases.getTypeName() + "/" + cases.getRegNo() + "\n");
                            }
                            FragmentManager fm = getSupportFragmentManager();
                            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                            BlankFragment searchDialogFragment = BlankFragment.newInstance(df.format(date), "");
                            searchDialogFragment.show(fm, "fragment_caseview");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChangeMonth(int month, int year) {
                String text = "month: " + month + " year: " + year;
/*                Toast.makeText(getApplicationContext(), text,
                        Toast.LENGTH_SHORT).show();*/
            }

            @Override
            public void onLongClickDate(Date date, View view) {
                Log.i("CalenderView", "Date clicked:");
/*                Toast.makeText(getApplicationContext(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();*/


            }

            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
/*                    Toast.makeText(getApplicationContext(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();*/
                }
            }

        };
        caldroidFragment.setCaldroidListener(listener);
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav, menu);
        return true;
/*        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchItem.getActionView();

        // Configure the search info and add any event listeners...

        return super.onCreateOptionsMenu(menu);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.search) {
            /*Here implement the search fragment*/

            FragmentManager fm = getSupportFragmentManager();
            AdvancedSearchDialogFragment searchDialogFragment = AdvancedSearchDialogFragment.newInstance("Advanced Search");
            searchDialogFragment.show(fm, "fragment_advanced_search");
        } else if (id == R.id.action_sync) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
//                if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED)
//                        & (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)) {
//                    syncutil.addAppointmentsToCalender(getApplicationContext(), "test", "test", "test", 1, System.currentTimeMillis(), true, true);
//                } else {
//                    askForPermission(Manifest.permission.WRITE_CALENDAR, 0, getApplicationContext(), NavActivity.this);
//                    askForPermission(Manifest.permission.READ_CALENDAR, 0, getApplicationContext(), NavActivity.this);
//                }

                //Commenting out for testing.
                //new CasesyncUtill().sendSMSUsingBR(getApplicationContext(),"+919441000464","Sample text for testing");

                try {
                    List<CaseObj> dataModel = new ArrayList<CaseObj>();
                    dataModel = syncutil.syncallcases(getApplicationContext(), CasesyncUtill.TYPE.GET);
                    if (!CollectionUtils.isEmpty(dataModel)) {
                        String url = PropertyUtil.getProperty("apiurl", getApplicationContext()) + "caseHistoryWebService.php";
                        LoginParams loginparams = new LoginParams();
                        loginparams.setUrl(url);
                        AsyncTaskRunner runner = new AsyncTaskRunner(loginparams, dataModel);
                        String sleepTime = "1";
                        runner.execute(sleepTime);
                    } else {
                        Toast.makeText(getApplicationContext(), "All cases are synchronized", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e("SYNC", "ERROR MESSAGE " + e.getMessage());
                }

            } else {
                AlertDialog.Builder loginalert = new AlertDialog.Builder(NavActivity.this);
                loginalert.setMessage("Synchronization requires internet");
                loginalert.setTitle("Please turn on the internet");
                loginalert.setPositiveButton("OK", null);
                loginalert.setCancelable(true);
                loginalert.create().show();
                loginalert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Log.i("Nav Activity", "nav_camera");
            Intent intent = new Intent(NavActivity.this, AddCase.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.courtcauselist) {
            Intent intent = new Intent(NavActivity.this, Causelist.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_cin) {
            Intent intent = new Intent(NavActivity.this, CINActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_gallery) {
            Log.i("Nav Activity", "nav_gallery");
            Intent intent = new Intent(NavActivity.this, CustomListView.class);
            intent.putExtra("disposed", "N");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_disposed) {
            Intent intent = new Intent(NavActivity.this, CustomListView.class);
            intent.putExtra("disposed", "Y");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();

        } else if (id == R.id.causelist) {
            Log.i(TAG, "Exporting todays causelist ");
            new CauseListUtil().generateTodaysCauseList(getApplicationContext(), NavActivity.this, CauseListUtil.CAUSELISTACTION.SHARE);
            return true;
        } else if (id == R.id.importcases) {
            int version = new PermissionUtility().getEcourtsVersion(getApplicationContext());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                //le Android 10 Full permissions are available.
                ErrorMessage message = new FileImportUtility().importCases(getApplicationContext());
                if (message.isStatus() == false) {
                    Snackbar.make(getWindow().getDecorView().getRootView(), message.getMessage(), Snackbar.LENGTH_SHORT)
                            .setAction("No action", null).show();
                }
            } else {
                Log.i(TAG, "Android 11. No permissions available.");
                String message = "Application will re-direct to file chooser. Go to -->@folder_name@.Please select myCases.txt file manually.\n If no file is available for selection please export using email/drive option and download from the email/drive.";
                if (version < 2) {
                    message = message.replaceAll("@folder_name@", "Internal Storage");
                } else {
                    message = message.replaceAll("@folder_name@", "Internal Storage-->Download");
                }
                AlertDialog.Builder loginalert = new AlertDialog.Builder(NavActivity.this);
                loginalert.setMessage(message);
                loginalert.setTitle("File Import.");
                loginalert.setCancelable(true);
                loginalert.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //Need to Open an activity. Where file to be.
                                //File picker which will open downloads folder so select a specific File. Give the file name as a hint to the user.
                                File downloads_folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TITLE, "myCases.txt");
                                intent.putExtra(EXTRA_INITIAL_URI, Uri.fromFile(downloads_folder));
                                try {
                                    startActivityForResult(intent, FILE_PICKER_RESULT);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                loginalert.create().show();

            }
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(NavActivity.this, CustomListView.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_manage) {
            Intent intent = new Intent(NavActivity.this, SettingsActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_share) {
            try {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Casesync");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.casesync");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } catch (Exception e) {
                Log.e(TAG, "Share exception:" + e.getMessage());
            }


        } else if (id == R.id.nav_rate) {
            try {
                Intent sharingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.casesync"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(sharingIntent);

            } catch (Exception e) {
                Log.e(TAG, "Share exception:" + e.getMessage());
            }


        } else if (id == R.id.orders) {
            Intent intent = new Intent(NavActivity.this, OrderActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_terms) {

            Intent intent = new Intent(NavActivity.this, TermsConditionsActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else if (id == R.id.support) {
            try {
                if (isAppInstalled("com.whatsapp")) {
                    String strWhatsAppNo = "919441000464";
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    intent.setType("text/plain");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.putExtra(Intent.EXTRA_TEXT, "Hello Casesync!");
                    intent.putExtra("data", "" + strWhatsAppNo);
                    intent.putExtra("jid", strWhatsAppNo + "@s.whatsapp.net"); //phone number without "+" prefix
                    intent.setPackage("com.whatsapp");
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        else if (id == R.id.nav_chat) {
//            try {
////                startActivity(new Intent(Intent.ACTION_VIEW,
////                        Uri.parse("fb://messaging/{#casesyncindia}")));
//                Uri uri = Uri.parse("fb-messenger://casesyncindia/");
//
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//
//            } catch (Exception e) {
//                Log.i("NAV", "Share exception:" + e.getMessage());
//            }
//
//        }
        else if (id == R.id.nav_send) {
            //Sign out
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.remove("logged");
            //uncomment after testing
            editor.remove("email");
            editor.commit();
            try {
                signOut();
                FirebaseAuth.getInstance().signOut();
                CaseSyncOpenDatabaseHelper adapter = new CaseSyncOpenDatabaseHelper(getApplicationContext());
                adapter.deleteTables();
                //Delete the data from calendar when the new app is having the permission for the calendar.
                //CasesyncUtill utill = new CasesyncUtill();
                //utill.clearCalendar(getApplicationContext());

                //Comment this
//                if (mBound) {
//                    // Unbind from the service. This signals to the service that this activity is no longer
//                    // in the foreground, and the service can respond by promoting itself to a foreground
//                    // service.
//                    unbindService(mServiceConnection);
//                    mBound = false;
//                }


            } catch (Exception e) {
                Log.e("NAV", "Logout exception:" + e.getMessage());
            }
            Intent intent = new Intent(NavActivity.this, PhoneAuthActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isAppInstalled(String packageName) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public boolean askForPermission(String permission, Integer requestCode, Context context, Activity activity) {
        boolean status = false;
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
            status = true;
        } else {
            status = true;
        }
        return status;
    }

    private void signOut() {
        Log.i(TAG, "Coming to sign out");
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }

    /**
     * Save current states of the Caldroid here
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }

        if (dialogCaldroidFragment != null) {
            dialogCaldroidFragment.saveStatesToKey(outState,
                    "DIALOG_CALDROID_SAVED_STATE");
        }
    }

    @Override
    public void processFinish(String output) {

        //Delegate

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onStop() {
//        if (mBound) {
//            // Unbind from the service. This signals to the service that this activity is no longer
//            // in the foreground, and the service can respond by promoting itself to a foreground
//            // service.
//            unbindService(mServiceConnection);
//            mBound = false;
//        }

        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void enable(boolean b) {
        enabled = b;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !enabled || super.dispatchTouchEvent(ev);
    }

    /*Main method which is used to display all the tiles in the calendar */
    private void setCustomResourceForDates() {

        try {
            Map<String, Object> extraData = new HashMap<String, Object>();

            java.util.Calendar cal = java.util.Calendar.getInstance();
            cal.add(java.util.Calendar.DATE, -7);
            Date blueDate = cal.getTime();
            Date minDate = cal.getTime();
            cal = java.util.Calendar.getInstance();
            cal.add(java.util.Calendar.DATE, 7);
            Date greenDate = cal.getTime();
            cal = java.util.Calendar.getInstance();
            cal.add(java.util.Calendar.DATE, 90);
            Date maxDate = cal.getTime();
            if (caldroidFragment != null) {
                ColorDrawable blue = new ColorDrawable(getColor(R.color.blue));
                ColorDrawable green = new ColorDrawable(Color.GREEN);
                caldroidFragment.setMinDate(minDate);
                caldroidFragment.setMaxDate(maxDate);
                //caldroidFragment.setSixWeeksInCalendar(true);
            }
            TextView casecount = findViewById(R.id.total_cases);
            TextView disposal_count = findViewById(R.id.disposed_cases);
            TextView todayscount = findViewById(R.id.todays_cases);
            TextView yettoupdate = findViewById(R.id.unupated_cases);

            casecount.setText("" + syncutil.getCasesCount(getApplicationContext(), CasesyncUtill.QUERY_TYPE.TOTAL));
            todayscount.setText("" + syncutil.getCasesCount(getApplicationContext(), CasesyncUtill.QUERY_TYPE.CURRENT_DATE));
            yettoupdate.setText("" + syncutil.getCasesCount(getApplicationContext(), CasesyncUtill.QUERY_TYPE.PENDING));
            disposal_count.setText("" + syncutil.getCasesCount(getApplicationContext(), CasesyncUtill.QUERY_TYPE.TOTAL_DIS));
            ;
            List<CaseObj> casedet = syncutil.getdistinctlist(getApplicationContext(), "N");
            for (com.casesync.utill.CaseObj cases : casedet) {
                if (caldroidFragment != null) {
                    if (cases.getDateNextList() != null) {
                        ColorDrawable green = new ColorDrawable(0xFF00DDFF);
                        CasesyncUtill.getcasecountofaday(getApplicationContext(), cases.getDateNextList());
                        caldroidFragment.setBackgroundDrawableForDate(green, cases.getDateNextList());
                        caldroidFragment.setTextColorForDate(R.color.white, cases.getDateNextList());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final Intent tempdata = data;
        String filename = "";
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case FILE_PICKER_RESULT:
                    ErrorMessage message = new FileImportUtility().importFileUsingFileChooser(tempdata, getApplicationContext());
                    if (message.isStatus() == false) {
                        Snackbar.make(getWindow().getDecorView().getRootView(), message.getMessage(), Snackbar.LENGTH_SHORT)
                                .setAction("No action", null).show();
                    }

                    break;
            }

        } else {
            //NO File selected scenario.
            Snackbar.make(getWindow().getDecorView().getRootView(), "Error in file selection.", Snackbar.LENGTH_SHORT)
                    .setAction("No action", null).show();

        }
    }

    class AsyncTaskRunner extends AsyncTask<String, Integer, Integer> {
        ProgressDialog progressDialog;
        private LoginParams parms;
        private List<CaseObj> cases;

        public AsyncTaskRunner(LoginParams parms, List<CaseObj> casesobj) {
            this.parms = parms;
            this.cases = casesobj;
        }

        @Override
        protected Integer doInBackground(String... params) {
            Integer count = 0;
            CasesyncUtill utill = new CasesyncUtill();
            try {
                RestTemplate restTemplate = new RestTemplate();
                String url = "";
                publishProgress(0);
                for (CaseObj dataModel : cases) {
                    count = count + 1;
                    //Added to keep the code clean.
                    CaseObj output = utill.getCaseObjFromCino(getApplicationContext(), dataModel.getCino(), false);
                    if (output != null) {
                        //This is the reason why duplicate calendar events are getting created. When it is executed a new event will get created.
                        //utill.createCalendarNotification(getApplicationContext(), Arrays.asList(output));
                    }
                    publishProgress(count);
                }
            } catch (Exception e) {
                Log.e("ASYNC", e.getMessage(), e);
            }
            return count;
        }

        @Override
        protected void onPostExecute(Integer result) {
            //FIXME Raising illegalargument exception.Handle it
            Snackbar.make(getWindow().getDecorView().getRootView(), "Updated " + result + " cases", Snackbar.LENGTH_SHORT)
                    .setAction("No action", null).show();
            //Call getactivity
            Intent intent = new Intent(NavActivity.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            progressDialog.dismiss();
            //Test SMS Feature.Need to fix it give the new APK for RTS if he is ok push it to playstore
            //new NotificationJob().createNotifications(getApplicationContext());
            startActivity(intent);
            finish();

        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(NavActivity.this,
                    "",
                    "Updating case details ......");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }


}
