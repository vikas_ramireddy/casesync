package com.casesync.casesync;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.core.app.NotificationManagerCompat;

import com.casesync.casesync.WhatsAppFeature.Const;

import java.util.List;
import java.util.Set;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {

    private static boolean homeactivity = true;

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueBooleanListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();
            if (preference instanceof SwitchPreference) {
                if (preference.getKey().equalsIgnoreCase("chat_bot_enable")) {
                    Log.i("SETTINGS", "Switch Preference " + preference.getKey() + ":" + stringValue);
                    SharedPreferences sharedPreferences = preference.getContext().getSharedPreferences(Const.BOT, Context.MODE_PRIVATE);
                    if (value.toString().equalsIgnoreCase("true")) {
                        //TODO Check for contact access.It should be mandatory for auto reply.
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                            if (!Settings.Secure.getString(preference.getContext().getContentResolver(), "enabled_notification_listeners").contains(preference.getContext().getPackageName())) {
                                Toast.makeText(preference.getContext(), "Please Enable Notification Access For Casesync Application", Toast.LENGTH_LONG).show();
                                preference.getContext().startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                            }

                        } else {
                            //Handle android 7 Cases.Send this and ask beta testers to test.
                            Set<String> packageNames = NotificationManagerCompat.getEnabledListenerPackages(preference.getContext());
                            if (!packageNames.contains(preference.getContext().getPackageName())) {
                                Toast.makeText(preference.getContext(), "Please Enable Notification Access For Casesync Application", Toast.LENGTH_LONG).show();
                                preference.getContext().startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                            }
                        }
                        sharedPreferences.edit().putBoolean(Const.STATUS, true).apply();
                        sharedPreferences.edit().putString(Const.MESSAGE, "");
                        //sharedPreferences.getBoolean(Const.STATUS, false);
                    } else {
                        sharedPreferences.edit().putBoolean(Const.STATUS, false).apply();
                    }
                }
            }
            return true;
        }
    };


    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     * @author vikasramireddy
     *
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();
            if (preference instanceof ListPreference) {
                //ListPreference. Need to populate list preference dynamically.
                //@see https://stackoverflow.com/questions/6474707/how-to-fill-listpreference-dynamically-when-onpreferenceclick-is-triggered/13828912
                ListPreference listPreference = (ListPreference) preference;
                //listPreference.get
                Log.i("SETTINGS", stringValue + "OLD:NEW" + preference.getKey() + "preference:" + preference.isPersistent());
                int index = listPreference.findIndexOfValue(stringValue);
                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);
                if (index >= 0) {
                    Toast.makeText(preference.getContext(), "Preference changed to " + (index >= 0
                                    ? listPreference.getEntries()[index]
                                    : null) + " After reboot the settings will auto apply ",
                            Toast.LENGTH_SHORT).show();
                }

            } else if (preference instanceof RingtonePreference) {
                // For ringtone preferences, look up the correct display value
                // using RingtoneManager.
                if (TextUtils.isEmpty(stringValue)) {
                    // Empty values correspond to 'silent' (no ringtone).
                    preference.setSummary(R.string.pref_ringtone_silent);

                } else {
                    Ringtone ringtone = RingtoneManager.getRingtone(
                            preference.getContext(), Uri.parse(stringValue));

                    if (ringtone == null) {
                        // Clear the summary if there was a lookup error.
                        preference.setSummary(null);
                    } else {
                        // Set the summary to reflect the new ringtone display
                        // name.
                        String name = ringtone.getTitle(preference.getContext());
                        preference.setSummary(name);
                    }
                }

            } else if (preference instanceof com.casesync.utill.TimePreference) {
                Log.i("SETTINGS", "Yet to implement List preference change " + stringValue);
                preference.setSummary(stringValue);
            } else if (preference instanceof SwitchPreference) {
                Log.i("SETTINGS", "Switch Preference " + preference.getKey());
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
                Log.i("SETTINGS", "List preference change " + stringValue);
            }

            return true;
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }


    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.


        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static void bindPreferenceSummaryToBooleanValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueBooleanListener);
        sBindPreferenceSummaryToValueBooleanListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext()).getBoolean(preference.getKey(), false));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Log.i("SETTINGS", "CHECKING WHEN IT WILL BE CALLAED" + homeactivity);
        if (homeactivity == false) {
            homeactivity = true;
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(SettingsActivity.this, NavActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();

        }
        // moveTaskToBack(true);

    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || DemoVideoFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            homeactivity = false;
        }
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */

    public static class DemoVideoFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_demo_videos);
            setHasOptionsMenu(true);
            homeactivity = false;
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);
            setHasOptionsMenu(true);
            homeactivity = false;
            bindPreferenceSummaryToValue(findPreference("notification_preference"));
            bindPreferenceSummaryToValue(findPreference("sync_frequency"));
            //bindPreferenceSummaryToValue(findPreference("sendsms_alerts"));
            bindPreferenceSummaryToBooleanValue(findPreference("chat_bot_enable"));

        }

    }
}
