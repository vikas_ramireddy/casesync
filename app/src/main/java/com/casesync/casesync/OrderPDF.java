package com.casesync.casesync;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.casesync.utill.OrderDetails;
import com.google.android.material.snackbar.Snackbar;

import org.springframework.util.StringUtils;

import java.net.URLEncoder;

public class OrderPDF extends AppCompatActivity {

    String caller = "";
    String casenumber = "";
    String url = "";
    WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_pdf);
        if (getSupportActionBar() != null) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        caller = getIntent().getStringExtra("caller");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.get("order") != null) {
                Intent i = getIntent();
                OrderDetails dataModels = (OrderDetails) i.getSerializableExtra("order");
                casenumber = dataModels.getOrderdetails();
                url = dataModels.getUrl();

                webView = findViewById(R.id.webviewgen);
                final ProgressDialog pd = ProgressDialog.show(this, "", "Loading...", true);
                webView.clearCache(true);

                webView.clearFormData();
                webView.clearHistory();
                //Added newly just cross check
                webView.requestFocus();
                webView.clearSslPreferences();
                webView.getSettings().setAppCacheEnabled(false);
                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setUseWideViewPort(true);
                webView.getSettings().setJavaScriptEnabled(true);
                //webView.getSettings().setAllowFileAccess(true);
                //webView.getSettings().setAllowContentAccess(true);
                //webView.setScrollbarFadingEnabled(false);
                //webView.getSettings().setAllowFileAccessFromFileURLs(true);
                //webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
                webView.getSettings().setBuiltInZoomControls(true);
                try {
                    webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + URLEncoder.encode(dataModels.getUrl(), "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //By default it will be opened in a chromeclient
                //webView.setWebChromeClient(new WebChromeClient());
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        if (pd != null && pd.isShowing()) {
                            try {
                                pd.dismiss();
                                //webView.reload();
                                Snackbar.make(getWindow().getDecorView().getRootView(), "Please refresh if blank page is displayed", Snackbar.LENGTH_LONG)
                                        .setAction("", null).show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });


            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == 16908332) {
            if (!StringUtils.isEmpty(caller)) {
                if (caller.equals("CaseDetail")) {
                    Intent intent = new Intent(this, CasedetailsTab.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    intent.putExtra("caller", "Order");
                    intent.putExtra("casenumber", casenumber);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(OrderPDF.this, OrderActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    startActivity(intent);
                    finish();
                }
            } else {
                Intent intent = new Intent(OrderPDF.this, OrderActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            }
            return true;
        } else if (id == R.id.view) {
            //Share the downloaded file
//            ConnectivityManager connMgr = (ConnectivityManager)
//                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//            if (networkInfo != null && networkInfo.isConnected()) {
//                Intent share = new Intent(android.content.Intent.ACTION_SEND);
//                share.setType("text/plain");
//                share.putExtra(Intent.EXTRA_SUBJECT, "Order_" + casenumber + ".pdf");
//                try {
//                    share.putExtra(Intent.EXTRA_TEXT, "https://drive.google.com/viewerng/viewer?embedded=true&url=" + URLEncoder.encode(url, "UTF-8"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                startActivity(Intent.createChooser(share, "Share link!"));
//            }
            Intent intent = new Intent(this, CasedetailsTab.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            intent.putExtra("casenumber", casenumber);
            startActivity(intent);
            finish();

            return true;
        } else if (id == R.id.download) {
            //Download the file from the URL.Check the netword connectivity
            ConnectivityManager connMgr = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(url);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setVisibleInDownloadsUi(true);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Order_" + casenumber + ".pdf");
                downloadManager.enqueue(request);
            }

            return true;
        } else if (id == R.id.reload) {
            //Download the file from the URL.Check the netword connectivity
            ConnectivityManager connMgr = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {

                webView.reload();
            }

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (!StringUtils.isEmpty(caller)) {
            if (caller.equals("CaseDetail")) {
                Intent intent = new Intent(this, CasedetailsTab.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                intent.putExtra("casenumber", casenumber);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(OrderPDF.this, OrderActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(OrderPDF.this, OrderActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            startActivity(intent);
            finish();
        }

    }
}
