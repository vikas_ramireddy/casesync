package com.casesync.casesync;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.Encryption;
import com.casesync.utill.ErrorMessage;
import com.casesync.utill.LoginParams;
import com.casesync.utill.PropertyUtil;
import com.casesync.utill.Useraccount;
import com.google.android.material.snackbar.Snackbar;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.scottyab.aescrypt.AESCrypt;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import org.springframework.util.StringUtils;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class MoreDetails extends Fragment {
    private static final int CONTACT_PICKER_RESULT = 1;
    private static final int FILE_PICKER_RESULT = 2;
    private static final int MESSAGE_SEND_RESULT = 3;

    //private ProgressBar moreprogress;
    //,attachfiles,capturephoto;
    private static final String PREFS_NAME = "LoginPrefs";
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 0;
    private static final int CAMERA_REQUEST = 0;
    public LoginParams loginparams = new LoginParams();
    private EditText clientname, mobile, emailid, notes;
    private ImageButton btnsave, btnupdate, btncall, btnsms, btnSubmit, addcontact, sendemail, sharecase;

    public MoreDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_more_details, container, false);
        //listView=(ListView)v.findViewById(R.id.fileslist);
        /*Implementing the contact picker*/
        btnSubmit = v.findViewById(R.id.add_contact);
        btnsave = v.findViewById(R.id.savecontact);
        btnupdate = v.findViewById(R.id.updatecase);
        btncall = v.findViewById(R.id.callclient);
        btnsms = v.findViewById(R.id.sendsms);
        addcontact = v.findViewById(R.id.add_contact);
        sendemail = v.findViewById(R.id.sendemail);
        sharecase = v.findViewById(R.id.sharecase);
        //attachfiles= (ImageButton) v.findViewById(R.id.attachfiles);
        //capturephoto= (ImageButton) v.findViewById(R.id.capturephoto);
        // moreprogress= (ProgressBar)v.findViewById(R.id.more_progress);
        clientname = v.findViewById(R.id.client_name);
        mobile = v.findViewById(R.id.client_mobile);
        emailid = v.findViewById(R.id.client_email);
        notes = v.findViewById(R.id.additional_notes);

        try {

/*            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<com.casesync.utill.Casedetails, Long> caseDao = todoOpenDatabaseHelper.getDao();
            CasedetailsTab activity = (CasedetailsTab) getActivity();
            String casenumber = activity.getcasenumber();
            Log.e("More", "casenumber:" + casenumber);
            List<Casedetails> todos = caseDao.queryForEq("casenumber", casenumber);*/

            CasedetailsTab activity = (CasedetailsTab) getActivity();
            String casenumber = activity.getcasenumber();

            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            //TODO Get Case Type also while getting above data from blank fragment.Based on that query the table and display the data.
            Dao<CaseObj, Long> casedetDao = null;
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            List<CaseObj> todos = casedetDao.queryForEq("cino", casenumber);

            if (todos.size() > 0) {
                CaseObj casedetails = todos.get(0);

                if (StringUtils.isEmpty(casedetails.getContactname()) && StringUtils.isEmpty(casedetails.getAddnotes())
                        && StringUtils.isEmpty(casedetails.getEmail()) && StringUtils.isEmpty(casedetails.getPhone())) {
                    btnupdate.setEnabled(false);
                    btnsave.setEnabled(true);

                    addcontact.setEnabled(true);
                    mobile.setEnabled(true);
                    emailid.setEnabled(true);
                    clientname.setEnabled(true);
                    notes.setEnabled(true);
                } else {
                    btnupdate.setEnabled(true);

                    addcontact.setEnabled(false);
                    btnsave.setEnabled(false);
                    clientname.setEnabled(false);
                    mobile.setEnabled(false);
                    emailid.setEnabled(false);
                    notes.setEnabled(false);

                }


                if (StringUtils.isEmpty(casedetails.getPhone())) {
                    btncall.setEnabled(false);
                    btnsms.setEnabled(false);
                } else {
                    mobile.setText(casedetails.getPhone());
                }

                if (StringUtils.isEmpty(casedetails.getEmail())) {
                    sendemail.setEnabled(false);
                } else {
                    emailid.setText(casedetails.getEmail());
                }

                if (!StringUtils.isEmpty(casedetails.getContactname())) {
                    clientname.setText(casedetails.getContactname());
                }

                if (!StringUtils.isEmpty(casedetails.getAddnotes())) {
                    notes.setText(casedetails.getAddnotes());
                }

//Old code fixed issue of not displaying notes if the mobile number is not available
//                if (casedetails.getPhone() != null) {
//                    if (!casedetails.getPhone().equals("")) {
//                        Log.e("MORE", "Phone number is not null");
//
//
//                        mobile.setText(casedetails.getPhone());
//                        emailid.setText(casedetails.getEmail());
//                        notes.setText(casedetails.getAddnotes());
//
//                        addcontact.setEnabled(false);
//                        btnsave.setEnabled(false);
//                        clientname.setEnabled(false);
//                        mobile.setEnabled(false);
//                        emailid.setEnabled(false);
//                        notes.setEnabled(false);
//
//                        sendemail.setEnabled(true);
//                        btncall.setEnabled(true);
//                        btnsms.setEnabled(true);
//                    } else {
//                        btnupdate.setEnabled(false);
//                        btncall.setEnabled(false);
//                        btnsms.setEnabled(false);
//                        sendemail.setEnabled(false);
//                    }
//                } else {
//                    btnupdate.setEnabled(false);
//                    btncall.setEnabled(false);
//                    btnsms.setEnabled(false);
//                    sendemail.setEnabled(false);
//                }


            }

        } catch (Exception e) {
            Log.e("MORE", "Error " + e.getMessage());
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Log.e("MORE", "Coming here");
                doLaunchContactPicker(v);


            }
        });

        sendemail.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                String emailid = "";
                String msgtext = "";
                String subject = "";
                Log.e("MORE", "Sending EMAIL");
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> caseDao = null;
                try {
                    caseDao = todoOpenDatabaseHelper.getCaseObjdao();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                CasedetailsTab activity = (CasedetailsTab) getActivity();
                String casenumber = activity.getcasenumber();
                Log.e("More", "cino:" + casenumber);
                List<CaseObj> todos = null;
                try {
                    todos = caseDao.queryForEq("cino", casenumber);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (todos.size() > 0) {
                    CaseObj casedetails = todos.get(0);
                    emailid = casedetails.getEmail();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    final SharedPreferences settings = getContext().getSharedPreferences(PREFS_NAME, 0);
                    settings.getString("username", "");
                    subject = "You have Case hearing on " + sdf.format(casedetails.getDateNextList()) + " for case number " + casedetails.getRegNo() + "/" + casedetails.getRegYear();
//                    msgtext = "Dear Client,The next date in your case " + casedetails.getRegNo() + " is " + sdf.format(casedetails.getDateNextList()) + " and listed in "
//                            + casedetails.getDesgname() + " please contact the advocate \n Regards \n" + settings.getString("username", "");

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    String smstext = prefs.getString("smstemplate", "");
                    msgtext=new CasesyncUtill().getSMSTemplate(smstext,casedetails,null);
                }

                final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                /* Fill it with Data */
                emailIntent.setType("plain/text");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"" + emailid});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                emailIntent.putExtra(Intent.EXTRA_TEXT, msgtext);

                startActivity(Intent.createChooser(emailIntent, "Send mail..."));

            }
        });
        btnsave.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Log.i("MORE", "Saving the contact");
                ErrorMessage msg = validateMoreDetails();
                if (msg.isStatus()) {
                    saveCaseDetails(v);
                } else {
                    Toast.makeText(getContext(), msg.getMessage(), Toast.LENGTH_LONG).show();
                    Snackbar.make(getView(), msg.getMessage(), Snackbar.LENGTH_SHORT)
                            .setAction("No action", null).show();
                }
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Log.e("More", "Updating the case details");
                addcontact.setEnabled(true);
                btnsave.setEnabled(true);
                clientname.setEnabled(true);
                mobile.setEnabled(true);
                emailid.setEnabled(true);
                notes.setEnabled(true);
                btnupdate.setEnabled(false);
                btncall.setEnabled(false);
                btnsms.setEnabled(false);
                sendemail.setEnabled(false);

            }
        });
        sharecase.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                Log.e("MORE", "Saving the contact");
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> caseDao = null;
                try {
                    caseDao = todoOpenDatabaseHelper.getCaseObjdao();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                List<CaseObj> todos = null;
                CasedetailsTab activity = (CasedetailsTab) getActivity();
                String casenumber = activity.getcasenumber();
                try {
                    todos = caseDao.queryForEq("cino", casenumber);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (todos.size() > 0) {
                    try {
                        CaseObj casedetails = todos.get(0);
                        String casnum = AESCrypt.encrypt(Encryption.password, casedetails.getCino());
                        String buildurl = PropertyUtil.getProperty("sitename", getContext()) + "/addcase?caseno=" + casnum;
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Add Case");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, "" + buildurl);
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    } catch (Exception e) {
                        Log.e("MORE", "Error" + e.getMessage());
                    }

                }


            }
        });
        btncall.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                String mobileno = "";
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> caseDao = null;
                try {
                    caseDao = todoOpenDatabaseHelper.getCaseObjdao();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                CasedetailsTab activity = (CasedetailsTab) getActivity();
                String casenumber = activity.getcasenumber();
                Log.e("More", "casenumber:" + casenumber);
                List<CaseObj> todos = null;
                try {
                    todos = caseDao.queryForEq("cino", casenumber);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (todos.size() > 0) {
                    CaseObj casedetails = todos.get(0);
                    mobileno = casedetails.getPhone();
                }
                Intent intent = new Intent("android.intent.action.DIAL");
                intent.setData(Uri.parse("tel:" + mobileno));
                startActivity(intent);

            }
        });
        btnsms.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                askForPermission(Manifest.permission.SEND_SMS, 0);
                String mobileno = "";
                String msgtext = "";
                Log.e("MORE", "Sending SMS");
                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> caseDao = null;
                try {
                    caseDao = todoOpenDatabaseHelper.getCaseObjdao();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                CasedetailsTab activity = (CasedetailsTab) getActivity();
                String casenumber = activity.getcasenumber();
                Log.e("More", "cino:" + casenumber);
                List<CaseObj> todos = null;
                try {
                    todos = caseDao.queryForEq("cino", casenumber);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (todos.size() > 0) {
                    CaseObj casedetails = todos.get(0);
                    mobileno = casedetails.getPhone();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                    String smstext = prefs.getString("smstemplate", "");
                    msgtext=new CasesyncUtill().getSMSTemplate(smstext,casedetails,null);
                }
                //MESSAGE_SEND_RESULT

//                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
//                smsIntent.setType("vnd.android-dir/mms-sms");
//                smsIntent.putExtra("address", "12125551212");
//                smsIntent.putExtra("sms_body","Body of Message");
//                startActivity(smsIntent);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + mobileno));
                sendIntent.putExtra("sms_body", msgtext);
                startActivity(sendIntent);

//                Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
//                smsIntent.setType("vnd.android-dir/mms-sms");
//                smsIntent.putExtra("address", ""+mobileno);
//                smsIntent.putExtra("sms_body",""+msgtext);
//                //startActivityForResult(smsIntent, MESSAGE_SEND_RESULT );
//                startActivity(smsIntent);

            }
        });
        return v;

    }

    public ErrorMessage validateMoreDetails() {
        ErrorMessage message = new ErrorMessage();
        message.setStatus(true);
        if (!StringUtils.isEmpty(mobile.getText().toString())) {
            boolean mobstatus = validateMobileNumber(mobile.getText().toString());
            if (mobstatus == false) {
                message.setStatus(false);
                message.setMessage("Invalid Mobile Number");
            }
        }

        if (!StringUtils.isEmpty(emailid.getText().toString())) {
            boolean emailstatus = isEmailValid(emailid.getText().toString());
            if (emailstatus == false) {
                message.setStatus(false);
                message.setMessage("Invalid Email Address");
            }
        }


        return message;
    }

    public boolean isEmailValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }


    public boolean validateMobileNumber(String mobilenumber) {
        //FIX. Enabling mobile numbers from 6-9
        Pattern p = Pattern.compile("[6-9][0-9]{9}");
        Matcher m = p.matcher(mobilenumber);
        return (m.find() && m.group().equals(mobilenumber));
    }

    public void doLaunchContactPicker(View view) {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
    }

    public void saveCaseDetails(View view) {
        try {
            CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                    CaseSyncOpenDatabaseHelper.class);
            Dao<com.casesync.utill.CaseObj, Long> caseDao = todoOpenDatabaseHelper.getCaseObjdao();
            CasedetailsTab activity = (CasedetailsTab) getActivity();
            String casenumber = activity.getcasenumber();
            List<CaseObj> todos = caseDao.queryForEq("cino", casenumber);
            if (todos.size() > 0) {

                if (todos.size() > 1) {
                    Log.e("More", "Duplicate cases are available");
                    return;
                }
                CaseObj casedetails = todos.get(0);
                UpdateBuilder<CaseObj, Long> updateBuilder = caseDao.updateBuilder();
                //? View.GONE : View.VISIBLE
                updateBuilder.updateColumnValue("phone", mobile.getText().toString().equals("") ? "" : mobile.getText().toString().replaceAll(" ", ""));
                updateBuilder.updateColumnValue("email", emailid.getText().toString().equals("") ? "" : emailid.getText().toString());
                updateBuilder.updateColumnValue("contactname", clientname.getText().toString().equals("") ? "" : clientname.getText().toString());
                updateBuilder.updateColumnValue("addnotes", notes.getText().toString().replace("'", ""));
                //Try to store the details in
                updateBuilder.where().eq("cino", casedetails.getCino());
                updateBuilder.update();
                addcontact.setEnabled(false);
                btnupdate.setEnabled(true);
                btnsave.setEnabled(false);
                clientname.setEnabled(false);
                mobile.setEnabled(false);
                emailid.setEnabled(false);
                notes.setEnabled(false);
                btncall.setEnabled(true);
                btnsms.setEnabled(true);
                sendemail.setEnabled(true);
                try {
                    Log.e("More", "casedetails:" + casedetails.getPhone());
                    Useraccount useraccount = new Useraccount.UserBuilder(casedetails.getCino(), true)
                            .clientMobileNumber(mobile.getText().toString().equals("") ? "" : mobile.getText().toString().replaceAll(" ", ""))
                            .clientEmail(emailid.getText().toString().equals("") ? "" : emailid.getText().toString())
                            .clientName(clientname.getText().toString().equals("") ? "" : clientname.getText().toString()).clientNotes(notes.getText().toString().replace("'", ""))
                            .build();
                    FirebaseUtil.insertuseraccountObj(useraccount);

                } catch (Exception e) {
                    Log.e("More", "ERROR:" + e.getMessage());
                }


            } else {
                Log.e("More", "Unable to find the case");
            }


        } catch (Exception e) {
            Log.e("More", "" + e.getMessage());
        }

    }


    private void contactPicked(Intent data) {
        ContentResolver cr = getContext().getContentResolver();

        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            String email = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            cr.query(uri, null, null, null, null);
            cursor.moveToFirst();
            String id = uri.getLastPathSegment();
            name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id}, null);
            while (pCur.moveToNext()) {
                String phone = pCur.getString(
                        pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                mobile.setText(phone);         //print data
            }
            pCur.close();
            clientname.setText(name);
            Cursor emailCur = cr.query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    new String[]{id}, null);
            while (emailCur.moveToNext()) {
                email = emailCur.getString(
                        emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                emailid.setText(email);         //print data
            }
            emailCur.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String retrieveContactName(Uri uriContact) {
        String contactName = null;

        Cursor cursor = getContext().getContentResolver().query(uriContact, null, null, null, null);
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }
        cursor.close();
        return contactName;
    }

    private String retrieveContactEmail(Uri result) {

        String email = null;
        String id = result.getLastPathSegment();
        Cursor cursor = null;
        try {
            cursor = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[]{id},
                    null);

            int emailIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);
            if (cursor.moveToFirst()) {
                email = cursor.getString(emailIdx);
            } else {
                Log.e("MORE", "No results");
            }

        } catch (Exception e) {
            Log.e("MORE", "" + e.getMessage());
        } finally {
            if (cursor != null) {
                cursor.close();
            }

        }
        return email;
    }

    private String retrieveContactNumber(Uri uriContact) {

        String contactID = "";

        String contactNumber = null;

        // getting contacts ID
        Cursor cursorID = getContext().getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {

            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();

        Log.i("More", "Contact ID: " + contactID);

        // Using the contact ID now we will get contact phone number
        Cursor cursorPhone = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll(" ", "");
        }

        cursorPhone.close();
        //Replacing +91 with Empty and Space with empty
        contactNumber = contactNumber.replaceAll(" ", "").replace("+91", "");
        Log.i("More", "Contact Phone Number: " + contactNumber);
        return contactNumber;
    }

    public void saveAttachment(Intent data) {
        try {
            CasedetailsTab activity = (CasedetailsTab) getActivity();
            String casenumber = activity.getcasenumber();
            Log.e("More", "casenumber:" + casenumber);
            String FilePath = data.getData().getPath();
            Toast.makeText(getContext(), data.getData().getLastPathSegment(), Toast.LENGTH_LONG).show();
            /*Storing files here */

            Storage storage = null;
            if (SimpleStorage.isExternalStorageWritable()) {
                storage = SimpleStorage.getExternalStorage();
            } else {
                storage = SimpleStorage.getInternalStorage(getContext());
            }

            boolean dirExists = storage.isDirectoryExists("casesync");
            Log.e("More", "Storage: dirExists" + dirExists);
            if (dirExists == false) {
                storage.createDirectory("casesync");
                Log.e("More", "dirExists:");
            }
            boolean subdirExists = storage.isDirectoryExists("casesync/" + casenumber);
            if (subdirExists == false) {
                Log.e("More", "subdirExists:");
                storage.createDirectory("casesync/" + casenumber);
            }
            File file = new File(FilePath);
            storage.copy(file, "casesync/" + casenumber, data.getData().getLastPathSegment());
            Toast.makeText(getContext(), file.getName() + " uploaded sucessfully ", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Permission issue ", Toast.LENGTH_LONG).show();
        }

    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
            //Toast.makeText(getContext(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    Cursor cursor = null;
                    String email = "";
                    String name = "";
                    String phoneNo = "";
                    try {
                        Uri result = data.getData();
                        Log.e("MORE", "Got a contact result: "
                                + result.toString());
                        name = retrieveContactName(result);
                        phoneNo = retrieveContactNumber(result);
                        email = retrieveContactEmail(result);
                        Log.e("More", "tempname:" + name);
                        Log.e("More", "tempno:" + phoneNo);
                        Log.e("More", "email:" + email);
                        clientname.setText(name);
                        mobile.setText(phoneNo);
                        emailid.setText(email);

                    } catch (Exception e) {
                        Log.e("More", "MSG:" + e.getMessage());
                        askForPermission(Manifest.permission.READ_CONTACTS, 0);

                        Snackbar.make(getView(), "Please enable permissions for Contacts", Snackbar.LENGTH_SHORT)
                                .setAction("No action", null).show();

                    }

                    break;
                case FILE_PICKER_RESULT:


                    /*Need to pick the files and upload to the related case directory*/
                    saveAttachment(data);

                    break;
                case CAMERA_REQUEST:
                    if (resultCode == Activity.RESULT_OK) {
                        /*Save the image*/
                        Toast.makeText(getContext(), "Image uploaded successfully", Toast.LENGTH_LONG).show();
                    }
                    break;
            }

        } else {
            // gracefully handle failure
            Log.e("MORE", "Warning: activity result not ok");
        }
    }

}
