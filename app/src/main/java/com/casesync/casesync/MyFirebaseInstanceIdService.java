package com.casesync.casesync;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        try {
            String recent_token = FirebaseInstanceId.getInstance().getToken();
            //When the method is getting triggered user won't be logged-in.
            Context context = getApplicationContext();
            setTokenPref(context, recent_token);
//            Log.i("FB", "Token For testing:" + recent_token);
//            Profile profile = new Profile();
//            profile.setFirebase_token(recent_token);
//            FirebaseUtil.insertProfile(profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setTokenPref(Context context, String token) {
        SharedPreferences prefs = context.getSharedPreferences(CasesyncUtill.getDefaultSharedPreferencesName(context), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("firebase_token", token);
        editor.commit();
    }
}