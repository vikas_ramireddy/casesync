package com.casesync.casesync;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.casesync.utill.CaseObj;
import com.casesync.utill.CaseSyncOpenDatabaseHelper;
import com.casesync.utill.CustomStringAdapter;
import com.casesync.utill.FileUtility;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.FileProvider.getUriForFile;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AttachmentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AttachmentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "AF";
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 0;
    private static final int CAMERA_REQUEST = 0;
    private static final String ARG_PARAM2 = "param2";
    private static final String AUTHORITY = "com.casesync.fileprovider";
    private static final int IMAGE_REQUEST_CODE = 1;
    private static final int FILE_PICKER_RESULT = 2;
    ListView listView;
    ArrayList<File> folderfiles = new ArrayList<File>();
    CustomStringAdapter adapter = null;
    CaseObj casedetails = new CaseObj();
    String casenumber = "";
    private ImageButton btnsave, btnupdate, btncall, btnsms, btnSubmit, addcontact, sendemail, attachfiles, capturephoto;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private OnFragmentInteractionListener mListener;

    public AttachmentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AttachmentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AttachmentFragment newInstance(String param1, String param2) {
        AttachmentFragment fragment = new AttachmentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static String getPath(Context context, Uri uri) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            //THIS WON'T WORK.
        } else {
            //DocumentFileCompat
            // Will return "image:x*"
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = context.getContentResolver().
                    query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);
            String filePath = "";
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attachment, container, false);
        attachfiles = view.findViewById(R.id.attachfiles);
        capturephoto = view.findViewById(R.id.capturephoto);

        listView = view.findViewById(R.id.fileslist);

        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(getContext(),
                CaseSyncOpenDatabaseHelper.class);
        Dao<CaseObj, Long> caseDao = null;
        try {
            caseDao = todoOpenDatabaseHelper.getCaseObjdao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CasedetailsTab activity = (CasedetailsTab) getActivity();
        casenumber = activity.getcasenumber();
        Log.d("More", "casenumber:" + casenumber);
        List<CaseObj> todos = null;
        try {
            todos = caseDao.queryForEq("cino", casenumber);
            if (todos.size() > 0) {
                Log.d("More", "Duplicate Case Number.");
                casedetails = todos.get(0);
                casenumber = todos.get(0).getCino();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Log.d("ATTACH", "COMING TO ATTACHMENTS ATTACH FRAGMENTS");
        /*List all the attachments*/
        try {
            List<File> FilesInFolder = FileUtility.getNestedFiles(getContext(), casenumber);
            Log.d("More", "FilesInFolder:" + FilesInFolder.size());
            folderfiles = new ArrayList<File>();
            int i = 0;
            for (File f : FilesInFolder) {
                Log.d("More", "NAMES:" + f.getName());
                folderfiles.add(i, f);
                i = i + 1;
            }
//            ArrayAdapter adapter = new ArrayAdapter<String>(getContext(),
//                    android.R.layout.simple_list_item_1, folderfiles);

            if (adapter != null) {
                adapter.resetData();
            }

            adapter = new CustomStringAdapter(folderfiles, getContext());
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
            registerForContextMenu(listView);
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//                                    // Clicking on items
//                                    Log.d("More", "position:"+position);
//                                }
//                            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    /*Handling the nougat scenario*/
                    final ArrayList<File> files = folderfiles;
                    File dataModel = folderfiles.get(position);
                    Log.d("More", "Coming to onclick:" + position);
                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                        if (dataModel.getName().contains(".pdf")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(dataModel), "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        } else if (dataModel.getName().contains(".jpg") || dataModel.getName().contains(".JPG") || dataModel.getName().contains(".PNG") || dataModel.getName().contains(".png")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(dataModel), "image/*");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        } else if (dataModel.getName().contains(".mp3") || dataModel.getName().contains(".MP3")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(dataModel), "audio/*");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(dataModel), "application/*");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            Log.d("More", "Open some thing else");
                        }

                    } else {
                        try {
                            Intent i =
                                    new Intent(Intent.ACTION_VIEW,
                                            FileProvider.getUriForFile(getContext(), AUTHORITY, dataModel));
                            i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(i);

                        } catch (Exception e) {
                            Log.d("More", "Error " + e.getMessage());
                        }


                    }
                }
            });
            attachfiles.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                        askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0);
                        askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0);
                    }

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    try {
                        startActivityForResult(intent, FILE_PICKER_RESULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
            capturephoto.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        CasedetailsTab activity = (CasedetailsTab) getActivity();
                        String filepath = FileUtility.getSubFolderPath(getContext(), FileUtility.ACTIONTYPE.CASEVIEW, casenumber);
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        try {
                            try {
                                intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            } catch (Exception e) {
                                Log.d("ATTACH", "ERROR1" + e.getMessage());
                                e.printStackTrace();
                            }
                            File imagesFolder = new File(filepath);
                            File image = new File(imagesFolder, "CS_" + timeStamp + ".png");
                            Uri outputFileUri = getUriForFile(getContext(), "com.casesync.fileprovider", image);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        } catch (Exception e) {
                            Log.d("ATTACH", "ERROR2" + e.getMessage());
                            //Toast.makeText(getContext(), "Error "+e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        PackageManager pm = getActivity().getPackageManager();
                        if (intent.resolveActivity(pm) != null) {
                            startActivityForResult(intent, CAMERA_REQUEST);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("ATTACH", "" + e.getMessage());
                    }


                }
            });

        } catch (Exception e) {
            Log.d("More", "ERROR:" + e.getMessage());
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select The Option");
        menu.add(0, v.getId(), 0, "View File");//groupId, itemId, order, title
        menu.add(0, v.getId(), 0, "Delete File");
        menu.add(0, v.getId(), 0, "Share File");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final MenuItem temp = item;
        if (item.getTitle() == "Share File") {
            final ArrayList<File> files = folderfiles;
            AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) temp.getMenuInfo();

            int position = aInfo.position;
            File dataModel = folderfiles.get(position);
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                /*Need to follow the another logic*/
                uri = FileProvider.getUriForFile(getContext(), AUTHORITY, dataModel);
            } else {
                uri = Uri.parse("file://" + dataModel.getAbsolutePath());
                Log.d("PATH", "" + uri.getPath());
            }
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            if (dataModel.exists()) {
                intentShareFile.setType("*/*");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);

                if (casedetails.getCino() != null) {
                    String text = "Document related to " + casedetails.getTypeName() + "/" + casedetails.getRegNo() + "/" + casedetails.getCourtName();
                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT, text
                    );
                    intentShareFile.putExtra(Intent.EXTRA_TEXT, text);
                } else {
                    intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                            "Sharing File...");
                    intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
                }

                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }


        }
        if (item.getTitle() == "View File") {
            final ArrayList<File> files = folderfiles;
            AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) temp.getMenuInfo();

            int position = aInfo.position;
            File dataModel = folderfiles.get(position);
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                if (dataModel.getName().contains(".pdf")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(dataModel), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                } else if (dataModel.getName().contains(".jpg") || dataModel.getName().contains(".JPG") || dataModel.getName().contains(".PNG") || dataModel.getName().contains(".png")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(dataModel), "image/*");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                } else if (dataModel.getName().contains(".mp3") || dataModel.getName().contains(".MP3")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(dataModel), "audio/*");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(dataModel), "application/*");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    Log.d("More", "Open some thing else");
                }

            } else {
                try {
                    Intent i =
                            new Intent(Intent.ACTION_VIEW,
                                    FileProvider.getUriForFile(getContext(), AUTHORITY, dataModel));
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(i);

                } catch (Exception e) {
                    Log.d("More", "Error " + e.getMessage());
                }


            }

        } else if (item.getTitle() == "Delete File") {
            AlertDialog.Builder loginalert = new AlertDialog.Builder(getContext());
            loginalert.setMessage("Deleting the attachment ");
            loginalert.setTitle("Are you sure ?");
            loginalert.setPositiveButton("OK", null);
            loginalert.setNegativeButton("CANCEL", null);
            loginalert.setCancelable(true);
            loginalert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("ATTACHMENT LIST", "Delete OK  :");

                            Log.d("CustomAd", "Coming inside thread");
                            File dataModel = null;
                            AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) temp.getMenuInfo();

                            int position = aInfo.position;
                            Object object = folderfiles.get(position);
                            dataModel = (File) object;

                            try {
                                folderfiles.remove(aInfo.position);
                                adapter.notifyDataSetChanged();
                                Log.d("DELETE", "THIRD LINE");
                                dataModel.delete();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });

            loginalert.setNegativeButton("CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("CASELIST", "DeleteCancel :");
                            dialog.cancel();

                        }
                    });
            loginalert.create().show();


        } else {
            return false;
        }
        return true;
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    /*Handle the nougat permissions here */
    public boolean askForPermission(String permission, Integer requestCode) {
        boolean status = false;
        if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            status = true;
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);
            }
        } else {
            status = true;
        }
        return status;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final Intent tempdata = data;
        String filename = "";
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case FILE_PICKER_RESULT:
                    try {
                        Log.d("ATTACH", "Saving the attachment");
                        boolean status = saveAttachment(tempdata, getContext());
                        if (status)
                            Toast.makeText(getContext(), "File uploaded successfully", Toast.LENGTH_LONG).show();
                        refreshfragment();
                    } catch (Exception e) {
                        Log.d("ATTACH", "Error" + e.getMessage());
                        //Toast.makeText(getContext(),  ""+e.getMessage(), Toast.LENGTH_LONG).show();
                    } finally {

                    }
                    break;
                case CAMERA_REQUEST:
                    if (resultCode == Activity.RESULT_OK) {
                        /*Save the image*/
                        Toast.makeText(getContext(), "File uploaded successfully", Toast.LENGTH_SHORT).show();
                        refreshfragment();
                    }
                    break;
            }

        } else {
            // gracefully handle failure
            Log.d("MORE", "Warning: activity result not ok");
        }
    }

    public boolean saveAttachment(Intent data, Context context) {
        try {
            String filename = "";
            Uri uri = data.getData();
            byte[] byte_data = new byte[100];
            String FilePath = getPath(context, uri);
            String sample_filepath = FileUtility.getSubFolderPath(getContext(), FileUtility.ACTIONTYPE.CASEVIEW, casenumber);
            try {
                String mimeType = context.getContentResolver().getType(uri);
                Log.d(TAG, "mimeType:" + mimeType);
                if (mimeType == null) {
                    String path = getPath(context, uri);
                    Log.d(TAG, "path:" + path);
                    if (path == null) {
                        Log.d(TAG, "NO FILE NAME AVAILABLE");
                        //filename = AttachmentFragment.getName(uri.toString());
                    } else {
                        File file = new File(path);
                        filename = file.getName();
                        Log.d(TAG, "filename:" + filename);
                    }
                } else {
                    Uri returnUri = data.getData();
                    Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();
                    filename = returnCursor.getString(nameIndex);
                    String size = Long.toString(returnCursor.getLong(sizeIndex));
                    Log.d("ATTACH", "filename:" + filename);
                }
                if (!StringUtils.isEmpty(FilePath)) {
                    File file = new File(FilePath);
                    Log.d("ATTACH", "filename:" + filename);
                    new FileUtility().saveFile(context, casenumber, file, filename);
                    return true;
                } else {

                    //Need to get the byte data.
                    InputStream is = context.getContentResolver().openInputStream(uri);
                    byte[] inputData = getBytes(is);
                    new FileUtility().saveFile(context, casenumber, inputData, filename);
                    return true;
                }
            } catch (Exception e) {
                Log.d("ATTACH", "error:" + e.getMessage());
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("More", "Error:" + e.getMessage());
        }
        return false;
    }

    public void refreshfragment() {
        try {
            CasedetailsTab activity = (CasedetailsTab) getActivity();
            List<File> FilesInFolder = FileUtility.getNestedFiles(getContext(), casenumber);
            Log.d("More", "FilesInFolder:" + FilesInFolder.size());
            folderfiles = new ArrayList<File>();
            int i = 0;
            for (File f : FilesInFolder) {
                Log.d("More", "NAMES:" + f.getName());
                folderfiles.add(i, f);
                i = i + 1;
            }

            if (adapter != null) {
                adapter.resetData();
            }

            adapter = new CustomStringAdapter(folderfiles, getContext());
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
            registerForContextMenu(listView);

        } catch (Exception e) {
            Log.d("ATTACHFRAG", "ERROR" + e.getMessage());
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
