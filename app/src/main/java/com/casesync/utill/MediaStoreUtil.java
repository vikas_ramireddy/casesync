package com.casesync.utill;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.casesync.casesync.AttachmentFragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class MediaStoreUtil implements MediaStore {

    private static final String TAG = "MSU";

    @Override
    public File getFileFromDownloads(Context context, String file_name) {
        Uri contentUri = android.provider.MediaStore.Files.getContentUri("external");
        String selection = android.provider.MediaStore.MediaColumns.RELATIVE_PATH + "=?";
        String[] selectionArgs = new String[]{Environment.DIRECTORY_DOWNLOADS+File.separator};
        Cursor cursor = context.getContentResolver().query(contentUri, null, selection, selectionArgs, null);
        Uri uri = null;
        if (cursor.getCount() == 0) {
            Log.i(TAG, "No folder found.");
        } else {
            while (cursor.moveToNext()) {
                String fileName = cursor.getString(cursor.getColumnIndex(android.provider.MediaStore.MediaColumns.DISPLAY_NAME));
                Log.i(TAG,"Current File Name:"+fileName);
                if (fileName.equals(file_name)) {
                    long id = cursor.getLong(cursor.getColumnIndex(android.provider.MediaStore.MediaColumns._ID));
                    uri = ContentUris.withAppendedId(contentUri, id);
                    break;
                }
            }

            if (uri == null) {
                Log.i(TAG, "File Not found.");
            } else {
                try {
                    InputStream inputStream = context.getContentResolver().openInputStream(uri);
                    byte[] inputData = new AttachmentFragment().getBytes(inputStream);
                    String folder_path = FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.IMPORT, null);
                    boolean imported_file = new FileUtility().saveFile(context, "import", inputData, file_name);
                    if (imported_file == true) {
                        File mycases = new File(folder_path + file_name);
                        return mycases;
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }

}