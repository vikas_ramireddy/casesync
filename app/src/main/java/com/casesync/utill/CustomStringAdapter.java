package com.casesync.utill;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.casesync.casesync.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by Vikas on 4/24/2017.
 */

public class CustomStringAdapter extends ArrayAdapter<File> implements View.OnClickListener {
    private ArrayList<File> dataSet;


    Context mContext;


    View view=null;
    File dataModel=null;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtType;
        TextView txtVersion;
        ImageView info;
    }



    public CustomStringAdapter(ArrayList<File> data, Context context) {
        super(context, R.layout.attachment_item, data);
        this.dataSet = data;
        this.mContext=context;

    }
    public int getCount() {
        return dataSet.size();
    }

    public File getItem(int position) {
        return dataSet.get(position);
    }

    public long getItemId(int position) {
        return dataSet.get(position).hashCode();
    }

    @Override
    public void onClick(View v) {


        final int position=(Integer) v.getTag();
        Object object= getItem(position);
        dataModel=(File) object;

        switch (v.getId())
        {

            case R.id.item_info:
                /*Execute the delete case here*/
                Log.e("CustomAd","Deleting the case:");
                Log.e("CustomAd","Try to add the listener");
                view=v;

                final Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_anim);
                anim.setAnimationListener(new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {

                       String filename=dataModel.getName();
//                        Snackbar.make(view, "Long press on file name and click on delete option ", Snackbar.LENGTH_LONG)
//                                .setAction("No action", null).show();
//
//
                        /*Need a alert dialog*/
                        AlertDialog.Builder loginalert = new AlertDialog.Builder(getContext());
                        loginalert.setMessage("Deleting the attachment");
                        loginalert.setTitle("Are you sure ?");
                        loginalert.setPositiveButton("OK",null);
                        loginalert.setNegativeButton("CANCEL",null);
                        loginalert.setCancelable(true);
                        loginalert.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dataSet.remove(position);
                                        notifyDataSetChanged();
                                        dataModel.delete();
                                        view.setHasTransientState(true);
                                        Snackbar.make(view, "File deleted successfully ", Snackbar.LENGTH_LONG)
                                                .setAction("No action", null).show();

                                    }
                                });
                        loginalert.setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Log.e("CASELIST","DeleteCancel :");
                                        dialog.cancel();

                                    }
                                });
                        loginalert.create().show();


                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {


                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        view.setHasTransientState(false);
                    }
                });
                v.startAnimation(anim);





                break;


        }


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        File dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.attachment_item, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtType = convertView.findViewById(R.id.type);
           // viewHolder.txtVersion = (TextView) convertView.findViewById(R.id.version_number);
            viewHolder.info = convertView.findViewById(R.id.item_info);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        lastPosition = position;



try
{
    /*Get the file size*/
    viewHolder.txtName.setText(dataModel.getName());
    viewHolder.txtType.setText(""+(dataModel.length()/1000)+"KB");
    viewHolder.info.setOnClickListener(this);
    viewHolder.info.setTag(position);
}
catch (Exception e)
{
Log.e("CUSSTADP",""+e.getMessage());
}
        return convertView;
    }

    public void resetData() {

    }
    /* *********************************
	 * We use the holder pattern
	 * It makes the view faster and avoid finding the component
	 * **********************************/

    private static class PlanetHolder {
        public TextView txtName;
        public TextView txtVersion;
    }
    /*
	 * We create our filter
	 */

}
