package com.casesync.utill;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GenericUtil {

    public static  String processClassName(String class_name) {

        if (StringUtils.isNotBlank(class_name)) {
            int start = class_name.indexOf("$");
            if (start > 0) {
                class_name = class_name.substring(0, start);
            }
        }

        return class_name;
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }


}