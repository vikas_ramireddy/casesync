/**
 * Created by vikas

 */
package com.casesync.utill;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author vikas
 *
 */
@DatabaseTable(tableName = "casetype")
public class Casetype {
	@DatabaseField(columnName = "casetype_id")
	private int casetype_id;
	@DatabaseField(columnName = "casecode")
	private int casecode;
	@DatabaseField(columnName = "casetypename")
	private String casetypename;
	@DatabaseField(columnName = "states_id")
	private States states_id;

	public int getCasetype_id() {
		return casetype_id;
	}
	public void setCasetype_id(int casetype_id) {
		this.casetype_id = casetype_id;
	}

	public int getCasecode() {
		return casecode;
	}
	public void setCasecode(int casecode) {
		this.casecode = casecode;
	}

	public String getCasetypename() {
		return casetypename;
	}
	public void setCasetypename(String casetypename) {
		this.casetypename = casetypename;
	}

	public States getStates_id() {
		return states_id;
	}
	public void setStates_id(States states_id) {
		this.states_id = states_id;
	}
	

}
