package com.casesync.utill;
public class ResponseVO {
public boolean status;
public String response;
public boolean isStatus() {
	return status;
}
public void setStatus(boolean status) {
	this.status = status;
}
public String getResponse() {
	return response;
}
public void setResponse(String response) {
	this.response = response;
}


}
