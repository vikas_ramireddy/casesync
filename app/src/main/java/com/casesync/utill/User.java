package com.casesync.utill;

import com.google.firebase.database.IgnoreExtraProperties;

// [START blog_user_class]
@IgnoreExtraProperties
public class User {

    public String mobilenumber;
    public String userid;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String mobilenumber, String userid) {
        this.mobilenumber = mobilenumber;
        this.userid = userid;
    }

}
// [END blog_user_class]
