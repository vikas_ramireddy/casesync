package com.casesync.utill;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.casesync.casesync.BuildConfig;
import com.casesync.casesync.CasesyncUtill;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.casesync.casesync.GenericConstants.APP_VERSION;

public class FileUtility implements FileUtil {

    private static final String AUTHORITY = "com.casesync.fileprovider";
    private static final String TAG = "FU";

    public static String getBaseFolderPath(Context context) {
        return context.getFilesDir().getAbsolutePath();
    }

    public static String getSubFolderPath(Context context, ACTIONTYPE actiontype, String case_number) {
        String path = "";
        if (actiontype == ACTIONTYPE.EXPORT) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                createStorage(context, "export");
                path = "storage/emulated/0/casesync/export/";
            } else {
                //TODO Change it to cache storage.
                path = context.getFilesDir().getAbsolutePath() + File.separator + "export/";
                createDir(path);
            }

        } else if (actiontype == ACTIONTYPE.IMPORT) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                createStorage(context, "import");
                path = "storage/emulated/0/casesync/import/";
            } else {
                path = context.getFilesDir().getAbsolutePath() + File.separator + "import/";
                createDir(path);
            }

        } else if (actiontype == ACTIONTYPE.CASEVIEW) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                createStorage(context, case_number);
                path = "storage/emulated/0/casesync/" + case_number + "/";
            } else {
                path = context.getFilesDir().getAbsolutePath() + File.separator + case_number;
                createDir(path);
            }
        } else if (actiontype == ACTIONTYPE.HOME) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                createStorage(context, case_number);
                path = "storage/emulated/0/casesync/";
            } else {
                path = context.getFilesDir().getAbsolutePath() + File.separator;
                createDir(path);
            }
        }

        return path;
    }

    public static void createDir(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    public static void deleteExistingFile(Context context, String filename) {
        //TODO Implement for other types  also.
        File existingfile = new File(FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.EXPORT, null) + filename);
        if (existingfile.exists()) {
            existingfile.delete();
        }
    }

    public static Storage createStorage(Context context, String sub_directory) {
        Storage storage = null;
        if (SimpleStorage.isExternalStorageWritable()) {
            storage = SimpleStorage.getExternalStorage();
        } else {
            storage = SimpleStorage.getInternalStorage(context);
        }
        boolean dirExists = storage.isDirectoryExists("casesync");
        Log.d("More", "Storage: dirExists" + dirExists);
        if (dirExists == false) {
            storage.createDirectory("casesync");
            Log.d("More", "dirExists:");
        }
        if (sub_directory != null) {
            boolean subdirExists = storage.isDirectoryExists("casesync/" + sub_directory);
            if (subdirExists == false) {
                Log.d("More", "subdirExists:");
                storage.createDirectory("casesync/" + sub_directory);
            }
        }
        return storage;
    }

    public static List<File> getNestedFiles(Context context, String sub_folder) {
        List<File> filesInFolder = new ArrayList<File>();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            Storage storage = createStorage(context, sub_folder);
            filesInFolder = storage.getNestedFiles("casesync/" + sub_folder);
        } else {
            File case_directory = new File(context.getFilesDir().getAbsolutePath() + File.separator + sub_folder);
            File[] files = case_directory.listFiles();
            if (files != null) {
                if (files.length > 0) {
                    filesInFolder = Arrays.asList(files);
                }

            }
        }
        return filesInFolder;
    }

    public static File loadEcourtsFile(Context context) {
        int version = new PermissionUtility().getEcourtsVersion(context);
        if (version < 2) {
            String sdcard = Environment.getExternalStorageDirectory() + File.separator + "";
            File file = new File(sdcard + File.separator + "myCases.txt");
            return file;
        } else {
            File downloadFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            if (downloadFilePath.exists()) {
                File fileToImport = new File(downloadFilePath.getAbsolutePath() + File.separator + "myCases.txt");
                return fileToImport;
            }
        }
        return null;
    }

    private static String readTextFromUri(Uri uri, Context context) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                     context.getContentResolver().openInputStream(uri);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

    public static String getFileNameFromUri(Context context, Uri returnUri) {
        try {
            Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
            returnCursor.moveToFirst();
            String filename = returnCursor.getString(nameIndex);
            return filename;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void migrateAllExistingFiles(Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                //This will execute untill the current folder name is not a case number.
                CasesyncUtill utill = new CasesyncUtill();
                File[] directories = new File(getSubFolderPath(context, ACTIONTYPE.HOME, null)).listFiles(File::isDirectory);
                if (directories != null) {
                    List<File> dirs = Arrays.asList(directories);
                    for (File filedir : dirs) {//Directory Loop
                        Log.i(TAG, "Directory:" + filedir.getName());
                        CaseObj casedetails = utill.fetchcasebycaseno(filedir.getName(), context);
                        if (casedetails != null) {
                            Storage storage = createStorage(context, filedir.getName());
                            String directory = getSubFolderPath(context, ACTIONTYPE.CASEVIEW, casedetails.getCino());
                            File[] listOfFiles = filedir.listFiles();
                            if (listOfFiles != null) {
                                if (listOfFiles.length > 0) {
                                    List<File> innerfiles = Arrays.asList(listOfFiles);
                                    for (File file : innerfiles) {//File Loop.
                                        file.renameTo(new File(directory + file.getName()));
                                    }
                                }
                            }
                        }
                        filedir.delete();

                    }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void performMigrate(Context context) {
        int versionCode = BuildConfig.VERSION_CODE;
        if (versionCode == APP_VERSION) {
            migrateAllExistingFiles(context);
        }

    }

    @Override
    public List<String> getListOfFilesInFolder(Context context, String subfolder) {
        String[] files = context.fileList();
        List<String> list = Arrays.asList(files);
        return list;
    }

    @Override
    public boolean saveFile(Context context, String subfolder, File file, String filename) {
        //Sub-folder will be the case_number.
        Storage storage = createStorage(context, subfolder);
        storage.copy(new File(file.getPath()), "casesync/" + subfolder, filename);
        return true;
    }


    @Override
    public boolean saveFile(Context context, String subfolder, byte[] data, String filename) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            String path = getSubFolderPath(context, ACTIONTYPE.CASEVIEW, subfolder);
            createDir(path);
            try (FileOutputStream stream = new FileOutputStream(path + filename)) {
                stream.write(data);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String path = context.getFilesDir().getAbsolutePath() + File.separator + subfolder + "" + File.separator;
            createDir(path);
            try (FileOutputStream stream = new FileOutputStream(path + filename)) {
                stream.write(data);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public File fetchFile(Context context, String subfolder, String filename) {
        return null;

    }


    public enum ACTIONTYPE {

        IMPORT, EXPORT, LIST, CASEVIEW, HOME
    }

}
