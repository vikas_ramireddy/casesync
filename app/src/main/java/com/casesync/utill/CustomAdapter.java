package com.casesync.utill;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.casesync.casesync.CasesyncUtill;
import com.casesync.casesync.Fragment.ImageGenerator;
import com.casesync.casesync.R;
import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by vikas on 09/02/16.
 */
public class CustomAdapter extends ArrayAdapter<CaseObj> implements View.OnClickListener, Filterable {

    public static String TAG = CustomAdapter.class.getSimpleName();
    View view = null;
    CaseObj dataModel = null;

    Context mContext;
    ImageGenerator mImageGenerator = new ImageGenerator(getContext());
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat sdfyear = new SimpleDateFormat("yyyy");
    ArrayList<com.casesync.utill.CaseObj> temp = new ArrayList<com.casesync.utill.CaseObj>();
    private Filter caseFilter;
    private ArrayList<CaseObj> dataSet;
    private ArrayList<String> cinno;
    private ArrayList<CaseObj> filter;
    private int lastPosition = -1;


    public CustomAdapter(ArrayList<CaseObj> data, ArrayList<String> cinnos, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext = context;
        this.filter = data;
        this.cinno = cinnos;

    }

    public int getCount() {
        return filter.size();
    }

    public CaseObj getItem(int position) {
        return filter.get(position);
    }

    public long getItemId(int position) {
        return filter.get(position).hashCode();
    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        Object object = getItem(position);
        dataModel = (CaseObj) object;
        CaseObj temp = null;
        switch (v.getId()) {

            case R.id.item_info:
                view = v;
                final Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Snackbar.make(view, "No Action Available", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                        view.setHasTransientState(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setHasTransientState(false);
                    }
                });
                v.startAnimation(anim);
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CaseObj dataModel = getItem(position);


        ViewHolder viewHolder; // view lookup cache stored in tag

        viewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.layout = convertView.findViewById(R.id.case_parent);
            viewHolder.txtName = convertView.findViewById(R.id.name);
            viewHolder.txtType = convertView.findViewById(R.id.type);
            viewHolder.info = convertView.findViewById(R.id.item_info);
            viewHolder.grouping = convertView.findViewById(R.id.grouping_section);
            if (cinno.contains(dataModel.getCino())) {
                viewHolder.grouping.setVisibility(View.VISIBLE);
                viewHolder.grouping.setText(dataModel.getCourtName() + "," + dataModel.getDistrictName());

            } else {
                viewHolder.grouping.setVisibility(View.GONE);
            }
            convertView.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();

            //Log.i(TAG, dataModel.getCino() + ":" + cinno.size());
            if (cinno.contains(dataModel.getCino())) {
                viewHolder.grouping.setVisibility(View.VISIBLE);
                viewHolder.grouping.setText(dataModel.getCourtName() + "," + dataModel.getDistrictName());
            } else {
                viewHolder.grouping.setVisibility(View.GONE);
            }
        }

/*        if (dataModel.isSelector() == true) {
            viewHolder.layout.setVisibility(View.GONE);
            viewHolder.grouping.setClickable(false);
            viewHolder.grouping.setVisibility(View.VISIBLE);
            viewHolder.grouping.setText(dataModel.getCourtName()+","+dataModel.getDistrictName()+"("+dataModel.getCourtno()+")");
        }*/

        if (dataModel.isSelector() == false) {
            String stage = "";
            if (dataModel.getPurposeName() != null) {
                if (dataModel.getPurposeName().contains("Â")) {
                    stage = dataModel.getPurposeName().replaceAll("Â", "");
                } else {
                    stage = dataModel.getPurposeName();
                }

            } else {
                stage = "";
            }
            if (dataModel.getRegNo() == null && dataModel.getRegYear() == null) {
                viewHolder.txtName.setText("" + dataModel.getTypeName() + "/ NO REG  YET/ N/A /" + stage.replace("Stage of Case:", ""));
            } else {
                viewHolder.txtName.setText("" + dataModel.getTypeName() + "/" + dataModel.getRegNo() + "/" + dataModel.getRegYear() + "/" + stage.replace("Stage of Case:", ""));
            }

            if (dataModel.getArchive().equals("Y")) {
                viewHolder.txtType.setText("Disposed Year:" + sdfyear.format(dataModel.getDateOfDecision()) + "/ " + dataModel.getPetName() + " VS " + dataModel.getResName());
            } else {
                viewHolder.txtType.setText(dataModel.getPetName() + " VS " + dataModel.getResName());
            }
//        if (dataModel.getDateNextList() != null) {
//            viewHolder.txtVersion.setText(sdf.format(dataModel.getDateNextList()));
//        } else {
//            viewHolder.txtVersion.setText("N/A");
//        }
            Calendar cal = Calendar.getInstance();
            if (dataModel.getDateNextList() != null) {
                if (dataModel.getArchive().equals("Y")) {
                    cal.setTime(dataModel.getDateOfDecision());
                    viewHolder.info.setImageBitmap(CasesyncUtill.createimagegenerator(mImageGenerator, cal, false));
                } else {
                    if (dataModel.getDateNextList() != null) {
                        cal.setTime(dataModel.getDateNextList());
                    } else {
                        cal.setTime(null);
                    }

                    viewHolder.info.setImageBitmap(CasesyncUtill.createimagegenerator(mImageGenerator, cal, true));
                }
            } else {
                //Yet to decide
                //It will display sand timer
                viewHolder.info.setImageBitmap(BitmapFactory.decodeFile(String.valueOf(R.drawable.empty_calendar)));
            }
            viewHolder.info.setOnClickListener(this);
        }
        viewHolder.info.setTag(position);
        lastPosition = position;
        return convertView;

    }

    public void resetData() {
        dataSet = filter;
    }

    @Override
    public Filter getFilter() {
        if (caseFilter == null)
            caseFilter = new CaseFilter();
        return caseFilter;
    }
    /* *********************************
     * We use the holder pattern
     * It makes the view faster and avoid finding the component
     * **********************************/

    // View lookup cache
    private static class ViewHolder {
        RelativeLayout layout;
        TextView txtName;
        TextView txtType;
        ImageView info;
        TextView grouping;
    }


    private class CaseFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            Log.e("Filter", "" + constraint);
            if (constraint == null || constraint.length() == 0) {
                results.values = dataSet;
                results.count = dataSet.size();
            } else {
                List<CaseObj> nPlanetList = new ArrayList<CaseObj>(dataSet.size());
                Log.e("Filter", "Size:" + dataSet.size());
                for (CaseObj p : dataSet) {
                    if (p.isSelector() == false) {
                        //ENABLE FOR DATE FILTER
                        //Log.e("Filter", "Looping through filter:" + p.getCaseNo());
                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        if (p.getArchive().equalsIgnoreCase("Y")) {
                            if (df.format(p.getDateOfDecision()).startsWith(constraint.toString())) {
                                nPlanetList.add(p);
                            }
                        } else {
                            if (p.getDateNextList() != null) {
                                if (df.format(p.getDateNextList()).startsWith(constraint.toString())) {
                                    //Log.i("Filter", "Coming inside the getNext_hearing_date:" + p.getDateNextList());
                                    nPlanetList.add(p);
                                }
                            }
                        }

                        if (p.getTypeName() != null) {
                            if (p.getTypeName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getRegNo() != null) {
                            if (p.getRegNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getFilNo() != null) {
                            if (p.getFilNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                if (p.getRegNo() != null) {
                                    if (p.getFilNo().equals(p.getRegNo())) {
                                        Log.i(TAG, "Filing number and Reg number is same. Ignored to avoid duplicates.");
                                    } else {
                                        nPlanetList.add(p);
                                    }
                                } else {
                                    nPlanetList.add(p);
                                }
                            }
                        }
                        if (p.getFirNo() != null) {
                            if (p.getFirNo().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }

                        if (p.getCourtName() != null) {
                            if (p.getCourtName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getPetName() != null) {
                            if (p.getPetName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getResName() != null) {
                            if (p.getResName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getPetAdv() != null) {
                            if (p.getPetAdv().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getResAdv() != null) {
                            if (p.getResAdv().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }
                        if (p.getContactname() != null) {
                            if (p.getContactname().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                nPlanetList.add(p);
                            }
                        }

                        results.values = nPlanetList;
                        results.count = nPlanetList.size();
                    }
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count == 0) {
                Log.i(TAG, "No data available for the given filter");
                filter = new ArrayList<CaseObj>();
                Toast.makeText(getContext(), "No cases available with the selected filter ", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
                //notifyDataSetInvalidated();
            } else {
                filter = (ArrayList<CaseObj>) filterResults.values;
                notifyDataSetChanged();
            }
        }


        public Bitmap createimagegenerator(ImageGenerator mImageGenerator, Calendar calendar, boolean status) {
            try {
                // Set the icon size to the generated in dip.
                mImageGenerator.setIconSize(512, 512);
                // Set the size of the date and month font in dip.
                mImageGenerator.setDateSize(200);
                mImageGenerator.setMonthSize(85);
                // Set the position of the date and month in dip.
                mImageGenerator.setDatePosition(380);
                mImageGenerator.setMonthPosition(140);
                // Set the color of the font to be generated
                //mImageGenerator.setDateColor(Color.parseColor("#3c6eaf"));
                mImageGenerator.setDateColor(Color.BLACK);
                mImageGenerator.setMonthColor(Color.WHITE);
                if (status == true) {
                    Bitmap bitmap = mImageGenerator.generateDateImage(calendar, R.drawable.empty_calendar);
                    return bitmap;
                } else {
                    Bitmap bitmap = mImageGenerator.generateDateImage(calendar, R.drawable.empty_calendar_dis);
                    return bitmap;
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}