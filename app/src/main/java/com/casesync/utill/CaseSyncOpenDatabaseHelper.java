package com.casesync.utill;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.casesync.casesync.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;


/**
 * Created by Vikas on 1/20/2017.
 */

public class CaseSyncOpenDatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "casesync";
    private static final String TAG = "ORM";
    private static final int DATABASE_VERSION = 1;

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<Casedetails, Long> todoDao;
    private Dao<Casehistory, Long> casehistorydao;
    private Dao<CaseObj, Long> caseobjdao;

    public CaseSyncOpenDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION,
                /**
                 * R.raw.ormlite_config is a reference to the ormlite_config.txt file in the
                 * /res/raw/ directory of this project
                 * */
                R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, Casedetails.class);
            TableUtils.createTable(connectionSource, Casehistory.class);
            TableUtils.createTable(connectionSource, CaseObj.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            Log.i(TAG, oldVersion + ":" + newVersion);
            TableUtils.dropTable(connectionSource, Casedetails.class, false);
            onCreate(database, connectionSource);
            TableUtils.dropTable(connectionSource, Casehistory.class, false);
            onCreate(database, connectionSource);
            TableUtils.dropTable(connectionSource, CaseObj.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteTables() {
        try {

            TableUtils.dropTable(connectionSource, Casedetails.class, false);
            TableUtils.dropTable(connectionSource, Casehistory.class, false);
            TableUtils.dropTable(connectionSource, CaseObj.class, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void createTables() {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Casedetails.class);
            TableUtils.createTableIfNotExists(connectionSource, Casehistory.class);
            TableUtils.createTableIfNotExists(connectionSource, CaseObj.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    /**
     * Returns an instance of the data access object
     *
     * @return
     * @throws SQLException
     */
    public Dao<Casedetails, Long> getDao() throws SQLException {
        if (todoDao == null) {
            todoDao = getDao(Casedetails.class);
        }
        return todoDao;
    }

    public Dao<Casehistory, Long> getCasehistorydao() throws SQLException {
        if (casehistorydao == null) {
            casehistorydao = getDao(Casehistory.class);
        }
        return casehistorydao;
    }

    public Dao<CaseObj, Long> getCaseObjdao() throws SQLException {
        if (caseobjdao == null) {
            caseobjdao = getDao(CaseObj.class);
        }
        return caseobjdao;
    }

}

