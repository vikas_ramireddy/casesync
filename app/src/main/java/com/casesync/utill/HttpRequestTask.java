package com.casesync.utill;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


/**
 * Created by Vikas on 12/30/2016.
 */

public class HttpRequestTask extends AsyncTask<Void, Void, String> {
    private LoginParams parms;

    // a constructor so that you can pass the object and use
    public HttpRequestTask(LoginParams parms){
        this.parms = parms;
    }
    @Override
    protected String doInBackground(Void... params) {
        /*write two classed
        * one should expect string and another with class*/
       String response=null;
        try {
             String USER_AGENT = "Mozilla/21.0";
            Log.i("MainActivity",""+parms.getUrl());
            //parms
             String uri = parms.getUrl();
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", "PHPSESSID=" + parms.getUsername());
            requestHeaders.add("User-Agent", USER_AGENT);
            HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
             RestTemplate restTemplate = new RestTemplate();
            ResponseEntity rssResponse = restTemplate.exchange(uri,
                    HttpMethod.GET,
                    requestEntity,
                    String.class);
             response = rssResponse.getBody().toString();




            //response = restTemplate.getForObject(uri, String.class);
        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }


        return response;
    }
}
