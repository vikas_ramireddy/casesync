package com.casesync.utill;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "casedetails")
public class Court {
	@DatabaseField(columnName = "courtcode_id")
	private int courtcode_id;
	@DatabaseField(columnName = "name")
	private String name;
	@DatabaseField(columnName = "states_id")
	private States states_id;
	@DatabaseField(columnName = "district_id")
	private District district_id;
	@DatabaseField(columnName = "courtcode")
	private String courtcode;
	

	public int getCourtcode_id() {
		return courtcode_id;
	}
	
	public void setCourtcode_id(int courtcode_id) {
		this.courtcode_id = courtcode_id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	public States getStates_id() {
		return states_id;
	}
	public void setStates_id(States states_id) {
		this.states_id = states_id;
	}

	public District getDistrict_id() {
		return district_id;
	}
	public void setDistrict_id(District district_id) {
		this.district_id = district_id;
	}

	public String getCourtcode() {
		return courtcode;
	}
	public void setCourtcode(String courtcode) {
		this.courtcode = courtcode;
	}
	

}
