package com.casesync.utill;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "casedetails")
public class Casedetails  {

	@DatabaseField(columnName = "casenumber")
	private String casenumber;
	@DatabaseField(columnName = "casetype")
	private String casetype;
	@DatabaseField(columnName = "filingnumber")
	private String filingnumber;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@DatabaseField(columnName = "filingdate", dataType = DataType.DATE_STRING,
			format = "dd-MM-yyyy")
	private Date filingdate;
	@DatabaseField(columnName = "registration_number")
	private String registration_number;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@DatabaseField(columnName = "registration_date", dataType = DataType.DATE_STRING,
			format = "dd-MM-yyyy")
	private Date registration_date;
	@DatabaseField(columnName = "courtname")
	private String courtname;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	@DatabaseField(columnName = "first_hearing_date", dataType = DataType.DATE_STRING,
			format = "dd-MM-yyyy")
	private Date first_hearing_date;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
//	@DatabaseField(columnName = "next_hearing_date", dataType = DataType.DATE_STRING,
//			format = "dd-MM-yyyy")
		@DatabaseField(columnName = "next_hearing_date", dataType = DataType.DATE_LONG)
	private Date next_hearing_date;
	@DatabaseField(columnName = "stageofcase")
	private String stageofcase;
	@DatabaseField(columnName = "courtnumberjudge")
	private String courtnumberjudge;
	@DatabaseField(columnName = "petitioner_advocate")
	private String petitioner_advocate;
	@DatabaseField(columnName = "respondentadvocate")
	private String respondentadvocate;
	@DatabaseField(columnName = "petitioner_names")
	private String petitioner_names;
	@DatabaseField(columnName = "petitioner_advocate_name")
	private String petitioner_advocate_name;
	@DatabaseField(columnName = "respondent_names")
	private String respondent_names;
	@DatabaseField(columnName = "respondent_advocate_name")
	private String respondent_advocate_name;
	@DatabaseField(columnName = "disposed")
	private boolean disposed;
	@DatabaseField(columnName = "natureofdisposal")
	private String natureofdisposal;
	@DatabaseField(columnName = "subcourtinfo")
	private String subcourtinfo;
	@DatabaseField(columnName = "firdetails")
	private String firdetails;
	@DatabaseField(columnName = "acts")
	private String acts;
	@DatabaseField(columnName = "casetransfer")
	private String casetransfer;
	@DatabaseField(columnName="statecode")
	private String statecode;
	@DatabaseField(columnName="districtcode")
	private String districtcode;
	@DatabaseField(columnName="courtcode")
	private String courtcode;
	@DatabaseField(columnName="phone")
	private String phone;
	@DatabaseField(columnName="email")
	private String email;
	@DatabaseField(columnName="contactname")
	private String contactname;
	@DatabaseField(columnName="addnotes")
	private String addnotes;


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContactname() {
		return contactname;
	}

	public void setContactname(String contactname) {
		this.contactname = contactname;
	}

	public String getAddnotes() {
		return addnotes;
	}

	public void setAddnotes(String addnotes) {
		this.addnotes = addnotes;
	}



	public String getStatecode() {
		return statecode;
	}

	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}

	public String getDistrictcode() {
		return districtcode;
	}

	public void setDistrictcode(String districtcode) {
		this.districtcode = districtcode;
	}

	public String getCourtcode() {
		return courtcode;
	}

	public void setCourtcode(String courtcode) {
		this.courtcode = courtcode;
	}

	public boolean isDisposed() {
		return disposed;
	}
	public void setDisposed(boolean disposed) {
		this.disposed = disposed;
	}
	public String getNatureofdisposal() {
		return natureofdisposal;
	}
	public void setNatureofdisposal(String natureofdisposal) {
		this.natureofdisposal = natureofdisposal;
	}
	public String getSubcourtinfo() {
		return subcourtinfo;
	}
	public void setSubcourtinfo(String subcourtinfo) {
		this.subcourtinfo = subcourtinfo;
	}
	public String getFirdetails() {
		return firdetails;
	}
	public void setFirdetails(String firdetails) {
		this.firdetails = firdetails;
	}
	public String getActs() {
		return acts;
	}
	public void setActs(String acts) {
		this.acts = acts;
	}
	public String getCasetransfer() {
		return casetransfer;
	}
	public void setCasetransfer(String casetransfer) {
		this.casetransfer = casetransfer;
	}
	public String getPetitioner_names() {
		return petitioner_names;
	}
	public void setPetitioner_names(String petitioner_names) {
		this.petitioner_names = petitioner_names;
	}
	public String getPetitioner_advocate_name() {
		return petitioner_advocate_name;
	}
	public void setPetitioner_advocate_name(String petitioner_advocate_name) {
		this.petitioner_advocate_name = petitioner_advocate_name;
	}
	public String getRespondent_names() {
		return respondent_names;
	}
	public void setRespondent_names(String respondent_names) {
		this.respondent_names = respondent_names;
	}
	public String getRespondent_advocate_name() {
		return respondent_advocate_name;
	}
	public void setRespondent_advocate_name(String respondent_advocate_name) {
		this.respondent_advocate_name = respondent_advocate_name;
	}
	public String getCasenumber() {
		return casenumber;
	}
	public void setCasenumber(String casenumber) {
		this.casenumber = casenumber;
	}
	public String getCasetype() {
		return casetype;
	}
	public void setCasetype(String casetype) {
		this.casetype = casetype;
	}
	public String getFilingnumber() {
		return filingnumber;
	}
	public void setFilingnumber(String filingnumber) {
		this.filingnumber = filingnumber;
	}
	
	public Date getFilingdate() {
		return filingdate;
	}
	public void setFilingdate(Date filingdate) {
		this.filingdate = filingdate;
	}
	public String getRegistration_number() {
		return registration_number;
	}
	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}
	public Date getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}
	public String getCourtname() {
		return courtname;
	}
	public void setCourtname(String courtname) {
		this.courtname = courtname;
	}
	public Date getFirst_hearing_date() {
		return first_hearing_date;
	}
	public void setFirst_hearing_date(Date first_hearing_date) {
		this.first_hearing_date = first_hearing_date;
	}
	public Date getNext_hearing_date() {
		return next_hearing_date;
	}
	public void setNext_hearing_date(Date next_hearing_date) {
		this.next_hearing_date = next_hearing_date;
	}
	public String getStageofcase() {
		return stageofcase;
	}
	public void setStageofcase(String stageofcase) {
		this.stageofcase = stageofcase;
	}
	public String getCourtnumberjudge() {
		return courtnumberjudge;
	}
	public void setCourtnumberjudge(String courtnumberjudge) {
		this.courtnumberjudge = courtnumberjudge;
	}
	public String getPetitioner_advocate() {
		return petitioner_advocate;
	}
	public void setPetitioner_advocate(String petitioner_advocate) {
		this.petitioner_advocate = petitioner_advocate;
	}
	public String getRespondentadvocate() {
		return respondentadvocate;
	}
	public void setRespondentadvocate(String respondentadvocate) {
		this.respondentadvocate = respondentadvocate;
	}
}
