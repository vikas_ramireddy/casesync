package com.casesync.utill;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = "states")
public class States {
	@DatabaseField(columnName = "states_id")
	private Integer states_id;
	@DatabaseField(columnName = "statename")
	private String  statename;
	
	private List<District> districts;
	private List<Casetype> casetypes;

	public Integer getStates_id() {
		return states_id;
	}
	public void setStates_id(Integer states_id) {
		this.states_id = states_id;
	}

	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}

	@JsonIgnore
	@JsonBackReference
	public List<District> getDistricts() {
		return districts;
	}
	public void setDistricts(List<District> districts) {
		this.districts = districts;
	}
	@JsonIgnore
	@JsonBackReference
	public List<Casetype> getCasetypes() {
		return casetypes;
	}
	public void setCasetypes(List<Casetype> casetypes) {
		this.casetypes = casetypes;
	}
	
}
