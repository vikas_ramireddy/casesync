package com.casesync.utill;

import java.io.Serializable;

public class Cases implements Serializable {
    boolean selected = true;

    private String court_code;
    private String establishment_name;
    private String state_code;
    private String dist_code;
    private String cino;
    private String case_no;
    private String case_no2;
    private String regcase_type;
    private String reg_year;
    private String pet_name;
    private String res_name;
    private String extra_party;
    private String party_name1;
    private String party_name2;
    private String date_of_decision;
    private String orcase;
    private String type_name;
    private String petnameadArr;
    private String adv_name1;
    private String adv_name2;
    private String ladv_name1;
    private String ladv_name2;
    private String fir_year;
    private String fir_no;
    private String case_type;
    private String case_year;

    private String lpet_name;
    private String lres_name;
    private String lextra_party;
    private String type;

    private boolean isSelected;


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getDist_code() {
        return dist_code;
    }

    public void setDist_code(String dist_code) {
        this.dist_code = dist_code;
    }

    public String getCourt_code() {
        return court_code;
    }

    public void setCourt_code(String court_code) {
        this.court_code = court_code;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public void setEstablishment_name(String establishment_name) {
        this.establishment_name = establishment_name;
    }

    public String getCino() {
        return cino;
    }

    public void setCino(String cino) {
        this.cino = cino;
    }

    public String getCase_no() {
        return case_no;
    }

    public void setCase_no(String case_no) {
        this.case_no = case_no;
    }

    public String getCase_no2() {
        return case_no2;
    }

    public void setCase_no2(String case_no2) {
        this.case_no2 = case_no2;
    }

    public String getRegcase_type() {
        return regcase_type;
    }

    public void setRegcase_type(String regcase_type) {
        this.regcase_type = regcase_type;
    }

    public String getReg_year() {
        return reg_year;
    }

    public void setReg_year(String reg_year) {
        this.reg_year = reg_year;
    }

    public String getPet_name() {
        return pet_name;
    }

    public void setPet_name(String pet_name) {
        this.pet_name = pet_name;
    }

    public String getRes_name() {
        return res_name;
    }

    public void setRes_name(String res_name) {
        this.res_name = res_name;
    }

    public String getExtra_party() {
        return extra_party;
    }

    public void setExtra_party(String extra_party) {
        this.extra_party = extra_party;
    }

    public String getParty_name1() {
        return party_name1;
    }

    public void setParty_name1(String party_name1) {
        this.party_name1 = party_name1;
    }

    public String getParty_name2() {
        return party_name2;
    }

    public void setParty_name2(String party_name2) {
        this.party_name2 = party_name2;
    }

    public String getDate_of_decision() {
        return date_of_decision;
    }

    public void setDate_of_decision(String date_of_decision) {
        this.date_of_decision = date_of_decision;
    }

    public String getOrcase() {
        return orcase;
    }

    public void setOrcase(String orcase) {
        this.orcase = orcase;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getPetnameadArr() {
        return petnameadArr;
    }

    public void setPetnameadArr(String petnameadArr) {
        this.petnameadArr = petnameadArr;
    }

    public String getAdv_name1() {
        return adv_name1;
    }

    public void setAdv_name1(String adv_name1) {
        this.adv_name1 = adv_name1;
    }

    public String getAdv_name2() {
        return adv_name2;
    }

    public void setAdv_name2(String adv_name2) {
        this.adv_name2 = adv_name2;
    }

    public String getLadv_name1() {
        return ladv_name1;
    }

    public void setLadv_name1(String ladv_name1) {
        this.ladv_name1 = ladv_name1;
    }

    public String getLadv_name2() {
        return ladv_name2;
    }

    public void setLadv_name2(String ladv_name2) {
        this.ladv_name2 = ladv_name2;
    }

    public String getFir_year() {
        return fir_year;
    }

    public void setFir_year(String fir_year) {
        this.fir_year = fir_year;
    }

    public String getFir_no() {
        return fir_no;
    }

    public void setFir_no(String fir_no) {
        this.fir_no = fir_no;
    }

    public String getCase_type() {
        return case_type;
    }

    public void setCase_type(String case_type) {
        this.case_type = case_type;
    }

    public String getCase_year() {
        return case_year;
    }

    public void setCase_year(String case_year) {
        this.case_year = case_year;
    }

    public String getLpet_name() {
        return lpet_name;
    }

    public void setLpet_name(String lpet_name) {
        this.lpet_name = lpet_name;
    }

    public String getLres_name() {
        return lres_name;
    }

    public void setLres_name(String lres_name) {
        this.lres_name = lres_name;
    }

    public String getLextra_party() {
        return lextra_party;
    }

    public void setLextra_party(String lextra_party) {
        this.lextra_party = lextra_party;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
