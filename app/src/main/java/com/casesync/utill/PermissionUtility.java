package com.casesync.utill;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.os.Build.VERSION.SDK_INT;

public class PermissionUtility {

    public boolean askForPermission(String permission, Integer requestCode, Context context, Activity activity) {
        boolean status = false;
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
            }
            status = true;
        } else {
            status = true;
        }
        return status;
    }


    public boolean checkForPermission(String permission, Context context) {
        boolean status = false;
        if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED) {
            status = true;
        }
        return status;
    }

    public void checkForFilePermissions(Context context, Activity activity) {
        if (SDK_INT < Build.VERSION_CODES.R) {
            boolean read = checkForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, context);
            boolean write = checkForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, context);
            if (read == true && write == true) {
                //No need for any operations.
            } else if (read == true && write == false) {
                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0, context, activity);
            } else if (read == false && write == true) {
                askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0, context, activity);
            } else {
                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0, context, activity);
                askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, 0, context, activity);
            }
        }

    }

    public boolean validateFilePermissions(Context context) {
        boolean status = false;
        if (SDK_INT < Build.VERSION_CODES.R) {
            boolean read = checkForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, context);
            boolean write = checkForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, context);
            if (read == true && write == true) {
                status = true;
            }
        } else {
            status = true;
        }
        return status;
    }

    public int getEcourtsVersion(Context context) {
        try {
            PackageInfo pinfo = context.getPackageManager().getPackageInfo("in.gov.ecourts.eCourtsServices", 0);
            String verName = pinfo.versionName;
            return convertVersionToInt(verName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int convertVersionToInt(String verName) {
        int verCode = Integer.parseInt(verName.substring(0, verName.indexOf(".")));
        return verCode;
    }
}
