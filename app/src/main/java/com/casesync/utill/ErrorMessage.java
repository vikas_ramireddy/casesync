package com.casesync.utill;

/**
 * Created by SPLLP02 on 06-11-2016.
 */

public class ErrorMessage {
    private boolean status;
    private String message;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
