package com.casesync.utill;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Vikas on 1/21/2017.
 */

public class PropertyUtil {
    public static String getProperty(String key,Context context) throws IOException {
        Properties properties = new Properties();;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open("casesync.properties");
        properties.load(inputStream);
        return properties.getProperty(key);

    }
//    public static boolean setProperty(String key,String value,Context context) throws IOException {
//
//        Properties properties = new Properties();;
//        AssetManager assetManager = context.getAssets();
//        InputStream inputStream = assetManager.open("casesync.properties");
//        properties.load(inputStream);
//        properties.setProperty(key,value);
//
//        return false;
//
//    }
}
