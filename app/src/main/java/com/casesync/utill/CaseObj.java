package com.casesync.utill;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "date_of_filing",
        "cino",
        "dt_regis",
        "fil_no",
        "fil_year",
        "reg_no",
        "reg_year",
        "date_first_list",
        "date_next_list",
        "archive",
        "date_of_decision",
        "disp_nature",
        "purpose_next",
        "court_no",
        "pet_name",
        "pet_adv",
        "res_name",
        "res_adv",
        "res_legal_heir",
        "pet_legal_heir",
        "hide_pet_name",
        "hide_res_name",
        "pet_status",
        "res_status",
        "under_act1",
        "under_act2",
        "under_act3",
        "under_act4",
        "under_sec1",
        "under_sec2",
        "under_sec3",
        "under_sec4",
        "fir_no",
        "police_st_code",
        "fir_year",
        "lower_court_code",
        "lower_court",
        "lower_court_dec_dt",
        "case_no",
        "goshwara_no",
        "date_last_list",
        "transfer_est",
        "purpose_name",
        "type_name",
        "petNameAdd",
        "str_error",
        "desgname",
        "resNameAdd",
        "str_error1",
        "act",
        "fir_details",
        "subordinateCourtInfoStr",
        "historyOfCaseHearing",
        "interimOrder",
        "finalOrder",
        "transfer",
        "courtno",
        "ldesgname",
        "desgcode",
        "judcode",
        "jcode",
        "petparty_name",
        "resparty_name",
        "court_name",
        "est_code",
        "state_code",
        "district_code",
        "state_name",
        "district_name",
        "transfer_est_flag",
        "transfer_est_name",
        "transfer_est_date",
        "writinfo"
})
@DatabaseTable(tableName = "caseobj")
public class CaseObj implements Serializable {
    @DatabaseField(columnName = "cino", generatedId = false, id = true)
    private String cino;
    @DatabaseField(columnName = "case_no")
    private String caseNo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "date_of_filing", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dateOfFiling;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "dt_regis", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dtRegis;
    @DatabaseField(columnName = "fil_no")
    private String filNo;
    @DatabaseField(columnName = "fil_year")
    private String filYear;
    @DatabaseField(columnName = "reg_no")
    private String regNo;
    @DatabaseField(columnName = "reg_year")
    private String regYear;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "date_first_list", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dateFirstList;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "date_next_list", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dateNextList;
    @DatabaseField(columnName = "archive")
    private String archive;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "date_of_decision", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dateOfDecision;
    @DatabaseField(columnName = "disp_nature")
    private Long dispNature;
    @DatabaseField(columnName = "purpose_next")
    private Long purposeNext;
    @DatabaseField(columnName = "court_no")
    private String courtNo;
    @DatabaseField(columnName = "pet_name")
    private String petName;
    @DatabaseField(columnName = "pet_adv")
    private String petAdv;
    @DatabaseField(columnName = "res_name")
    private String resName;
    @DatabaseField(columnName = "res_adv")
    private String resAdv;
    @DatabaseField(columnName = "res_legal_heir")
    private String resLegalHeir;
    @DatabaseField(columnName = "pet_legal_heir")
    private String petLegalHeir;
    @DatabaseField(columnName = "hide_pet_name")
    private String hidePetName;
    @DatabaseField(columnName = "hide_res_name")
    private String hideResName;
    @DatabaseField(columnName = "pet_status")
    private Long petStatus;
    @DatabaseField(columnName = "res_status")
    private Long resStatus;
    @DatabaseField(columnName = "under_act1")
    private Long underAct1;
    @DatabaseField(columnName = "under_act2")
    private Long underAct2;
    @DatabaseField(columnName = "under_act3")
    private Long underAct3;
    @DatabaseField(columnName = "under_act4")
    private Long underAct4;
    @DatabaseField(columnName = "under_sec1")
    private String underSec1;
    @DatabaseField(columnName = "under_sec2")
    private String underSec2;
    @DatabaseField(columnName = "under_sec3")
    private String underSec3;
    @DatabaseField(columnName = "under_sec4")
    private String underSec4;
    @DatabaseField(columnName = "fir_no")
    private String firNo;
    @DatabaseField(columnName = "police_st_code")
    private Long policeStCode;
    @DatabaseField(columnName = "fir_year")
    private Long firYear;
    @DatabaseField(columnName = "lower_court_code")
    private Long lowerCourtCode;
    @DatabaseField(columnName = "lower_court")
    private String lowerCourt;
    @DatabaseField(columnName = "lower_court_dec_dt")
    private Long lowerCourtDecDt;

    @DatabaseField(columnName = "goshwara_no")
    private Long goshwaraNo;
    @DatabaseField(columnName = "date_last_list", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date dateLastList;
    @DatabaseField(columnName = "transfer_est")
    private String transferEst;
    @DatabaseField(columnName = "purpose_name")
    private String purposeName;
    @DatabaseField(columnName = "type_name")
    private String typeName;
    @DatabaseField(columnName = "petNameAdd")
    private String petNameAdd;
    @DatabaseField(columnName = "str_error")
    private String strError;
    @DatabaseField(columnName = "desgname")
    private String desgname;
    @DatabaseField(columnName = "resNameAdd")
    private String resNameAdd;
    @DatabaseField(columnName = "str_error1")
    private String strError1;


    @DatabaseField(columnName = "disp_name")
    private String disp_name;

    @DatabaseField(columnName = "act")
    private String act;
    @DatabaseField(columnName = "fir_details")
    private String firDetails;
    @DatabaseField(columnName = "subordinateCourtInfoStr")
    private String subordinateCourtInfoStr;
    @DatabaseField(columnName = "historyOfCaseHearing")
    private String historyOfCaseHearing;
    @DatabaseField(columnName = "interimOrder")
    private String interimOrder;
    @DatabaseField(columnName = "finalOrder")
    private String finalOrder;
    @DatabaseField(columnName = "transfer")
    private String transfer;
    @DatabaseField(columnName = "courtno")
    private Long courtno;
    @DatabaseField(columnName = "ldesgname")
    private String ldesgname;
    @DatabaseField(columnName = "desgcode")
    private Long desgcode;
    @DatabaseField(columnName = "judcode")
    private Long judcode;
    @DatabaseField(columnName = "jcode")
    private Long jcode;
    @DatabaseField(columnName = "petparty_name")
    private String petpartyName;
    @DatabaseField(columnName = "resparty_name")
    private String respartyName;
    @DatabaseField(columnName = "court_name")
    private String courtName;
    @DatabaseField(columnName = "est_code")
    private String estCode;
    @DatabaseField(columnName = "state_code")
    private String stateCode;
    @DatabaseField(columnName = "district_code")
    private String districtCode;
    @DatabaseField(columnName = "state_name")
    private String stateName;
    @DatabaseField(columnName = "district_name")
    private String districtName;
    @DatabaseField(columnName = "transfer_est_flag")
    private String transferEstFlag;
    @DatabaseField(columnName = "transfer_est_name")
    private String transferEstName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DatabaseField(columnName = "transfer_est_date", dataType = DataType.DATE_STRING,
            format = "yyyy-MM-dd")
    private Date transferEstDate;
    @DatabaseField(columnName = "writinfo")
    private String writinfo;
    @DatabaseField(columnName = "phone")
    private String phone;
    @DatabaseField(columnName = "email")
    private String email;

    @DatabaseField(columnName = "contactname")
    private String contactname;
    @DatabaseField(columnName = "addnotes")
    private String addnotes;

    @JsonIgnore
    private boolean selector;
    @JsonIgnore
    private int courtcount;

    @JsonIgnore
    private Map<String, String> additionalProperties = new HashMap<String, String>();

    @JsonProperty("disp_name")
    public String getDisp_name() {
        return disp_name;
    }

    public void setDisp_name(String disp_name) {
        this.disp_name = disp_name;
    }


    @JsonProperty("date_of_filing")
    public Date getDateOfFiling() {
        return dateOfFiling;
    }

    @JsonProperty("date_of_filing")
    public void setDateOfFiling(Date dateOfFiling) {
        this.dateOfFiling = dateOfFiling;
    }

    @JsonProperty("cino")
    public String getCino() {
        return cino;
    }

    @JsonProperty("cino")
    public void setCino(String cino) {
        this.cino = cino;
    }

    @JsonProperty("dt_regis")
    public Date getDtRegis() {
        return dtRegis;
    }

    @JsonProperty("dt_regis")
    public void setDtRegis(Date dtRegis) {
        this.dtRegis = dtRegis;
    }

    @JsonProperty("fil_no")
    public String getFilNo() {
        return filNo;
    }

    @JsonProperty("fil_no")
    public void setFilNo(String filNo) {
        this.filNo = filNo;
    }

    @JsonProperty("fil_year")
    public String getFilYear() {
        return filYear;
    }

    @JsonProperty("fil_year")
    public void setFilYear(String filYear) {
        this.filYear = filYear;
    }

    @JsonProperty("reg_no")
    public String getRegNo() {
        return regNo;
    }

    @JsonProperty("reg_no")
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    @JsonProperty("reg_year")
    public String getRegYear() {
        return regYear;
    }

    @JsonProperty("reg_year")
    public void setRegYear(String regYear) {
        this.regYear = regYear;
    }

    @JsonProperty("date_first_list")
    public Date getDateFirstList() {
        return dateFirstList;
    }

    @JsonProperty("date_first_list")
    public void setDateFirstList(Date dateFirstList) {
        this.dateFirstList = dateFirstList;
    }

    @JsonProperty("date_next_list")
    public Date getDateNextList() {
        return dateNextList;
    }

    @JsonProperty("date_next_list")
    public void setDateNextList(Date dateNextList) {
        this.dateNextList = dateNextList;
    }

    @JsonProperty("archive")
    public String getArchive() {
        return archive;
    }

    @JsonProperty("archive")
    public void setArchive(String archive) {
        this.archive = archive;
    }

    @JsonProperty("date_of_decision")
    public Date getDateOfDecision() {
        return dateOfDecision;
    }

    @JsonProperty("date_of_decision")
    public void setDateOfDecision(Date dateOfDecision) {
        this.dateOfDecision = dateOfDecision;
    }

    @JsonProperty("disp_nature")
    public Long getDispNature() {
        return dispNature;
    }

    @JsonProperty("disp_nature")
    public void setDispNature(Long dispNature) {
        this.dispNature = dispNature;
    }

    @JsonProperty("purpose_next")
    public Long getPurposeNext() {
        return purposeNext;
    }

    @JsonProperty("purpose_next")
    public void setPurposeNext(Long purposeNext) {
        this.purposeNext = purposeNext;
    }

    @JsonProperty("court_no")
    public String getCourtNo() {
        return courtNo;
    }

    @JsonProperty("court_no")
    public void setCourtNo(String courtNo) {
        this.courtNo = courtNo;
    }

    @JsonProperty("pet_name")
    public String getPetName() {
        return petName;
    }

    @JsonProperty("pet_name")
    public void setPetName(String petName) {
        this.petName = petName;
    }

    @JsonProperty("pet_adv")
    public String getPetAdv() {
        return petAdv;
    }

    @JsonProperty("pet_adv")
    public void setPetAdv(String petAdv) {
        this.petAdv = petAdv;
    }

    @JsonProperty("res_name")
    public String getResName() {
        return resName;
    }

    @JsonProperty("res_name")
    public void setResName(String resName) {
        this.resName = resName;
    }

    @JsonProperty("res_adv")
    public String getResAdv() {
        return resAdv;
    }

    @JsonProperty("res_adv")
    public void setResAdv(String resAdv) {
        this.resAdv = resAdv;
    }

    @JsonProperty("res_legal_heir")
    public String getResLegalHeir() {
        return resLegalHeir;
    }

    @JsonProperty("res_legal_heir")
    public void setResLegalHeir(String resLegalHeir) {
        this.resLegalHeir = resLegalHeir;
    }

    @JsonProperty("pet_legal_heir")
    public String getPetLegalHeir() {
        return petLegalHeir;
    }

    @JsonProperty("pet_legal_heir")
    public void setPetLegalHeir(String petLegalHeir) {
        this.petLegalHeir = petLegalHeir;
    }

    @JsonProperty("hide_pet_name")
    public String getHidePetName() {
        return hidePetName;
    }

    @JsonProperty("hide_pet_name")
    public void setHidePetName(String hidePetName) {
        this.hidePetName = hidePetName;
    }

    @JsonProperty("hide_res_name")
    public String getHideResName() {
        return hideResName;
    }

    @JsonProperty("hide_res_name")
    public void setHideResName(String hideResName) {
        this.hideResName = hideResName;
    }

    @JsonProperty("pet_status")
    public Long getPetStatus() {
        return petStatus;
    }

    @JsonProperty("pet_status")
    public void setPetStatus(Long petStatus) {
        this.petStatus = petStatus;
    }

    @JsonProperty("res_status")
    public Long getResStatus() {
        return resStatus;
    }

    @JsonProperty("res_status")
    public void setResStatus(Long resStatus) {
        this.resStatus = resStatus;
    }

    @JsonProperty("under_act1")
    public Long getUnderAct1() {
        return underAct1;
    }

    @JsonProperty("under_act1")
    public void setUnderAct1(Long underAct1) {
        this.underAct1 = underAct1;
    }

    @JsonProperty("under_act2")
    public Long getUnderAct2() {
        return underAct2;
    }

    @JsonProperty("under_act2")
    public void setUnderAct2(Long underAct2) {
        this.underAct2 = underAct2;
    }

    @JsonProperty("under_act3")
    public Long getUnderAct3() {
        return underAct3;
    }

    @JsonProperty("under_act3")
    public void setUnderAct3(Long underAct3) {
        this.underAct3 = underAct3;
    }

    @JsonProperty("under_act4")
    public Long getUnderAct4() {
        return underAct4;
    }

    @JsonProperty("under_act4")
    public void setUnderAct4(Long underAct4) {
        this.underAct4 = underAct4;
    }

    @JsonProperty("under_sec1")
    public String getUnderSec1() {
        return underSec1;
    }

    @JsonProperty("under_sec1")
    public void setUnderSec1(String underSec1) {
        this.underSec1 = underSec1;
    }

    @JsonProperty("under_sec2")
    public String getUnderSec2() {
        return underSec2;
    }

    @JsonProperty("under_sec2")
    public void setUnderSec2(String underSec2) {
        this.underSec2 = underSec2;
    }

    @JsonProperty("under_sec3")
    public String getUnderSec3() {
        return underSec3;
    }

    @JsonProperty("under_sec3")
    public void setUnderSec3(String underSec3) {
        this.underSec3 = underSec3;
    }

    @JsonProperty("under_sec4")
    public String getUnderSec4() {
        return underSec4;
    }

    @JsonProperty("under_sec4")
    public void setUnderSec4(String underSec4) {
        this.underSec4 = underSec4;
    }

    @JsonProperty("fir_no")
    public String getFirNo() {
        return firNo;
    }

    @JsonProperty("fir_no")
    public void setFirNo(String firNo) {
        this.firNo = firNo;
    }

    @JsonProperty("police_st_code")
    public Long getPoliceStCode() {
        return policeStCode;
    }

    @JsonProperty("police_st_code")
    public void setPoliceStCode(Long policeStCode) {
        this.policeStCode = policeStCode;
    }

    @JsonProperty("fir_year")
    public Long getFirYear() {
        return firYear;
    }

    @JsonProperty("fir_year")
    public void setFirYear(Long firYear) {
        this.firYear = firYear;
    }

    @JsonProperty("lower_court_code")
    public Long getLowerCourtCode() {
        return lowerCourtCode;
    }

    @JsonProperty("lower_court_code")
    public void setLowerCourtCode(Long lowerCourtCode) {
        this.lowerCourtCode = lowerCourtCode;
    }

    @JsonProperty("lower_court")
    public String getLowerCourt() {
        return lowerCourt;
    }

    @JsonProperty("lower_court")
    public void setLowerCourt(String lowerCourt) {
        this.lowerCourt = lowerCourt;
    }

    @JsonProperty("lower_court_dec_dt")
    public Long getLowerCourtDecDt() {
        return lowerCourtDecDt;
    }

    @JsonProperty("lower_court_dec_dt")
    public void setLowerCourtDecDt(Long lowerCourtDecDt) {
        this.lowerCourtDecDt = lowerCourtDecDt;
    }

    @JsonProperty("case_no")
    public String getCaseNo() {
        return caseNo;
    }

    @JsonProperty("case_no")
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    @JsonProperty("goshwara_no")
    public Long getGoshwaraNo() {
        return goshwaraNo;
    }

    @JsonProperty("goshwara_no")
    public void setGoshwaraNo(Long goshwaraNo) {
        this.goshwaraNo = goshwaraNo;
    }

    @JsonProperty("date_last_list")
    public Date getDateLastList() {
        return dateLastList;
    }

    @JsonProperty("date_last_list")
    public void setDateLastList(Date dateLastList) {
        this.dateLastList = dateLastList;
    }

    @JsonProperty("transfer_est")
    public String getTransferEst() {
        return transferEst;
    }

    @JsonProperty("transfer_est")
    public void setTransferEst(String transferEst) {
        this.transferEst = transferEst;
    }

    @JsonProperty("purpose_name")
    public String getPurposeName() {
        return purposeName;
    }

    @JsonProperty("purpose_name")
    public void setPurposeName(String purposeName) {
        this.purposeName = purposeName;
    }

    @JsonProperty("type_name")
    public String getTypeName() {
        return typeName;
    }

    @JsonProperty("type_name")
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @JsonProperty("petNameAdd")
    public String getPetNameAdd() {
        return petNameAdd;
    }

    @JsonProperty("petNameAdd")
    public void setPetNameAdd(String petNameAdd) {
        this.petNameAdd = petNameAdd;
    }

    @JsonProperty("str_error")
    public String getStrError() {
        return strError;
    }

    @JsonProperty("str_error")
    public void setStrError(String strError) {
        this.strError = strError;
    }

    @JsonProperty("desgname")
    public String getDesgname() {
        return desgname;
    }

    @JsonProperty("desgname")
    public void setDesgname(String desgname) {
        this.desgname = desgname;
    }

    @JsonProperty("resNameAdd")
    public String getResNameAdd() {
        return resNameAdd;
    }

    @JsonProperty("resNameAdd")
    public void setResNameAdd(String resNameAdd) {
        this.resNameAdd = resNameAdd;
    }

    @JsonProperty("str_error1")
    public String getStrError1() {
        return strError1;
    }

    @JsonProperty("str_error1")
    public void setStrError1(String strError1) {
        this.strError1 = strError1;
    }

    @JsonProperty("act")
    public String getAct() {
        return act;
    }

    @JsonProperty("act")
    public void setAct(String act) {
        this.act = act;
    }

    @JsonProperty("fir_details")
    public String getFirDetails() {
        return firDetails;
    }

    @JsonProperty("fir_details")
    public void setFirDetails(String firDetails) {
        this.firDetails = firDetails;
    }

    @JsonProperty("subordinateCourtInfoStr")
    public String getSubordinateCourtInfoStr() {
        return subordinateCourtInfoStr;
    }

    @JsonProperty("subordinateCourtInfoStr")
    public void setSubordinateCourtInfoStr(String subordinateCourtInfoStr) {
        this.subordinateCourtInfoStr = subordinateCourtInfoStr;
    }

    @JsonProperty("historyOfCaseHearing")
    public String getHistoryOfCaseHearing() {
        return historyOfCaseHearing;
    }

    @JsonProperty("historyOfCaseHearing")
    public void setHistoryOfCaseHearing(String historyOfCaseHearing) {
        this.historyOfCaseHearing = historyOfCaseHearing;
    }

    @JsonProperty("interimOrder")
    public String getInterimOrder() {
        return interimOrder;
    }

    @JsonProperty("interimOrder")
    public void setInterimOrder(String interimOrder) {
        this.interimOrder = interimOrder;
    }

    @JsonProperty("finalOrder")
    public String getFinalOrder() {
        return finalOrder;
    }

    @JsonProperty("finalOrder")
    public void setFinalOrder(String finalOrder) {
        this.finalOrder = finalOrder;
    }

    @JsonProperty("transfer")
    public String getTransfer() {
        return transfer;
    }

    @JsonProperty("transfer")
    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    @JsonProperty("courtno")
    public Long getCourtno() {
        return courtno;
    }

    @JsonProperty("courtno")
    public void setCourtno(Long courtno) {
        this.courtno = courtno;
    }

    @JsonProperty("ldesgname")
    public String getLdesgname() {
        return ldesgname;
    }

    @JsonProperty("ldesgname")
    public void setLdesgname(String ldesgname) {
        this.ldesgname = ldesgname;
    }

    @JsonProperty("desgcode")
    public Long getDesgcode() {
        return desgcode;
    }

    @JsonProperty("desgcode")
    public void setDesgcode(Long desgcode) {
        this.desgcode = desgcode;
    }

    @JsonProperty("judcode")
    public Long getJudcode() {
        return judcode;
    }

    @JsonProperty("judcode")
    public void setJudcode(Long judcode) {
        this.judcode = judcode;
    }

    @JsonProperty("jcode")
    public Long getJcode() {
        return jcode;
    }

    @JsonProperty("jcode")
    public void setJcode(Long jcode) {
        this.jcode = jcode;
    }

    @JsonProperty("petparty_name")
    public String getPetpartyName() {
        return petpartyName;
    }

    @JsonProperty("petparty_name")
    public void setPetpartyName(String petpartyName) {
        this.petpartyName = petpartyName;
    }

    @JsonProperty("resparty_name")
    public String getRespartyName() {
        return respartyName;
    }

    @JsonProperty("resparty_name")
    public void setRespartyName(String respartyName) {
        this.respartyName = respartyName;
    }

    @JsonProperty("court_name")
    public String getCourtName() {
        return courtName;
    }

    @JsonProperty("court_name")
    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    @JsonProperty("est_code")
    public String getEstCode() {
        return estCode;
    }

    @JsonProperty("est_code")
    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    @JsonProperty("state_code")
    public String getStateCode() {
        return stateCode;
    }

    @JsonProperty("state_code")
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @JsonProperty("district_code")
    public String getDistrictCode() {
        return districtCode;
    }

    @JsonProperty("district_code")
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    @JsonProperty("state_name")
    public String getStateName() {
        return stateName;
    }

    @JsonProperty("state_name")
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @JsonProperty("district_name")
    public String getDistrictName() {
        return districtName;
    }

    @JsonProperty("district_name")
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @JsonProperty("transfer_est_flag")
    public String getTransferEstFlag() {
        return transferEstFlag;
    }

    @JsonProperty("transfer_est_flag")
    public void setTransferEstFlag(String transferEstFlag) {
        this.transferEstFlag = transferEstFlag;
    }

    @JsonProperty("transfer_est_name")
    public String getTransferEstName() {
        return transferEstName;
    }

    @JsonProperty("transfer_est_name")
    public void setTransferEstName(String transferEstName) {
        this.transferEstName = transferEstName;
    }

    @JsonProperty("transfer_est_date")
    public Date getTransferEstDate() {
        return transferEstDate;
    }

    @JsonProperty("transfer_est_date")
    public void setTransferEstDate(Date transferEstDate) {
        this.transferEstDate = transferEstDate;
    }

    @JsonProperty("writinfo")
    public String getWritinfo() {
        return writinfo;
    }

    @JsonProperty("writinfo")
    public void setWritinfo(String writinfo) {
        this.writinfo = writinfo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getAddnotes() {
        return addnotes;
    }

    public void setAddnotes(String addnotes) {
        this.addnotes = addnotes;
    }

    public boolean isSelector() {
        return selector;
    }

    public void setSelector(boolean selector) {
        this.selector = selector;
    }

    public int getCourtcount() {
        return courtcount;
    }

    public void setCourtcount(int courtcount) {
        this.courtcount = courtcount;
    }

    @JsonAnyGetter
    public Map<String, String> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, String value) {
        this.additionalProperties.put(name, value);
    }

}