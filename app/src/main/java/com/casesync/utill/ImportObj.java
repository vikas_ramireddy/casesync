package com.casesync.utill;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({
        "cino",
        "type_name",
        "case_no",
        "reg_year",
        "reg_no",
        "petparty_name",
        "resparty_name",
        "fil_year",
        "fil_no",
        "establishment_name",
        "establishment_code",
        "state_code",
        "district_code",
        "state_name",
        "district_name",
        "date_next_list",
        "date_of_decision",
        "date_last_list",
        "updated",
        "court_no_desg_name"
})
public class ImportObj implements Serializable {

    private final static long serialVersionUID = 2627689564467983224L;
    @JsonProperty("cino")
    private String cino;
    @JsonProperty("type_name")
    private String typeName;
    @JsonProperty("case_no")
    private String caseNo;
    @JsonProperty("reg_year")
    private Integer regYear;
    @JsonProperty("reg_no")
    private Integer regNo;
    @JsonProperty("petparty_name")
    private String petpartyName;
    @JsonProperty("resparty_name")
    private String respartyName;
    @JsonProperty("fil_year")
    private String filYear;
    @JsonProperty("fil_no")
    private String filNo;
    @JsonProperty("establishment_name")
    private String establishmentName;
    @JsonProperty("establishment_code")
    private String establishmentCode;
    @JsonProperty("state_code")
    private String stateCode;
    @JsonProperty("district_code")
    private String districtCode;
    @JsonProperty("state_name")
    private String stateName;
    @JsonProperty("district_name")
    private String districtName;
    @JsonProperty("date_next_list")
    private String dateNextList;
    @JsonProperty("date_of_decision")
    private Object dateOfDecision;
    @JsonProperty("date_last_list")
    private String dateLastList;
    @JsonProperty("updated")
    private Boolean updated;
    @JsonProperty("court_no_desg_name")
    private String courtNoDesgName;
    private boolean selected = true;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cino")
    public String getCino() {
        return cino;
    }

    @JsonProperty("cino")
    public void setCino(String cino) {
        this.cino = cino;
    }

    @JsonProperty("type_name")
    public String getTypeName() {
        return typeName;
    }

    @JsonProperty("type_name")
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @JsonProperty("case_no")
    public String getCaseNo() {
        return caseNo;
    }

    @JsonProperty("case_no")
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    @JsonProperty("reg_year")
    public Integer getRegYear() {
        return regYear;
    }

    @JsonProperty("reg_year")
    public void setRegYear(Integer regYear) {
        this.regYear = regYear;
    }

    @JsonProperty("reg_no")
    public Integer getRegNo() {
        return regNo;
    }

    @JsonProperty("reg_no")
    public void setRegNo(Integer regNo) {
        this.regNo = regNo;
    }

    @JsonProperty("petparty_name")
    public String getPetpartyName() {
        return petpartyName;
    }

    @JsonProperty("petparty_name")
    public void setPetpartyName(String petpartyName) {
        this.petpartyName = petpartyName;
    }

    @JsonProperty("resparty_name")
    public String getRespartyName() {
        return respartyName;
    }

    @JsonProperty("resparty_name")
    public void setRespartyName(String respartyName) {
        this.respartyName = respartyName;
    }

    @JsonProperty("fil_year")
    public String getFilYear() {
        return filYear;
    }

    @JsonProperty("fil_year")
    public void setFilYear(String filYear) {
        this.filYear = filYear;
    }

    @JsonProperty("fil_no")
    public String getFilNo() {
        return filNo;
    }

    @JsonProperty("fil_no")
    public void setFilNo(String filNo) {
        this.filNo = filNo;
    }

    @JsonProperty("establishment_name")
    public String getEstablishmentName() {
        return establishmentName;
    }

    @JsonProperty("establishment_name")
    public void setEstablishmentName(String establishmentName) {
        this.establishmentName = establishmentName;
    }

    @JsonProperty("establishment_code")
    public String getEstablishmentCode() {
        return establishmentCode;
    }

    @JsonProperty("establishment_code")
    public void setEstablishmentCode(String establishmentCode) {
        this.establishmentCode = establishmentCode;
    }

    @JsonProperty("state_code")
    public String getStateCode() {
        return stateCode;
    }

    @JsonProperty("state_code")
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @JsonProperty("district_code")
    public String getDistrictCode() {
        return districtCode;
    }

    @JsonProperty("district_code")
    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    @JsonProperty("state_name")
    public String getStateName() {
        return stateName;
    }

    @JsonProperty("state_name")
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @JsonProperty("district_name")
    public String getDistrictName() {
        return districtName;
    }

    @JsonProperty("district_name")
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @JsonProperty("date_next_list")
    public String getDateNextList() {
        return dateNextList;
    }

    @JsonProperty("date_next_list")
    public void setDateNextList(String dateNextList) {
        this.dateNextList = dateNextList;
    }

    @JsonProperty("date_of_decision")
    public Object getDateOfDecision() {
        return dateOfDecision;
    }

    @JsonProperty("date_of_decision")
    public void setDateOfDecision(Object dateOfDecision) {
        this.dateOfDecision = dateOfDecision;
    }

    @JsonProperty("date_last_list")
    public String getDateLastList() {
        return dateLastList;
    }

    @JsonProperty("date_last_list")
    public void setDateLastList(String dateLastList) {
        this.dateLastList = dateLastList;
    }

    @JsonProperty("updated")
    public Boolean getUpdated() {
        return updated;
    }

    @JsonProperty("updated")
    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    @JsonProperty("court_no_desg_name")
    public String getCourtNoDesgName() {
        return courtNoDesgName;
    }

    @JsonProperty("court_no_desg_name")
    public void setCourtNoDesgName(String courtNoDesgName) {
        this.courtNoDesgName = courtNoDesgName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
