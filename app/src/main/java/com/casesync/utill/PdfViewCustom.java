package com.casesync.utill;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.print.PdfPrint;
import android.print.PdfPrint.CallbackPrint;
import android.print.PrintAttributes;
import android.print.PrintAttributes.Builder;
import android.print.PrintAttributes.Margins;
import android.print.PrintAttributes.MediaSize;
import android.print.PrintAttributes.Resolution;
import android.webkit.WebView;

import androidx.core.content.FileProvider;

import com.webviewtopdf.R.string;

import java.io.File;

public class PdfViewCustom {
    private static final int REQUEST_CODE = 101;

    public PdfViewCustom() {
    }

    public static void createWebPrintJob(Activity activity, WebView webView, File directory, String fileName, final com.webviewtopdf.PdfView.Callback callback) {

            String jobName = activity.getString(string.app_name) + " Document";
            PrintAttributes attributes = null;
            if (VERSION.SDK_INT >= 19) {
                attributes = (new Builder()).setMediaSize(MediaSize.ISO_A4).setResolution(new Resolution("pdf", "pdf", 600, 600)).setMinMargins(Margins.NO_MARGINS).build();
            }

            PdfPrint pdfPrint = new PdfPrint(attributes);
            if (VERSION.SDK_INT >= 21) {
                pdfPrint.print(webView.createPrintDocumentAdapter(jobName), directory, fileName, new CallbackPrint() {
                    public void success(String path) {
                        callback.success(path);
                    }

                    public void onFailure() {
                        callback.failure();
                    }
                });
            }
    }

    public static void openPdfFile(final Activity activity, String title, String message, final String path) {
        if (VERSION.SDK_INT >= 23 && activity.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 101);
        } else {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Open", new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    fileChooser(activity, path);
                }
            });
            builder.setNegativeButton("Dismiss", new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private static void fileChooser(Activity activity, String path) {
        File file = new File(path);
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri uri = FileProvider.getUriForFile(activity, "com.casesync.fileprovider", file);
        intent.setDataAndType(uri, "application/pdf");
        //Intent intent = Intent.createChooser(target, "Open File");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException var7) {
            var7.printStackTrace();
        }
    }

    public interface Callback {
        void success(String var1);

        void failure();
    }
}

