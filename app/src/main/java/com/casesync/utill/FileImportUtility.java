package com.casesync.utill;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.casesync.casesync.AttachmentFragment;
import com.casesync.casesync.PackageActivity;
import com.casesync.casesync.jobs.InitialForegroundService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.casesync.casesync.NavActivity.maxcases;

public class FileImportUtility {

    private static final String TAG = "IMPOUTL";


    public ErrorMessage importCases(Context context) {

        File file = FileUtility.loadEcourtsFile(context);
        ErrorMessage message = importCasesFromFile(context, file);
        return message;
    }

    public ErrorMessage importCasesFromFile(Context context, File file) {
        ErrorMessage message = new ErrorMessage();
        List<ImportObj> importlist = new ArrayList<ImportObj>();
        if (file.exists()) {
            Log.i(TAG, file.getName());
            StringBuilder text = readFile(file);
            try {
                if (!StringUtils.isEmpty(text.toString())) {
                    Log.i(TAG, text.toString());
                    ObjectMapper objectMapper = new ObjectMapper();
                    JSONArray jsonarray = new JSONArray(text.toString());
                    for (int i = 0; i < jsonarray.length(); i++) {
                        String jsonostring = jsonarray.getString(i);
                        ImportObj caseobj = objectMapper.readValue(jsonostring, ImportObj.class);
                        if (caseobj != null) {
                            importlist.add(caseobj);
                        } else {
                            Log.i(TAG, "Object not available");
                        }

                    }
                } else {
                    message.setStatus(false);
                    message.setMessage("Please select option \\\" Export \\\" in Ecourts Application.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                message.setStatus(false);
                message.setMessage("Invalid File format.");
            } catch (JsonParseException e) {
                e.printStackTrace();
                message.setStatus(false);
                message.setMessage("Invalid File format.");
            } catch (JsonMappingException e) {
                e.printStackTrace();
                message.setStatus(false);
                message.setMessage("Invalid File format.");
            } catch (IOException e) {
                e.printStackTrace();
                message.setStatus(false);
                message.setMessage("Unable to import file.");
            }

        } else {
            Log.i(TAG, "Unable to find the file ");
            message.setStatus(false);
            message.setMessage("Unable to find the required File.Please select option \\\" Export \\\" in Ecourts Application.");
        }


        if (!CollectionUtils.isEmpty(importlist)) {
            Log.i(TAG, "No of cases in the import file:" + importlist.size());
            if (importlist.size() <= maxcases) {
                if (importlist.size() < 400) {
                    //Toast.makeText(context, "Importing " + importlist.size() + " cases ", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, PackageActivity.class);
                    intent.putExtra("LIST", (Serializable) importlist);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, InitialForegroundService.class);
                    intent.setAction(InitialForegroundService.ACTION_IMPORT);
                    intent.putExtra("key", PackageActivity.TYPE.IMPORTOBJ);
                    context.startService(intent);
                }
            } else {
                message.setStatus(false);
                message.setMessage("File consist more than " + maxcases + " cases.please contact developer");
            }
        }

        return message;
    }


    public StringBuilder readFile(File file) {
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }


    public ErrorMessage importFileUsingFileChooser(Intent data, Context context) {
        ErrorMessage message = new ErrorMessage();
        try {
            Uri uri = data.getData();
            InputStream is = context.getContentResolver().openInputStream(uri);
            byte[] inputData = new AttachmentFragment().getBytes(is);
            String file_name = FileUtility.getFileNameFromUri(context, uri);
            if (!file_name.equals("myCases.txt")) {
                message.setStatus(false);
                message.setMessage("Invalid file selected. Please select myCases.txt");
                return message;
            }
            String folder_path = FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.IMPORT, null);
            boolean imported_file = new FileUtility().saveFile(context, "import", inputData, file_name);
            if (imported_file == true) {
                File mycases = new File(folder_path + file_name);
                message = new FileImportUtility().importCasesFromFile(context, mycases);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }
}
