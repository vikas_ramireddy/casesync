package com.casesync.utill;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import com.casesync.casesync.CasesyncUtill;
import com.casesync.casesync.R;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CauseListUtil {

    private static final String AUTHORITY = "com.casesync.fileprovider";
    private final String TAG = "CLUTILL";
    PermissionUtility permissionUtility = new PermissionUtility();
    CasesyncUtill syncutil = new CasesyncUtill();
    PDFUtility pdfUtility = new PDFUtility();


    public void generateTodaysCauseList(Context context, Activity activity, CAUSELISTACTION action) {
        try {
            permissionUtility.checkForFilePermissions(context, activity);
            boolean status = permissionUtility.validateFilePermissions(context);
            if (status) {
                Calendar now = Calendar.getInstance();
                int currentHour = now.get(Calendar.HOUR_OF_DAY);
                Date dt = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(dt);
                boolean today = true;
                Log.i(TAG, "Current Hour:" + currentHour);
                if (currentHour >= 18 && currentHour <= 24) {
                    today = false;
                }
                dt = c.getTime();

                CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(context,
                        CaseSyncOpenDatabaseHelper.class);
                Dao<CaseObj, Long> casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
                List<CaseObj> casedet = new ArrayList<CaseObj>();
                List<CaseObj> casedetnyu = new ArrayList<CaseObj>();
                //Not yet updated
                if (today) {
                    casedet = casedetDao.queryForEq("date_next_list", dt);
                    if (CollectionUtils.isEmpty(casedet)) {
                        Toast.makeText(context, "No Cases available today to export", Toast.LENGTH_LONG).show();
                    } else {
                        File stat = syncutil.createCauseListPdf(casedet, casedetnyu, context, today);
                        if (stat.exists()) {
                            if (action == CAUSELISTACTION.SHARE) {
                                shareIntent(context, stat, activity);
                            } else if (action == CAUSELISTACTION.VIEW) {
                                openPDFFile(context, stat, activity);
                            }
                        } else {
                            Toast.makeText(context, "Unable to generate causelist.", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    casedet = casedetDao.queryForEq("date_last_list", dt);
                    casedetnyu = casedetDao.queryForEq("date_next_list", dt);
                    if (CollectionUtils.isEmpty(casedet) && CollectionUtils.isEmpty(casedetnyu)) {
                        Toast.makeText(context, "No Cases available today to export", Toast.LENGTH_LONG).show();
                    } else {
                        File stat = syncutil.createCauseListPdf(casedet, casedetnyu, context, today);
                        if (stat.exists()) {
                            if (action == CAUSELISTACTION.SHARE) {
                                shareIntent(context, stat, activity);
                            } else if (action == CAUSELISTACTION.VIEW) {
                                openPDFFile(context, stat, activity);
                            }
                        } else {
                            Toast.makeText(context, "Unable to generate Cause list.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            } else {
                String message = context.getResources().getString(R.string.file_permission_rw_text);
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void shareIntent(Context context, File file, Activity activity) {
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            intentShareFile.setType("application/pdf");
            Uri uri;
            uri = FileProvider.getUriForFile(context, AUTHORITY, file);
            intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing File...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intentShareFile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            }
            activity.startActivity(intentShareFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPDFFile(Context context, File file, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        }
        activity.startActivity(intent);

    }

    public void pdfExport(Context context, Activity activity, CAUSELISTACTION action, List<CaseObj> casedet, String charsequence) {

        permissionUtility.checkForFilePermissions(context, activity);
        boolean status = permissionUtility.validateFilePermissions(context);
        if (status) {
            List<CaseObj> list = new CasesyncUtill().getSearchableResults(casedet, charsequence);
            //Sort by
            try {
                String filepath = FileUtility.getSubFolderPath(context, FileUtility.ACTIONTYPE.EXPORT, null);
                String filename = "export" + charsequence.replaceAll("-", "") + ".pdf";

                FileUtility.deleteExistingFile(context, filepath + filename);
                if (CollectionUtils.isEmpty(list)) {
                    Toast.makeText(context, "NO cases available to export ", Toast.LENGTH_LONG).show();
                } else {
                    pdfUtility.createPdf(filepath + filename, list);
                    Toast.makeText(context, "Generated PDF Successfully ", Toast.LENGTH_LONG).show();
                    File stat = new File(filepath + filename);
                    if (stat.exists()) {
                        if (action == CAUSELISTACTION.SHARE) {
                            shareIntent(context, stat, activity);
                        } else if (action == CAUSELISTACTION.VIEW) {
                            openPDFFile(context, stat, activity);
                        }
                    } else {
                        Toast.makeText(context, "Unable to export cases in to a PDF File.", Toast.LENGTH_LONG).show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String message = context.getResources().getString(R.string.file_permission_rw_text);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }


    }

    public void excelUpload(Context context, Activity activity, CAUSELISTACTION action, List<CaseObj> casedet, String charsequence) {

        permissionUtility.checkForFilePermissions(context, activity);
        boolean status = permissionUtility.validateFilePermissions(context);
        if (status) {
            List<CaseObj> list = new CasesyncUtill().getSearchableResults(casedet, charsequence);
            List<CaseObj> templist = new ArrayList<CaseObj>();
            try {
                if (!CollectionUtils.isEmpty(list)) {
                    Calendar c = Calendar.getInstance();
                    //Changing only for export please try to find a work arround as a bugfix
                    for (CaseObj obj : list) {
                        if (obj.getArchive().equals("N")) {
                            if (obj.getDateNextList() != null) {
                                c.setTime(obj.getDateNextList());
                                c.add(Calendar.DATE, 1);
                                Date dt = c.getTime();
                                obj.setDateNextList(dt);
                            } else {
                                Log.i(TAG, "Date is null for " + obj.getCino());
                                obj.setDateNextList(null);
                            }
                            templist.add(obj);
                        } else {
                            if (obj.getDateOfDecision() != null) {
                                c.setTime(obj.getDateOfDecision());
                                c.add(Calendar.DATE, 1);
                                Date dt = c.getTime();
                                obj.setDateOfDecision(dt);
                            } else {
                                obj.setDateNextList(null);
                            }

                            templist.add(obj);
                        }
                    }


                    File stat = new CasesyncUtill().generatecsv(templist, context);
                    if (stat.exists()) {
                        if (action == CAUSELISTACTION.SHARE) {
                            shareIntent(context, stat, activity);
                        } else if (action == CAUSELISTACTION.VIEW) {
                            openPDFFile(context, stat, activity);
                        }
                    } else {
                        Toast.makeText(context, "Unable to export cases in to a CSV File.", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(context, "No cases available to export", Toast.LENGTH_LONG).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String message = context.getResources().getString(R.string.file_permission_rw_text);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }


    }

    public enum CAUSELISTACTION {
        VIEW, SHARE
    }

}
