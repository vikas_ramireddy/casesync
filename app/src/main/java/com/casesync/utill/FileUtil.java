package com.casesync.utill;

import android.content.Context;

import java.io.File;
import java.util.List;

public interface FileUtil {

    List<String> getListOfFilesInFolder(Context context, String subfolder);

    boolean saveFile(Context context, String subfolder, File file, String filename);

    boolean saveFile(Context context, String subfolder, byte[] data, String filename);

    File fetchFile(Context context, String subfolder, String filename);


}
