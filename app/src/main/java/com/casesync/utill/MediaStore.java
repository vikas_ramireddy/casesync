package com.casesync.utill;

import android.content.Context;

import java.io.File;

public interface MediaStore {

    public File getFileFromDownloads(Context context, String file_name);

}
