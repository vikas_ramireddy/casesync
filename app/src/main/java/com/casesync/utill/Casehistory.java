package com.casesync.utill;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "casehistory")
public class Casehistory {
	@DatabaseField(columnName = "casehistory_id")
	private String casehistory_id;
	@DatabaseField(columnName = "businessondate",dataType = DataType.DATE_STRING,
			format = "dd-MM-yyyy")
	private Date businessondate;
	@DatabaseField(columnName = "purposeofhearing")
	private String purposeofhearing;
	@DatabaseField(columnName = "hearingdate",dataType = DataType.DATE_STRING,
			format = "dd-MM-yyyy")
	private Date hearingdate;
	@DatabaseField(columnName = "business")
	private String business;
	@DatabaseField(columnName = "casenumber")
	private String casenumber;

	public String getCasenumber() {
		return casenumber;
	}

	public void setCasenumber(String casenumber) {
		this.casenumber = casenumber;
	}

	public String getCasehistory_id() {
		return casehistory_id;
	}
	public void setCasehistory_id(String casehistory_id) {
		this.casehistory_id = casehistory_id;
	}
	public Date getBusinessondate() {
		return businessondate;
	}
	public void setBusinessondate(Date businessondate) {
		this.businessondate = businessondate;
	}
	public String getPurposeofhearing() {
		return purposeofhearing;
	}
	public void setPurposeofhearing(String purposeofhearing) {
		this.purposeofhearing = purposeofhearing;
	}
	public Date getHearingdate() {
		return hearingdate;
	}
	public void setHearingdate(Date hearingdate) {
		this.hearingdate = hearingdate;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}

	
	
	

}
