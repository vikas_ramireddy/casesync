package com.casesync.utill;

import android.util.Log;

import com.casesync.casesync.CasesyncUtill;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PDFUtility {

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public void createPdf(String dest, List<CaseObj> caselist) throws IOException {
        //TODO Remove the itext dependency and migrate to librepdf
        if (CollectionUtils.isEmpty(caselist)) {
            Log.i("More", "subdirExists:");
        } else {
            if (caselist.get(0).getArchive().equalsIgnoreCase("N")) {
                //Sorting based on Hearing date and Court Name.
                //Collections.sort(caselist, new CasesyncUtill.StateNameComparator());
                //Collections.sort(caselist, new CasesyncUtill.DistrictNameComparator());
                Collections.sort(caselist, new CasesyncUtill.CourtNameComparator());
                Collections.sort(caselist, new CasesyncUtill.HearingDateComparator());

            } else {
                Collections.sort(caselist, new Comparator<CaseObj>() {
                    @Override
                    public int compare(CaseObj obj1, CaseObj obj2) {
                        return obj2.getDateOfDecision().compareTo(obj1.getDateOfDecision());
                    }
                });
            }

            //Check for file browsing permission
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
            Document doc = new Document(pdfDoc);
            Table table = new Table(new float[]{1, 1, 1, 3});
            table.setWidthPercent(100);

            table.addCell(createCell("Hearing Date", 1, 1, TextAlignment.LEFT));
            table.addCell(createCell("Dairy Date", 1, 1, TextAlignment.LEFT));
            table.addCell(createCell("Case Details", 1, 1, TextAlignment.LEFT));
            //table.addCell(createCell("Court Name", 1, 1, TextAlignment.LEFT));
            //table.addCell(createCell("Pet VS Res", 1, 1, TextAlignment.LEFT));
            table.addCell(createCell("Dairy Notes", 1, 2, TextAlignment.LEFT));
            for (CaseObj obj : caselist) {
                if (obj.getDateNextList() != null) {
                    table.addCell(createCell(sdf.format(obj.getDateNextList()), 1, 1, TextAlignment.LEFT));
                } else {
                    table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                }
                if (obj.getDateLastList() != null) {
                    table.addCell(createCell(sdf.format(obj.getDateLastList()), 1, 1, TextAlignment.LEFT));
                } else {
                    table.addCell(createCell("N/A", 1, 1, TextAlignment.LEFT));
                }//Added stage.
                table.addCell(createCell("Stage: " + obj.getPurposeName() + "," + obj.getTypeName() + "/" + obj.getRegNo() + "/" + obj.getRegYear() + "-" + obj.getPetName() + " VS " + obj.getResName() + "-" + obj.getCourtName() + "/" + obj.getDesgname(), 1, 1, TextAlignment.LEFT));

                //table.addCell(createCell(obj.getCourtName(), 1, 1, TextAlignment.LEFT));
                //table.addCell(createCell(obj.getPetName() + " VS " + obj.getResName(), 1, 1, TextAlignment.LEFT));
                table.addCell(createCell(obj.getWritinfo() != null ? obj.getWritinfo() : "", 1, 2, TextAlignment.LEFT));
            }
            table.addCell(createCell("Total Cases", 1, 3, TextAlignment.LEFT));
            table.addCell(createCell("" + caselist.size(), 1, 1, TextAlignment.LEFT));
            doc.add(table);
            doc.close();
        }
    }

    public Cell createCell(String content, float borderWidth, int colspan, TextAlignment alignment) {
        Cell cell = new Cell(1, colspan).add(new Paragraph(content));
        cell.setTextAlignment(alignment);
        cell.setBorder(new SolidBorder(borderWidth));
        return cell;
    }


}
