package com.casesync.utill;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Useraccount {

    private boolean active;
    private String cinno;
    private String clientmobnumber;
    private String clientname;
    private String clientemail;
    private String clientnotes;

//    public Useraccount() {
//
//    }


//    public Useraccount(String cinno, boolean isactive) {
//        this.cinno = cinno;
//        this.active = isactive;
//    }

    public Useraccount(UserBuilder builder) {
        this.cinno = builder.cinno;
        this.active = builder.active;
        this.clientmobnumber = builder.clientmobnumber;
        this.clientname = builder.clientname;
        this.clientnotes = builder.clientnotes;
        this.clientemail = builder.clientemail;
    }

//    public Useraccount(String cinno, boolean isactive, String clientmobnumber, String clientname, String clientemail, String clientnotes) {
//        this.cinno = cinno;
//        this.active = isactive;
//        this.clientmobnumber = clientmobnumber;
//        this.clientname = clientname;
//        this.clientnotes = clientnotes;
//        this.clientemail = clientemail;
//    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCinno() {
        return cinno;
    }

    public void setCinno(String cinno) {
        this.cinno = cinno;
    }

    public String getClientmobnumber() {
        return clientmobnumber;
    }

    public void setClientmobnumber(String clientmobnumber) {
        this.clientmobnumber = clientmobnumber;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getClientemail() {
        return clientemail;
    }

    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }

    public String getClientnotes() {
        return clientnotes;
    }

    public void setClientnotes(String clientnotes) {
        this.clientnotes = clientnotes;
    }

    public static class UserBuilder {
        private boolean active;
        private String cinno;
        private String clientmobnumber;
        private String clientname;
        private String clientemail;
        private String clientnotes;

        public UserBuilder(String cinno, boolean active) {
            this.active = active;
            this.cinno = cinno;
        }

        public UserBuilder clientMobileNumber(String clientmobnumber) {
            this.clientmobnumber = clientmobnumber;
            return this;
        }

        public UserBuilder clientName(String clientname) {
            this.clientname = clientname;
            return this;
        }

        public UserBuilder clientEmail(String clientemail) {
            this.clientemail = clientemail;
            return this;
        }

        public UserBuilder clientNotes(String clientnotes) {
            this.clientnotes = clientnotes;
            return this;
        }

        public Useraccount build() {
            Useraccount useraccount = new Useraccount(this);
            return useraccount;

        }

    }

}
