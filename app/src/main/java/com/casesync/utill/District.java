package com.casesync.utill;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = "district")
public class District {
	@DatabaseField(columnName = "district_id")
	private Integer district_id;
	@DatabaseField(columnName = "name")
	private String name;
	@DatabaseField(columnName = "states_id")
	private States states_id;
	@DatabaseField(columnName = "districtcode")
	private String districtcode;
	private List<Court> court;
	

	public Integer getDistrict_id() {
		return district_id;
	}
	public void setDistrict_id(Integer district_id) {
		this.district_id = district_id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public States getStates_id() {
		return states_id;
	}
	public void setStates_id(States states_id) {
		this.states_id = states_id;
	}

	public String getDistrictcode() {
		return districtcode;
	}
	public void setDistrictcode(String districtcode) {
		this.districtcode = districtcode;
	}

	@JsonIgnore
	@JsonBackReference
	public List<Court> getCourt() {
		return court;
	}
	public void setCourt(List<Court> court) {
		this.court = court;
	}
}
