package com.casesync.utill;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.casesync.casesync.R;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vikas on 3/31/2017.
 */

public class AutoCompleteAdapter extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 10;
    String searchtype;
    private Context mContext;
    private List<CaseObj> resultList = new ArrayList<CaseObj>();

    public AutoCompleteAdapter(Context context, String search) {
        mContext = context;
        searchtype = search;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public CaseObj getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.simple_dropdown_item_2line, parent, false);
        }
        if (searchtype.equals("reg_no")) {
            ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getRegNo().replaceAll("Â", ""));
            ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getCourtName());

        }
        if (searchtype.equals("pet_name")) {
            String petnames = getItem(position).getPetName().replaceAll("Â", "");
            petnames = petnames.replaceAll("||", "");
            ((TextView) convertView.findViewById(R.id.text1)).setText(petnames);
            ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getPetAdv() != null ? getItem(position).getPetAdv().replaceAll("Â", "") : "");
        }
        if (searchtype.equals("res_name")) {
            try {
                String respnames = getItem(position).getResName().replaceAll("Â", "");
                respnames = respnames.replaceAll("||", "");
                ((TextView) convertView.findViewById(R.id.text1)).setText(respnames);
                ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getResAdv() != null ? getItem(position).getResAdv().replaceAll("Â", "") : "");
            } catch (Exception e) {
                Log.e("AUTO", e.getMessage());
            }
        }
        if (searchtype.equals("type_name")) {
            try {
                ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getTypeName().replaceAll("Â", "") + "/" + getItem(position).getRegNo().replaceAll("Â", ""));
                ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getCourtName().replaceAll("Â", ""));
            } catch (Exception e) {
                Log.e("AUTO", e.getMessage());
            }
        }
        if (searchtype.equals("desgname")) {
            try {
                ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getDesgname().replaceAll("Â", ""));
                ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getRegNo().replaceAll("Â", ""));
            } catch (Exception e) {
                Log.e("AUTO", e.getMessage());
            }
        }
        if (searchtype.equals("fir_details")) {
            try {
                if (getItem(position).getFirDetails() != null) {

                    String[] result = getItem(position).getFirDetails().split("\\^");
                    if (result.length == 3) {
                        ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getRegNo().replaceAll("Â", ""));
                        ((TextView) convertView.findViewById(R.id.text2)).setText(result[0] + "/" + result[1] + "/" + result[2]);
                    }
                }
            } catch (Exception e) {
                Log.e("AUTO", e.getMessage());
            }
        }


        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    //TODO Need to handle search of all case types such as district & highcourts & Consumer courts.
                    List<CaseObj> books = findCase(mContext, constraint.toString());
                    filterResults.values = books;
                    filterResults.count = books.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<CaseObj>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    /**
     * Returns a search result for the given book title.
     */
    private List<CaseObj> findCase(Context context, String searchkey) {
        List<CaseObj> casedet = null;
        Log.e("AUTO", "searchkey:" + searchkey);
        // GoogleBooksProtocol is a wrapper for the Google Books API
        CaseSyncOpenDatabaseHelper todoOpenDatabaseHelper = OpenHelperManager.getHelper(mContext,
                CaseSyncOpenDatabaseHelper.class);
        Dao<CaseObj, Long> casedetDao = null;
        try {
            casedetDao = todoOpenDatabaseHelper.getCaseObjdao();
            QueryBuilder<CaseObj, Long> qb = casedetDao.queryBuilder();
            if (searchtype.equalsIgnoreCase("reg_no")) {
                if (searchkey.length() == 1) {
                    //Cases which are having number like 9 is having issue
                    qb.where().like(searchtype, searchkey + "%");

                } else {
                    qb.where().like(searchtype, "%" + searchkey + "%");
                }
            } else {
                qb.where().like(searchtype, "%" + searchkey + "%");

            }
            PreparedQuery<CaseObj> pq = qb.prepare();
            casedet = casedetDao.query(pq);
            Log.i("AUTO", "" + casedet.size());
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e("AUTO", "" + e.getMessage());
        }
        return casedet;
    }
}
