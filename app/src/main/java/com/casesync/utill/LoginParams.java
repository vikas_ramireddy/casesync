package com.casesync.utill;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * Created by SPLLP02 on 06-11-2016.
 */

public class LoginParams {
    private String url;
    private String username;
    private String extraparam1;
    private String extraparam2;

    MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();

    public MultiValueMap<String, String> getMap() {
        return map;
    }

    public void setMap(MultiValueMap<String, String> map) {
        this.map = map;
    }


    public String getExtraparam2() {
        return extraparam2;
    }

    public void setExtraparam2(String extraparam2) {
        this.extraparam2 = extraparam2;
    }

    public String getExtraparam1() {
        return extraparam1;
    }

    public void setExtraparam1(String extraparam1) {
        this.extraparam1 = extraparam1;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String password;

    public enum TYPE {
        DISTRITCT, COURT, CASETYPE, ADDCASE, ELSE, POLICE, COURTNAME, PDF
    }

}


