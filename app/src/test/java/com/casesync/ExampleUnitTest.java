package com.casesync;

import android.content.Context;
import android.util.Log;

import com.casesync.utill.CaseObj;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Before
    public void setUp() {
        Security.insertProviderAt(new BouncyCastleProvider(), 1);
    }

    @Ignore
    public void addition_isCorrect() {
        //assertEquals(4, 2 + 2);
        String courtComplexWebServiceUrl = "https://app.ecourts.gov.in/courtEstWebService.php?action_code=pKLSnIaHHw4CCm9o9Mo9YBs%2BVaxrK3LJw5duQfjTRiQ%3D&state_code=aEbp9qQ66cQ9c8JtS1Un1Q%3D%3D&dist_code=jBfv9l7MrDMZccsicGg8%2FA%3D%3D";
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(courtComplexWebServiceUrl, String.class);
        System.out.print(response);

    }

    private Context getTestContext() {
        try {
            Method getTestContext = ExampleUnitTest.class.getMethod("getTestContext");
            return (Context) getTestContext.invoke(this);
        } catch (final Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Test
    public void fetchBusiness() {
        String responsetmp = "";
        try {

            CaseObj caseobj = getCaseObjFromCino("PYPY040005022015");
            String data_params = getDataparams(caseobj);
            String baseurl = "https://app.ecourts.gov.in/s_show_business.php?" + data_params;
            System.out.println("url:" + baseurl.toString());
            RestTemplate restTemplate = new RestTemplate();
            try {
                ResponseEntity<String> out = restTemplate.exchange(baseurl, HttpMethod.GET, null, String.class);
                System.out.println(out.getStatusCode().toString() + "");
                if (out.getStatusCode() == HttpStatus.OK) {
                    responsetmp = out.getBody();
                    if (!responsetmp.isEmpty() && responsetmp != null) {
                        System.out.println(responsetmp);
                        if (!responsetmp.equals("error_ERROR_")) {
                            responsetmp = AESCryptCustom.decrypt(responsetmp);
                            JSONObject history = new JSONObject(responsetmp);
                            String business = history.getString("viewBusiness");
                            System.out.println("business:" + business);
                            Document doc = Jsoup.parse(business);
                            for (Element table : doc.select("table")) { //this will work if your doc contains only one table element
                                String businesstext = "";
                                if (table.select("tr").get(1).select("td").size() > 1) {
                                    if (table.select("tr").get(0).select("td").get(0).text().contains("Business")) {
                                        businesstext = table.select("tr").get(0).select("td").get(2).text();
                                    } else if (table.select("tr").get(1).select("td").get(0).text().contains("Business")) {
                                        businesstext = table.select("tr").get(1).select("td").get(2).text();
                                    }
                                    //String businesstext = table.select("tr").get(1).select("td").get(2).text();
                                    System.out.println("businesstext:" + businesstext);

                                } else {
                                    System.out.println("Not enough text is available");
                                }
                            }
                        } else {
                            System.out.println("Error Occured");
                        }
                    }
                }

            } catch (HttpServerErrorException e) {
                System.out.println("Status code is not OK" + ":" + caseobj.getCourtNo() + ":" + caseobj.getCourtno());
                if (caseobj.getCourtNo().equalsIgnoreCase("" + caseobj.getCourtno())) {
                    Long courtcode = getCourtNumber(caseobj);
                    System.out.println("courtcode:" + courtcode);
                    if (!courtcode.equals(0L)) {
                        caseobj.setCourtno(courtcode);
                        String nwqdata_params = getDataparams(caseobj);
                        String newbaseurl = "https://app.ecourts.gov.ins_show_business.php?" + nwqdata_params;
                        ResponseEntity<String> output = restTemplate.exchange(newbaseurl, HttpMethod.GET, null, String.class);
                        if (output.getStatusCode() == HttpStatus.OK) {
                            responsetmp = output.getBody();
                            if (!responsetmp.isEmpty() && responsetmp != null) {
                                System.out.println(responsetmp);
                                if (!responsetmp.equals("error_ERROR_")) {
                                    responsetmp = AESCryptCustom.decrypt(responsetmp);
                                    JSONObject history = new JSONObject(responsetmp);
                                    String business = history.getString("viewBusiness");
                                    System.out.println("business:" + business);
                                    Document doc = Jsoup.parse(business);
                                    for (Element table : doc.select("table")) { //this will work if your doc contains only one table element
                                        String businesstext = "";
                                        if (table.select("tr").get(1).select("td").size() > 1) {
                                            if (table.select("tr").get(0).select("td").get(0).text().contains("Business")) {
                                                businesstext = table.select("tr").get(0).select("td").get(2).text();
                                            } else if (table.select("tr").get(1).select("td").get(0).text().contains("Business")) {
                                                businesstext = table.select("tr").get(1).select("td").get(2).text();
                                            }
                                            //String businesstext = table.select("tr").get(1).select("td").get(2).text();
                                            System.out.println("businesstext:" + businesstext);

                                        } else {
                                            System.out.println("Not enough data available to parse");
                                        }
                                    }
                                } else {
                                    System.out.println("Error Occured");
                                }
                            }
                        } else {
                            System.out.println("Response is not OK");
                        }
                    } else {
                        System.out.println("courtcode:" + courtcode + " is invalid");
                    }
                }
                System.out.println("N/A");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public CaseObj getCaseObjFromCino(String cinno) throws IOException, GeneralSecurityException {
        String baseurl = "https://app.ecourts.gov.in/caseHistoryWebService.php";
        CaseObj caseobj = new CaseObj();
        String data_params = "?cino=" + AESCryptCustom.encrypt(cinno);
        String url = baseurl + data_params;
        System.out.println(cinno + ":url:" + url);
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory());
        SimpleClientHttpRequestFactory rf = (SimpleClientHttpRequestFactory) restTemplate
                .getRequestFactory();
        rf.setReadTimeout(60000);
        rf.setConnectTimeout(60000);


        String responsetmp = restTemplate.getForObject(url, String.class);
        try {
            if (!responsetmp.isEmpty() && responsetmp != null) {
                if (!AESCryptCustom.decrypt(responsetmp).equals("null")) {
                    try {
                        JSONObject objtemp = new JSONObject(AESCryptCustom.decrypt(responsetmp));
                        if (objtemp != null) {
                            System.out.println(objtemp.toString());
                            ObjectMapper objectMapper = new ObjectMapper();
                            caseobj = objectMapper.readValue(objtemp.toString(), CaseObj.class);
                            if (caseobj != null) {

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (GeneralSecurityException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("Case not found ");
                }
            }
        } finally {

        }
        return caseobj;
    }

    public String getDataparams(CaseObj caseobj) {
        DateFormat sc = new SimpleDateFormat("yyyyMMdd");
        DateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        String data_params = "";
        try {
            String courtno = "court_code=" + AESCryptCustom.encrypt("" + caseobj.getCourtno());
            data_params = courtno;
            data_params += "&dist_code=" + AESCryptCustom.encrypt(caseobj.getDistrictCode());
            if (caseobj.getArchive().equals("N")) {
                data_params += "&nextdate1=" + AESCryptCustom.encrypt(sc.format(caseobj.getDateNextList()));
            } else {
                data_params += "&nextdate1=";
            }
            data_params += "&case_number1=" + AESCryptCustom.encrypt(caseobj.getCaseNo());
            data_params += "&state_code=" + AESCryptCustom.encrypt(caseobj.getStateCode());
            data_params += "&disposal_flag=" + AESCryptCustom.encrypt((caseobj.getArchive().equals("N") ? "Pending" : "Disposed"));
            if (caseobj.getArchive().equals("Y")) {
                data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateOfDecision()));
            } else {
                data_params += "&businessDate=" + AESCryptCustom.encrypt(dt.format(caseobj.getDateLastList()));
            }
            data_params += "&court_no=" + AESCryptCustom.encrypt("" + caseobj.getCourtNo());
            data_params += "&appFlag=";
        } catch (Exception e) {
            e.printStackTrace();
        }


        return data_params;
    }

    public Long getCourtNumber(CaseObj caseobj) {
        if (!StringUtils.isEmpty(caseobj.getHistoryOfCaseHearing())) {
            Document doc = Jsoup.parse(caseobj.getHistoryOfCaseHearing().replace("\\", ""));
            Elements table = doc.select("table.tbl-result");
            Elements rows = table.select("tr");
            try {
                if (rows.size() > 0) {
                    String case_data = rows.get(rows.size() - 1).getElementsByTag("a").attr("onclick");
                    if (!StringUtils.isEmpty(case_data)) {
                        Log.i("TST", "Function Data:" + case_data);
                        case_data = case_data.replace("viewBusiness", "");
                        case_data = case_data.replace("(", "");
                        case_data = case_data.replaceAll("\\)", "");
                        String[] split = case_data.split(",");
                        if (!StringUtils.isEmpty(split[0])) {
                            System.out.println("Court Code" + split[0]);
                            return Long.parseLong(split[0].replace("'", ""));
                        }
                    } else {
                        Log.i("TST", "No case data");
                    }
                } else {
                    Log.i("TST", "No records");
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("TST", "No History");
        }
        return 0L;
    }
}